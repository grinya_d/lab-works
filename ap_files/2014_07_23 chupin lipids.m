% See full description of parameters at bottom of file

%Reduction Info: if it has a value it will be included in the reduction 
matlab_center_file = '/media/mike/DATA/data_saxs/!ap_files/2014_07_23 center.mat' %REQUIRED: must be created in SAXSGUI
matlab_calib_file = '/media/mike/DATA/data_saxs/!ap_files/2014_07_23 qcal.mat' %REQUIRED: must be created in SAXSGUI
%flatfield_filename= ''
%abs_int_fact= 
%mask_filename= ''
%empty_filename= ''
%empty_transfact=  %ideally should be included in the info file. 
%solvent_filename= ''
%solvent_transfact=  %ideally should be included in the info file. 
%solvent_thickness=  %ideally should be included in the info file, or the info-files comment field 
%transfact=  %ideally should be included in the info file 
%sample_thickness=  %ideally should be included in the info file 

%Generic Info:\n
error_calib= 1 %Should be 1 for photon counting detectors 
%darkcurrent_filename= ''
%readoutnoise_filename= ''
zinger_removal= 0 %Should be 0 for Molmet detectors 

%Averaging Info - this section is optional:\n
do_average= 1 
I_vs_q= 1 % 1=do I_vs_q, 0=do I_vs_phi. Default is I_vs_q  
phistart= 0 % azimuthal start angle in degrees 
phiend= 360 % azimuthal end angle in degrees
qstart= 8.000000e-02 % radial start value 
qend= 1.200000e+00 % radial end value 
xaxis_type= 'lin' % can be 'lin' or 'log'
num_xpoints= 1200 % number of points on the x-axis 
reduction_type= 's' % can be default='s' or 'log'
reduction_text= 1 % =1 means include reduction text, =0 means do not include 

%Output Info - This section is optional- Comment out to 'stop' at 1D-plot figure window.
do_output=1       % value=1 means output is desired, value=0 no output.
new = 0	       % 1= plots in new window, 0=plots in existing window.
print= 0       % 1= print result on the printer.
print1D2jpeg=1   % 1= save 1D figure in a jpeg file.
%print2D2jpeg=1   % 1= save 2D saxsgui-window in a jpeg file. Does not work nicely.
save2fig=0     % 1= save figure to matlab figure_file.
save2columns=1 % 1= Saves data in commaseparated variable q/phi,I; 0= does not save.
save3columns=0 % 1= Saves data in comma separated variable q/phi,I,dI; 0=does not save.
addon_name='' % saved files have these additional extensions.
%output_dir='C:\reduced' % if not given output_dir will be directed to the current matlab directory .
 
%---------------------------DESCRIPTION--------------------------------
% This file contains the variables required to do various degrees of 
% auto-processing in SAXSGUI. It can be applied without changes to both
% single file processing and multiple file processing(averaging only for
% now)
%
% The variables are divided into 2 sections
% 1) Reduction Info relevant for any particular setup of the existing SAXS
% instrument. Will need to be changed when the setup is changed.
% 2) Reduction parameters that might change for different SAXS systems.
% 
% This file may optionally include information on
% 3) How to average the data
% 4) What to do with the averaged data
%
% Only q-calibration and centering are absolutely required. If other 
% reductions are desired they must be included herein. If they are excluded
% the relevant reduction will not be performed. 
% 
% The information in this file complements the information that is saved
% in the data info files. Any information given in the info-file always
% overrides information given in this setup-file. 
% Presently only the mpa-info files are supported
%------------------------ DESCRIPTION ----------------------------
