import csvhandler
import substractbg
import peakfinder
from classifier import Classifier
from optparse import OptionParser
import os
import json
from matplotlib import pyplot as plt
import pprint
import numpy
import csv


class LCPy(object):

    """LCPy - SAXS-based lipidic cubic phase classifier"""

    settings = None

    peakfinder = None

    classifier = None

    def __init__(self, settings):
        super(LCPy, self).__init__()
        self.settings = settings

        # initializing peakfinder here
        self.peakfinder = peakfinder.PeakFinder(
            self.settings.get("peakfinder", None)
        )

        self.classifier = Classifier()

        # initializing graphics here
        fig, ((ax1), (ax2)) = plt.subplots(nrows=2, ncols=1)
        # can't find how to enlarge figure size properly
        self.fig = fig
        self.ax1 = ax1
        self.ax2 = ax2
        plt.ion()
        plt.show

    def initGraphics(self):
        self.ax1.cla()
        self.ax2.cla()
        self.ax1.set_xlabel("q, 1/A")
        self.ax1.set_ylabel("Intensity, a.u.")
        self.ax2.set_xlabel("q, 1/A")
        self.ax2.set_ylabel("Intensity, a.u.")
        # self.ax1.set_title("Signal and background")
        # self.ax2.set_title("Peaks")
        self.ax1.hold(True)
        self.ax2.hold(True)

    def processFile(self, inputFile, bgFile):
        IN = csvhandler.read1d(
            inputFile, forced2columns=True,
            sigma=self.settings["sigma"],
            boundaryBox=(0.05, 0.2))
        BG = csvhandler.read1d(
            bgFile, forced2columns=True,
            sigma=self.settings["sigma"],
            boundaryBox=(0.05, 0.2))
        self.processOld(IN, BG)

    def processFiles(self, files, outFile, bgFile):
        BG = csvhandler.read1d(
            bgFile, forced2columns=True,
            sigma=self.settings["sigma"],
            boundaryBox=(0.05, 0.2))

        csvoutfile = open(outFile, 'w')
        csvout = csv.writer(csvoutfile)

        for f in files:
            print "processing", f
            for r in self.process(f, BG):
                csvout.writerow(r)

    def process(self, inFile, BG):
        """
            Leo processing, need thoughful mergin
        """

        IN = csvhandler.read1d(
            inFile, forced2columns=True,
            sigma=self.settings["sigma"],
            boundaryBox=(0.05, 0.2))

        prefix = inFile.split("_xy.csv")[0]

        time = ''
        samp = ''

        with open(prefix + ".info", "r") as f:
            for line in f:
                if line.find("Sample Description") != -1:
                    samp = line.split("=")[1].strip()
                if line.find("Date") != -1:
                    time = line.split("=")[1].strip()

        plt.clf()
        plt.xlabel("q, 1/A")
        plt.ylabel("Intensity, a.u.")
        plt.title(inFile[0])

        # here we obtain pure signal with no background scattering
        woBG, sigma = substractbg.substractBackGround(IN, BG)

        plt.plot(*zip(*woBG))

        # this peakSettings must be constructed automatically in future
        peakSettings = {"CWT": {"reflexWidth": 0.005}}
        peaks = self.peakfinder.find(woBG, method=peakSettings)
        if peaks:
            for p in peaks:
                plt.plot(p[0], p[1], "or")

        plt.savefig(prefix + "_LCPy.png")

        return [[prefix, samp, time, res[0], repr(res[1]), res[2]]
                for res in self.classifier.classify(peaks, [sigma(p[0]) for p in peaks])]

    def processOld(self, IN, BG):
        """
            Old processing
        """
        self.initGraphics()
        (ax1, ax2) = (self.ax1, self.ax2)

        # here we plot input signal and its bg for reduction
        ax1.plot(*zip(*IN))
        ax1.plot(*zip(*BG))
        plt.show()

        # here we obtain pure signal with no background scattering
        woBG, sigma = substractbg.substractBackGround(IN, BG)

        ax2.plot(*zip(*woBG))

        # this peakSettings must be constructed automatically in future
        refW = 0.005
        peakSettings = {"CWT": {"reflexWidth": refW}}
        peaks = self.peakfinder.find(woBG, method=peakSettings)

        pprint.pprint(peaks)

        if peaks:
            for p in peaks:
                ax2.plot(p[0], p[1], "or")

        # and now classification
        clsr = Classifier()
        print clsr.classify(peaks, [sigma(p[0]) for p in peaks])

        plt.draw()


def initExecOpts():
    parser = OptionParser()
    parser.add_option(
        "-o", "--output",
        dest="output",
        help="write output to FILE",
        metavar="FILE",
        default="out")
    parser.add_option(
        "-i", "--input",
        dest="input",
        help="classify phase from FILE",
        metavar="FILE",
        default=None)
    parser.add_option(
        "-b", "--background",
        dest="background",
        help="use FILE as background",
        metavar="FILE",
        default=None)
    parser.add_option(
        "-s", "--settings",
        dest="settings",
        help="take settings from FILE",
        metavar="FILE",
        default=os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "lcpy.settings"))
    (opts, args) = parser.parse_args()
    return opts.__dict__


def gatherInput(path, out):
    items = [os.path.join(path, i) for i in os.listdir(path)]
    for i in items:
        if os.path.isdir(i):
            gatherInput(i, out)
        else:
            f = os.path.split(i)[1].split("_xy.csv")
            if len(f) == 2:
                out.append(i)


def main():
    localopts = initExecOpts()

    with open(localopts["settings"], "r") as f:
        opts = json.load(f)

    print "local options:"
    print json.dumps(localopts, indent=4)
    print "global options:"
    print json.dumps(opts, indent=4)


    # Part from old version, needs thoughful merging

    # # IN - input signal to process
    # workingDirectory = os.path.dirname(os.path.abspath(__file__))
    # IN = localopts["input"]#os.path.join(workingDirectory, "049617_xy.csv")
    # print "IN = ", IN
    # # BG - background for reduction
    # BG = localopts["background"]#os.path.join(workingDirectory, "049624_xy.csv")
    # print "BG = ", BG
    # OUT = "output"

    # filePrefix = IN.split("_xy.csv")[0]

    # #localopts["background"] = BG
    # #localopts["input"] = IN
    # localopts["output"] = OUT

    # lcpy = LCPy(opts)
    # lcpy.processFile(localopts["input"], localopts["background"])

    # print 'writing image to %s' % (filePrefix + "_LCPy.png")
    # lcpy.fig.savefig(filePrefix + "_LCPy.png")
    # raw_input("Press Enter")


    # New Leo part
    # gathering input
    IN = localopts["input"]

    infiles = []

    if os.path.isfile(IN):
        infiles.append(IN)
    elif os.path.isdir(IN):
        gatherInput(IN, infiles)
    else:
        print "IOError: No such file or directory:", IN
        exit(1)

    # BG - background for reduction
    BG = localopts["background"]

    if localopts["output"]:
        OUT = localopts["output"]
    else:
        print "Error: unspecified output file"

    lcpy = LCPy(opts)

    lcpy.processFiles(infiles, OUT, BG)


if __name__ == '__main__':
    main()
