import numpy
from scipy import signal


class PeakFinder(object):

    """
    PeakFinder aggregates information about peak finding algorithms
    and launches them depending on settings
    """

    def __init__(self, settings):
        super(PeakFinder, self).__init__()
        self.settings = settings

    def find(self, IN, method=None):
        """
        find function launches peak finding algorithms depending
        on method parameters

        returns list of tuples:
        (peak position, peak amplitude, peak width)

        method contains parameters for peak-finding functions:
        method = {
            $METHODNAME:{
                "$METHODNAME_VARIABLE_NAME: $METHODNAME_VARIABLE_VALUE
            },
            "STDDEV": $NOISE_STANDARD_DEVIATION
        }
        """

        if not method:
            method = {"RELATIVE_MAXIMUM":
                      {
                          "HALF_WIDTH": 10
                      },
                      "STDDEV": None}

        (X, Y) = numpy.transpose(IN)

        if "STDDEV" not in method or method["STDDEV"] is None:
            Ytemp = Y[-50:]
            method["STDDEV"] = numpy.sqrt(
                numpy.mean(abs(Ytemp - Ytemp.mean()) ** 2))
            #print "standard deviation = %f" % method["STDDEV"]

        if "RELATIVE_MAXIMUM" in method:
            return relativeMaximum(
                IN,
                halfWidth=method["RELATIVE_MAXIMUM"]["HALF_WIDTH"]
            )

        elif "CWT" in method:
            return signalCWT(
                IN,
                reflexWidth=method["CWT"].get("reflexWidth", None)
            )

        return None


def signalCWT(IN, reflexWidth):
    (X, Y) = numpy.transpose(IN)

    samplingNumber = len(X)
    dq = (X.max() - X.min()) / (samplingNumber - 1)

    # different reflex widths:
    # SC1: 0.001
    # SC2: 0.05

    if not reflexWidth:
        raise Exception("reflexWidth is not specified in CWT settings")
    pointsInReflex = int(reflexWidth / dq) + 1

    # here we modulate peak width on scale of 100% of reflexWidth
    # around reflexWidth
    widths = range(
        max(1, pointsInReflex / 2),
        3 * pointsInReflex / 2,
        int(reflexWidth / 10 / dq)
    )
    widths = numpy.array(widths)

    # try with threshold
    res = signal.find_peaks_cwt(Y, widths=widths)
    if res:
        return [(X[x], Y[x], reflexWidth) for x in res]
    else:
        return None


def relativeMaximum(IN, halfWidth):

    hw = halfWidth
    Yd = []
    (X, Y) = numpy.transpose(IN)

    # dilation
    for i, y in enumerate(Y):
        window = (max(i - hw, 0), min(i + hw, len(IN)))
        temp = Y[window[0]:window[1]]
        yd = temp.max()
        Yd.append(yd)

    Yd = Yd - Y
    Yd = numpy.array(Yd)

    candidates = []

    for i, (x, yd) in enumerate(numpy.transpose((X, Yd))):
        if numpy.abs(yd) < 1e-12:
            candidates.append((i, x, Y[i], yd))

    if len(candidates) > 0:
        return [(c[1], c[2], 0.001) for c in candidates]
    else:
        return None


if __name__ == '__main__':
    main()
