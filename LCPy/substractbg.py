import numpy
import sys
from matplotlib import pyplot as plt
from csvhandler import read1d
from numpy.polynomial.polynomial import polyfit
from numpy.polynomial.polynomial import polyval
from math import log, exp


def bg_func(q):
    return 0.0


def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i + n]


def substractBackGround(
        source=None, background=None,
        method=None, multiplicator=None,
        polyOrder=None, window=30, sigmas=3):

    if not method:
        method = ("MUL_AUTO", "BG_INTERPOLATE")

    if not polyOrder:
        polyOrder = 8

    xbg, ybg = numpy.transpose(background)
    x, y = numpy.transpose(source)

    # print xbg
    # print ybg

    if "BG_INTERPOLATE" in method:
        deg = polyOrder
        parameters = polyfit(xbg, ybg, deg)

        def bg_func(q):
            return polyval(q, parameters)
    elif "BG_CONST" in method:
        bg_min = y.min()

        def bg_func(q):
            return bg_min
    else:
        pass

    # let's find noise function sigma(q)
    ybgnoise = ybg - map(bg_func, xbg)

    wndws = map(numpy.transpose, chunks(zip(xbg, ybgnoise), window))
    sig = numpy.transpose([(numpy.mean(w[0]), numpy.std(w[1])) for w in wndws])

    xs = map(log, sig[0])
    ys = map(log, sig[1])

    # sigma(q) = C/q^(1/2)
    const = exp(numpy.mean([ys[i] + 0.5 * xs[i] for i in range(len(xs))]))

    if "MUL_AUTO" in method:
        multiplicator = (y / map(bg_func, x)).min()
        # print multiplicator
    else:
        multipicator = 0.0

    ans = y - multiplicator * numpy.array(map(bg_func, x))
    return numpy.transpose([x, ans]), (lambda q: sigmas * const * q ** (-0.5))


if __name__ == '__main__':
    (source, bg, output, method) = (None, None, None, None)
    try:
        (source, bg, output, method) = sys.argv[1:]
    except Exception, e:
        try:
            (source, bg, output) = sys.argv[1:]
        except Exception, e:
            try:
                (source, bg) = sys.argv[1:]
            except Exception, e:
                raise e

    source = read1d(source, forced2columns=True, sigma=5)
    bg = read1d(bg, forced2columns=True, sigma=5)
    ans = substractBackGround(source, bg, method)

    plt.plot(*zip(*ans))
    plt.show()
