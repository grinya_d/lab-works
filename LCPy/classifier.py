from operator import itemgetter
from math import pi, exp, sqrt, log


class Classifier(object):

    """
    CLASSIFIER CLASS

    TODO:
    1. add documentation
    2. estimate shift as delta/ratio (0.05/1.22)
    3. maybe convert NAMES&RATIOS to dict
    """

    RATIOS = [[1., 2., 3., 4.],
              [1., 1.22, 1.41, 1.73],
              [1., 1.15, 1.53, 1.63],
              [1., 1.41, 2., 2.24],
              [1., 1.73, 2., 2.45]]

    NAMES = ["La", "Pn3m", "Ia3d", "Im3m", "H"]

    def __init__(self, ratioThr=1.5, shiftThr=0.05, rTP=0.5, sTP=0.5):
        """
        ratioThr - ampl-noise ratio where probability = 50%
        shiftThr - shift from ideal phase peaks position ratio where probability = 50%
        rTP - THRESHhold for ampl-noise ratio
        sTP - THRESHhold for peak position ratio
        """
        super(Classifier, self).__init__()
        self.result = []
        self.temp = []
        self.SC = -log(0.5) / shiftThr
        self.RB = (
            log((0.5) ** (-0.25) - 1) - log((0.001) ** (-0.25) - 1)) / (1. - ratioThr)
        self.RA = log((0.5) ** (-0.25) - 1) + self.RB * ratioThr
        self.DEDUCTION = [(rTP * sTP) ** 3, (rTP * sTP) ** 2, rTP * sTP, 1.]
        self.THRESHHOLD = rTP * (rTP * sTP) ** 3

    def ampl(self, a):
        """
        a = peak_amplitude / noise
        takes account of amplitude if the peak relative to noise
        returns probability that it is a peak
        """
        return 1. / (1. + exp(self.RA - a * self.RB)) ** 4.

    def shift(self, x, r):
        """
        x = peak_amplitude / the_first_peak_amplitude
        r - current phase ratio (1., 1.22, etc)
        takes account of difference between the real and the needed peak
        position ratio
        returns probability that given peak suites phase ratio
        """
        return exp(-abs(x - r) * self.SC)

    def lattice(self, phase, peak):
        """
        phase - determined phase
        peak - absolute peak position
        """
        if phase == "Pn3m":
            return sqrt(2) * 2 * pi / peak
        elif phase == "La" or phase == "Sponge":
            return 2 * pi / peak
        elif phase == "Ia3d":
            return sqrt(6) * 2 * pi / peak
        elif phase == "Im3m":
            return sqrt(2) * 2 * pi / peak
        elif phase == "H":
            return sqrt(3) * 2 * pi / peak

    def estimate(self, peaks):
        """
        estimates likelihood of given peaks to be phase's peaks
        """
        if len(peaks) == 1:
            if peaks[0][2] < 0.008:
                self.temp.append(
                    [peaks, "La", self.ampl(peaks[0][3]) * self.DEDUCTION[0]])
            else:
                self.temp.append(
                    [peaks, "Sponge", self.ampl(peaks[0][3]) * self.DEDUCTION[0]])
        else:
            desc = [p[0] / peaks[0][0] for p in peaks]
            results = []
            for r in self.RATIOS:
                res = 1.
                i = 0
                while i < len(desc):
                    res *= self.ampl(peaks[i][3]) * self.shift(desc[i], r[i])
                    i += 1
                results.append(res * self.DEDUCTION[len(desc) - 1])
            imax = 0
            for i in range(len(results)):
                if results[i] > results[imax]:
                    imax = i
            self.temp.append((peaks, self.NAMES[imax], results[imax]))

    def combine(self, init, tail):
        """
        recursive function needed to find all combinations of peaks
        """
        if len(init) < 4 and len(tail) > 0:
            for i in range(len(tail)):
                new_init = init + [tail[i]]
                self.estimate(new_init)
                self.combine(new_init, tail[(i + 1):])

    def classify(self, p, n):
        """
        p - list of peaks            : [(x, ampl, sigma)]
        n - noise value for each peak: [noise]
        """
        peaks = [(p[i][0], p[i][1], p[i][2], p[i][1] / n[i])
                 for i in range(len(p))]

        # the main cycle
        while len(peaks) != 0:
            imax = 0
            for i in range(1, len(peaks)):
                if peaks[i][1] > peaks[imax][1]:
                    imax = i
            self.combine([], peaks[imax:])
            best = max(self.temp, key=itemgetter(2))
            if best[2] < self.THRESHHOLD:
                break
            else:
                self.result.append(
                    (best[1], self.lattice(best[1], best[0][0][0]), best[0]))
                for p in best[0]:
                    peaks.remove(p)
            self.temp = []

        if len(self.result) == 0:
            return [("Flat", 0.)]
        else:
            return self.result


if __name__ == '__main__':
    main()
