'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 9 2014


'''

import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def convert(list):
	return (float(list[2]), float(list[3]), list[4], list[8], float(list[7]),float(list[9]))

phases = ['Im3m', 'Pn3m', 'La', 'Ia3d', 'Sponge']
clrs = ['red', 'green', 'blue', 'cyan', 'yellow']
colors = dict(zip(phases, clrs))
	
def render(data, salt):
	points = dict(zip(phases, [([],[],[]), ([],[],[]), ([],[],[]), ([],[],[]), ([],[],[])]))
	for row in data:
		if row[2] == salt:
			points[row[3]][0].append(row[0])
			points[row[3]][1].append(row[1])
			points[row[3]][2].append(row[4])
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	for p in phases:
		#plt.scatter(points[p][0], points[p][1], c=colors[p], s=300, label=p, alpha=1, edgecolors='none')
		ax.scatter(points[p][0], points[p][1], points[p][2],  c=colors[p],s=200, label=p, alpha=1, edgecolors='none')#c=colors[p], marker='o')
	#plt.legend(bbox_to_anchor=(1, 1), loc=2)
	plt.show()

	


with open('data.csv', 'rb') as input:
	data_reader = csv.reader(input)
	data = [convert(row) for row in data_reader]
	render(data, 'Ammonium sulfate')