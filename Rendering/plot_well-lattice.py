'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 9 2014


'''

import matplotlib.pyplot as plt
import csv

def convert(list):
	w = (ord(list[0][0]) - 65.0) * 12.0 + float(list[0][1:list[0].index(' ')])
	return [w, float(list[1])]

input = open('/home/leonid/test plate/output_well_lattice.csv', 'rb')
data_reader = csv.reader(input)
data = [convert(row) for row in data_reader]

for p in data:
	plt.scatter(p[0], p[1])#, c=repr(p[2]-1), s=100, alpha=1, edgecolor='black') #c=repr((p[3]-3.5)/9.5), s=p[2]*100-150, alpha=1)#c=colors[p], marker='o')
plt.grid()
plt.xlabel('Well Number')
plt.ylabel('Lattice Parameter (Angstr)')
plt.title('Well Number - Lattice plot for MQ-MO solution')
plt.show()
