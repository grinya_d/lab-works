'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 9 2014

'''

import csv
import matplotlib.pyplot as plt


def convert(list):	
	return [float(list[0]), float(list[1]), float(list[2]), float(list[4]), float(list[5]), float(list[13])]

nm = {'OG': 0, 'salt': 1, 'Fraction': 2, 'cry68': 3, 'cry274': 4, 'd68': 5}

input = open('og_sets1234_full.csv', 'rb')
data_reader = csv.reader(input)
data = []
for row in data_reader:
	try:
		data.append(convert(row))
	except:
		print 'bad row'
	
def render(constant, salt, ph):
	points = dict(zip(phases, [([],[]), ([],[]), ([],[]), ([],[]), ([],[])]))
	for row in data:
		if row[2] == salt and row[4] == ph:
			points[row[3]][0].append(row[0])
			points[row[3]][1].append(row[1])
	plt.axis([-5, 100, 0, 3])
	plt.ylabel('Conc')
	plt.xlabel('HTAC')
	plt.title(salt)
	for p in phases:
		plt.scatter(points[p][0], points[p][1], c=colors[p], s=300, label=p, alpha=1, edgecolors='none')
	#plt.legend(bbox_to_anchor=(1, 1), loc=2)
	plt.grid(True)

