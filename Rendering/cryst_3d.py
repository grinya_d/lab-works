'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 9 2014

'''

import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def convert(list):	
	return [float(list[0]), float(list[1]), float(list[2]),float(list[5])]

input = open('og_sets1234_full.csv', 'rb')
data_reader = csv.reader(input)
data = []
for row in data_reader:
	try:
		data.append(convert(row))
	except:
		print 'bad row'

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for p in data:
	ax.scatter(p[0], p[1], p[2],  c='blue', s=p[3], alpha=1, edgecolors='none')#c=colors[p], marker='o')

plt.show()
