'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 12 2014

The script reads data from declared CSV's columns and plots graph. Third
parameter is displayed using color. It's possible to add a fourth parameter and
display via points' size.
'''

import matplotlib.pyplot as plt
import csv

# read columns: Cryst 68 d, 68d-d, salt
def convert(list):
	return [float(list[4]), float(list[13]), float(list[1])]

# obtain data 
input = open('../data/og_sets1234.csv', 'rb')
data_reader = csv.reader(input)
data = []
for row in data_reader:
	try:
		data.append(convert(row))
	except:
		print 'Got an unconvertable value'

# plot the graph
# x: 68d-d
# y: Cryst 68 d
# color: salt (normalized values)
# use <s> to display another column
for p in data:
	plt.scatter(p[1], p[0], c=repr(p[2]-1), s=100, alpha=1, edgecolor='black')

# show the graph
plt.show()
