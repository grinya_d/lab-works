import matplotlib.pyplot as pyplot
from matplotlib.colors import ListedColormap
from sklearn import neighbors
import numpy as np
from math import exp, sqrt, pi


def weigthFunc(a, sigma):
    # creates gaussian kernel weigths
    res = []
    for i in range(len(a)):
        res.append([])
        for j in range(len(a[i])):
            res[i].append(
                exp(-a[i][j] ** 2 / (2 * sigma ** 2)) / (sigma * sqrt(2 * pi)))
    return np.array(res)


def phase_diagram_render_2d(X, labels,
                            title='',
                            n_neighbors=0, resolution=50, sigma=0.2,
                            xlabel='', ylabel='', scatter=True, legend=True,
                            font='xx-large'):
    """
    Function recieves coordinates and corresponding phases
    and constructs phase diagramm with legend to return

    The function is based on KNN classifier for sklearn. 
    It uses normalized coordinates for classifier

    X - coordinates in which classification takes plate X = [[x,y]]
    labels - phase labels, corresponding to coordinates X
    title - title of the plot
    n_neighbors - number of nearest neighbors for KNN Classifier
    resolution - resulting sampling number of points in the diagram
    sigma - size of relative normalized gaussian kernel
    scatter - if set True plots initial raw data on the diagram
    legend - if set True plots diagram legend
    xlabel and ylabel - labels of axes
    """
    print sigma

    if n_neighbors == 0:
        n_neighbors = int(sqrt(len(labels)) / 2) + 1
    print n_neighbors

    res_step = 1.0 / resolution

    # settings for classification and visualization
    labelsDict = {'Pn3m': (0, '#B3B3FF', '#0000FF'),
                  'Ia3d': (1, '#B7E4E6', '#00D8DB'),
                  'Im3m': (2, '#9999BF', '#000070'),
                  'La': (3, '#8FB38F', '#008B00'),
                  'Sponge': (4, '#B38E8E', '#800000'),
                  'H': (5, '#FBCCFF', '#EA00FF'),
                  'Flat': (6, '#FFE7CC', '#FF8800')}
    y = np.array([labelsDict[l][0] for l in labels])
    labs = set(labels)

    # Create color maps
    cmap_light = [s[1] for s in sorted(labelsDict.values())]
    cmap_bold = ListedColormap([s[2] for s in sorted(labelsDict.values())])

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [[x_min, x_max],[y_min, y_max]].
    x_min, x_max = X[:, 0].min(), X[:, 0].max()
    y_min, y_max = X[:, 1].min(), X[:, 1].max()

    def normX(x):
        return (x - x_min) / (x_max - x_min)

    def normY(y):
        return (y - y_min) / (y_max - y_min)

    def renormX(xn):
        return x_min + xn * (x_max - x_min)

    def renormY(yn):
        return y_min + yn * (y_max - y_min)

    print x_min, x_max, y_min, y_max

    # x and y coordinates require renormalizations due to different scales of
    # units
    xn, yn = np.transpose(X)
    xn, yn = normX(xn), normY(yn)

    # we create an instance of Neighbors Classifier and fit the data.
    # ! clusterization and classification must be calculated
    # in normalized coordinates
    clf = neighbors.KNeighborsClassifier(
        n_neighbors, lambda a: weigthFunc(a, sigma))

    clf.fit(np.transpose([xn, yn]), y)

    xx, yy = np.meshgrid(np.arange(0, 1 + res_step, res_step),
                         np.arange(0, 1 + res_step, res_step))

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    # Put the result into a color plot
    Z = Z.reshape(xx.shape)

    fig = pyplot.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    ax.set_position([0.1, 0.1, 0.7, 0.8])

    # plotting results of classification in renormalized coordinates
    ax.pcolormesh(renormX(xx), renormY(yy), Z,
                  cmap=ListedColormap(cmap_light), vmin=0, vmax=6)
    ax.grid()

    ax.set_xlim(x_min - (x_max - x_min) * 0.1, x_max + (x_max - x_min) * 0.1)
    ax.set_ylim(y_min - (y_max - y_min) * 0.1, y_max + (y_max - y_min) * 0.1)
    ax.set_title(title, size=font, weight='heavy')
    ax.set_ylabel(ylabel, size='large')
    ax.set_xlabel(xlabel, size='large')

    if scatter:
        ax.scatter(X[:, 0], X[:, 1], c=y, cmap=cmap_bold, vmin=0, vmax=6, s=80)

    if legend:
        ax.legend([pyplot.Rectangle((0, 0), 1, 1, fc=labelsDict[l][1]) for l in labs],
                  labs, bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)

    return fig


if __name__ == '__main__':

    # first, prepare data for phase diagram rendering
    import csv

    outputPath = "../test/PCR-SALT-OG after month.csv"
    sampleDescriptionPath = "../test/2014_10_21 OG-Salt phases in PCR - Sheet2.csv"

    with open(sampleDescriptionPath, 'r') as f:
        sampleDescription = csv.reader(f)
        sampleCoordinates = dict((s[0], (float(s[1]), float(s[2])))
                                 for s in sampleDescription)

    # pprint.pprint(sampleCoordinates)

    with open(outputPath, 'r') as f:
        output = list(csv.reader(f))
        data = []
        for line in output:

            try:
                sampleName = line[1].split()[6]
                data.append([sampleCoordinates[sampleName],
                             line[2],
                             float(line[3])])
            except Exception, e:
                print line
                print e

    coords, phases, lattices = zip(*data)

    # use renderer with the provided data
    phaseDiag = phase_diagram_render_2d(X=np.array(coords),
                                        labels=phases,
                                        n_neighbors=13,
                                        xlabel="NaKPi ph5.6, M",
                                        ylabel=r"OG/MO, % w/w",
                                        title="OG-Salt phase variation in PCR tubes",
                                        scatter=True,
                                        sigma=0.2)

    pyplot.show(phaseDiag)
    phaseDiag.savefig("./phase_diag.png")
