import os
import sys
import json
import shutil
import glob


class Catalogizer(object):

    """
    Catalogizer reads *.info files and if it have source name it
    stores it to storageFolderName

    catalog principle:
    /destinationFolder/
    -$USERNAME/
    --$YYYYMMDD/
    ---$SCRIPTUUID/
    ----$SAMPLEFILES
    """

    settings = None

    def __init__(self, settings):
        super(Catalogizer, self).__init__()
        self.settings = settings
        print json.dumps(self.settings, indent=4)

    def catalogize(self, rootdir=None):
        if rootdir is None:
            return 0

        print "starting rootdir = %s" % rootdir

        samples = [f for f in os.listdir(rootdir) if f.endswith(".info")]
        samplesCopied = 0
        samplesMoved = 0
        samplesExcept = 0
        samplesSkipped = 0
        print "found %d samples" % len(samples)

        for ff in samples:
            print ff
            sample = os.path.splitext(ff)[0]

            fName = os.path.join(rootdir, ff)
            info = {}
            try:
                with open(fName, 'r') as f:
                    for w in f.readlines():
                        split = w.split(" = ", 2)
                        info[split[0]] = split[1].strip()
            except Exception, e:
                print e
                samplesExcept += 1
                continue

            # print json.dumps(info, indent=4)
            sampleDescription = info['Sample Description']
            scriptUUID = "UNKNOWN"
            if "SOURCE" in sampleDescription:
                scriptUUID = sampleDescription.split(" SOURCE ")[1]
                scriptUUID = scriptUUID.split(" ")[0]
                scriptUUID = scriptUUID.lower()
            else:
                samplesSkipped += 1
                continue

            username = "UNKNOWN"
            if len(info['Username']) > 0:
                username = info['Username']
                username = username.upper()

            date = "UNKNOWN"
            if len(info["Date"]) > 0:
                date = info["Date"]
                date = date.split(" ")[0]
                date = date.replace("/", "")

            print sample, date, username, scriptUUID

            fileDestinationFolder = os.path.join(
                self.settings['destinationFolder'],
                username,
                date,
                scriptUUID
            )

            if not os.path.exists(fileDestinationFolder):
                os.makedirs(fileDestinationFolder)

            sampleFiles = glob.iglob(os.path.join(rootdir, sample + "*"))
            if self.settings["strategy"].upper() == "MOVE":
                try:
                    for sf in sampleFiles:
                        print "move %s to %s..." % (sf, fileDestinationFolder),
                        shutil.move(sf, fileDestinationFolder)
                        print "success"
                    samplesMoved += 1
                except Exception, e:
                    samplesExcept += 1
                    print e

            elif self.settings["strategy"].upper() == "COPY":
                try:
                    for sf in sampleFiles:
                        print "copy %s to %s..." % (sf, fileDestinationFolder),
                        shutil.copy(sf, fileDestinationFolder)
                        print "success"
                    samplesCopied += 1
                except Exception, e:
                    samplesExcept += 1
                    print e

            else:
                raise NotImplementedError

        print "Samples moved   %d" % samplesMoved
        print "Samples copied  %d" % samplesCopied
        print "Samples skipped %d" % samplesSkipped
        print "Errors          %d" % samplesExcept
        print "Total Samples   %d" % len(samples)
        return samplesMoved + samplesCopied


if __name__ == '__main__':
    settingsFile = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "catalogizer.config")

    with open(settingsFile, "r") as f:
        settings = json.load(f)

    cat = Catalogizer(settings)

    rootdir = "./"
    if len(sys.argv) == 2:
        rootdir = sys.argv[1]

    cat.catalogize(rootdir)
