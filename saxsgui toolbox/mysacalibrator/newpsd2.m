% newpsd2 resuscitating very old things Irena was alive and Peter a baby
% psd - data analysis from PSD edited by M. Steinhart from the
%  original script pcd written by V. Havlena
% this tests my rewritten routine
% parameters can be fixed using globals mask and parx
% parx contains fixed parameters or zeroes
% mask contains zeros on fixed and ones on free positions
% qe ... q values; il ... intensities to fit; it ... intensities calculated
% par(1)=X0, par(2)=Y0, par(3)=RX, par(4)=RY

%global PcdData1
global qe il it %% ee 
global mask parx

mask=[1 1 1 1]; parx=[0 0 0 0];
man=0;              %% man=1 manual input
if man==1
  disp(' Define data parameters')
  x0 = input('X0 = ');
  y0 = input('Y0 = '); 
  rx0 = input('Rx = '); 
  ry0 = input('Ry = '); 
  sigma = input('sigma = '); 
  n = input('n = '); 
else
  x0=500; y0=510; rx0=200; ry0=300; sigma=10; n=1000; 
end
  
PcdData1 = datgen(x0,y0,rx0,ry0,sigma,n);

%
% initial estimate
%
if man==1
  disp(' To define initial estimate, click:')
  disp(' - center [x,y]')
  disp(' - right x')
  disp(' - top y')
  [gx,gy] = ginput(3); 
  x0 = gx(1);
  y0 = gy(1); 
  rx0 = gx(2)-gx(1); 
  ry0 = gy(3)-gy(1); 
else
% initial parameters are estimated from the limits
  x1=min(PcdData1(:,1));
  x2=max(PcdData1(:,1));
  y1=min(PcdData1(:,2));
  y2=max(PcdData1(:,2));

  x0=(x1+x2)/2;
  rx0=(x2-x1)/2;
  y0=(y1+y2)/2;
  ry0=(y2-y1)/2;
end

figure(1)
hold on

phi=0:2*pi/100:2*pi;

plot(x0+rx0*cos(phi),y0+ry0*sin(phi),':y');
plot(x0,y0,'+y')
drawnow

%P_ini = [x0 y0 rx0 ry0 phi];
%P_ini = [x0 y0 rx0 ry0 0];
P_ini = [x0 y0 rx0 ry0];
            
%
% call nnls - hacked to avoid optimization toolbox licensing problem
%

%%% P = msleastsq('psderror',P_ini);
%%% P = fmins('psderror',P_ini);
qe=PcdData1(:,1);
il=PcdData1(:,2);
P = msleastsq('mssph03',P_ini);
%P = msleastsq('psderror',P_ini);

x = P(1)
y = P(2)
rx = P(3)
ry = P(4)
% phi = P(5) 
plot(x+rx*cos(phi),y+ry*sin(phi),'g');
plot(x,y,'+g')

legend('r+','Data',':y','Init. est.','g','Opt. est.') % obsolete but works!
%%%legend('Data','Init. est.','Opt. est.');

hold off


