function [s,f] = cubici3(f2,f1,c2,c1,dx)
%CUBICI3  Cubicly interpolates 2 points and gradients to find step and min.
%
%   This function uses cubic interpolation and the values of 
%   two points and their gradients in order estimate the minimum of a 
%   a function along a line.

%   Copyright 1990-2001 The MathWorks, Inc.
%   $Revision: 1.13 $  $Date: 2001/03/27 19:55:08 $

if isinf(f2), f2 = 1/eps; end
a = 3*(c2*dx-2*f2+c1*dx+2*f1)/dx^3;
b = -(c2*dx-3*f2+2*c1*dx+3*f1)/dx^2;
disc = b^2 - a*c1;
if a == 0
  s = -c1/b;
elseif disc <= 0
  s = -b/a;
else
  s = (-b+sqrt(disc))/a;
end
if s<0,  s = -s; end
f = a/3*s^3 + b*s^2 + c1*s + f1;
