function hline_out = mscirclen(ipar,swaxs,nfi,nrr,qfi,varargin)
%MSCIRCLEN Draw elipses, Mysa : Milky Way 13, 19. 07. 2007
%   MSCIRCLEN(ipar,swaxs,nfi,nrr,qfi,varargin)
%     draws an elipse or elipses:
%  ipar   ... parameters of the calibration elipse (ring) [cy cx ry rx] 
%  swaxs  ... 0 SAXS or 1 WAXS
%  varargin ... parameters as for the the LINE command
%  if swaxs=0 (SAXS):
%    nfi ... order of the calibration ring 
%    nrr ... nr of orders to display starting with the first one
%    qfi ... q of the first ring (not used yet)
%  if swaxs=1 (WAXS):
%    draws only the calibration elipse
%   H = CIRCLE(...) returns the handle to the line object that CIRCLE
%   creates.
%
%   See also:  LINE.
if nargin == 0
    error('Supply some parameters young man!')
end
cx=ipar(2); cy=ipar(1); rxf=ipar(4); ryf=ipar(3);
phi=0:2*pi/100:2*pi;
if swaxs==0
  rx0=rxf/nfi; ry0=ryf/nfi;
  for ii=1:nrr
    rx=rx0*ii; ry=ry0*ii;
    circx = cx+rx*cos(phi);
    circy = cy+ry*sin(phi);

    hline = line(circx, circy, varargin{:});
    set(hline,'color','w');
  end
 else
  circx = cx+rxf*cos(phi);
  circy = cy+ryf*sin(phi);

  hline = line(circx, circy, varargin{:});
  set(hline,'color','w','linewidth',2);
end
if nargout
    hline_out = hline;
end
