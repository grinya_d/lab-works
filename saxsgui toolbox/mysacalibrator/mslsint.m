function [xold,fold,para,how]=lsint(xnew,fnew,para)
%LSINT  Function to initialize NLSQ routine.

%   Copyright 1990-2001 The MathWorks, Inc.
%   $Revision: 1.16 $  $Date: 2001/03/27 19:55:11 $
%   Andy Grace 7-9-90.
xold=xnew;
fold=fnew;

para=foptions(para); 
if para(14)==0, para(14)=length(xnew)*100;end 
if para(1)>0,
    disp('')
    if para(5)>0
      if isinf(para(1))
        disp('f-COUNT      RESID    STEP-SIZE      GRAD/SD  LINE-SEARCH')
      else
        disp('f-COUNT      RESID    STEP-SIZE      GRAD/SD')
      end
else
      if isinf(para(1))
    disp('f-COUNT      RESID    STEP-SIZE      GRAD/SD        LAMBDA LINE-SEARCH')
      else
    disp('f-COUNT      RESID    STEP-SIZE      GRAD/SD        LAMBDA')
      end
    end
end
