function [x,OPTIONS,CostFunction,JACOB] = leastsq(FUN,x,OPTIONS,GRADFUN,varargin)
%LEASTSQ Solves non-linear least squares problems.
%   LEASTSQ has been replaced with LSQNONLIN.  LEASTSQ currently works but
%   will be removed in the future.  Use LSQNONLIN instead.
%
%   LEASTSQ solves problems of the form:
%   min  sum {FUN(X).^2}    where FUN and X may be vectors or matrices.   
%             x
%
%   X=LEASTSQ('FUN',X0) starts at the matrix X0 and finds a minimum to the
%   sum of squares of the functions described in FUN. FUN is usually
%   an M-file which returns a vector of objective functions: F=FUN(X).
%   NOTE: FUN should return FUN(X) and not the sum-of-squares 
%   sum(FUN(X).^2)) (FUN(X) is summed and squared implicitly in
%   the algorithm).
%
%   X=LEASTSQ('FUN',X0,OPTIONS) allows a vector of optional parameters to
%   be defined. OPTIONS(2) is a measure of the precision required for the 
%   values of X at the solution. OPTIONS(3) is a measure of the precision
%   required of the objective function at the solution. See HELP FOPTIONS. 
%
%   X=LEASTSQ('FUN',X0,OPTIONS,'GRADFUN') enables a function'GRADFUN'
%   to be entered which returns the partial derivatives of the functions,
%   dF/dX, (stored in columns) at the point X: gf = GRADFUN(X).
%
%   X=LEASTSQ('FUN',X,OPTIONS,'GRADFUN',P1,P2,..) passes the 
%   problem-dependent parameters P1,P2,... directly to the functions FUN 
%   and GRADFUN: FUN(X,P1,P2,...) and GRADFUN(X,P1,P2,...).  Pass
%   empty matrices for OPTIONS and 'GRADFUN' to use the default values.
%
%   [X,OPTIONS,F,J]=LEASTSQ('FUN',X0,...) returns, F, the value of FUN(X)
%   at the solution X, and J the Jacobian of the function FUN at the 
%   solution. 

%   Copyright 1990-2001 The MathWorks, Inc. 
%   $Revision: 1.36 $  $Date: 2001/03/27 19:55:11 $
%   Andy Grace 7-9-90.
%   revised by Wes Wang 6-29-93.

%   The default algorithm is the Levenberg-Marquardt method with a 
%   mixed quadratic and cubic line search procedure.  A Gauss-Newton
%   method is selected by setting  OPTIONS(5)=1. 

%  This function is old, the current version should be used.
WarningStr=sprintf(...
['The function LEASTSQ is obsolete and has been replaced by\n',...
'LSQNONLIN.  LEASTSQ will be removed in a future release of the\n',...
'Optimization Toolbox.  Update your code to call LSQNONLIN to\n',...
'suppress this warning message.  See "Converting your code to\n',...
'Version 2 Syntax" in the Optimization Toolbox User''s Guide\n',...
'for more information.\n']);

%%%warning(WarningStr)




% ------------Initialization----------------

if nargin < 2, error('leastsq requires two input arguments');end
if nargin < 3, OPTIONS=[]; end
if nargin < 4, GRADFUN=[]; end

% Convert to inline function as needed.
if ~isempty(FUN)
  [funfcn, msg] = fcnchk(FUN,length(varargin));
  if ~isempty(msg)
    error(msg);
  end;
else
  error('FUN must be a function name or valid expression.')
end

if ~isempty(GRADFUN)
  [gradfcn, msg] = fcnchk(GRADFUN,length(varargin));
  if ~isempty(msg)
    error(msg);
  end
else
  gradfcn = [];
end

[x,OPTIONS,CostFunction,JACOB] = msnlsq(funfcn,x,OPTIONS,gradfcn,varargin{:});

%--end of leastsq--

