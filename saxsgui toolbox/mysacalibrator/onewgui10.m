% guis to SAXS Calibrator
% Milos Steinhart, Milky Way 16, 19. 07. 2007
function onewgui10(action)
global k pixelcal koffset wavelength pixelsize detector_dist calib_type oldc
global wavelength_default
global mysaS
global fig ax1
global bg br bw bz matbe matbg matbr matbr matbw matby matbz
global qe il opar oldcko nrr
global pixels ilda dpixels patre % tpixels
global inter cgood
global tmax tmin nlin ncol cire swaxs
global calib_type wavelength detector_dist wavel deted
global msqmin msqmax
global cxcc cycc rc rc2 hline cttl %cxcr cycr 
global hecon2 hecon3 hecon4 heconA heconB heconFs heconF 

hecon1=findobj('Tag','Con1');
hecon2=findobj('Tag','Con2');
hecon3=findobj('Tag','Con3');
hecon4=findobj('Tag','Con4');
hecon5=findobj('Tag','Con5');
hecon6=findobj('Tag','Con6');
hecon7=findobj('Tag','Con7');
hecon8=findobj('Tag','Con8');
hecon9=findobj('Tag','Con9');
heconA=findobj('Tag','ConA');
heconB=findobj('Tag','ConB');
heconC=findobj('Tag','ConC');
heconD=findobj('Tag','ConD');
heconE=findobj('Tag','ConE');
heconF=findobj('Tag','ConF');
heconFs=findobj('Tag','ConFs');
heconX=findobj('Tag','ConX');
axes(ax1);
switch action
  
  case 'test'
    title('Nothing is real');
% ***********************************************************************
% real beggining of callbacks 
% ***********************************************************************    
  case 'lorig'          % mysaS.pixels -> pixels, dpixels
    if ~isempty(wavelength) wavel=wavelength; else wavel=1.54; end
    if ~isempty(detector_dist) deted=detector_dist; else deted=1; end
    cire=0; set(hecon2,'string','circle'); % set to circullar ROI
    k=0.1076; set(hecon7,'string',num2str(k)); % AgBeh the 1st
    ko=1; set(hecon8,'string',int2str(ko)); % expect the 1st
    nrr=1; set(hecon9,'string',int2str(nrr)); % default draw only one ring
    swaxs=0; set(heconA,'string','SAXS');  % set to SAXS
    cgood=0;
    pixels=mysaS.pixels;
    [nlin ncol]=size(pixels);
    patre=logical(ones(nlin,ncol));
    tmax=max(max(pixels));
    set(hecon5,'string',num2str(tmax));
    tmin=min(min(pixels));
    set(hecon6,'string',num2str(tmin));
    dpixels=pixels;
    set(hecon1,'backgroundcolor',matbg);
    set(hecon2,'backgroundcolor',matbz);
    local_cursor('off');
    local_plot; axis equal;
  
  case 'tcire'         % toggle circullar / rectangullar ROI not used
    if cire==0
      cire=1; set(hecon2,'string','rect');
     else
      cire=0; set(hecon2,'string','circle');
    end

  case 'dcirc'
    set(hecon2,'backgroundcolor',matbg);
    local_sroi
    local_cursor('on');
    set(hecon3,'backgroundcolor',matbz);
    set(hecon4,'backgroundcolor',matbz);
    set(heconFs,'backgroundcolor',matbz);
      
  case 'sint'         % select interior of the ROI
    inter=0;
    local_set;        

  case 'sext'         % select exterior of the ROI
    inter=1;
    local_set
     
  case 'cres'
    set(hecon2,'backgroundcolor',matbz);
    set(hecon3,'backgroundcolor',matbg);
    set(hecon4,'backgroundcolor',matbg);
    set(heconFs,'backgroundcolor',matbg);
    local_cursor('off');
    local_plot
    cttl='Modify further the ROI of check the parameters and FIT';
    title(cttl);

    case 'cleft'
      cxcc=cxcc-1;
      local_adjust;
      
    case 'cright'
      cxcc=cxcc+1;
      local_adjust;

    case 'cup'
      cycc=cycc+1;
      local_adjust;
    
    case 'cdown'
      cycc=cycc-1;
      local_adjust;
      
    case 'rplus'
      rc=rc+1;
      rc2=rc^2;
      local_adjust;

    case 'rminus'
      rc=rc-1;
      rc2=rc^2;
      local_adjust;
     
    case 'shil'
      tmax=eval(get(hecon5,'string'));
%      t0=clock;
      patre=and(dpixels<tmax,dpixels>tmin);
      pixels=dpixels.*patre;
%      t1=clock; etime(t1,t0)
      local_plot;
      
    case 'slol'
      tmin=eval(get(hecon6,'string'));
      patre=and(dpixels<tmax,dpixels>tmin);
      pixels=dpixels.*patre;
      local_plot;

    case 'sq0'  % SAXS: q of the first ring; WAXS: q of the fitted ring
      k=eval(get(hecon7,'string')); 

    case 'sord' % SAXS: order of the fitted ring; WAXS: qmin
      ko=eval(get(hecon8,'string'));

    case 'snrr' % SAXS: nr of rings to plot; WAXS: qmax
      nrr=eval(get(hecon9,'string'));

   case 'tswaxs'    
    if swaxs==0
      swaxs=1; set(heconA,'string','WAXS');
      ko=0.1; set(hecon8,'string',num2str(ko));  % qmin
      nrr=1.8; set(hecon9,'string',num2str(nrr)); % qmax
      set(hecon8,'TooltipString','Set qmin for averagex');
      set(hecon9,'TooltipString','Set qmax for averagex');
     else
      swaxs=0; set(heconA,'string','SAXS');
      ko=1; set(hecon8,'string',int2str(ko)); % expect the 1st
      nrr=1; set(hecon9,'string',int2str(nrr)); % default draw one ring
      set(hecon8,'TooltipString','Set order of the calibration ring');
      set(hecon9,'TooltipString','Set nr of rings to plot');
    end   
    set(heconA,'backgroundcolor',matbg);
    
    case 'fit'
      [qe,il]=find(dpixels);
      set(heconA,'backgroundcolor',matbg);
      mytest2
      dpixels=pixels;
      pixels=mysaS.pixels;
%      patre=[];
      set(heconA,'backgroundcolor',matbz);
      set(heconB,'backgroundcolor',matbg);
      set(heconC,'backgroundcolor',matbz);
      local_plot;
      title('Accept and return to SAXSGUI or start again');
      ko=eval(get(hecon8,'string'));
      nrr=eval(get(hecon9,'string'));
      hline=mscirclen(opar,swaxs,ko,nrr,k*ko);
%      if swaxs>0 set(hline,'linewidth',2); end

    case 'accept'
      % have rewritten this to accomodate for changes in newer SAXSGUI
      % versions
      if get(heconE,'Value')==1
          k=eval(get(hecon7,'string'))*eval(get(hecon8,'string'));
          pixelcal=[opar(4) opar(3)];
          koffset=0;
          oldc=[opar(2) opar(1)];
          calib_type='std';
          title('Press End to proceed or start again');
          prompt={'Enter the Wavelength A:'};
          name='Input for Wavelength';
          numlines=1;
          if isempty(wavelength)
              defaultanswer={num2str(wavelength_default)};
          else
              defaultanswer={num2str(wavelength)};
          end

          answer=inputdlg(prompt,name,numlines,defaultanswer);
          if ~isempty(answer)
              if ~isempty(str2num(answer{1}))
                  wavelength=str2num(answer{1});
              end
          end
      end
      if get(heconF,'Value')==1
          oldc=[opar(2) opar(1)];
      end
      
      dpixels=[];

    case 'pend'
     close(gcbf);
    
    otherwise
     title(['action -> ',action,' <- is unknown']);    

  end %case   
% ***********************************************************************    
% end of callbacks - beggining of local functiond 
% ***********************************************************************    
function local_plot  
  global pixels ax1 ilda
  global tmin tmax
  axes(ax1);
  ilda=imagesc(pixels,[tmin,tmax]);
  set(get(ilda,'parent'),'ydir','normal');


function local_sroi
  global pixels patre cgood
  global cire nlin ncol
  global phi cxcr cxcc cycr cycc hline bla rc rc2
  patre=logical(zeros(nlin,ncol));
  if cire==0
% Draw a circle etc - for adjusted circle the center is kept the same
    if cgood==0
      title('Mark center of a circle by right mouse button)');
      but=1;
      while but==1
        [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
      end   
      cxcc=cx;
      cycc=cy;
      title('Mark a point on a a circle by right mouse button)');
    else
      title('Center Previous - Mark a point on a circle by right mouse button)');
    end
    bla=plot(cxcc,cycc,'+y');
% mark the middle in the pixels array - can be removed later
%    pixels((cxcc-1)*nlin+cycc)=1000; local_plot
    but=1;
    while but==1
      [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
    end   
    cxcr=cx; cycr=cy;
    phi=0:2*pi/100:2*pi;
    rc2=(cxcr-cxcc)^2+(cycr-cycc)^2;
    rc=sqrt(rc2);
%   draw a circle
    circx = cxcc+rc*cos(phi);
    circy = cycc+rc*sin(phi);
    hline = line(circx, circy);
    set(hline,'color','y');    % tentative circle
    title('Adjust the ROI then SET it');
   else
    title('Mark the lower left corner of the ROI eith the right button');
    but=1;
    while but==1
      [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
    end   
    cxld=cx; cyld=cy;
    title('Mark the upper right corner of the ROI eith the right button');
    but=1;
    while but==1
      [cx,cy,but]=ginput(1); cx=round(cx); cy=round(cy);
    end   
    cxru=cx; cyru=cy;
    patre(cyld:cyru,cxld:cxru)=1; % !switch indices due to image properties! 
  end

  
function local_adjust
  global phi cxcr cxcc cycr cycc hline bla rc rc2 cttl deted wavel
  set(bla,'visible','off');
  set(hline,'visible','off');
  bla=plot(cxcc,cycc,'+y');
  circx = cxcc+rc*cos(phi);
  circy = cycc+rc*sin(phi);
  hline = line(circx, circy);
  set(hline,'color','y');    % tentative circle
  cqc=4*pi*sin(atan(rc/deted)/2)/wavel;
  cdc=2*pi/cqc;
  title([cttl,' cx=',int2str(cxcc),' cy=',int2str(cycc),...
      ' rc=',int2str(rc),' qc=',num2str(cqc),' dc=',num2str(cdc)]); 

  
function local_set
  global pixels dpixels patre
  global cire nlin ncol
  global phi cxcr cxcc cycr cycc hline bla rc rc2
  global inter cgood
  global hecon2 hecon3 hecon4 heconA heconB heconFs 
  global bg br bw bz matbe matbg matbr matbr matbw matby matbz

% limits of indices surely outside the ROI    
    xlout=round(cxcc-rc)*nlin;
    xhout=round(cxcc-1+rc)*nlin;
% patre ... interior of the ROI
    patre(1:xlout)=0; patre(xhout:end)=0;
    for ii=xlout:xhout
      ccol=ceil(ii/(nlin));
      clin=ii-(ccol-1)*(nlin);
      if clin>cycc-rc && clin<cycc+rc    % further elimination
        if ((cxcc-ccol)^2+(cycc-clin)^2)<rc2
          patre(ii)=1;
         else
          patre(ii)=0;
        end
       else  
        patre(ii)=0;
      end
    end
    set(hecon3,'backgroundcolor',matbg);
    set(hecon4,'backgroundcolor',matbg);
    if cgood==1 
      set(heconA,'backgroundcolor',matbz); 
      set(heconB,'backgroundcolor',matbz); 
     else
      set(hecon2,'backgroundcolor',matbz);
    end
    cgood=1;
    if inter==0
      pixels=pixels.*patre;
     else    
     pixels=pixels.*~patre;
    end
    dpixels=pixels;
    set(heconFs,'backgroundcolor',matbg);
    local_cursor('off');
    local_plot
    cttl='Modify further the ROI of check the parameters and FIT';
    title(cttl);


function local_cursor(visible)
  hecon3=findobj('Tag','Con3');
  hecon4=findobj('Tag','Con4');
  heconFs=findobj('Tag','ConFs');
  heconFl=findobj('Tag','ConFl');
  heconFr=findobj('Tag','ConFr');
  heconFu=findobj('Tag','ConFu');
  heconFd=findobj('Tag','ConFd');
  heconFp=findobj('Tag','ConFp');
  heconFm=findobj('Tag','ConFm');
  set(hecon3,'visible',visible);        
  set(hecon4,'visible',visible);        
  set(heconFs,'visible',visible);        
  set(heconFl,'visible',visible);        
  set(heconFr,'visible',visible);        
  set(heconFu,'visible',visible);        
  set(heconFd,'visible',visible);        
  set(heconFp,'visible',visible);        
  set(heconFm,'visible',visible);        
    
  
function unused
aa=zeros(3,4);
[sizx sizy]=size(aa);
[nlin ncol]=size(aa);
for ii=1:sizx*sizy aa(ii)=ii; end
aa
for nn=1:nlin*ncol;
%ccol=3; clin=1; cnn=(ccol-1)*nlin+clin
ccol=ceil(nn/(nlin));
clin=nn-(ccol-1)*(nlin);
%aa(clin,ccol); %aa(cnn);
disp([int2str(nn),' ',int2str(clin),' ',int2str(ccol)])
end

