% test waxs fitting
% Milos Steinhart, Milky Way 13, 18. 07. 2007
global qe il it %% ee 
global mask parx
qe=0.1076*[2:13]';
il=[29.271 44.095 58.92 76.709 91.533 106.36 124.15 138.97 154 ...
    171.58 192.34 201.23]';
ipar=[500 1.54];
mask=[1 0]; parx=[0 1];
%opar=msleastsq('mswaxs01',ipar)
opar=fmins('mswaxs01',ipar)
%for ii=500:700
%  bre(ii)=ii;
%  bra(ii)=feval('mswaxs01',[ii 1.4]);
%end
%figure(bry);
%plot(bre,bra,'y');
%hold on
