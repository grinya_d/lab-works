function biftfig=plotbiftresults(results,biftfig)

%   PLOTBIFT creates a plotwindow for plotting the results
%   of the Bayesian IFT process 
%
%   hfig=plotbiftresults(results,biftfig)
%
%     
%     The input structure is as follows:
%     results.xin;
%     results.yin;
%     results.sigmayin;
%     results.yfit;
%     results.r;
%     results.pr;
%     results.sigmapr;  
%     results.evix
%     results.eviy
%     results.eviz
%     results.ndmax; % number of dmax values in evimatrix
%     results.rg;
%     results.sigmarg;
%     results.Io
%     results.sigma;Io
%     results.DmaxMean;
%     results.SigmaDmaxMean;
if nargin==2
    figure(biftfig(1))
else
    biftfig=figure;
end
set(gcf,'Name','Results of Bayesian IFT')
calcmenu = uimenu('Label', 'Calculations');
changeparametersmenu=uimenu(calcmenu, 'Label', 'Redo IFT', 'Callback', ...
    {@DoIFT, biftfig});
changeparametersmenu=uimenu(calcmenu, 'Label', 'Refine IFT', 'Callback', ...
    {@RefineIFT, biftfig});
advancedparametersmenu=uimenu(calcmenu, 'Label', 'Advanced', 'Visible','Off','Callback', ...
    {@AdvancedParam, biftfig});


outputmenu = uimenu('Label', 'Output');
figmenu=uimenu(outputmenu, 'Label', 'As Matlab figure', 'Callback', ...
    {@SaveFig, biftfig});
mfilemenu=uimenu(outputmenu, 'Label', 'As M-file', 'Callback', ...
    {@SaveMfile, biftfig});
asciifilemenu=uimenu(outputmenu, 'Label', 'As Ascii-file', 'Callback', ...
    {@SaveAscii, biftfig});

biftplotcontent(results,biftfig)
set(biftfig,'UserData',results)

%***********************
function DoIFT(hmenu, eventdata, biftfig)

results=get(biftfig,'UserData');
datain=[results.xin;results.yin;results.sigmayin];
resultsnew=biftfull(datain,1,0);
biftfignew=findobj('Name','Data for Bayesian IFT');
plotbiftresults(resultsnew,biftfignew)
set(biftfignew,'UserData',resultsnew)

%***********************
function RefineIFT(hmenu, eventdata, biftfig)

results=get(biftfig,'UserData');  %get data from old figure;
resultsnew=biftrefine(results);  % Make Bayesian IFT;
biftfignew=figure; % always make new figure;
plotbiftresults(resultsnew,biftfignew) % Plot results in window
set(biftfignew,'UserData',resultsnew) %save new data in window

%***********************
function SaveFig(hmenu, eventdata, biftfig)

[filename, pathname] = uiputfile('*.fig', 'Save Figure As');
if  ~(isequal(filename,0) || isequal(pathname,0))
    saveas(biftfig,[pathname,filename],'fig')
end

%***********************
function SaveMfile(hmenu, eventdata, biftfig)
results=get(biftfig,'UserData');
[filename, pathname] = uiputfile('*.m', 'Save As M-file');
if  ~(isequal(filename,0) || isequal(pathname,0))
    save([pathname,filename],results)
end

%***********************
function SaveAscii(hmenu, eventdata, biftfig)
results=get(biftfig,'UserData');
[filename, pathname] = uiputfile('*.csv', 'Save as Ascii-file (Comma-separated)');
%add extension if not there


if  ~(isequal(filename,0) || isequal(pathname,0))
    %add extension if not there
    file=fullfile(pathname,filename);
    [a,b,c]=fileparts(file);
    file2=fullfile(a,[b,'.csv']);
    fid=fopen(file2,'w');
    fprintf(fid,'Filename, %s  \n',results.name);
    fprintf(fid,'Rg +- sigma (A), %-10.4g, %-10.4g \n',results.rg,results.sigmarg);
    fprintf(fid,'Io +- sigma , %-10.4e, %-10.4g \n',results.Io,results.sigmaIo);
    fprintf(fid,'Dmaxmean +- sigma (A), %-10.4g, %-10.4g \n',results.DmaxMean,results.SigmaDmaxMean);
    fprintf(fid,'Dmaxmostlikely (A), %-10.4g \n',results.DmaxMostLikely);
    fprintf(fid,'q[1/A],I,sigI,fit\n');
    for ii=1:length(results.xin)
        fprintf(fid,'%-10.3e, %-10.3e, %-10.3e, %-10.3e \n',results.xin(ii),results.yin(ii),results.sigmayin(ii),results.yfit(ii));
    end
    fprintf(fid,'r[A],p(r),sigma_p(r)\n');
    for ii=1:length(results.r)
        fprintf(fid,'%-6.2f, %-10.3e, %-10.3e \n',results.r(ii),results.pr(ii),results.sigmapr(ii));
    end
    fclose(fid)
    % the alpha, dmax variation results will not be saved.
end


