function [qdata,guinier]=guiniercalc(p,qdata,Idata,sigIdata)

ro=1 ;   %density (assumed = 1)
Rg=p(1); %radius of gyration
fac=p(2); %factor in front of whole expression
%back=p(3);
back=3;
q=qdata;

% particle parameters
ro=1;
rm=Rg*1.12;

%Establishing Behavior in Beaucage Model
G=ro*ro*(4/3*pi*rm);
B=9*G/(2*rm^4);
qs=q./(erf(q*Rg/sqrt(6))).^3;
guinier=fac*(G*exp(-(q.^2)*(Rg.^2)/3))+back;

% figure(10)
% plot(qdata,Idata,qdata,guinier)
% drawnow