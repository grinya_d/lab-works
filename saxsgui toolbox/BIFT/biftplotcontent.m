
function biftplotcontent(results,biftfig)
%     
subplot(2,2,1)
errorbar(results.xin,results.yin,results.sigmayin)
title('Data with errorbars')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Intensity a.u.')
if max(results.yin)/min(results.yin)>100 
    set(gca,'Yscale','Log')
end

subplot(2,2,2)
errorbar(results.r,results.pr,results.sigmapr)
title('Bayesian Weighted Distance Distribution Function p(r)')
xlabel('Distance, (A)')
ylabel('p(r) (relative units)')
txt1=['Rg=',num2str(results.rg),'�',num2str(results.sigmarg),' A '];
txt2=['Io=',num2str(results.Io),'�',num2str(results.sigmaIo)];
txt3=['Dmax=',num2str(results.DmaxMean),'�',num2str(results.SigmaDmaxMean),' A '];
txt4=['Dmax(most likely)=',num2str(results.DmaxMostLikely),' A '];
xlim=get(gca,'Xlim');
ylim=get(gca,'Ylim');
text(0.8,0.9,txt1,'HorizontalAlignment','right','Units','Normalized')
text(0.8,0.8,txt2,'HorizontalAlignment','right','Units','Normalized')
text(0.8,0.7,txt3,'HorizontalAlignment','right','Units','Normalized')
text(0.8,0.6,txt4,'HorizontalAlignment','right','Units','Normalized')

subplot(2,2,3)
plot(results.xin,results.yin,results.xin,results.yfit)
title('Comparison between data and fit ')
xlabel('Momentum Transfer q, (1/A)')
ylabel('Intensity a.u.')
if max(results.yin)/min(results.yin)>100 
    set(gca,'Yscale','Log')
end

subplot(2,2,4)
mesh(results.evix,results.eviy,exp(results.eviz))
title('Probability for different model parameters (\alpha ,Dmax) ')
xlabel('Dmax, (A)')
ylabel('\alpha')
zlabel('Evidence (relative units)')
set(gca,'Yscale','Log') 
%