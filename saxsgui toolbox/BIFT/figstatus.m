function h = figstatus(message, fig, color)
%FIGSTATUS Display a status line message in a figure.
%   H = FIGSTATUS('message') displays a yellow bar at the bottom of the
%   current figure with 'message' inscribed.  FIGSTATUS returns the handle
%   H of the bar.
%
%   FIGSTATUS replaces any newlines in the message with three blank characters,
%   '   '.
%
%   DELETE(H) deletes the bar via its handle.
%
%   H = FIGSTATUS('message', FIG) displays the message in the figure whose
%   handle is FIG.
%
%   H = FIGSTATUS('message', FIG, COLOR) sets the background color of the
%   bar to something besides yellow.
if nargin < 3
    color = 'yellow';
end
if nargin < 2
    fig = gcf;
end
if nargin < 1
    message = '';
end
if ~ ischar(message)
    error('1st argument must be a character string.')
end
if ~ ishandle(fig) || (~ isequal(get(fig, 'Type'), 'figure'))
    error('2nd argument must be a figure handle.')
end

message = regexprep(message, '\n', '   ');

oldfigunits = get(fig, 'Units');
set(fig, 'Units', 'characters')
figpos = get(fig, 'Position');
set(fig, 'Units', oldfigunits)
lenchars = figpos(3);
h = uicontrol(fig, 'Style', 'text', 'String', [' ', message], ...
    'FontWeight', 'bold', 'HorizontalAlignment', 'left', ...
    'BackgroundColor', color, ...
    'Units', 'characters', 'Position', [0.5, 0.5, lenchars - 1, 1.1]);
set(h,'Tag','statusline') % tag it so we can find it later
drawnow
