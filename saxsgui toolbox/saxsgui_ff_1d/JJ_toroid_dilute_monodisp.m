function Icalc=JJ_toroid_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for dilute monodisperse dispersion
% of toroids
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function
% parameter 2: Rt : Toroid Radius
% parameter 3: Rc : Cross section Radius
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it use by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of toroids
% 
% - The averaging over orientations is performed numerically
% Taken from J.S.Pedersen, ch 16. case 14, eq. 32
%

values=varargin{1};
Amp=values(1);
Rt=values(2);
Rc=values(3);
Backg=values(4);

V=2*pi^2*Rc^2*Rt;
betastep=0.01;
beta=betastep:betastep:pi/2;

sumoverbeta=0;
for ii=1:length(beta)
    F=FTor(q,Rt,Rc,beta(ii));
    sumoverbeta=sumoverbeta+sin(beta(ii))*abs(F.^2)*betastep;
end
Icalc=Amp*2*pi^2/V*sumoverbeta+Backg;

function Fout=FTor(q,Rt,Rc,beta)
zstep=Rc/50;
z=-Rc:zstep:Rc;
tempsum=0;
for jj=1:length(z)
    Rp=Rplus(Rt,Rc,z(jj));
    Rm=Rminus(Rt,Rc,z(jj));
    temp1=(Rp*besselj(1,q*Rp*sin(beta))-Rm*besselj(1,q*Rm*sin(beta))).*...
        cos(q*z(jj)*cos(beta))./(q*sin(beta));
    tempsum=tempsum+temp1*zstep;
end
Fout=tempsum;


function Rplusout=Rplus(Rt,Rc,z)
Rplusout=Rt+sqrt(Rc-z);

function Rminusout=Rminus(Rt,Rc,z)
Rminusout=Rt-sqrt(Rc-z);

