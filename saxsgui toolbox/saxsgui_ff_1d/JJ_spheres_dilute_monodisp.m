function Icalc=JJ_spheres_dilute_monodisp(q,I,varargin)
% Description
% This function calculates 1D SAXS data for monodisperse hard spheres
% Description end
% Number of Parameters: 3
% parameter 1: Amp : Amplitude of function 
% parameter 2: R : Hard-sphere Radius 
% parameter 3: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute monodisperse dispersion of hard
% spheres. Taken from J.S.Pedersen, ch 16. case 1, eq. 21
% 

values=varargin{1};
Amp=values(1);
R=values(2);
Backg=values(3);

F=F1(q,R);
Icalc=Amp*abs(F.^2)+Backg;

function Fout=F1(q,R)
Fout=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);
