function Icalc=JJ_spheres_dilute_polydisp_gaussian(q,I,varargin)
% Description
% This function calculates 1D SAXS data for a gaussian
% size distribution of spheres
% Description end
% Number of Parameters: 4
% parameter 1: Amp : Amplitude of function 
% parameter 2: Ravg : Average Hard-sphere Radius 
% parameter 3: Rsigma : Standard Deviation of Radius Value 
% parameter 4: Backg: Constant Background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% Intensity calculation for a dilute polydisperse dispersion of hard
% spheres. Polydispersity follows a true gaussian
% 

values=varargin{1};
Amp=values(1);
Ravg=values(2);
Rsigma=values(3);
Backg=values(4);

numRval=101; %number of values of R to use
Rsigmarange=3; % the R-range will be evaluated from +- Rsigmarange standard deviations

rarray=max(0.1,Ravg-Rsigmarange*Rsigma):(2*Rsigmarange*Rsigma)/(numRval-1):Ravg+Rsigmarange*Rsigma;
normarray=normfun(rarray,Ravg,Rsigma);

imax=max(size(rarray));
ii=1;
Fsqsum=q*0;
wsum=0;
while ii<=imax,
    Fsqsum=abs(F1(q,rarray(ii)).^2)*normarray(ii)*(4*pi/3*rarray(ii)^3)^2+Fsqsum;
    wsum=wsum+normarray(ii);
    ii=ii+1;
end
Fsqavg=Fsqsum/wsum;

Icalc=Amp*Fsqavg+Backg;



function F=F1(q,R)
F=3*(sin(q*R)-q*R.*cos(q*R))./((q*R).^3);
