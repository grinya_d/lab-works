function wpos = windowsize(maxfrac, proportion)
%WINDOWSIZE Compute window size in pixels, given proportion information.
%   WINDOWSIZE(MAXFRAC, PROP) returns a position array, 
%   [LEFT, BOTTOM, WIDTH, HEIGHT], in pixels.  The window position is
%   centered in the screen.  MAXFRAC is the maximum fraction of the
%   screen's width or height to use.  MAXFRAC = 1 makes the window size as
%   large as possible without overlapping the screen in either width or
%   height.  PROP is the desired width / height proportion for the new
%   window.
%
%   WARNING:  Keep MAXFRAC less than 1...

% Check arguments.
if ~ (nargin == 2 && ...
        isnumeric([proportion maxfrac]) && ...
        isequal(size([proportion maxfrac]),[1 2]) && ...
        maxfrac <= 1)
    error('I need two arguments:  window proportion; fraction of screen to use.')
end

% Get screen size in pixels.
set(0, 'Units', 'pixels')
scrsize = get(0, 'ScreenSize');
scrwidth = scrsize(3);
scrheight = scrsize(4);

% Try sizing by width first.
wwidth = scrwidth * maxfrac;
wheight = wwidth / proportion;

if wheight > scrheight  % too tall:  size by height
    wheight = scrheight * maxfrac;
    wwidth = wheight * proportion;
end

% Center the new window position.
wleft = 1 + floor((scrwidth - wwidth) / 2);
wbottom = 1 + ceil((scrheight - wheight) / 2);

% Return a position vector.
wpos = [wleft wbottom wwidth wheight];
