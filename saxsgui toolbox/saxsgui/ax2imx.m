function imcol = ax2imx(im, axx)
%AX2IMX Convert an axis x value to the nearest image pixel column.
%   IMCOL = AX2IMX(IM, AXX) converts an x axis value AXX to the nearest
%   image pixel column IMCOL in the image IM.  AXX is an x value in
%   the axes containing IM.
%
%   If AXX is a vector or array, AX2IMX returns a like-sized
%   array of image column numbers.

% Assume our arguments are sane.

xdata = get(im, 'XData');
ncols = size(get(im, 'CData'), 2);
if length(xdata) == 2
    xdata = [xdata(1) : (xdata(2) - xdata(1)) / (ncols - 1) : xdata(2)];
end
imcol = interp1(xdata, [1 : ncols], axx, 'nearest');
