function [x,y] = ellip2cart2(u,v,a);
%ELLIP2CART2 Elliptical to cartesian coordinate transformation
% Inputs are the elliptical coordinates for radial (u) and
% angular dimension (v), degree of ellipticity (a). Outputs
% are the cartesian coordinates (x) and (y). This function
% uses the transformation:
%   x = sqrt(a^2+u.^2).*cos(v);
%   y = u.*sin(v);
%
% I/O: [x,y] = ellip2cart2(u,v,a);
%
% See also FINDELLIP

% x = sqrt(a^2+u.^2).*cos(v);
% Try this instead?
f = u .* a;
x = sqrt(f.^2+u.^2).*cos(v);

y = u.*sin(v);
