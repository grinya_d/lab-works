function varargout = Fitting1D(varargin) 
%Fitting1D  Explore intensity averaging in a SAXS image.
%   Fitting1D(SAXSIM) starts the 1D Fitting window with data passed
%   on from a figure window in the form of data and a SAXS structure S
%
%   With the 1D fitting window you select the fitting conditions and
%   fitting functions to invoke fitting routines on 1D data. 
%   
%   You can choose a particular fitting function from a list of available
%   fitting functions in directory, and you can then choose which
%   of the parameters you want to fix and which ones you want to let vary
%   i.e. fit.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Fitting1D, by itself, creates a new Fitting1D figure or raises the existing
%   singleton*.
%
%   H = Fitting1D returns the handle to a new Fitting1D or the handle to
%   the existing singleton*.
%
%   Fitting1D('CALLBACK',hObject,eventData,handles,...) calls the local
%   function named CALLBACK in Fitting1D.M with the given input arguments.
%
%   Fitting1D('Property','Value',...) creates a new Fitting1D or raises the
%   existing singleton*.  Starting from the left, property value pairs are
%   applied to the GUI before Fitting1D_OpeningFunction gets called.  An
%   unrecognized property name or invalid value makes property application
%   stop.  All inputs are passed to Fitting1D_OpeningFcn via varargin.
%
%   *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%   instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Fitting1D

% Last Modified by GUIDE v2.5 25-Jun-2007 11:01:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Fitting1D_OpeningFcn, ...
                   'gui_OutputFcn',  @Fitting1D_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes just before Fitting1D is made visible.
function Fitting1D_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Fitting1D (see VARARGIN)

global executableversion fitfunctions
% Choose default command line output for Fitting1D
handles.output = hObject;

% Get the rth SAXS image.
if ~isempty(varargin) && isa(varargin{1}, 'saxs')
    handles.saxsin = varargin{1};
else
     error('The input to the fitting routine is not a SAXS argument. It must be!');
end


% Record pixel spacing in the SAXS image.
handles.xspacing = (handles.saxsin.xaxis(2) - handles.saxsin.xaxis(1)) ...
    / (size(handles.saxsin.pixels, 2) - 1);
    
% Display the SAXS image (in log if log in SAXSGUI window)
handles.image1 = loglog(handles.data,handles.saxsin.xaxisfull,handles.saxsin.pixels);
title(handles.data,'Data to fit')

% Insert the filename
filename=find_filename(handles.saxsin);
set(handles.Filename, 'String', filename);

% Set the bounds.
set(handles.xleft, 'String', num2str(handles.saxsin.xaxis(1)))
set(handles.xright, 'String', num2str(handles.saxsin.xaxis(2)))

% Now we need to load the available functions into the handles and
% put them into the pulldown menus and so first we find the functions
if executableversion
    handles.fitfunctions=fitfunctions;
else
    handles.fitfunctions.oneD=find_fitfunctions(1);% looking for 1D fitfunctions
end
% Update handles structure
guidata(hObject, handles);

% now load the first function
update_gui_new(handles,1);

% UIWAIT makes Fitting1D wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Outputs from this function are returned to the command line.
function varargout = Fitting1D_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function xleft_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xleft_Callback(hObject, eventdata, handles)
% hObject    handle to xleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xleft_validate(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xleft_validate(handles)
val = str2double(get(handles.xleft,'String'));  % interpret
val = max([val, handles.saxsin.xaxis(1)]);  % limit low value
val = min([val, str2double(get(handles.xright, 'String')) - handles.xspacing]);
    % at least one pixel short of right bounding line
set(handles.xleft, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function xright_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xright_Callback(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xright_validate(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xright_validate(handles)
val = str2double(get(handles.xright,'String'));  % interpret
val = min([val, handles.saxsin.xaxis(2)]);  % limit high value
val = max([val, str2double(get(handles.xleft, 'String')) + handles.xspacing]);
    % at least one pixel right of left bounding line
set(handles.xright, 'String', num2str(val))  % display what we interpreted


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in newplot.
function newfit_Callback(hObject, eventdata, handles)
% hObject    handle to newplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fitpar
hstatus=figstatus('Loading parameters',get(hObject,'Parent'));
fitpar=read_gui_vals(handles);
bounds=[str2double(get(handles.xleft,'String')) str2double(get(handles.xright,'String'))];
hstatus=figstatus('Initializing Fit',get(hObject,'Parent'));
set(handles.fit,'YScale','log');
set(handles.fit,'XScale','lin');

clearstatus
drawnow
[Fitparam,Fitval,Info] = fitsaxs1D(handles.saxsin,bounds,handles);

%update table
update_gui_fitted(handles,Fitparam,Fitval)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in calculate.
function calculate_Callback(hObject, eventdata, handles)
% hObject    handle to calculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fitpar
hstatus=figstatus('Calculating function based on Start values',get(hObject,'Parent'));
fitpar=read_gui_vals(handles);
bounds=[str2double(get(handles.xleft,'String')) str2double(get(handles.xright,'String'))];
clearstatus
drawnow
fitpar.numiter=0;
fitpar.ul=fitpar.start;
fitpar.ll=fitpar.start;
oldfitpargraphics=fitpar.graphics;
fitpar.graphics=1;
[Fitparam,Fitval,Info] = fitsaxs1D(handles.saxsin,bounds,handles);

set(handles.chisqval,'String',num2str(Fitval,3))
fitpar.graphics=oldfitpargraphics;

% --- Executes on button press in GuessStartVals.
function GuessStartVals_Callback(hObject, eventdata, handles)
global fitpar
% hObject    handle to GuessStartVals (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hstatus=figstatus('Loading parameters',get(hObject,'Parent'));
fitpar=read_gui_vals(handles);
fitpar.start=fitpar.fit.*NaN;
bounds=[str2double(get(handles.xleft,'String')) str2double(get(handles.xright,'String'))];

clearstatus
drawnow
[Fitparam,Fitval,Info] = fitsaxs1D(handles.saxsin,bounds,handles);

%update table
update_gui_startval(handles,fitpar)

% --- Executes on selection change in BinningPopUp.
function BinningPopUp_Callback(hObject, eventdata, handles)
% hObject    handle to BinningPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns BinningPopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BinningPopUp


% --- Executes during object creation, after setting all properties.
function BinningPopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BinningPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in FittingFilenamePopup.
function FittingFilenamePopup_Callback(hObject, eventdata, handles)
% hObject    handle to FittingFilenamePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns FittingFilenamePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FittingFilenamePopup

fitfilenum=get(handles.FittingFilenamePopup,'Value');
update_gui_new(handles,fitfilenum);


% --- Executes during object creation, after setting all properties.
function FittingFilenamePopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FittingFilenamePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in fit1.
function fit1_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit2.
function fit2_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit3.
function fit3_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit4.
function fit4_Callback(hObject, eventdata, handles)


% --- Executes on button press in fit5.
function fit5_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit6.
function fit6_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit7.
function fit7_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit8.
function fit8_Callback(hObject, eventdata, handles)

% --- Executes on button press in fit9.
function fit9_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function startval1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval1_Callback(hObject, eventdata, handles)
startval_input_check(handles,1)

% --- Executes during object creation, after setting all properties.
function startval2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval2_Callback(hObject, eventdata, handles)
startval_input_check(handles,2)

% --- Executes during object creation, after setting all properties.
function startval3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval3_Callback(hObject, eventdata, handles)
startval_input_check(handles,3)

% --- Executes during object creation, after setting all properties.
function startval4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval4_Callback(hObject, eventdata, handles)
startval_input_check(handles,4)

% --- Executes during object creation, after setting all properties.
function startval5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval5_Callback(hObject, eventdata, handles)
startval_input_check(handles,5)

% --- Executes during object creation, after setting all properties.
function startval6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval6_Callback(hObject, eventdata, handles)
startval_input_check(handles,6)

% --- Executes during object creation, after setting all properties.
function startval7_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval7_Callback(hObject, eventdata, handles)
startval_input_check(handles,7)

% --- Executes during object creation, after setting all properties.
function startval8_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval8_Callback(hObject, eventdata, handles)
startval_input_check(handles,8)

% --- Executes during object creation, after setting all properties.
function startval9_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startval9_Callback(hObject, eventdata, handles)
startval_input_check(handles,9)


% --- Executes on button press in Graphics_on.
function Graphics_on_Callback(hObject, eventdata, handles)
% hObject    handle to Graphics_on (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Graphics_on



function num_of_iter_Callback(hObject, eventdata, handles)
% hObject    handle to num_of_iter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_of_iter as text
%        str2double(get(hObject,'String')) returns contents of num_of_iter as a double


% --- Executes during object creation, after setting all properties.
function num_of_iter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_of_iter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Tolerance_Callback(hObject, eventdata, handles)
% hObject    handle to Tolerance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tolerance as text
%        str2double(get(hObject,'String')) returns contents of Tolerance as a double

% --- Executes during object creation, after setting all properties.
function Tolerance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tolerance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in refinefit.
function refinefit_Callback(hObject, eventdata, handles)
% hObject    handle to refinefit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update_gui_fit_to_start(handles)


%************************************************
function update_gui_new(handles,functionnum)

a=handles.fitfunctions.oneD;
%Insert the function name in the popup
set(handles.FittingFilenamePopup,'String',a.names)
%Set it to the one specific one indicated
set(handles.FittingFilenamePopup,'Value',functionnum)
%set(handles.FittingFilenamePopup,'TooltipString',a.name_help{functionnum}) 
set(handles.refinefit,'Enable','off')

%now set the fields according to the how many parameters in functionnum
for ii=1:a.numparams{functionnum}
    if (a.param_bool{functionnum}{ii}==1) % the parameter is boolean
        eval(['set(handles.par',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.par',num2str(ii),',''String'',[a.param_name{functionnum}{ii},'' bool'']);'])
        eval(['set(handles.par',num2str(ii),',''TooltipString'',a.param_help{functionnum}{ii});'])
        eval(['set(handles.fit',num2str(ii),',''Visible'',''off'');'])
        eval(['set(handles.fit',num2str(ii),',''Value'',0.0);'])
        eval(['set(handles.startval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.startval',num2str(ii),',''String'',''0'');'])
        eval(['set(handles.startval',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Enable'',''off'');'])
        eval(['set(handles.lowlim',num2str(ii),',''String'',''0'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.hilim',num2str(ii),',''Enable'',''off'');'])
        eval(['set(handles.hilim',num2str(ii),',''String'',''0'');'])
        eval(['set(handles.hilim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''String'','' '');'])
        eval(['set(handles.fitval',num2str(ii),',''Visible'',''on'');'])
    else %the parameter is not boolean
        eval(['set(handles.par',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.par',num2str(ii),',''String'',a.param_name{functionnum}{ii});'])
        eval(['set(handles.par',num2str(ii),',''TooltipString'',a.param_help{functionnum}{ii});'])
        eval(['set(handles.fit',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.fit',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.fit',num2str(ii),',''Value'',1.0);'])
        eval(['set(handles.startval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.startval',num2str(ii),',''String'','' '');'])
        eval(['set(handles.startval',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.lowlim',num2str(ii),',''String'',''-Inf'');'])
        eval(['set(handles.lowlim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.hilim',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.hilim',num2str(ii),',''String'',''Inf'');'])
        eval(['set(handles.hilim',num2str(ii),',''Visible'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''Enable'',''on'');'])
        eval(['set(handles.fitval',num2str(ii),',''String'','' '');'])
        eval(['set(handles.fitval',num2str(ii),',''Visible'',''on'');'])
    end

end

for ii=a.numparams{functionnum}+1:9
    eval(['set(handles.par',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.par',num2str(ii),',''String'',[''#',num2str(ii),''']);'])
%    eval(['set(handles.par',num2str(ii),',''TooltipString'','' '';'])
    eval(['set(handles.fit',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.fit',num2str(ii),',''Value'',0);'])
    eval(['set(handles.startval',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.startval',num2str(ii),',''String'','' '');'])
    eval(['set(handles.startval',num2str(ii),',''Visible'',''off'');'])
    eval(['set(handles.lowlim',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.lowlim',num2str(ii),',''String'','' '');'])
    eval(['set(handles.lowlim',num2str(ii),',''Visible'',''off'');'])
    eval(['set(handles.hilim',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.hilim',num2str(ii),',''String'','' '');'])
    eval(['set(handles.hilim',num2str(ii),',''Visible'',''off'');'])
    eval(['set(handles.fitval',num2str(ii),',''Enable'',''off'');'])
    eval(['set(handles.fitval',num2str(ii),',''String'','' '');'])
    eval(['set(handles.fitval',num2str(ii),',''Visible'',''off'');'])
end


function fitpar=read_gui_vals(handles)

fitfilenum=get(handles.FittingFilenamePopup,'Value');
fitnamelist=get(handles.FittingFilenamePopup,'String');
fitpar.name=fitnamelist{fitfilenum};
fitpar.binning=2^(get(handles.BinningPopUp','Value')-1);
fitpar.incl_resolution=get(handles.calcres,'Value'); % this is not implemented yet (1/1-2007)
for ii=1:9 
    eval(['if strcmp(get(handles.par',num2str(ii),',''Enable''),''on'');'...
            'fitpar.fit(ii)=get(handles.fit',num2str(ii),',''Value'');'...
            'if ~isempty(str2num(get(handles.startval',num2str(ii),',''String'')));'...
                'fitpar.start(ii)=str2num(get(handles.startval',num2str(ii),',''String''));'...
            'else;'...
                'fitpar.start(ii)=NaN;'... 
            'end;'...
            'if ~isempty(str2num(get(handles.lowlim',num2str(ii),',''String'')));'...
                'fitpar.ll(ii)=str2num(get(handles.lowlim',num2str(ii),',''String''));'...
            'else;'...
                'fitpar.ll(ii)=-Inf;'...
            'end;'...
            'if ~isempty(str2num(get(handles.hilim',num2str(ii),',''String'')));'...
                'fitpar.ul(ii)=str2num(get(handles.hilim',num2str(ii),',''String''));'...
            'else;'...
                'fitpar.ul(ii)=Inf;'...
            'end;'...
            'if ~isempty(get(handles.par',num2str(ii),',''String''));'...
                 'fitpar.param_name{ii}=get(handles.par',num2str(ii),',''String'');'...
            'else;'...
                'fitpar.param_name{ii}=[''p',num2str(ii),'''];'...
            'end;'...
          'end;'])
end

fitpar.numiter=str2num(get(handles.num_of_iter,'String'));
if isempty(fitpar.numiter); fitpar.numiter=100; end
fitpar.tol=str2num(get(handles.Tolerance,'String'));
if isempty(fitpar.tol); fitpar.tol=1e-4; end
fitpar.graphics=get(handles.Graphics_on,'Value');

%************************************************
function update_gui_startval(handles,fitpar)

numparams=length(fitpar.start);

%now set the fields according to the how many parameters in functionnum
for ii=1:numparams
    eval(['set(handles.startval',num2str(ii),',''Enable'',''on'');'])
    eval(['set(handles.startval',num2str(ii),',''String'',num2str(fitpar.start(',num2str(ii),'),''%4.2e''));'])
    eval(['set(handles.startval',num2str(ii),',''Visible'',''on'');'])
end

%************************************************
function update_gui_fitted(handles,Fitparam,Fitval)

numparams=length(Fitparam);

%now set the fields according to the how many parameters in functionnum
for ii=1:numparams
    eval(['set(handles.fitval',num2str(ii),',''Enable'',''on'');'])
    eval(['set(handles.fitval',num2str(ii),',''String'',num2str(Fitparam(',num2str(ii),'),''%5.3e''));'])
    eval(['set(handles.fitval',num2str(ii),',''Visible'',''on'');'])
end

set(handles.chisqval,'String',num2str(Fitval,3))
set(handles.refinefit,'Enable','on')

%************************************************
function update_gui_fit_to_start(handles)

fitfilenum=get(handles.FittingFilenamePopup,'Value');
numparams=handles.fitfunctions.oneD.numparams{fitfilenum};

%now set the fields 
for ii=1:numparams
    eval(['a=get(handles.fitval',num2str(ii),',''String'');'])
    eval(['set(handles.startval',num2str(ii),',''String'',a);'])
end

function startval_input_check(handles,ii)
fitfilenum=get(handles.FittingFilenamePopup,'Value');
if handles.fitfunctions.oneD.param_bool{fitfilenum}{ii}
    txt=['if isempty(str2num(get(handles.startval',num2str(ii),',''String'')));'...
              'errordlg(''StartVal for Boolean Variable must be 0 or 1 !'',''Input Error'');'...
              'set(handles.startval',num2str(ii),',''String'',''0'');'...
              'return;'...
        'end;' ];
    eval(txt)
    txt=['if str2num(get(handles.startval',num2str(ii),',''String''))~=1 &&'...
              'str2num(get(handles.startval',num2str(ii),',''String''))~=0;'...
            'errordlg(''StartVal for Boolean Variable must be 0 or 1 !'',''Input Error'');'...
        'set(handles.startval',num2str(ii),',''String'',''0'');'...
        'return;'...
        'end;' ];
    eval(txt)
    txt=['set(handles.hilim',num2str(ii),',''String'',get(handles.startval',num2str(ii),',''String''));'];
    eval(txt)
    txt=['set(handles.lowlim',num2str(ii),',''String'',get(handles.startval',num2str(ii),',''String''));'];
    eval(txt)
else
    txt=['if isempty(str2num(get(handles.startval',num2str(ii),',''String'')));'...
        'errordlg(''StartVal must be a number!'',''Input Error'');'...
        'set(handles.startval',num2str(ii),',''String'','' '');'...
        'end;' ];
    eval(txt)
    return
end


function lowlim_input_check(handles,ii)

txt=['if isempty(str2num(get(handles.lowlim',num2str(ii),',''String'')));'...
         'errordlg(''LowLim must be real number or -Inf'',''Input Error'');'...
         'set(handles.lowlim',num2str(ii),',''String'',''-Inf'');'...
     'end;'...
     'if strcmp(get(handles.lowlim',num2str(ii),',''String''),''Inf'')'...
              ' || strcmp(get(handles.lowlim',num2str(ii),',''String''),''+Inf'');'... 
         'errordlg(''LowLim cannot be +Inf'',''Input Error'');'...
         'set(handles.lowlim',num2str(ii),',''String'',''-Inf'');'...
     'end;'];
eval(txt)

function hilim_input_check(handles,ii)

txt=['if isempty(str2num(get(handles.hilim',num2str(ii),',''String'')));'...
         'errordlg(''HiLim must be real number or +Inf'',''Input Error'');'...
         'set(handles.hilim',num2str(ii),',''String'',''+Inf'');'...
     'end;'...
     'if strcmp(get(handles.hilim',num2str(ii),',''String''),''-Inf'')'...
         'errordlg(''HiLim cannot be -Inf'',''Input Error'');'...
         'set(handles.hilim',num2str(ii),',''String'',''+Inf'');'...
     'end;'];
eval(txt)



function lowlim1_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,1)

function lowlim1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim2_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,2)

function lowlim2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim3_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,3)

function lowlim3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim4_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,4)

function lowlim4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim5_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,5)

function lowlim5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim6_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,6)

function lowlim6_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim7_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,7)

function lowlim7_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim8_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,8)

function lowlim8_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlim9_Callback(hObject, eventdata, handles)
lowlim_input_check(handles,9)

function lowlim9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim1_Callback(hObject, eventdata, handles)
hilim_input_check(handles,1)

function hilim1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hilim2_Callback(hObject, eventdata, handles)
hilim_input_check(handles,2)

function hilim2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim3_Callback(hObject, eventdata, handles)
hilim_input_check(handles,3)

function hilim3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hilim4_Callback(hObject, eventdata, handles)
hilim_input_check(handles,4)

function hilim4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim5_Callback(hObject, eventdata, handles)
hilim_input_check(handles,5)

function hilim5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim6_Callback(hObject, eventdata, handles)
hilim_input_check(handles,6)

function hilim6_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim7_Callback(hObject, eventdata, handles)
hilim_input_check(handles,7)

function hilim7_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim8_Callback(hObject, eventdata, handles)
hilim_input_check(handles,8)

function hilim8_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function hilim9_Callback(hObject, eventdata, handles)
hilim_input_check(handles,9)


function hilim9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in calcres.
function calcres_Callback(hObject, eventdata, handles)
% hObject    handle to calcres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of calcres



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function lowlim82_CreateFcn(a,b,c)
function edit51_CreateFcn(a,b,c)
function edit53_CreateFcn(a,b,c)
function edit54_CreateFcn(a,b,c)
function edit55_CreateFcn(a,b,c)
function edit56_CreateFcn(a,b,c)
function edit57_CreateFcn(a,b,c)



% --- Executes on selection change in weightingfunc.
function weightingfunc_Callback(hObject, eventdata, handles)
% hObject    handle to weightingfunc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns weightingfunc contents as cell array
%        contents{get(hObject,'Value')} returns selected item from weightingfunc


% --- Executes during object creation, after setting all properties.
function weightingfunc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to weightingfunc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function clearstatus
fig=findobj(get(0,'Children'),'Name','Fitting2D');
%delete any old status message.
hstatusold=findobj(fig,'Tag','statusline');
if ~isempty(hstatusold)
    delete(hstatusold)
end


