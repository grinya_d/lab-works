function y=normfun(x,x0,xsigma)
% Gives y-value of the normal curve with parameters
% 
% Format:
%         y=normfun(x,x0,xsigma)
% Parematers
% x        value of interest
% x0       center of curve
% xsigma   standard deviation of normal curve
%           equal to FWHM/2.35

y=1/sqrt(2*pi)*exp(-0.5*((x-x0)/xsigma).^2);
y=y/sum(sum(y));

