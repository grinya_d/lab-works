function varargout = kcal(varargin)
%INPUTFIELD1 Get K calibration data.
%   [K, PIXEL, OFFSET] = KCAL presents a dialog window to get K calibration
%   data.
%
%   KCAL(K, PIXEL, OFFSET) returns the same values but starts by showing
%   the values supplied.
%
%   KCAL requires you to fill in a numeric value for each parameter.
%   If you cancel or close the dialog, KCAL returns [] for all three
%   values.

%      KCAL, by itself, creates a new KCAL or raises the existing
%      singleton*.
%
%      H = KCAL returns the handle to a new KCAL or the handle to
%      the existing singleton*.
%
%      KCAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KCAL.M with the given input arguments.
%
%      KCAL('Property','Value',...) creates a new KCAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before inputfield1_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to inputfield1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

%   Jun 3, 2006 Incorporated possibility to enter values by geometry
%   using the following structures
%   tempcal which holds temporary calibration values as they are typed in
%   oldcal which holds the calibration values that were last accepted
%
%   please note that if geometrical calibration is chosen then the rest of 
%   saxsgui will recalculate the k, pixelal and koffset values for the 
%   pseudo calibration line
%   but because of tempcal and oldcal the old values will be remembered
%   and plotted here.

% Last Modified by GUIDE v2.5 21-Jul-2007 02:04:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @kcal_OpeningFcn, ...
                   'gui_OutputFcn',  @kcal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%*******************************************************************
% --- Executes just before inputfield1 is made visible.
function kcal_OpeningFcn(hObject, eventdata, handles, varargin)
global tempcal oldcal wavelength_default
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to inputfield1 (see VARARGIN)
tempcal.varargin=varargin;
if length(varargin)~=7
    errordlg('kcal window requires 7 parameters')
    return
end
        
%first determine if existing values were calibrated by geometry
% i.e. if detector_dist is empty
% also set the respective structures 
if strcmp(varargin{7},'geom')
    tempcal.type='geom';
    % the following is required since the rest of saxsgui has
    % worked on pseudo standard calibration, which is 
    % not what we want to appear in the table
    if ~isempty(oldcal)
        tempcal.k=oldcal.k;
        tempcal.pixelcal=oldcal.pixelcal;
        tempcal.koffset=oldcal.koffset;
        tempcal.wavelength=oldcal.wavelength;
    else
        tempcal.k=[];
        tempcal.pixelcal=[];
        tempcal.koffset=[];   
        tempcal.wavelength=wavelength_default;
    end
    tempcal.wavelength=varargin{4};
    tempcal.pixelsize=varargin{5};
    tempcal.detector_dist=varargin{6};
else
    tempcal.type='std';
    tempcal.k=varargin{1};
    tempcal.pixelcal=varargin{2};
    tempcal.koffset=varargin{3};
    tempcal.wavelength=varargin{4};
    if ~isempty(oldcal)
        tempcal.pixelsize=oldcal.pixelsize;
        tempcal.detector_dist=oldcal.detector_dist;
    else
        tempcal.pixelsize=[];
        tempcal.detector_dist=[];       
    end    
end
if isempty(tempcal.wavelength), tempcal.wavelength==wavelength_default; end



%update text according to tempcal
update_kcal_text(hObject,handles)
update_kcal_fields(hObject,handles)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes inputfield1 wait for user response (see UIRESUME)
uiwait(handles.figure1);

%*******************************************************************
% --- updates the handle values and the guidata
function update_kcal_text(hObject,handles)
global tempcal 
if strcmp(tempcal.type,'geom')
    set(handles.inputfield0text, 'Visible', 'off')
    set(handles.inputfield0unit, 'Visible', 'off')
    set(handles.inputfield0super, 'Visible', 'off')
    set(handles.inputfield0, 'Visible', 'off')
    set(handles.inputfield1text, 'String', 'Wavelength')
    set(handles.inputfield1unit, 'String', 'A') 
    set(handles.inputfield3text, 'String', 'Detector Dist.') 
    set(handles.inputfield3unit, 'String', 'mm')
    set(handles.inputfield3super, 'String', '') 
    set(handles.inputfield2atext, 'String', 'Size of pixel (X)')
    set(handles.inputfield2aunit, 'String', 'mm')
    set(handles.inputfield2btext, 'String', 'Size of pixel (Y)')
    set(handles.inputfield2bunit, 'String', 'mm')
    set(handles.headertext,'String',{'Enter the values for system geometry.',...
            'The size of pixel must be calibrated from elsewhere',...
            ''})
else
    set(handles.inputfield0text, 'Visible', 'on')
    set(handles.inputfield0unit, 'Visible', 'on')
    set(handles.inputfield0super, 'Visible', 'on')
    set(handles.inputfield0, 'Visible', 'on')
    set(handles.inputfield1text, 'String', 'Wavelength')
    set(handles.inputfield1unit, 'String', 'A')
    set(handles.inputfield3text, 'String', 'q-offset') 
    set(handles.inputfield3unit, 'String', 'A')
    set(handles.inputfield3super, 'String', '-1') 
    set(handles.inputfield2atext, 'String', 'Pixel radius (X)')
    set(handles.inputfield2aunit, 'String', 'pixels')
    set(handles.inputfield2btext, 'String', 'Pixel radius (Y)')
    set(handles.inputfield2bunit, 'String', 'pixels')
    set(handles.headertext,'String',{'Enter the wavelength, the q-value for your calibration line,',...
            'and the pixel radius of your calibration line.',...
            'The q-offset is usually 0'})
end
% Update handles structure
guidata(hObject, handles);


%*******************************************************************
% --- updates the handle values and the guidata
function update_kcal_fields(hObject,handles)
global tempcal
if strcmp(tempcal.type,'geom')
    set(handles.BySystemGeometry,'Value',1)
    set(handles.ByStandardSample,'Value',0)
    set(handles.inputfield1, 'String', num2str(tempcal.wavelength))
    set(handles.inputfield3, 'String', num2str(tempcal.detector_dist)) 
    if length(tempcal.pixelsize)==2
        set(handles.inputfield2a, 'String', num2str(tempcal.pixelsize(1)))
        set(handles.inputfield2b, 'String', num2str(tempcal.pixelsize(2)))
    else
        set(handles.inputfield2a, 'String', num2str(tempcal.pixelsize))
        set(handles.inputfield2b, 'String', num2str(tempcal.pixelsize))
    end
else
    set(handles.BySystemGeometry,'Value',0)
    set(handles.ByStandardSample,'Value',1)
    set(handles.inputfield0, 'String', num2str(tempcal.k))
    set(handles.inputfield1, 'String', num2str(tempcal.wavelength))
    set(handles.inputfield3, 'String', num2str(tempcal.koffset)) 
    if length(tempcal.pixelcal)==2
        set(handles.inputfield2a, 'String', num2str(tempcal.pixelcal(1)))
        set(handles.inputfield2b, 'String', num2str(tempcal.pixelcal(2)))
    else
        set(handles.inputfield2a, 'String', num2str(tempcal.pixelcal))
        set(handles.inputfield2b, 'String', num2str(tempcal.pixelcal))
    end
end
% Update handles structure
guidata(hObject, handles);

%*******************************************************************
% --- updates tempcal
function update_tempcal(handles)
global tempcal
%tempcal is updated 
if get(handles.ByStandardSample,'Value')==0 % by System Geometry
    tempcal.type='geom';
    tempcal.wavelength=str2num(get(handles.inputfield1,'String'));
    tempcal.pixelsize(1)=str2num(get(handles.inputfield2a,'String'));
    tempcal.pixelsize(2)=str2num(get(handles.inputfield2b,'String'));
    tempcal.detector_dist=str2num(get(handles.inputfield3,'String'));
else
    tempcal.type='std';
    tempcal.k=str2num(get(handles.inputfield0,'String'));
    tempcal.wavelength=str2num(get(handles.inputfield1,'String'));
    tempcal.pixelcal(1)=str2num(get(handles.inputfield2a,'String'));
    tempcal.pixelcal(2)=str2num(get(handles.inputfield2b,'String'));
    tempcal.koffset=str2num(get(handles.inputfield3,'String'));
end



% --- Outputs from this function are returned to the command line.
function varargout = kcal_OutputFcn(hObject, eventdata, handles)
global tempcal oldcal
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



oldcal=tempcal;

if isstruct(handles)  % figure still around -> user pressed OK
    update_tempcal(handles)
    oldcal=tempcal;
    varargout{1} = oldcal.k;
    varargout{2} = oldcal.pixelcal;
    varargout{3} = oldcal.koffset;
    varargout{4} = oldcal.wavelength;
    varargout{5} = oldcal.pixelsize;
    varargout {6} = oldcal.detector_dist;
    varargout {7} = oldcal.type;
        
    delete(handles.figure1)  % delete it now
else  % user cancelled or closed dialog
    varargout=tempcal.varargin;
end

% --- Executes during object creation, after setting all properties.
function inputfield2a_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputfield2a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function inputfield2a_Callback(hObject, eventdata, handles)
% hObject    handle to inputfield2a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputfield2a as text
%        str2double(get(hObject,'String')) returns contents of inputfield2a as a double
global tempcal
if get(handles.ByStandardSample,'Value')==0 % by System Geometry
    tempcal.pixelsize(1)=str2num(get(handles.inputfield2a,'String'));
else
    tempcal.pixelcal(1)=str2num(get(handles.inputfield2a,'String'));
end


% --- Executes during object creation, after setting all properties.
function inputfield3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputfield3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function inputfield3_Callback(hObject, eventdata, handles)
% hObject    handle to inputfield3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputfield3 as text
%        str2double(get(hObject,'String')) returns contents of inputfield3 as a double
global tempcal
if get(handles.ByStandardSample,'Value')==0 % by System Geometry
    tempcal.detector_dist=str2num(get(handles.inputfield3,'String'));
else
    tempcal.koffset=str2num(get(handles.inputfield3,'String'));
end

% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if inputsvalid(handles)
    uiresume(handles.figure1)
else
    errordlg('Enter a number for each parameter.', 'Invalid parameters', 'modal')
end


function isvalid = inputsvalid(handles)
if get(handles.BySystemGeometry,'Value')==1
    firstfieldisvalid=1;
else
    firstfieldisvalid= ~isnan(str2double(get(handles.inputfield0,'String')));
end    
isvalid = ~ (isnan(str2double(get(handles.inputfield1,'String'))) || ...
             isnan(str2double(get(handles.inputfield2a,'String'))) || ...
             isnan(str2double(get(handles.inputfield2b,'String'))) || ...
             isnan(str2double(get(handles.inputfield3,'String')))) && ...
            firstfieldisvalid;

    

% --- Executes on button press in Cancel.
function Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to Cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


close(handles.figure1)  % this is how we abort--see kcal_OutputFcn above


% --- Executes on button press in fromfile.
function fromfile_Callback(hObject, eventdata, handles)
% hObject    handle to fromfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file dir] = uigetfile('*.mat', 'Get calibration from image file');
if file
    [k pixx pixy off] = calfrom([dir file]);
    if k
        set(handles.inputfield0, 'String', num2str(k))
        set(handles.inputfield1, 'String', num2str(wavelength))
        set(handles.inputfield2a, 'String', num2str(pixx))
        set(handles.inputfield2b, 'String', num2str(pixy))
        set(handles.inputfield3, 'String', num2str(off))
    end
end


function [k, pixx, pixy, off] = calfrom(path)
k = [];
pixx = [];
pixy = [];
off = [];
f = load(path);
% Take the first SAXS object we find.  That covers simple saved SAXS
% variables as well as SAXSGUI saves.
names = fieldnames(f);
for iname = 1 : length(names)
    obj = eval(['f.', names{iname}]);
    if isa(obj, 'saxs')
        k = obj.kcal;
        if length(obj.pixelcal==2) 
            pixx = obj.pixelcal(1);
            pixy = obj.pixelcal(2);
        else
            pixx = obj.pixelcal(1);
            pixy = pixx;
        end
        off = obj.koffset;
        wavelength=obj.wavelength;
    end
end


% --- Executes during object creation, after setting all properties.
function inputfield2b_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputfield2b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function inputfield2b_Callback(hObject, eventdata, handles)
% hObject    handle to inputfield2b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputfield2b as text
%        str2double(get(hObject,'String')) returns contents of inputfield2b as a double
global tempcal
if get(handles.ByStandardSample,'Value')==0 % by System Geometry
    tempcal.pixelsize(2)=str2num(get(handles.inputfield2b,'String'));
else
    tempcal.pixelcal(2)=str2num(get(handles.inputfield2b,'String'));
end

% --- Executes on button press in ByStandardSample.
function ByStandardSample_Callback(hObject, eventdata, handles)
% hObject    handle to ByStandardSample (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ByStandardSample
global tempcal
tempcal.type='std';
%update text according to tempcal

update_kcal_text(hObject,handles)
update_kcal_fields(hObject,handles)

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in BySystemGeometry.
function BySystemGeometry_Callback(hObject, eventdata, handles)
% hObject    handle to BySystemGeometry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of BySystemGeometry
global tempcal
tempcal.type='geom';
%update text according to tempcal
update_kcal_text(hObject,handles)
update_kcal_fields(hObject,handles)

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function inputfield1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputfield1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function inputfield1_Callback(hObject, eventdata, handles)
% hObject    handle to inputfield1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputfield1 as text
%        str2double(get(hObject,'String')) returns contents of inputfield1 as a double
global tempcal
if get(handles.ByStandardSample,'Value')==0 % by System Geometry
    tempcal.wavelength=str2num(get(handles.inputfield1,'String'));
else
    tempcal.wavelength=str2num(get(handles.inputfield1,'String'));
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function inputfield0_Callback(hObject, eventdata, handles)
% hObject    handle to inputfield0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputfield0 as text
%        str2double(get(hObject,'String')) returns contents of inputfield0 as a double
global tempcal
if get(handles.ByStandardSample,'Value')==0 % by System Geometry
    tempcal.wavelength=str2num(get(handles.inputfield0,'String'));
else
    tempcal.k=str2num(get(handles.inputfield0,'String'));
end

% --- Executes during object creation, after setting all properties.
function inputfield0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputfield0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


