%COPYING Copyright statement for MOLMET programs.
%   Molecular Metrology, Inc. retains copying rights to these programs.  
%   If you obtained these programs with the approval of Molecular
%   Metrology, Inc., you may make unlimited copies for your own use.  
%   You must obtain permission from Molecular Metrology, Inc. before
%   redistributing these programs, modified versions of these programs, 
%   or programs which incorporate part or all of these programs.
%   Request such permissions by e-mail to barton@molmet.com.
