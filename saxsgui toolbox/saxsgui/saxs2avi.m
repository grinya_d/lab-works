function saxs2avi(varargin)
% saxs2avi converts a list of 2D saxs images to a movie
%       saxs2avi; loads saxsdata files, displays related images, creates frames
%       and compiles frames to an avi-file
%

image_process = [];
if (length(varargin) >1 )
    strcat(image_process,varargin(1))
end
    
[path,files]=getfiles('Choose the data files');
if path==0 
    warndlg('No files chosen. Activity exited.')
    return
end

[outname,outpath]=uiputfile('*.avi','Choose the output filename');
if outpath==0 
    warndlg('Output directory can not be found. Activity exited.')
    return
end
outfile=fullfile(outpath,outname);
for ii=1:length(files)
    s=sprintf('Printing file %f of %f',ii,length(files));
    disp(s)
    pathname=fullfile(path,files{ii});
    A=getsaxs(pathname);
    if strcmp(image_process,'smooth')
        A=smooth(A,10,3);
    end
    image(log(A))
    title([pathname,' ',A.raw_date])
    F(ii)=getframe;
end

movie2avi(F,outfile)

