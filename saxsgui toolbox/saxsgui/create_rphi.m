function[R,phi]=create_rphi(M,rowcen,colcen,rowcal,colcal)
% Example
% [R,phi]=create_rphi(M,rowcen,colcen,rowcal,colcal)
%
% This function is used an intermediarey function in
% creating matrices for use in the azimuthal and annular
% averaging functions used for SAXS data analysis
% takes a given matrix and produces 
% 
% Input parameters are
%		M 			the input matrix
%		rowcen	the center row
%		colcen	the center column
%		rowcal	the calibrated R-value of the row (fx. 85 mu/pixel)
% 		colcal   the calibrated R-value of the column
% Output parameters are
%		R			the output matrix given the 
%					"distance from the center" for every pixel
%		phi		the angle (0,360 degrees) from the horizontal vector
% please note that MATLAB format is (Row,Column)

[rowmax,colmax]=size(M);
[col,row]=meshgrid(1:colmax,1:rowmax);

R=sqrt(((col-colcen)*colcal).^2+((row-rowcen)*rowcal).^2);
phi=-atan2((row-rowcen),-(col-colcen))/(2*pi)+0.5;
%qx=R.*cos(phi*360);
%qy=R.*sin(phi*360);
%mesh(qx,qy,M);
phi=phi*360;
