function xyzwrite(path, table, format)
%XYZWRITE Write an XYZ table to a file.
%   XYZWRITE('path', T, 'fmt') writes an XYZ table array T (see XYZTABLE) to
%   an ASCII file at 'path' (directory and file name).  'fmt' specifies a
%   C-style format string for each number in the table.  You may omit
%   'fmt', in which case XYZWRITE uses '%.1f', printing one digit to the
%   right of the decimal point.
%
%   Note that XYZWRITE prints an empty string for the first element in T,
%   which is unused (stored as 0) in an XYZ table.
%
%   See also:  XYZTABLE; FPRINTF.

% Oh, why not check arguments.
if nargin < 2 || ~ ischar(path) || ~ (ndims(table) == 2)
    error('I need a filename string and an XYZ table.')
end  % We think path and table are okay.

if nargin > 2
    if ~ ischar(format)
        error('The ''format'' argument must be a character string.')
    end  % We think format is okay.
else
    format = '%.1f';  % default format
end

xyzwrite_really(path, table, format)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xyzwrite_really(path, table, format)
file = fopen(path, 'wt');
if isequal(file, -1)
    error(['I cannot open the file ', path, ' for writing.'])
end  % We have an open file.
% The first element is always empty.
fprintf(file, ',');
% First row:
fprintf(file, [format, ','], table(1, 2 : end - 1));
fprintf(file, [format, '\n'], table(1, end));
% Subsequent rows:
for row = 2 : size(table, 1)
    fprintf(file, [format, ','], table(row, 1 : end - 1));
    fprintf(file, [format, '\n'], table(row, end));
end
fclose(file);
