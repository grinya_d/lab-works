function outfile = writeradfile(fileformat,filename,comment1,comment2,X,Y,E,DX)
%RADWRITEFILE Write 1D SAXS data to an ascii file.
%   RADWRITELINE(fileformat, filename, comment1, comment2, X, Y, E, DX) 
%   writes the data points to the file
%   Possible file formats are
%   'RAD' Ris� .rad format 
%   'PDH' Glatter .pdh format
%   'CSVXY' Comma-separated variables X,Y
%   'CSVXYdY' Comma-separated variables X,Y,dY
%   'CSVXYdYdX' Comma-separated variables X,Y,dY,dX

[tmp1,tmp2,tmp3]=fileparts(filename);

if strcmp(fileformat,'RAD')
    filename=[tmp1,filesep,tmp2,'.rad'];
    outfile = fopen(filename,'wt');
    fprintf(outfile,'%s\n',comment1);
    fprintf(outfile,'%s\n',comment2);
    fprintf(outfile,'%9d\n',length(X));
    for ii=1:length(X)
        fprintf(outfile,'%14.6e %14.6e %14.6e %14.6e\n',X(ii),Y(ii),E(ii),DX(ii));
    end
end

if strcmp(fileformat,'PDH')
    filename=[tmp1,filesep,tmp2,'.pdh'];
    outfile = fopen(filename,'wt');
    fprintf(outfile,'%s\n',comment1);
    fprintf(outfile,'SAXS\n');
    fprintf(outfile,'%9d %9d %9d %9d %9d %9d %9d %9d \n',[length(X),0,0,0,0,0,0,0]);
    fprintf(outfile,'%14.6e %14.6e %14.6e %14.6e %14.6e \n',[0,0,0,1,0]);
    fprintf(outfile,'%14.6e %14.6e %14.6e %14.6e %14.6e \n',[0,0,0,0,0]);
    for ii=1:length(X)
        fprintf(outfile,'%14.6e %14.6e %14.6e \n',X(ii),Y(ii),E(ii));
    end
end

if strcmp(fileformat,'CSVXY')
    filename=[tmp1,filesep,tmp2,'.csv'];
    outfile = fopen(filename,'wt');
    fprintf(outfile,'%s\n',comment1);
    fprintf(outfile,'X,Y\n');
    for ii=1:length(X)
        fprintf(outfile,'%14.6e,%14.6e \n',X(ii),Y(ii));
    end
end

if strcmp(fileformat,'CSVXYdY')
    filename=[tmp1,filesep,tmp2,'.csv'];
    outfile = fopen(filename,'wt');
    fprintf(outfile,'%s\n',comment1);
    fprintf(outfile,'X,Y,dY\n');
    for ii=1:length(X)
        fprintf(outfile,'%14.6e,%14.6e.%14.6e \n',X(ii),Y(ii),E(ii));
    end
end

if strcmp(fileformat,'CSVXYdYdX')
    filename=[tmp1,filesep,tmp2,'.csv'];
    outfile = fopen(filename,'wt');
    fprintf(outfile,'%s\n',comment1);
    fprintf(outfile,'X,Y,dY,dX\n');
    for ii=1:length(X)
        fprintf(outfile,'%14.6e %14.6e %14.6e \n',X(ii),Y(ii),E(ii),DX(ii));
    end
end

try 
    fclose(outfile);
catch
    errordlg('Desired fileformat not recognized')
end


