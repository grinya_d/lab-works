function [im_unzing] = zinger_removal2(im,error_norm_fact)
% function to take out zingers using high-frequency filter
% basic idea stolen from Jon Almer 2003.

bas=[1 0 1;0 0 0;1 0 1];
if nargin==1
    error_norm_fact=1;
end
ave=conv2(im,bas,'same');

[i j]=find(im>2000);
im_unzing=im;
for index=1:1:size(i,1); 
    im_unzing(i(index),j(index))=ave(i(index),j(index)); 
end

