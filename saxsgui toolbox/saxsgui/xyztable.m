function table = xyztable(X, Y, Z)
%XYZTABLE Convert a 2-D array to an XYZ table.
%   XYZTABLE(X, Y, Z) where Z is a 2-D array, and X and Y are vectors
%   containing a corresponding number of values for columns and rows
%   of Z.
%
%   Output elements are arranged in an array like this:
%
%   0       Y1      Y2      ...
%   X1      Z1,1    Z1,2    ...
%   X2      Z2,1    Z2,2    ...
%   ...
%
%   Note the first element of the resulting array is zero.
%   The final array size should be one row and one column larger than the
%   size of the input array Z.
%
%   See also:  XYZWRITE.

% Check input arguments.
if nargin ~= 3
    error('I require three input arguments.')
end
if ~ (ndims(Z) == 2)
    error('First argument must be a 2-D array.')
end
if ~ (length(X) == size(Z, 2) && length(Y) == size(Z, 1))
    error('Arguments 2 and 3 must correspond with columns and rows of argument 1.')
end

% Silly me.  I don't confirm that X and Y are vectors, not arrays.

table = [X(:), Z'];
table = [[0; table(:, 1)], [Y(:)'; table(:, 2 : end)]];
