function varargout = reduction_parameters(varargin)
% REDUCTION_PARAMETERS M-file for reduction_parameters.fig
%      REDUCTION_PARAMETERS, by itself, creates a new REDUCTION_PARAMETERS or raises the existing
%      singleton*.
%
%      H = REDUCTION_PARAMETERS returns the handle to a new REDUCTION_PARAMETERS or the handle to
%      the existing singleton*.
%
%      REDUCTION_PARAMETERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REDUCTION_PARAMETERS.M with the given input arguments.
%
%      REDUCTION_PARAMETERS('Property','Value',...) creates a new REDUCTION_PARAMETERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before reduction_parameters_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to reduction_parameters_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help reduction_parameters

% Last Modified by GUIDE v2.5 26-Jan-2009 17:20:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @reduction_parameters_OpeningFcn, ...
                   'gui_OutputFcn',  @reduction_parameters_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before reduction_parameters is made visible.
function reduction_parameters_OpeningFcn(hObject, eventdata, handles, varargin)
global reduc_par_temp
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to reduction_parameters (see VARARGIN)

set(hObject,'Name','Reduction Parameter Panel')
reduc_par_temp=reduct_par_init; % gets initial values for GUI
reduct_par_GUIupdate(handles); %updates GUI

% we'll store the old saxs structure on the cancel button "UserData"
% and the revised saxs structure on the apply button 
set(handles.cancel,'UserData',varargin{1})
set(handles.ApplyCorrections,'UserData',varargin{1})


% Choose default command line output for reduction_parameters
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes reduction_parameters wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = reduction_parameters_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout = {get(handles.ApplyCorrections,'UserData')};

% --- Executes on button press in sample_check.
function sample_check_Callback(hObject, eventdata, handles)
% hObject    handle to sample_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of sample_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in mask_check.
function mask_check_Callback(hObject, eventdata, handles)
% hObject    handle to mask_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mask_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in flatfield_check.
function flatfield_check_Callback(hObject, eventdata, handles)
% hObject    handle to flatfield_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flatfield_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in empty_check.
function empty_check_Callback(hObject, eventdata, handles)
% hObject    handle to empty_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of empty_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in dark_check.
function dark_check_Callback(hObject, eventdata, handles)
% hObject    handle to dark_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dark_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in readout_check.
function readout_check_Callback(hObject, eventdata, handles)
% hObject    handle to readout_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of readout_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in absolute_check.
function absolute_check_Callback(hObject, eventdata, handles)
% hObject    handle to absolute_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of absolute_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function sample_trans_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function sample_trans_Callback(hObject, eventdata, handles)
% hObject    handle to sample_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_trans as text
%        str2double(get(hObject,'String')) returns contents of sample_trans as a double
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function empty_trans_CreateFcn(hObject, eventdata, handles)
% hObject    handle to empty_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function empty_trans_Callback(hObject, eventdata, handles)
% hObject    handle to empty_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of empty_trans as text
%        str2double(get(hObject,'String')) returns contents of empty_trans as a double
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function abs_int_factor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to abs_int_factor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function abs_int_factor_Callback(hObject, eventdata, handles)
% hObject    handle to abs_int_factor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of abs_int_factor as text
%        str2double(get(hObject,'String')) returns contents of abs_int_factor as a double
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure



% --- Executes during object creation, after setting all properties.
function sample_thickness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_thickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function sample_thickness_Callback(hObject, eventdata, handles)
% hObject    handle to sample_thickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_thickness as text
%        str2double(get(hObject,'String')) returns contents of sample_thickness as a double
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in maskfile_get.
function maskfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to maskfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.maskname,'UserData');
if exist(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mat', 'Pick a mask-file');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.maskname,'UserData',fullfilename);
    set(handles.maskname,'String',filename);
    reduct_par_struct_update(handles) %updates the structure based on the input in GUI
    reduct_par_GUIupdate(handles) %updates the GUI based on the structure
end
% --- Executes on button press in flatfieldfile_get.
function flatfieldfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to flatfieldfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.flatfieldname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm;', 'Pick a flatfield-file');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.flatfieldname,'UserData',fullfilename);
    set(handles.flatfieldname,'String',filename);
    reduct_par_struct_update(handles) %updates the structure based on the input in GUI
    reduct_par_GUIupdate(handles) %updates the GUI based on the structure
end
% --- Executes on button press in emptyfile_get.
function emptyfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to emptyfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.emptyname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm', 'Pick the "empty"-file');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.emptyname,'UserData',fullfilename);
    set(handles.emptyname,'String',filename);
    reduct_par_struct_update(handles) %updates the structure based on the input in GUI
    reduct_par_GUIupdate(handles) %updates the GUI based on the structure
end
% --- Executes on button press in darkcurrentfile_get.
function darkcurrentfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to darkcurrentfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.darkcurrentname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm', 'Pick the "dark current"-file');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.darkcurrentname,'UserData',fullfilename);
    set(handles.darkcurrentname,'String',filename);
    reduct_par_struct_update(handles) %updates the structure based on the input in GUI
    reduct_par_GUIupdate(handles) %updates the GUI based on the structure
end

% --- Executes on button press in readoutnoisefile_get.
function readoutnoisefile_get_Callback(hObject, eventdata, handles)
% hObject    handle to darkcurrentfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.readoutnoisename,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm', 'Pick the "dark current"-file');
if isstr(filename) 
    fullfilename=fullfile(pathname,filename);
    set(handles.readoutnoisename,'UserData',fullfilename);
    set(handles.readoutnoisename,'String',filename);
    reduct_par_struct_update(handles) %updates the structure based on the input in GUI
    reduct_par_GUIupdate(handles) %updates the GUI based on the structure
end

% --- Executes on button press in DoneCorrections.
function DoneCorrections_Callback(hObject, eventdata, handles)
% hObject    handle to DoneCorrections (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SaxsguiWindowName;

close(gcf)

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SaxsguiWindowName;
saxsguiwindow=findobj(get(0,'Children'), 'Name', SaxsguiWindowName);
gui=guidata(saxsguiwindow); % here we are getting the handle structure for the saxsgui window
%here we are changing the value of the rth data. This data was passed to
%the reduction parameters window and the various reduction parameters have
%been edited..so it is time we update this infomration
gui.img.rth=get(handles.cancel,'UserData');
gui.img.xy=copy_reduction_parameters(gui.img.xy,gui.img.rth);
gui.img.xy=insert_saxs_pixels(gui.img.xy,gui.img.xy_prereduction_pixels); %using the original data 
% and send it back to the original window.
guidata(saxsguiwindow, gui)
close(gcf)
% since this does not automatically update the image, we also need to do
% that specifically from here.
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
%   delete(findobj(gui.figure, 'Type', 'image'))  % delete all our images
    init_colorscale(gui)
    gui = makepolar(gui);
    images(gui);
    guidata(gui.figure, gui)
end



% --- Executes on button press in ApplyCorrections.
function ApplyCorrections_Callback(hObject, eventdata, handles)
% hObject    handle to ApplyCorrections (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SaxsguiWindowName

s=get(handles.ApplyCorrections,'UserData');
s=apply_reduction_parameters(s,handles);
set(handles.ApplyCorrections,'UserData',s); %local storage
saxsguiwindow=findobj(get(0,'Children'), 'Name', SaxsguiWindowName);
gui=guidata(saxsguiwindow); % here we are getting the handle structure for the saxsgui window

%if the "interactive reduction" is checked we must apply the corrections
%and so we communicate this through the variable reduction_state
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
    %find the appropriate reduction state and put it in the structure
    s=reduction_state2D(s,handles);
else
    s=reduction_state(s); %the lack of a second argument indicates an empty reduction_state
end

%here we are changing the value of the rth data. This data was passed to
%the reduction parameters window and the various reduction parameters have
%been edited..so it is time we update this information (it is not yet
%reduced)
gui.img.rth=s;
gui.img.xy=copy_reduction_parameters(gui.img.xy,gui.img.rth);
gui.img.xy=insert_saxs_pixels(gui.img.xy,gui.img.xy_prereduction_pixels); %using the original data 
%that has not been reduced (but maybe smoothed and rotated)
% now we reduce it, if it is required.
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
    gui.img.xy=pix2pix_red(gui.img.xy);
end
% and send it back to the original window.
guidata(saxsguiwindow, gui)
% since this does not automatically update the image, we also need to do
% that specifically from here.
if isequal(get(gui.MenuReduction2D, 'Checked'), 'on')
%   delete(findobj(gui.figure, 'Type', 'image'))  % delete all our images
    init_colorscale(gui)
    gui = makepolar(gui);
    images(gui);
    guidata(gui.figure, gui)
end


% --- Is a common function called by the load_config buttons
function load_config(ii)
global reduc_par_temp
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tmp=reduc_par_temp.saved(ii); %putting the configuration ii into tmp
tmp=rmfield(tmp,'saved'); % remove .saved configuration info
tmp=rmfield(tmp,'description'); 
tmp.saved=reduc_par_temp.saved; % adding the saved configurations to the structure
reduc_par_temp=tmp;



% --- Executes on button press in load_config1.
function load_config1_Callback(hObject, eventdata, handles)
% hObject    handle to load_config1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load_config(1)
reduct_par_GUIupdate(handles)

% --- Executes on button press in load_config2.
function load_config2_Callback(hObject, eventdata, handles)
% hObject    handle to load_config2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load_config(2)
reduct_par_GUIupdate(handles)

% --- Executes on button press in load_config3.
function load_config3_Callback(hObject, eventdata, handles)
% hObject    handle to load_config3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load_config(3)
reduct_par_GUIupdate(handles)

% --- Executes on button press in load_config4.
function load_config4_Callback(hObject, eventdata, handles)
% hObject    handle to load_config4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load_config(4)
reduct_par_GUIupdate(handles)

% --- Executes on button press in load_config5.
function load_config5_Callback(hObject, eventdata, handles)
% hObject    handle to load_config5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load_config(5)
reduct_par_GUIupdate(handles)

% --- Is a common function called by the save_config buttons
function save_config(ii)
global reduc_par_temp
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tmp=reduc_par_temp;
description=inputdlg('Enter a short description','Input for description');
tmp=rmfield(tmp,'saved'); % remove .saved configuration info
tmp.saved=1; 
if isempty(description)
    description={'No description'};
end  
tmp.description=description{1};

reduc_par_temp.saved(ii)=tmp;


% --- Executes on button press in save_config1.
function save_config1_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
save_config(1)
reduct_par_GUIupdate(handles)

% --- Executes on button press in save_config2.
function save_config2_Callback(hObject, eventdata, handles)
% hObject    handle to save_config2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
save_config(2)
reduct_par_GUIupdate(handles)

% --- Executes on button press in save_config3.
function save_config3_Callback(hObject, eventdata, handles)
% hObject    handle to save_config3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
save_config(3)
reduct_par_GUIupdate(handles)

% --- Executes on button press in save_config4.
function save_config4_Callback(hObject, eventdata, handles)
% hObject    handle to save_config4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
save_config(4)
reduct_par_GUIupdate(handles)

% --- Executes on button press in save_config5.
function save_config5_Callback(hObject, eventdata, handles)
% hObject    handle to save_config5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
save_config(5)
reduct_par_GUIupdate(handles)

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)

% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uiputfile('*.mat','Save reduction paramater file as','reduct.mat');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    save(fullfilename,'reduc_par_temp')
end

% --------------------------------------------------------------------
function LoadMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat','Load reduction parameter file ');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    eval(['load ''',fullfilename,''' reduc_par_temp'])
    reduct_par_GUIupdate(handles) %updates the GUI based on the structure
end


% --------------------------------------------------------------------
function CreateMaskMenu_Callback(hObject, eventdata, handles)
% hObject    handle to CreatMaskMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
maskimage=getsaxs;
%reduce NAN's to 0
masktemp=maskimage.pixels;
masktemp(isnan(maskimage.pixels))=0;
maskmakergui(masktemp)
%msgbox('If you created and saved a mask, you can enter now enter the filename')


% --------------------------------------------------------------------
function CalcAbsIntFactorMenu_Callback(hObject, eventdata, handles)
% hObject    handle to CalcAbsIntFactorMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in zinger_check.
function zinger_check_Callback(hObject, eventdata, handles)
% hObject    handle to zinger_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of zinger_check
reduct_par_struct_update(handles) %updates the structure based on the input in GUI
reduct_par_GUIupdate(handles) %updates the GUI based on the structure

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = images(gui_in)
% Draw images, including the color bar.
% Avoid depending on 'current' axes, since the user could be clicking
% around.
gui = gui_in;
guimage(gui, gui.xy_axes, gui.img.xy);
set(get(gui.xy_axes, 'Title'), 'String', gui.img.name, 'Interpreter', 'none')
guimage(gui, gui.rth_axes, gui.img.rth)
set(get(gui.rth_axes, 'Title'), 'String', 'Polar Transformation')
if ischecked(gui.MenuViewZoom)
    set(gui.MenuViewZoom, 'Checked', 'off')
end
% If we're looking at log display, hide the color scale controls.
colorcontrols = [gui.top_label, gui.top_text, gui.top_slider];
if ischecked(gui.MenuViewLog)
    set(colorcontrols, 'Visible', 'off')
else
    set(colorcontrols, 'Visible', 'on')
end
set(gui.rth_axes, 'CLim', get(gui.xy_axes, 'CLim'))  % use one color scale (xy)
gui = update_colorbar(gui);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function guimage(gui, haxes, sax, varargin)
% Put up an image of saxs object s in an axes.
% If sax, the saxs object, is empty, hide the axes.
% We take the log of the image or not, then pass arguments on to
% saxs/image.
% Keep current with View menu options (Grid, etc.) and color scale.
if isempty(sax)
    set(haxes, 'Visible', 'off')
else
    set(haxes, 'Visible', 'on')
    if isequal(get(gui.MenuViewLog, 'Checked'), 'on')
        hstatus = status('Plotting log image...', gui);
        himage = image(log10((sax)), 'Parent', haxes, varargin{1:end});
        delete(hstatus)
    else
        ctop = get(gui.top_slider, 'Value');
        himage = image(sax, 'Parent', haxes, varargin{:}, [0 ctop]);
        % Work around stupid bug where CLim specifier is ignored.
        set(haxes, 'CLim', [0 ctop]);
    end
    if ischecked(gui.MenuViewGrid)
        grid on
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLOR SCALE CONTROL CALLBACKS AND UTILITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_slider(slider, gui)
% Callback for slider controlling top of color scale.
% top_text calls this too.
top = fix(get(slider, 'Value'));
set(slider, 'Value', top)
set(gui.top_text, 'String', num2str(top))
set([gui.xy_axes, gui.rth_axes], 'CLim', [0 top])
gui = update_colorbar(gui);
guidata(gui.figure, gui)  % save gui data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_text(edit, gui)
% Callback for text edit box controlling top of color scale.
num = str2double(get(edit, 'String'));
if ~ isnan(num)  % we have a number
    slider = gui.top_slider;
    if num >= get(slider, 'Min') && num <= get(slider, 'Max')
        set(slider, 'Value', num)
    end
    top_slider(slider, gui)  % update everything
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = update_colorbar(gui_in)
% Because of technical difficulties with colorbar, it seems we must delete
% and recreate it.
gui = gui_in;
haxes=guihandles(gui.figure);
if ~isfield(haxes,'Colorbar')
    colorbar(gui.colorbar_axes, 'peer', gui.xy_axes)
    haxes=guihandles(gui.figure);
end
%cbpos = get(gui.colorbar_axes, 'Position');
%delete(gui.colorbar_axes)
%gui.colorbar_axes = axes('Position', cbpos);
%guidata(gui.figure, gui) % save info
%colorbar(gui.colorbar_axes, 'peer', gui.xy_axes)
colorlim=get(gui.xy_axes,'Clim');
set(haxes.Colorbar,'Xlim',colorlim);
set(haxes.TMW_COLORBAR,'XData',colorlim);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function checked = ischecked(menu)
% True if menu item is checked.
checked = isequal(get(menu, 'Checked'), 'on');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function init_colorscale(gui)
% Set up color scale controls for a new image.
top = max(gui.img.xy(:)+2);  % top of color scale
set(gui.top_text, 'String', num2str(top))
set(gui.top_slider, 'Min', 1, 'Max', top, 'Value', top)
set(gui.top_slider, 'SliderStep', [1 / (top - 1), 20 / (top - 1)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gui = makepolar(gui_in)
gui = gui_in;
hstatus = status('Transforming to polar image...', gui);
gui.img.rth = polar(gui.img.xy);
gui.img.xy=raw_center(gui.img.xy,gui.img.rth.raw_center);
delete(hstatus)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATUS BAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h_out = status(message, gui, seconds)
% Display a status line message, returning its handle.
% If you the caller don't specify SECONDS, then you must delete the status
% text control via its handle.
if nargin == 2
    h = figstatus(message, gui.figure);
elseif nargin == 3
    h = figstatusbrief(message, seconds, gui.figure);
end
if nargout
    h_out = h;
end


% --------------------------------------------------------------------
function ConstructFlatfield_Callback(hObject, eventdata, handles)
% hObject    handle to ConstructFlatfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
flatfieldmaker


