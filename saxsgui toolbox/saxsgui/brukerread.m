function s = brukerread(pathname)
%MPAREAD Read an image from an MPA file, returning a saxs image object.
%   SAXS_OBJECT = MPAREAD('pathname'); reads the file 'pathname' and attempts
%   to load a SAXS image, returning a saxs image object.
%
%   See also SAXS/SAXS, GETSAXS, SAXS/DISPLAY.
% Feb 25 ,2006 added option for reading binary MPA files

% Try to open the file for reading.
fid = fopen(pathname,'r');
if  fid== -1
    error('Could not open file.')
end

header = fread(fid,7680, 'uint8=>char');

%extract xdim in image 
lineno = 40;
xdim = str2double(header(lineno*80+9:(lineno+1)*80)');
%extract  ydim in image 
lineno = 41;
ydim = str2double(header(lineno*80+9:(lineno+1)*80)');


%read pixeldepth and set int type
lineno = 39;
pixdepth = str2double(header(lineno*80+9:(lineno+1)*80)');
switch int8(pixdepth);
    case 1
        bits = 'uint8';
    case 2
        bits = 'uint16';
    case 4
        bits = 'uint32';
end

%read image from file
frame =fread(fid,bits);
fclose(fid);

%To conserve file space Bruker uses an overflow format where a "few" pixels
%with overflow values are saved at the end of the file in binary format.
%16 bits are used to represent the data, where I believe 
% the first 9 bit are used to represent the pixel value
% the last 7 bits are used to represent the pixel number from 1 to
% xdim*ydim.
% If one transforms the 16 binary bits to characters (using char) 
% the values pop out...looking like this.
%         300 544282
% This has been tested on Bruker 1 bit data (which is common in SAXS)
% So the following should decode this format

overflowdata=frame(xdim*ydim+1+512:end); %the first 512 bits seem to be rubbish
overflowdata=overflowdata(overflowdata~=0); %remove extra padding of 0's
%sometime bruker saves something after the overflow data. We don't know
%what this is but we remove it by looking for it.
if ~isempty(overflowdata)
    %look for non integer values to signify end of overflow data
    tailhigh=find(uint8(overflowdata)>57);
    taillow=find(uint8(overflowdata)<32 );
    if isempty(tailhigh) && isempty(taillow)
        tailstart=length(overflowdata)+1;
    else
        tailstart=min([tailhigh;taillow]);
    end
    overflowdata=overflowdata(1:tailstart-1);
end
overflowchar=char(reshape(overflowdata(1:floor(length(overflowdata)/16)*16),16,length(overflowdata)/16))';

overflowval=str2num(overflowchar(:,1:9));
overflowpix=str2num(overflowchar(:,10:16));
frame(overflowpix)=overflowval;

% Extract real time.

lineno = 28;
realtime1 = str2double(header(lineno*80+9:(lineno+1)*80)');

% Extract live time.

lineno = 29;
livetime1 = str2double(header(lineno*80+9:(lineno+1)*80)');



% Extract starting date from header

lineno = 27;
filedate = str2double(header(lineno*80+9:(lineno+1)*80)');



% Reshape to a square array; get x and y right.
pixels = reshape(frame(1:xdim*ydim), xdim, ydim)';
    
    
    
% Return a saxs object.
s = saxs(pixels, ['Bruker SAXS file ''', pathname, ''' ', datestr(now)]);
s = type(s, 'xy');
s = realtime(s, realtime1);
s = livetime(s, livetime1);
s = datatype(s,'Bruker');
s = detectortype(s,'Bruker');
s = raw_filename(s, pathname);
s = raw_date(s,filedate);


