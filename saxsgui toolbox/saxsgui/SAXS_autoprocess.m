function varargout=SAXS_autoprocess(varargin)
%SAXS_AUTOPROCESS process (load,reduce, average and output) saxs data
%   SAXS_AUTOPROCESS is called from the Quik_Enter Menu
%
%   The SAXS_autoprocess performs the autoprocessing of SAXS data
%   following instructions given in the autoprocessing file
%   which contains all the instructions for the autoprocessing.
%
%   SAXS_AUTOPROCESS(autoprocess_filename,data_filename)
%   If called with no argument the program will request the name of an
%   autoprocessing file. The first argument is assumed to be the name of
%   the autorprocessfile.

persistent remember_apdir remember_fdir ap_struct
ap_file=[];
data_file=[];

if isempty(remember_fdir)
    remember_fdir = '';  % avoid warning from FULLFILE
end
if isempty(remember_apdir)
    remember_apdir = '';  % avoid warning from FULLFILE
end

if length(varargin) > 0
    ap_file=varargin{1};
end
if length(varargin) > 1
    data_file=varargin{2};
end
if ~exist(ap_file) || isempty(ap_file)
    [file dir] = uigetfile([fullfile(remember_apdir, '*.m'), ...
        ';*.txt;*.*;'], 'Find AutoProcessing File');
    if file  % we have a file path
        ap_file = fullfile(dir, file);
        remember_apdir = dir;  % look in this directory next time
    end
end

ap_struct=autoprocfileread(ap_file); %loads parameters from the autoprocess file
if ~isfield(ap_struct,'filename') ap_struct.filename=[]; end
if isempty(ap_struct)
    errordlg('Loading the configuration file failed--aborting')
    return
end

if ~isempty(data_file)
    im_file=data_file;
else
    if isempty(data_file) && exist(ap_struct.filename)
        im_file=ap_struct.filename;
    else
        % if no file is given then prompt for one
        dlgtitle='Input data file';
        [file dir] = uigetfile([fullfile(remember_fdir, '*.mpa'), ...
            ';*.mat;*.tif;*.tiff;*.img;*.inf;*.gfrm;*.unw;*.cmb;*.edf,*.lst'], dlgtitle);
        if file  % we have a file path
            im_file = fullfile(dir, file);
            remember_fdir = dir;  % look in this directory next time
        end
    end
end



S=getsaxs(im_file); %loads parameters from the datafile itself and
info_struct=mpainforead(im_file); %loads parameter from the info-dat file if it is there

%merge the values from the info file into those of the autoprocessing file 
%ap_struct values

S=apinfo_merge(S,info_struct,ap_struct); 

%begin to prepare for the averaging (as much as like averagex.m)
az=1;
if ap_struct.I_vs_q==0, az=0; end
bounds=[ap_struct.qstart ap_struct.phistart ap_struct.qend ap_struct.phiend];
if strcmp(ap_struct.reduction_type,'p') 
    S=reduction_type(S,'p');
else
    S=reduction_type(S,'s');
end

S=reduction_state_from_files(S)
npts=ap_struct.num_xpoints; 
if ap_struct.I_vs_q==1
    S=xaxisbintype(S,strcmp(ap_struct.xaxis_type,'log'));
    if strcmp(S.reduction_type,'s')
        profile = average_raw(S, 'x',bounds, npts); 
    else
        %perform pixel-by-pixel averaging
        profile =average_p(S,'x',bounds,npts);
    end
    txt='averagex_plot';
else
    S=xaxisbintype(S,0); %0 means it is linear
    if strcmp(handles.saxsin.reduction_type,'s')
        %perform spectra-by-spectra averaging
        profile = average_raw(S, 'y',bounds, npts); 
    else
        %perform pixel-by-pixel averaging
        profile =average_p(S,'y',bounds,npts);
    end
    txt='averagey_plot';
end
profile=strip_reduction_pixels(profile);
fig = plotx(profile);
set(fig, 'Tag', txt)  % mark it so we can find it later
set(findobj(get(fig,'Children'),'Type','axes'),'Tag','plot');
h=reduction_text(findobj(get(fig,'Children'),'Tag','plot'));

% This should plot what needs to be plotted









