function outfile = phdwriteline(hline, filename)
%PHDWRITELINE Write line data to a comma-separated-values file.
%   PHDWRITELINE(HLINE, FILENAME) writes the data points of the line whose
%   handle is HLINE to a text file named FILENAME as phd-format
%   values, that is an x,y pair on each line.

if ~ (nargin == 2 && ishandle(hline) && isequal(get(hline, 'Type'), 'line') ...
        && ischar(filename) && ndims(filename) == 2 && size(filename, 1) == 1)
    error('I need a handle to a line object and a filename string.')
end

X = get(hline, 'XData');
Y = get(hline, 'YData');
S = get(hline, 'UserData'); 

E=S.relerr.*Y;                 %added by KJ 26 April 2005, in preparation for errorbars
%setting points where we have NaN's  zero.
Y(isnan(Y)) = 0.0;
E(isnan(E)) = 0.0;
DX=0*E;

comment1=sprintf('%s',S.raw_filename(1:end));
comment2='';

if exist(filename)
    ButtonName=questdlg(['Overwrite existing file ',filename,' ?'], ...
        'File Warning', ...
        'Yes','No');
    switch ButtonName
        case 'No'
            return
    end % switch
end

outfile=writeradfile('PDH',filename,comment1,comment2,X,Y,E,DX);




