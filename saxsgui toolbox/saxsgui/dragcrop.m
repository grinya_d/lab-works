function cropped = dragcrop(A)
%DRAGCROP Crop an image by dragging a rectangle.
%   CROPPED = CROP(A) displays two-dimensional numeric array A as a
%   gray-scale image then waits for you to drag a rectangle in the image.
%   CROP returns the corresponding cropped array CROPPED.

fig = figure; imagesc(A), axis image xy, colormap gray
figstatus('Drag a rectangle.  Hint: pause between pressing the button and moving the pointer.', fig);
box = fix(dragbox);
xrange = box(:, 1);
yrange = box(:, 2);
cropped = A(yrange(1) : yrange(2), xrange(1) : xrange(2));
close(fig)
