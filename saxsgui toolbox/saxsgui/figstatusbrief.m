function h = figstatusbrief(message, seconds, fig)
%FIGSTATUSBRIEF Display a status line message for a number of seconds.
%   FIGSTATUSBRIEF('message', SECONDS, FIG) displays a message using
%   FIGSTATUS, automatically deleting the message after SECONDS elapsed
%   time.
%
%   See FIGSTATUS.

if nargin < 3
    fig = gcf;
end
if nargin < 2
    error('I need a message string and a number of seconds.')
end
if ~ ischar(message)
    error('1st argument must be a character string.')
end
if ~ isnumeric(seconds) || length(seconds) > 1
    error('2nd argument must be a scalar number (of seconds).')
end
if ~ ishandle(fig) || (~ isequal(get(fig, 'Type'), 'figure'))
    error('3rd argument must be a figure handle.')
end

% Put up the status bar.
hstatus = figstatus(message, fig);

% Create a self-destructing timer to delete the status bar.
htimer = timer;
set(htimer, 'TimerFcn', {@times_up, hstatus}, 'StartDelay', seconds)
start(htimer)

if nargout
    h = hstatus;
end

function times_up(obj, event, hstatus)
if ishandle(hstatus)  % status bar still exists
    delete(hstatus)
end
stop(obj)
delete(obj)
