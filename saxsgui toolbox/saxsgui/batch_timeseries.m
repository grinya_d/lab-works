function varargout = batch_timeseries(varargin)
% BATCH_TIMESERIES M-file for batch_timeseries.fig
%      BATCH_TIMESERIES, by itself, creates a new BATCH_TIMESERIES panel or raises the existing
%      singleton*.
%
%      H = BATCH_TIMESERIES returns the handle to a new BATCH_TIMESERIES or the handle to
%      the existing singleton*.
%
%      BATCH_TIMESERIES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BATCH_TIMESERIES.M with the given input arguments.
%
%      BATCH_TIMESERIES('Property','Value',...) creates a new BATCH_TIMESERIES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before batch_timeseries_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to batch_timeseries_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help batch_timeseries

% Last Modified by GUIDE v2.5 24-Mar-2011 23:06:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @batch_timeseries_OpeningFcn, ...
    'gui_OutputFcn',  @batch_timeseries_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before batch_timeseries is made visible.
function batch_timeseries_OpeningFcn(hObject, ~, handles, varargin)
global batch_timeseries_temp
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to batch_timeseries (see VARARGIN)

set(hObject,'Name','Batch: Time Series Panel')

% we'll store the template SAXS structure used as a template in the Batch Start button "UserData"

set(handles.StartBatch,'UserData',varargin{1})
batch_timeseries_temp=batch_timeseries_init; % gets initial values for GUI
batch_timeseries_GUIupdate(handles); %updates GUI



% Choose default command line output for batch_timeseries
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes batch_timeseries wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = batch_timeseries_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout = {get(handles.StartBatch,'UserData')};



% --- Executes during object creation, after setting all properties.
function sample_trans_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function sample_trans_Callback(hObject, eventdata, handles)
% hObject    handle to sample_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_trans as text
%        str2double(get(hObject,'String')) returns contents of sample_trans as a double
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes during object creation, after setting all properties.
function Sample_thickness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sample_thickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function Sample_thickness_Callback(hObject, eventdata, handles)
% hObject    handle to Sample_thickness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sample_thickness as text
%        str2double(get(hObject,'String')) returns contents of Sample_thickness as a double
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes during object creation, after setting all properties.
function EvalExpr1String_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EvalExpr1String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function updata_evalexprstrings(hObject,handles)
% this functions tests that the expression entered can actually be executed
% and returns a warning if it cannot.
Y=[1 3];
X=[0.01 0.1];
teststr=get(hObject,'String');
try
    eval(['a=',teststr,';'])
    batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
catch
    warndlg('The string cannot be interpreted. Remember to use Capital X and Y, and only statement recognized by Matlab')
    batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
end

function EvalExpr1String_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr1String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EvalExpr1String as text
%        str2double(get(hObject,'String')) returns contents of
%        EvalExpr1String as a double

updata_evalexprstrings(hObject,handles) % this function checks that the expression can
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in ChooseFiles.
function ChooseFiles_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% here we choose the files to reduce
[path,names]=getfiles('Choose the files you want to reduce');
files.path=path;
files.names=names;
%save files-structure to the buttons 'UserData' parameter
set(hObject,'UserData',files);
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in cancel.
function Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(gcf)


% --- Executes on button press in StartBatch.
function StartBatch_Callback(hObject, eventdata, handles)
% hObject    handle to StartBatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global multiple_file_run
global batch_timeseries_temp

multiple_file_run=1;
files=get(handles.ChooseFiles,'UserData'); % this loads the names of the files to process
if ~exist(files.path)
    warndlg('No input files chosen')
    return
end
if isempty(files.path)
    warndlg('Input files are not valid')
    return
end

%Getting template data
S_template=get(handles.StartBatch,'UserData');

% now we need to allow the user to put them in an appropriate directory
%One could argue this should go in the panel but we leave it out for now
outputpath=uigetdir(pwd,'Pick a directory for the output files');
if outputpath==0, outputpath=pwd; end
outputpath=[outputpath,filesep];

if str2num(S_template.reduction_state(5))==0 % if sample transmission is not to be used
    transmission=1;
else
    transmission=str2num(get(handles.sample_trans,'String'));
end

if str2num(S_template.reduction_state(1))==0 % if sample thickness is to not to be used
    thickness=1;
else
    thickness=str2num(get(handles.Sample_thickness,'String'));
end

%now to begin looping
for ii=1:length(files.names)
    filename=[files.path files.names{ii}];
    %first adjust transfact
    if get(handles.UseMetaData,'Value')==1
        Stemp=getsaxs(filename);
        Stemp=fill_from_saxslab_header(Stemp);
        S_template=transfact(S_template,Stemp.transfact);
        S_template=sample_thickness(S_template,Stemp.sample_thickness);
        if strcmp(Stemp.reduction_state(5),'1')
            S_template=insert_reduction_state(S_template,5,1);
        else
            S_template=insert_reduction_state(S_template,5,0);
        end
    end
    %then adjust reduction_state
    
    hstatus = figstatus(['Reducing: ',files.names{ii}],get(hObject,'Parent'));  % tell user what to do in a status bar
    [S,Sred]=reduce_templatewise(filename,S_template);
    %this is to ensure that xaxis and yaxis are also calibrated in 2D data
    %however there is a bug in the program since it does not show the
    %reduced image (pixel-by-pixel)
    S=calibrate(S,S.kcal,S.pixelcal,S.koffset,[],[],[],'std'); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
    S=center(S,S.raw_center); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
    delete(hstatus)
    
    X = Sred.xaxisfull;
    Y = Sred.pixels/Sred.livetime;
    E=Sred.relerr.*Y;
    DX=0*X;
    E(isnan(E)) = 0.0;
    Y(isnan(Y)) = 0.0;
    
    %now for various per-file-output options
    % saving 2D images
    if get(handles.Save2DImages,'Value')==1
        tag='2D';
        tag2=get(handles.DescriptiveTagString,'String');
        [pathstr,name,ext] = fileparts(S.raw_filename);
        file=[outputpath,name,'_',tag,'_',tag2,'.jpg'];
        
        figure2D=findobj(get(0,'Children'),'Tag','2D SAXS image');
        if isempty(figure2D)
            figure2D=figure;
            set(figure2D,'Tag','2D SAXS image')
        end
        figure(figure2D)
        image(log(S))
        hax=gca;
        title([file,'(log intensity scale)'])
        
        set(hax,'XGrid','on')
        set(hax,'YGrid','on')
        colorbar
        warning off, drawnow, warning on
        print(figure2D,'-djpeg',file)
    end
    
    % saving 1D spectra
    if get(handles.Save1DFigures,'Value')==1
        tag='1D';
        tag2=get(handles.DescriptiveTagString,'String');
        [pathstr,name,ext] = fileparts(S.raw_filename);
        file=[outputpath,name,'_',tag,'_',tag2,'.jpg'];
        figure1D=findobj(get(0,'Children'),'Tag','1D SAXS spectrum');
        if isempty(figure1D)
            figure1D=figure;
            set(figure1D,'Tag','1D SAXS spectrum')
        end
        figure(figure1D)
        image(Sred)
        title([file,''])
        hplot=get(figure1D,'Children');
        set(hplot,'Xscale','lin')
        set(hplot,'Yscale','log')
        warning off, drawnow, warning on
        print(figure1D,'-djpeg',file)
    end
    
    %saving to separate files
    %     if get(handles.ReducedDataToSeparateFiles,'Value')==1
    %         tag=get(handles.DescriptiveTagString,'String');
    %         [pathstr,name,ext] = fileparts(S.raw_filename);
    %         file=[outputpath,name,'_',tag,'.csv'];
    %         if get(handles.xyformat,'Value')==1
    %             data = [X(:), Y(:)];
    %         else
    %             if length(Y)==length(E)
    %                 data = [X(:), Y(:), E(:)];
    %             else
    %                 data = [X(:), Y(:)];
    %             end
    %         end
    %         dlmwrite(file, data, ',')
    %     end
    if get(handles.ReducedDataToSeparateFiles,'Value')==1
        tag=get(handles.DescriptiveTagString,'String');
        [pathstr,name,ext] = fileparts(S.raw_filename);
        if strcmp(batch_timeseries_temp.FileFormat,'CSV')
            filename=[outputpath,name,'_',tag,'.csv'];
            datafile=sprintf('%s',S.raw_filename(1:end));
            comment2='';
            %
            if get(handles.xydeltayformat,'Value')==1
                outfile=writeradfile('CSVXYdY',filename,datafile,comment2,X,Y,E,DX);
            else
                outfile=writeradfile('CSVXY',filename,datafile,comment2,X,Y,E,DX);
            end
        end
        if strcmp(batch_timeseries_temp.FileFormat,'RAD')
            filename=[outputpath,name,'_',tag,'.rad'];
            datafile=sprintf('%s',S.raw_filename(max(1,end-80):end));
            if isfield(S.metadata,'Meas')
                tmpstr=struct2cell(S.metadata.Meas);
                datadesc=sprintf('Desc:%s',tmpstr{1});
            else
                datadesc=sprintf('Sample descriptor not yet implemented\n');
            end
            outfile=writeradfile('RAD',filename,datafile,datadesc,X,Y,E,DX);
        end
        if strcmp(batch_timeseries_temp.FileFormat,'PDH')
            filename=[outputpath,name,'_',tag,'.pdh'];
            comment1=sprintf('%s',S.raw_filename(1:end));
            comment2='';
            outfile=writeradfile('PDH',filename,comment1,comment2,X,Y,E,DX);
        end
    end
    
    %preparing data to save to same file (without errorbars)
    if get(handles.ReducedDataToSameFile,'Value')==1
        if get(handles.xyformat,'Value')==1
            if ii==1
                datagrand=zeros(length(files.names)+1,length(X));
                tag=[get(handles.DescriptiveTagString,'String'),'_combined_'];
                [pathstr,name,ext] = fileparts(S.raw_filename);
                file=[outputpath,name,'_',tag,'.csv'];
                datagrand(ii,:)=[X];
            end
            datagrand(ii+1,:)=[Y];
        else
            if ii==1
                datagrand=zeros(length(files.names)+1,2*length(X));
                tag=[get(handles.DescriptiveTagString,'String'),'_combined_'];
                [pathstr,name,ext] = fileparts(S.raw_filename);
                file=[outputpath,name,'_',tag,'.csv'];
                datagrand(ii,:)=[X X];
            end
            datagrand(ii+1,:)=[Y E];
        end
    end
    
    evalxtemp=ii;
    if  get(handles.UseTimeFromHeader,'Value')==1
        evalxtemp=datenum(S.raw_date)*24*60;
    end % time string is converted into a timenumber with 1= 1 minute
    if  get(handles.UseFiledSavedTime,'Value')==1
        evalxtemp=datenum(S.raw_date)*24*60;
    end % time string is converted into a timenumber with 1= 1 minute
    evalx(ii)=evalxtemp;
    
    if get(handles.EvalExpr1,'Value')==1
        evaly1(ii)=eval(get(handles.EvalExpr1String,'String'),'warndlg(''Expression 1 cannot be evaluated''); set(handles.EvalExpr1,''Value'',0)');
    end
    if get(handles.EvalExpr2,'Value')==1
        evaly2(ii)=eval(get(handles.EvalExpr2String,'String'),'warndlg(''Expression 2 cannot be evaluated''); set(handles.EvalExpr2,''Value'',0)');
    end
    if get(handles.EvalExpr3,'Value')==1
        evaly3(ii)=eval(get(handles.EvalExpr3String,'String'),'warndlg(''Expression 3 cannot be evaluated''); set(handles.EvalExpr3,''Value'',0)');
    end
    if get(handles.EvalExpr4,'Value')==1
        evaly4(ii)=eval(get(handles.EvalExpr4String,'String'),'warndlg(''Expression 4 cannot be evaluated''); set(handles.EvalExpr4,''Value'',0)');
    end
    if get(handles.EvalExpr5,'Value')==1
        evaly5(ii)=eval(get(handles.EvalExpr5String,'String'),'warndlg(''Expression 5 cannot be evaluated''); set(handles.EvalExpr5,''Value'',0)');
    end
    batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure
end

% now to do the things that are batch-wide outputs

% first saving the file with everything
if get(handles.ReducedDataToSameFile,'Value')==1
    tag=[get(handles.DescriptiveTagString,'String'),'_combined_'];
    [pathstr,name,ext] = fileparts(S.raw_filename); % filename will be associated with the last file in the list
    file=[outputpath,name,'_',tag,'.csv'];
    dlmwrite(file, datagrand', ',')
end

% now for making the plot will make different plots for each data

% normalization first time to zero
if ~isempty(evalx)
    evalx=evalx-evalx(1);
    jj=0;
    legendstrings=[];
    datagrand=[];
    for ii=1:5
        execstring=['if exist(''evaly',num2str(ii),'''),'];
        execstring=[execstring,' jj=jj+1; y=evaly',num2str(ii),'; figure, plot(evalx,y); '];
        execstring=[execstring,'legendstring=get(handles.EvalExpr',num2str(ii),'String,''String''); '];
        execstring=[execstring,'title(''Time Series''); xlabel(''Time (minutes from first measurement)''); '];
        execstring=[execstring,'ylabel(legendstring); '];
        % storing the date in a matrix..should maybe change the legendstrings as a function of input time type
        execstring=[execstring,'if jj==1,datagrand(:,jj)=evalx''; legendstrings=''Time''; end; '] ;
        execstring=[execstring,'datagrand(:,jj+1)=y''; '];
        execstring=[execstring,'legendstrings=[legendstrings,'','',legendstring]; '];
        execstring=[execstring,'end'];
        eval(execstring)
    end
    % now saving the same data to an ascii separated file
    tag=['_timeseries_'];
    [pathstr,name,ext] = fileparts(S.raw_filename); % filename will be associated with the last file in the list
    file=[outputpath,name,'_',tag,'.csv'];
    tempfile=[outputpath,name,'_temp.csv'];
    dlmwrite(tempfile, datagrand, ',')
    fileid=fopen(file,'w');
    tempid=fopen(tempfile,'r');
    fprintf(fileid,'%s \n',legendstrings);
    while 1
        tline = fgets(tempid);
        if ~ischar(tline), break, end
        fprintf(fileid,'%s',tline)  ;
    end
    fclose(tempid);
    fclose(fileid);
    delete(tempfile);
end
multiple_file_run=0;





% --- Executes on button press in ReducedDataToSeparateFiles.
function ReducedDataToSeparateFiles_Callback(hObject, eventdata, handles)
% hObject    handle to ReducedDataToSeparateFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ReducedDataToSeparateFiles
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in ReducedDataToSameFile.
function ReducedDataToSameFile_Callback(hObject, eventdata, handles)
% hObject    handle to ReducedDataToSameFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ReducedDataToSameFile
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in xydeltayformat.
function xydeltayformat_Callback(hObject, eventdata, handles)
% hObject    handle to xydeltayformat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of xydeltayformat
if get(hObject,'Value')==1
    set(handles.xyformat,'Value',0)
else
    set(handles.xyformat,'Value',1)
end
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in xyformat.
function xyformat_Callback(hObject, eventdata, handles)
% hObject    handle to xyformat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of xyformat
if get(hObject,'Value')==1
    set(handles.xydeltayformat,'Value',0)
else
    set(handles.xydeltayformat,'Value',1)
end
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in Save1DFigures.
function Save1DFigures_Callback(hObject, eventdata, handles)
% hObject    handle to Save1DFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save1DFigures
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in Save2DImages.
function Save2DImages_Callback(hObject, eventdata, handles)
% hObject    handle to Save2DImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save2DImages
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in UseFiledSavedTime.
function UseFiledSavedTime_Callback(hObject, eventdata, handles)
% hObject    handle to UseFiledSavedTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of UseFiledSavedTime

set(handles.UseFiledSavedTime,'Value',1);
set(handles.UseTimeFromHeader,'Value',0);
set(handles.UseIncrementalTime,'Value',0);

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in UseIncrementalTime.
function UseIncrementalTime_Callback(hObject, eventdata, handles)
% hObject    handle to UseIncrementalTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of UseIncrementalTime
set(handles.UseFiledSavedTime,'Value',0);
set(handles.UseTimeFromHeader,'Value',0);
set(handles.UseIncrementalTime,'Value',1);

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in UseTimeFromHeader.
function UseTimeFromHeader_Callback(hObject, eventdata, handles)
% hObject    handle to UseTimeFromHeader (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of UseTimeFromHeader
set(handles.UseFiledSavedTime,'Value',0);
set(handles.UseTimeFromHeader,'Value',1);
set(handles.UseIncrementalTime,'Value',0);

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in EvalExpr1.
function EvalExpr1_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EvalExpr1
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function EvalExpr2String_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EvalExpr2String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function EvalExpr2String_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr2String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EvalExpr2String as text
%        str2double(get(hObject,'String')) returns contents of EvalExpr2String as a double
updata_evalexprstrings(hObject,handles) % this function checks that the expression can
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in EvalExpr2.
function EvalExpr2_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EvalExpr2

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function EvalExpr3String_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EvalExpr3String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function EvalExpr3String_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr3String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EvalExpr3String as text
%        str2double(get(hObject,'String')) returns contents of
%        EvalExpr3String as a double
updata_evalexprstrings(hObject,handles) % this function checks that the expression can
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in EvalExpr3.
function EvalExpr3_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EvalExpr3

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function EvalExpr4String_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EvalExpr4String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function EvalExpr4String_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr4String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EvalExpr4String as text
%        str2double(get(hObject,'String')) returns contents of EvalExpr4String as a double
updata_evalexprstrings(hObject,handles) % this function checks that the expression can
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in EvalExpr4.
function EvalExpr4_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EvalExpr4

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function EvalExpr5String_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EvalExpr5String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function EvalExpr5String_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr5String (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EvalExpr5String as text
%        str2double(get(hObject,'String')) returns contents of EvalExpr5String as a double
updata_evalexprstrings(hObject,handles) % this function checks that the expression can
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in EvalExpr5.
function EvalExpr5_Callback(hObject, eventdata, handles)
% hObject    handle to EvalExpr5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EvalExpr5

batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function DescriptiveTagString_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DescriptiveTagString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function DescriptiveTagString_Callback(hObject, eventdata, handles)
% hObject    handle to DescriptiveTagString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DescriptiveTagString as text
%        str2double(get(hObject,'String')) returns contents of DescriptiveTagString as a double
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --------------------------------------------------------------------
function Save_Callback(hObject, eventdata, handles)
global batch_timeseries_temp
% hObject    handle to Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uiputfile('*.mat', 'Pick a file to save the parameters to');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    save(fullfilename,'batch_timeseries_temp')
end

% --------------------------------------------------------------------
function Load_Callback(hObject, eventdata, handles)
global batch_timeseries_temp
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat','Load timeseries parameter file ');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    load(fullfilename)
    batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure
end

% --- Executes on button press in DefaultValues.
function DefaultValues_Callback(hObject, eventdata, handles)
global batch_timeseries_temp

% hObject    handle to DefaultValues (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

batch_timeseries_temp=[];
batch_timeseries_temp=batch_timeseries_init; % gets initial values for GUI
batch_timeseries_GUIupdate(handles); %updates GUI


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
global batch_timeseries_temp
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on selection change in FileFormat.
function FileFormat_Callback(hObject, eventdata, handles)
% hObject    handle to FileFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FileFormat contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FileFormat
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function FileFormat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FileFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in UseMetaData.
function UseMetaData_Callback(hObject, eventdata, handles)
% hObject    handle to UseMetaData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of UseMetaData
batch_timeseries_struct_update(handles) %updates the structure based on the input in GUI
batch_timeseries_GUIupdate(handles) %updates the GUI based on the structure
