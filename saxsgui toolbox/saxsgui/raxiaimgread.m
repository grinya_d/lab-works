function saxsobj=raxiaimgread(pathname,silent)



%read in data from the meta file
[raxiaheader,byte_order]=read_raxis_header(pathname);
%getting the size of the image
s1=raxiaheader.x_direction_pixel_number;
s2=raxiaheader.y_direction_pixel_number;
livetime1=raxiaheader.exposure_time_minutes*60;

try
    fid=fopen(pathname,'r');
catch
    caught = lasterr;  % error message
    error(['Error reading image file .img for RAXIA IP data', pathname, ':  ', caught])
end
%getting the header out of the way
raxiaheader.full=fread(fid,raxiaheader.record_length,'char');

%reading in data
im = fread(fid,[s1,s2],'uint16',0,byte_order);
fclose(fid);

%transforming data
im=(im>=2^15).*(im-2^15)*8+(im<2^15).*im;

%creating the SAXS image
saxsobj = saxs(im, ['imgread file ''', pathname, ''' ', datestr(now)]);

saxsobj = type(saxsobj, 'xy');
saxsobj = realtime(saxsobj, max(0,livetime1));
saxsobj = livetime(saxsobj, max(0,livetime1));
saxsobj = raw_filename(saxsobj, pathname);
saxsobj = raw_date(saxsobj,[]);
saxsobj = datatype(saxsobj,'img');
saxsobj = detectortype(saxsobj,'RAXIA');

function [header,byte_order]=read_raxis_header(path)

byte_order='b';
try
    fid=fopen(path,'r');
catch
    header=[];
    return
end
header.device_name=fread(fid,10,'char');
header.version=fread(fid,10,'char');
header.crystal_name=fread(fid,20,'char');
header.crystal_system=fread(fid,12,'char');
header.a=fread(fid,1,'float',0,byte_order);
if abs(header.a)>1E6 || abs(header.a)<1E-6
    fclose(fid)
    byte_order='l';
    fid=fopen(filename,'r');header.device_name=fread(fid,10,'char');
    header.version=fread(fid,10,'char');
    header.crystal_name=fread(fid,20,'char');
    header.crystal_system=fread(fid,12,'char');
    header.a=fread(fid,1,'float',0,byte_order);
end   
header.b=fread(fid,1,'float',0,byte_order);
header.c=fread(fid,1,'float',0,byte_order);
header.alpha=fread(fid,1,'float',0,byte_order);
header.beta=fread(fid,1,'float',0,byte_order);
header.gamma=fread(fid,1,'float',0,byte_order);
header.space_group=fread(fid,12,'char');
header.mosaic_spread=fread(fid,1,'float',0,byte_order);
header.memo=fread(fid,80,'char');
header.reserve1=fread(fid,84,'char');
header.date=fread(fid,12,'char');
header.measure_person=fread(fid,20,'char');
header.xray_target=fread(fid,4,'char');
header.wavelength=fread(fid,1,'float',0,byte_order);
header.monochromator=fread(fid,20,'char');
header.monochrome2theta=fread(fid,1,'float',0,byte_order);
header.collimator=fread(fid,20,'char');
header.kbetafilter=fread(fid,4,'char');
header.camera_length=fread(fid,1,'float',0,byte_order);
header.xray_pipe_voltage=fread(fid,1,'float',0,byte_order);
header.xray_pipe_electric_current=fread(fid,1,'float',0,byte_order);
header.xray_focus=fread(fid,12,'char');
header.xray_optics=fread(fid,80,'char');
header.camera_shape=fread(fid,1,'int32',0,byte_order);
header.weissenberg_oscillation=fread(fid,1,'float',0,byte_order);
header.reserve2=fread(fid,56,'char');
header.mount_axis=fread(fid,4,'char');
header.beam_axis=fread(fid,4,'char');
header.phi_zero=fread(fid,1,'float',0,byte_order);
header.phi_start=fread(fid,1,'float',0,byte_order);
header.phi_end=fread(fid,1,'float',0,byte_order);
header.times_of_oscillation=fread(fid,1,'int32',0,byte_order);
header.exposure_time_minutes=fread(fid,1,'float',0,byte_order);
header.direct_beam_position_x=fread(fid,1,'float',0,byte_order);
header.direct_beam_position_y=fread(fid,1,'float',0,byte_order);
header.omega=fread(fid,1,'float',0,byte_order);
header.chi=fread(fid,1,'float',0,byte_order);
header.two_theta=fread(fid,1,'float',0,byte_order);
header.mu=fread(fid,1,'float',0,byte_order);
header.reserve3=fread(fid,204,'char');
header.x_direction_pixel_number=fread(fid,1,'int32',0,byte_order);
header.y_direction_pixel_number=fread(fid,1,'int32',0,byte_order);
header.x_direction_pixel_size=fread(fid,1,'float',0,byte_order);
header.y_direction_pixel_size=fread(fid,1,'float',0,byte_order);
header.record_length=fread(fid,1,'int32',0,byte_order);
header.number_of_record=fread(fid,1,'int32',0,byte_order);
header.red_start_line=fread(fid,1,'int32',0,byte_order);
header.IP_number=fread(fid,1,'int32',0,byte_order);
header.output_ratio=fread(fid,1,'float',0,byte_order);
header.fading_time1=fread(fid,1,'float',0,byte_order);
header.fading_time2=fread(fid,1,'float',0,byte_order);
header.host_computer_classification=fread(fid,10,'char');
header.IP_classification=fread(fid,10,'char');
header.data_direction_horizontal=fread(fid,1,'int32',0,byte_order);
header.data_direction_vertical=fread(fid,1,'int32',0,byte_order);
header.data_direction_front_back=fread(fid,1,'int32',0,byte_order);
header.reserve4=fread(fid,10,'char');
fclose(fid);
