function [pixels] = zinger_removal(im,old_mask)
global zinger_params
%function to mask away regions with zingers
%The function performs a smoothing with size , and then compares
%the smoothed values to the pixel values. If actual/smoothed> threshold
%then all pixels within a box of pixels is masked for non-use.
%The zinger_params values would usually be given in the SAXS_preferences.m
%file.
%Output is the newmask
%For Photonic Science Gemstar seems to work well with 9,1.5,7
%For Pilatus detectors the best values seem to be 9,8,3
zsm_size=zinger_params.smoothingsize;
z_thresh=zinger_params.threshold;
zm_s=zinger_params.zinger_masksize;

sm_box=1/zsm_size^2*ones(zsm_size);
sm_im=conv2(im,sm_box,'same');
is=size(sm_im);
new_mask=old_mask;
%tic
for ii=1:is(1)
   for jj=1:is(2)
       if sm_im(ii,jj)~=0
           if im(ii,jj)/sm_im(ii,jj)>z_thresh && im(ii,jj)>z_thresh
               new_mask(min(max(1:ii-zm_s),is(1)):min(max(1:ii+zm_s),is(1)),min(max(1:jj-zm_s),is(2)):min(max(1:jj+zm_s),is(2)))=NaN;
           end
       end
   end
end
%toc
pixels=im./new_mask;

