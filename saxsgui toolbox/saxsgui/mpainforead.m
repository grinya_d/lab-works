%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function info_struct=mpainforead(im_file)
[pathstr,name,ext] = fileparts(im_file);
info_file=[fullfile(pathstr,name),'.info'];
file = fopen(info_file, 'rt');
info_struct=[];
if file == -1
    %disp('Trying .inf');
    info_file=[fullfile(pathstr,name),'.inf'];
    file = fopen(info_file, 'rt');
    if file == -1
        %disp('No .info nor .inf found');
        info_struct=[];
        return
        %error('Could not open info file for this mpa-file.')
    end
end
while ~feof(file)
    line=fgetl(file);
    eq_split=findstr(line,'=');
    comment_split=findstr(line,'%');
    if isempty(comment_split), comment_split=length(line)+1;end
    if ~isempty(eq_split)
        if eq_split(1)<comment_split
            param=strtrim(line(1:eq_split-1));
            pval=strtrim(line(eq_split(1)+1:comment_split-1));
            if ~isempty(str2num(pval))
                pval=str2num(pval);
            else
                pval=strrep(pval,'''','');
            end
            if ~isempty(param)
                info_struct=assign_infostruct(info_struct,param,pval);
            end
        end
    end
end
fclose(file);
% Since the info file is missing things like:
%    sample_thickness
%    mask_filename
%    abs_int_fact
%    background_transmission (empty_transfact)
%    readoutnoise_filename
%    zinger_removal
%    solvent_filename
%    solvent_transmission
%    solvent_thickness
%    temperature
% we have constructed a code in the comments file to allow for this
% for example Comments could look the sample infofile at the bottom
% (commented out)
%
% not all of the variable have to be there...and in fact the program will
% also work if these variables are not in the comments line but somewhere
% else using the regular nomenclature
% so now we start the parser
% if isfield(info_struct,'Comments')
%     if ~isempty(info_struct.Comments)
%         % perhaps comments is parcable
%         % let's look for separators
%         txt=strcat(info_struct.Comments,';');
%         sep_loc=[0,findstr(txt,';')];
%         num_vals=length(sep_loc);
%         for ii=1:num_vals-1
%             a=txt(sep_loc(ii)+1:sep_loc(ii+1)-1);
%             eq_split=findstr(a,'=');
%             if ~isempty(eq_split(1))
%                 param=strtrim(a(1:eq_split-1));
%                 pval=strtrim(a(eq_split(1)+1:length(a)));
%                 if ~isempty(str2num(pval))
%                     pval=str2num(pval);
%                 else
%                     pval=strrep(pval,'''','');
%                 end
%                 if ~isempty(param)
%                     info_struct=assign_infostruct(info_struct,param,pval);
%                 end
%             end
%         end
%     end
% end



function is=assign_infostruct(is,parameter,pval)

switch parameter
    case 'SamplePosition'
        is.SamplePosition=pval;
    case 'SamplePositionMM'
        is.SamplePositionMM=pval;
    case 'SampleAngle'
        is.SampleAngle=pval;
    case 'SampleRotation'
        is.SampleRotation=pval;
    case 'SampleTemp'
        is.SampleTemp=pval;
    case 'Sample Description'
        is.Sample_Description=pval;
    case 'Date'
        is.Date=pval;
    case 'Username'
        is.Username=pval;
    case 'Trans Factor'
        is.Trans_Factor=pval;
    case 'LiveTime'
        is.LiveTime=pval;
    case 'RealTime'
        is.RealTime=pval;
    case 'Background file'
        is.Background_file=pval;
    case 'Intensity file'
        is.Intensity_file=pval;
    case 'Flatfield file'
        is.Flatfield_file=pval;
    case 'Dark count file'
        is.Dark_count_file=pval;
    case 'Comments'
        is.Comments=pval;
    case 'PhotoDiode'
        is.PhotoDiode=pval;
    case 'sample_thickness'
        is.sample_thickness=pval;
    case 'mask_filename'
        is.mask_filename=pval;
    case 'sample_thickness'
        is.sample_thickness=pval;
    case 'abs_int_fact'
        is.abs_int_fact=pval;
    case 'background_transmission'
        is.background_transmission=pval;
    case 'readoutnoise_filename'
        is.readoutnoise_filename=pval;
    case 'zinger_removal'
        is.zinger_removal=pval;
    case 'solvent_transmission'
        is.solvent_transmission=pval;
    case 'solvent_filename'
        is.solvent_filename=pval;
    case 'solvent_thickness'
        is.solvent_thickness=pval;
    case 'temperature'
        is.temperature=pval;
end

% % MPA Info file (June 2010)
% SamplePosition = 0,0
% SamplePositionMM = 36.600,12.200
% SampleAngle = 
% SampleRotation = 
% SampleTemp = 
% Sample Description = POS1
% Username = 
% Date = 11/8/2006 10:20 PM
% Trans Factor = 1.000000
% LiveTime = 60
% RealTime = 60
% Background file = 'C:\Data\Yale\Solution 850 mm\000065.mpa'
% Intensity file = 
% Flatfield file = 
% Dark count file = 
% Comments = 'sample_thickness=0.1 ; background_transmission=0.15 ; solvent_filename=C:\Data\Yale\Solution 850 mm\000065.mpa; solvent_transmission=0.4; solvent_thickness=0.2; temperature=30'
% PhotoDiode = 730.632E-12