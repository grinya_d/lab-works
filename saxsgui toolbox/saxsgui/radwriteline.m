function outfile = radwriteline(hline, filename)
%RADWRITELINE Write line data to a Ris� RAD file.
%   RADWRITELINE(HLINE, FILENAME) writes the data points of the line whose
%   handle is HLINE to a text file named FILENAME as a rad-file
%   That is with the following format
%   TEXTSTRING 1
%   TEXTSTRING 2
%   NUMBER OF POINTS
%   data in q I deltaI

if ~ (nargin == 2 && ishandle(hline) && isequal(get(hline, 'Type'), 'line') ...
        && ischar(filename) && ndims(filename) == 2 && size(filename, 1) == 1)
    error('I need a handle to a line object and a filename string.')
end

X = get(hline, 'XData');
Y = get(hline, 'YData');
S = get(hline, 'UserData'); 

E=S.relerr.*Y;                 %added by KJ 26 April 2005, in preparation for errorbars
DX=0*E;
%setting points where we have NaN's  zero.
Y(isnan(Y)) = 0.0;
E(isnan(E)) = 0.0;
%
if exist(filename)
    ButtonName=questdlg(['Overwrite existing file ',filename,' ?'], ...
        'File Warning', ...
        'Yes','No');
    switch ButtonName
        case 'No'
            return
    end % switch
end
datafile=sprintf('%s',S.raw_filename(max(1,end-80):end));
if isfield(S.metadata,'Meas')
    tmpstr=struct2cell(S.metadata.Meas);
    datadesc=sprintf('Desc:%s',tmpstr{1});
else
    datadesc=sprintf('Sample descriptor not yet implemented\n');
end
outfile=writeradfile('RAD',filename,datafile,datadesc,X,Y,E,DX);




