function saxs_preferences_write()
% program to write the saxs_preferences.m file
global water_abs_int glassy_carbon_abs_int water_attenuation_length
global cmask_radius smooth_size smooth_std
global fatpix_fontsize
global zinger_params
global averagex_prefs
global fujiimgrot90 fujiimgflip
global executableversion

sp_name='saxs_preferences.txt';
sp_file=which('saxs_preferences.txt');
if isempty(sp_file) 
    sp_file=sp_name;
end
fid=fopen(sp_file,'w');
if (fid<1)
    errordlg('saxs_prefences.m file is not writeable. Check privileges');
else
%     txt='global water_abs_int glassy_carbon_abs_int water_attenuation_length';
%     fprintf(fid,'%s\n',txt);
%     txt='global cmask_radius smooth_size smooth_std';
%     fprintf(fid,'%s\n',txt);
%     txt='global fatpix_fontsize';
%     fprintf(fid,'%s\n',txt);
%     txt='global zinger_params';
%     fprintf(fid,'%s\n',txt);
%     txt='global averagex_prefs';
%     fprintf(fid,'%s\n',txt);
%     txt='global fujiimgrot90 fujiimgflip';
%     fprintf(fid,'%s\n',txt);
    
    txt='%parameters for absolution calibration';
    fprintf(fid,'%s\n',txt);
    txt=['water_abs_int = ',num2str(water_abs_int),';  % Units are in cm-1 at 25 degrees C.'];
    fprintf(fid,'%s\n',txt);
    txt=['glassy_carbon_abs_int= ',num2str(glassy_carbon_abs_int),';  % This is for a specific calibrated sample (1mm Glassy Carbon in Haifa)'];
    fprintf(fid,'%s\n',txt);
    txt=['water_attenuation_length =',num2str(water_attenuation_length),'; % Attenuation lenght of water (cm) must be entered for the energy in question'];
    fprintf(fid,'%s\n',txt);

    txt='% parameters for auto-finding the center' ;
    fprintf(fid,'%s\n',txt);
    txt=['cmask_radius = ',num2str(cmask_radius),';'];
    fprintf(fid,'%s\n',txt);
    txt='%parameters for smoothing';
    fprintf(fid,'%s\n',txt);
    txt=['smooth_size = ',num2str(smooth_size),';'];
    fprintf(fid,'%s\n',txt);
    txt=['smooth_std = ',num2str(smooth_std),';'];
    fprintf(fid,'%s\n',txt);
    txt='%parameters for fatpix';
    fprintf(fid,'%s\n',txt);
    txt=['fatpix_fontsize = ',num2str(fatpix_fontsize),';'];
    fprintf(fid,'%s\n',txt);

    txt='% parameters for zingers : this is put in this since this is usually site specific';
    fprintf(fid,'%s\n',txt);
    txt=['zinger_params.smoothingsize = ',num2str(zinger_params.smoothingsize),'; %size of smoothing box (N x N)'];
    fprintf(fid,'%s\n',txt);
    txt=['zinger_params.threshold = ',num2str(zinger_params.threshold),';  %threshold for determining zingers (actual/average>threshold)'];
    fprintf(fid,'%s\n',txt);
    txt=['zinger_params.zinger_masksize = ',num2str(zinger_params.zinger_masksize),'; %size of mask around zinger will be 2M+1,2M+1'];
    fprintf(fid,'%s\n',txt);
    
    txt='% parameters for averaging : this is put in here to allow site specific preferences';
    fprintf(fid,'%s\n',txt);
    txt=['averagex_prefs.avtype = ''',averagex_prefs.avtype,'''; %Average type: "az" is azimuthal (I vs. q), "mo" is radial average (I vs. chi)']; 
    fprintf(fid,'%s\n',txt);
    txt=['averagex_prefs.aznumpoints = ', num2str(averagex_prefs.aznumpoints),'; %default number of points in the az average'];
    fprintf(fid,'%s\n',txt);
    txt=['averagex_prefs.azbintype = ''',averagex_prefs.azbintype,'''; %default binning type for az average']; 
    fprintf(fid,'%s\n',txt);
    txt=['averagex_prefs.monumpoints = ', num2str(averagex_prefs.monumpoints),'; %default number of points in the mo average'];
    fprintf(fid,'%s\n',txt);
    txt=['averagex_prefs.reducttype = ''',averagex_prefs.reducttype,'''; %default reduction method: s is spectra-by-spectra, p is pixel-by-pixel'];
    fprintf(fid,'%s\n',txt);

    txt='% parameter for loading fujiimgplates (controlling their orientation)';
    fprintf(fid,'%s\n',txt);
    txt=['fujiimgrot90 = ',num2str(fujiimgrot90),'; %number of times to rotate raw image by 90 degrees']; 
    fprintf(fid,'%s\n',txt);
    txt=['fujiimgflip = ',num2str(fujiimgflip),'; %To flip or not to flip image (no flip=0, flip=1)']; 
    fprintf(fid,'%s\n',txt);
end
fclose(fid);
if executableversion==0
    rehash toolboxcache
end


