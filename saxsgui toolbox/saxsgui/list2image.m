function img = list2image(liststruct,timestart,timeend)
%function img = list2image(liststruct,time,timewindow)
%LIST2IMAGE Create 2D image object from a list structure (from MPA List file) 
% take the photons arriving in the timeinterval time-˝timewindow to
% time-˝timewindow

if (nargin==1)
    time=(liststruct.timestart+liststruct.timeend)/2;
    timewindow=time*2;
else
    timestart=max(liststruct.timestart,timestart);
    timeend=min(liststruct.timeend,timeend);
    time=(liststruct.timestart+liststruct.timeend)/2;
    timewindow=(liststruct.timestart+liststruct.timeend);
end
time=time*1000/liststruct.timerreduce;
timewindow=timewindow*1000/liststruct.timerreduce;
%Creating the image
[X,Y]=meshgrid(1:liststruct.xdim,1:liststruct.ydim);
img=0*X;
iistart=find(liststruct.list(1,:)>=(time-timewindow/2),1);
iiend=find((liststruct.list(1,end)-liststruct.list(1,:))<=(liststruct.list(1,end)-(time+timewindow/2)),1);
if isempty(iiend)
    iiend=max(liststruct.list(1,:));
end
for ii=iistart:iiend
    img(liststruct.list(2,ii),liststruct.list(3,ii))=img(liststruct.list(2,ii),liststruct.list(3,ii))+1;
end


