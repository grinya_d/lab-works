function [I_smeared]=smear_v2(varargin)
%this function smears data I with a four-dimensional smearing matrix R It
%needs to be insanely fast in order to be useful. So I'm open to
%suggestions on how to make it faster.
%this is version 2, slightly less speedy than version 1, which is due to
%the smearing matrix R not being able to be generated, for it would
%require 8 TB of memory. Oh how things grow....

%input:
i=1; %read the variables..
q=varargin{i};i=i+1;
phi=varargin{i};i=i+1;
sigma_d1=varargin{i};i=i+1;
sigma_d2=varargin{i};i=i+1;
sigma_w=varargin{i};i=i+1;
sigma_c1=varargin{i};i=i+1;
sigma_c2=varargin{i};i=i+1;
q_use=varargin{i};i=i+1;
smear_shape=varargin{i};i=i+1;
I=varargin{i};i=i+1;
A=varargin{i};i=i+1;
R=varargin{i};i=i+1;
loadsofmemory=varargin{i};i=i+1;


%preallocate the I_smeared matrix
I_smeared=zeros(size(I,1),size(I,2));

%precalculate some constants
a=2*pi*sqrt(sigma_d1^2+sigma_w^2+sigma_c1^2+sigma_c2^2+sigma_d2^2);
b=(sigma_w^2+sigma_c1^2+sigma_d1^2);
c=(sigma_c2^2+sigma_d2^2);
switch (size(smear_shape,1))
    case 3
        extra_boundary=1;
    case 5
        extra_boundary=2;
end

%determine which pixels to spend time on.
[all_i,all_j]=find(q_use==1);
%run through each pixel point i,j
if strcmp(loadsofmemory,'No')==1;
for i=1:size(all_i)
    q2=abs(tan(phi(all_i(i),all_j(i))-phi(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary)).*q(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary));
    q1=abs(q(all_i(i),all_j(i))-sqrt(q(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary).^2+q2.^2));
    R=smear_shape.*(a^(-1).*exp(-1/2.*((q1-q(all_i(i),all_j(i)))).^2./b+q2.^2./c));
    I_smeared(all_i(i),all_j(i))=sum(sum(R.*I(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary).*A(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary)))+I(all_i(i),all_j(i));
end
else
    for i=1:size(all_i)
    R=smear_shape.*(a^(-1).*exp(-1/2.*((q1-q(all_i(i),all_j(i)))).^2./b+q2.^2./c));
    I_smeared(all_i(i),all_j(i))=sum(sum(R.*I(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary).*A(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary)))+I(all_i(i),all_j(i));
    end
end
