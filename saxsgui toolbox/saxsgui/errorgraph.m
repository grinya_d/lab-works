function a = errorbargraph(hline,S)
%ERRORBARGRAPH plot averaged data with errorbars
%   ERRORBAR(HLINE) takes an averaged dataline and makes a new plot with errorbars.
%   handle is HLINE 

if ~ (nargin == 2 && ishandle(hline) && isequal(get(hline, 'Type'), 'line') ...
         == 1)
    error('I need a handle to a line object and a filename string.')
end

X = get(hline, 'XData');
Y = get(hline, 'YData');
E = get(hline, 'UserData');
a=0;
hfig = figure;  % always in a new window
hplot = plot(S);  % using SAXS/PLOT
gca %locating data S
cla % getting rid of data S
hold on
errorbar(X,Y,E) %putting correct data o the graph
hold off
