function box = dragbox()
%DRAGBOX Get box coordinates by dragging within an axes.
%   DRAGBOX returns a 2x2 array, [xmin, ymin; xmax, ymax] in terms of axes
%   coordinates.  DRAGBOX waits for a mouse button press to initiate the drag.
%   A keyboard keypress aborts DRAGBOX.

if waitforbuttonpress ~= 0  % it was a keyboard key, not a mouse button
    error('Keyboard key pressed.  Aborting.')
end

% Get a dragged rectangle in axes units.  Code initially
% copied from MATLAB Function Reference: rbbox.
point1 = get(gca,'CurrentPoint');  % button down position in axes
dragrect = rbbox;  % dragrect is in figure units
point2 = get(gca,'CurrentPoint');  % button up position in axes
this_axes = gca;  % hang on to these axes
point1 = point1(1,1:2);  % extract x and y
point2 = point2(1,1:2);
p1 = min(point1,point2);  % calculate locations
offset = abs(point1-point2);  % and dimensions

% Get rid of any sloshes outside the axes.
xlimits = get(this_axes, 'XLim');
ylimits = get(this_axes, 'YLim');
p1 = max(p1, [xlimits(1), ylimits(1)]);
offset = min(offset, [xlimits(2) - p1(1), ylimits(2) - p1(2)]);

% box = [p1(1), p1(2); p1(2), p1(1) + offset(1) + offset(2)];
box = [p1; p1+offset];