function h = iq4qplot(s, varargin)
%SAXS/IQ4QPLOT Plot a Iq^4 versus q plot of 1D SAXS object.

x = s.xaxisfull;

% calibrated
if s.kcal
    distance_units = 'q (A^-^1)';   %was originally K, changed by KJ April 12, 2005
else
    distance_units = 'pixels';
end
%do we know the time? if not assume 1 second
if s.livetime
    if strcmp(s.reduction_state(1:2),'11') 
      ylabeltext='I*q^4 (Absolute intensity*q^4 (cm^-1 A^-^4)';
    else
      ylabeltext='I*q^4 (average intensity*q^4 (counts per mr^2 per second * A^-^4)';
    end
    livetime=s.livetime;
else
    ylabeltext='I*q^4 (average intensity*q^4 (counts per mr^2 * A^-^4)';
    livetime=1;
end
 
type = s.type;
switch type
    case 'r-ave'
        hplot = semilogy(x, s.pixels/livetime, varargin{:});% added by KJ April 12, 2005 normalizes;
        % shows aveage number of counts per mr^2 per second

        lastline=findobj(gcf,'Type','line');  %This returns all the line in the plot but last is always element number 1
        set(lastline(1),'UserData',s); %saving saxsobj in data in the UserData variable
        set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
% this seems to work for both new and added lines.
        axes(get(hplot, 'Parent'));
        xlabel(['momentum transfer, ', distance_units])
        ylabel(ylabeltext)
        xdata=get(hplot,'Xdata');
        ydata=get(hplot,'Ydata');
        ydatanew=ydata.*xdata.^4;
        set(hplot,'Ydata',ydatanew)
    case 'th-ave' % should not happen
        errordlg('This plottype does not apply to Azimuthal averages')
        

    case 'x-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['x, ', distance_units])
        ylabel(ylabeltext)
    case 'y-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['y, ', distance_units])
        ylabel(ylabeltext)
    otherwise
        error('This is the wrong type of SAXS image for PLOT.  Try IMAGE.')
end


if nargout
    h = hplot;
end

