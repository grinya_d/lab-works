%SAXS-METHODS Public methods (functions) specific to SAXS image objects.
%   saxs
%   image  (note use with LOG, expressions)
%   history
%   mask
%   crop   (note use with DRAGBOX)
%   center (note use with XYCLICK)
%   polar
%   xsum and rsum
%   ysum and thsum
%   axiscoords
%   pixel
%   pixelfrac
%   asciiwrite
%       DOUBLE, IMAGE, PLOT, TYPE, XAXIS, YAXIS, XDATA, YDATA,
%       REALTIME, LIVETIME, TRANSFACT, CENTER, MASK, HISTORY,
%       CROP, POLAR, RSUM, THSUM, XSUM, YSUM,
%       ASCIIWRITE, 
%       AXISCOORDS, PIXEL, PIXELFRAC, CENTEROF,
%       LOG10, LOG, PLUS, MINUS, TIMES, DIVIDE
%       FINDROT
%   SPACING
%   SMOOTH
%
%  This list is not up to date.
%  For an up-to-date list try  'help saxs'
