function argout = darkcurrent_transfact(s, t)
%SAXS/ENMPTY_TRANSFACT Set or retrieve transmission factor for SAXS image.
%   darkcurrent_TRANSFACT(S) returns the transmission factor recorded for SAXS image S.
%
%   darkcurrent_TRANSFACT(S, T) returns a SAXS image object copied from S with
%   transmission factor T for the darkcurrent holder

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.darkcurrent_transfact;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if ~isempty(t)
        if isnumeric(t) && length(t) == 1
            argout.darkcurrent_transfact = t;
            argout.history = strvcat(argout.history, ['transfact ', num2str(t)]);
        else
            error('I do not understand this darkcurrent_TRANSFACT argument.')
        end
    else
        argout.darkcurrent_transfact = [];
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
