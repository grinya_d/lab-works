%SAXS Object Fields
%   You may reference the contents of most fields either by method name or
%   by field name, such as pixels(S) or S.pixels, where S is a SAXS object.
%   Set field values using method names, such as  S = cmask_radius(S, 70).
%   The displayed form of a SAXS object, such as produced by typing a
%   variable name at the command line, includes values of all fields except
%   pixels.  Only the dimensions of the pixel array are given.
%
%   pixels
%       An array of pixel values, which may represent linear or logarithmic
%       image intensity.  See the log field below, and the log10 and log
%       methods.  Values may include NaNs for pixels where the image
%       contains no data, such as unrolled polar images.
%
%       Retrieve or assign pixel subsets with subscripts attached to the
%       SAXS variable, such as S(500:510, :).  When you assign pixel values
%       with this form the SAXS object automatically notes the direct
%       assignment in a history entry.
%   type
%       The image type, 'xy', 'rth', 'xsum', or 'ysum'.
%   relerr
%       The relative error of the individual data point...only defined for 
%       1D data.
%   error_norm_fact
%       The Error normalization factor is a factor that allow is muliplied
%       to the errors that are calculated assuming that pixel values were
%       actual photons. This parameter has been incorporated since for CCD
%       detectors and Image Plates the actual readout value is proportional
%       to the photon number but the actual correspondance is often not
%       known. So if for example the pixel value was 100 then the
%       photon-counting standard deviation would be 10. If however the
%       error_norm_fact was 0.1 then the standard deviation would only be
%       1. This could actually be the case if a CCD value of 100
%       corresponded to 10000 actual photons. The saxsgui window allows one
%       to calibratie. 
%   datatype
%       The "datafile format" such as 'mpa', 'tif'. Presently supports mpa
%       and tif

%       The detector that the data was taken on ..for example MolMet or
%       FujiIP.
%   cmask_radius
%       The 'radius' of a central area in the image which symmetry searches
%       (centering, rotating, stretching) ignore to avoid radiation
%       spilling around the beamstop.
%   xaxis
%       A vector of two numbers giving the x axis coordinates of the first
%       and last pixel columns.
%   xaxisfull
%       A vector corresponding to the values of the X-axis in averaged data%   detectortype
%   yaxis
%       A vector of two numbers giving the y axis coordinates of the first
%       and last pixel rows.
%   xaxisbintype
%       Indicates how the x-vales are binned (or spaced). 'Lin' when
%       linearly binned, and 'Log' when Logarithmically binned, and 'Misc'
%       when thrown together as for example in a spliced plot. xaxis values
%       are always first and last values.
%   log
%       Empty when pixel values are linear.  'log10' or 'log' when pixel
%       values are common or natural logarithms.  You cannot take a log of
%       an image already in log scale.  The log methods treat zero and
%       negative values specially.  See the log10 and log SAXS methods.
%   realtime
%       Real data collection time.
%   livetime
%       Live detector time during data collection.
%   transfact
%       Transmission factor of sample.
%   sample_thickness
%       Thickness of sample in mm
%   calibration type
%       Calibration type "standard" or "dimensions"..where standard uses a
%       pre-determined standard and values of kcal, pixelcal and koffset
%       are used..or dimension where wavelength, pixelsixe and
%       detector_dist are use. 
%   kcal
%       k-value (or q-value) of calibration line (for example 1st order
%       AgBeh is 0.1076 �-1)
%   pixelcal
%       pixel value of calibration line
%   koffset
%       offset of k-values
%   wavelength
%       wavelength in � of radiation 
%   pixelsize
%       pixel size in mm
%   detector_dist
%       Detector distance (from sample)
%   reduction_type
%       The data can be reduced either pixel-by-pixel 'p' or
%       spectra-by-spectry 's'.
%   reduction_state
%       The data can be in various states of reduction
%           0   no reduction
%           1   readout noise corrected
%           2   zinger corrected
%           4   mask applied
%           8   flatfield corrected (Average)
%           16  flatfield corrected (raw)
%           32  sample transmission corrected
%           64  empty corrected
%           128 dark current corrected
%           256 absolute intensity corrected
%           512 sample thickness corrected
%
%       These states can be added up to create states from 0 to 1023.
%       Although some states are not available
%   raw_filename
%       filename of 2D datafile
%   raw_date
%       date (and time) associated with 2D datafile
%   raw_pixels 
%       original data (2D) that has not been reduced or transformed in any
%       way. This is stored for later calculation of averages etc.
%   raw_center
%       A 2x1 matrix containing the center of the image. This is stored for
%       later calculation in averages.
%   mask_filename
%       filename of 2D maskfile
%   mask_pixels (not implemented)
%       mask data (2D) which has same size as pixels. mask_pixels only relevant
%       for 2D data
%   flatfield_filename
%       filename of 2D flatfield file
%   flatfield_pixels (not implemented)
%       flatfield data. Can be either 2D or 1D depending on type. Will be
%       empty if flatfield is not to be applied.
%   flatfieldtype (not implemented)
%       We can imagine many types of flatfield reductions (point-by-point,
%       average, fourier-filtered etc). This parameter allows us to specify
%       this in later versions
%   empty_filename
%       filename of 2D "Empty Cell" file
%   empty_pixels
%       "Empty Cell" data (2D). Can be either 2D or 1D depending on type.
%   empty_transfact
%       transmission factor measured for "empty cell" measurement. The transmission corrected 
%       value is always used for subtracting background
%   empty_livetime
%       actual time that the detector was was exposed (i.e. realtime minus
%       deadtime) during "Empty Cell" measurement
%   solvent_filename
%       filename of 2D "Solvent" file. This is a file used when subtracting
%       2 backgrounds (solvent+Capillary and Capillary
%   solvent_thickness
%       Thickness of solvent
%   solvent_pixels
%       "Solvent" data (2D). Can be either 2D or 1D depending on type.
%   solvent_transfact
%       transmission factor measured for "solvent + empty". The transmission corrected 
%       value is always used for subtracting background
%   solvent_livetime
%       actual time that the detector was was exposed (i.e. realtime minus
%       deadtime) during "Solvent + holder" measurement
%   darkcurrent_filename
%       filename of 2D darkcurrent file
%   darkcurrent_pixels
%       darkcurrent data (2D).With beam turned off. Can be either 2D or 1D depending on type.
%   darkcurrent_livetime
%       actual time that the detector was was exposed (i.e. realtime minus
%       deadtime)
%   readoutnoise_filename
%       filename of 2D readoutnoise file
%   readoutnoise
%       calculated readoutnoise. It is assumed that the readout noise is
%       constant across the detector, so only 1 value is required
%   zinger_removal
%       sets a flag for removal of zingers (high intensity pixels arising
%       from cosmic radiation)
%   abs_int_fact
%       absolute intensity factor which must be multiplied to the data to
%       bring it to absolute intensity (cm-1)
%   sample_condition
%       This is a structure of parameters which are supposed to express
%       the condition of the sample  for example position, orientation,
%       temperature, stress etc.
%   sample_condition.xpos
%       The x-value of the sample position , Unit mm
%   sample_condition.ypos
%       The y-value of the sample position, Unit mm
%   sample_condition.angle1
%       One angle of sample rotation, Unit deg
%   sample_condition.angle2
%       Second angle of sample rotation, Unit deg
%   sample_condition.angle1
%       Third angle of sample rotation , Unit deg
%   sample_condition.temp
%       Sample temperature , Unit degrees C
%   sample_condition.pressure
%       Sample pressure, Unit Bar
%   sample_condition.strain
%       Sample Strain
%   sample_condition.stress
%       Sample stress
%   sample_condition.shear_rate
%       Sample Shear rate
%   sample_condition.sample_conc
%       Sample concentration (could be used for dilutions studies)
%   Experiment Configuration
%       This is a structure of parameters which are supposed to express
%       the experimental configuration such as pinhole size, distances
%       and detector characteristics.
%   exp_conf.d1
%       Diameter of pinhole 1, Unit mm
%   exp_conf.d2
%       Diameter of pinhole 2, Unit mm
%   exp_conf.d3
%       Diameter of pinhole 3, Unit mm
%   exp_conf.l1
%       Distance between pinhole 1 and 2, Unit mm
%   exp_conf.l2
%       Distance between pinhole 2 and 3, Unit mm
%   exp_conf.l3
%       Distance between pinhole 3 and Sample, Unit mm
%   exp_conf.l4
%       Distance between Sample and detector, Unit mm
%   exp_conf.wavelength
%       Wavelength of radiation in Angstrom, Unit mm
%   exp_conf.deltawavelength
%       Wavelength spread of radiation (assuming gaussion shape with
%       deltawavelength = standard deviation). Unit Angstrom
%   exp_conf.Izero'
%       Incident intensity in photons per second
%   exp_conf.det_offx
%       Angle of detector in x-direction in degrees Explanation: This would
%       be a parameter for a detector that was offset at some angle from
%       the direct beam.  Unit degrees
%   exp_conf.det_offy
%       Angle of detector in x-direction in degrees Explanation: This would
%       be a parameter for a detector that was offset at some angle from
%       the direct beam. Unit degrees
%   exp_conf.det_rotx
%       Rotation of detector in x-direction in degrees Explanation: This would
%       be a parameter for a detector that was rotated at some angle with
%       respect to the the direct beam. 
%   exp_conf.det_roty
%       Rotation of detector in x-direction in degrees Explanation: This would
%       be a parameter for a detector that was rotated at some angle with
%       respect to the the direct beam. Unit degrees
%   exp_conf.det_pixsizex
%       Pixelsize in the x-direction. Unit mm
%   exp_conf.det_pixsizey
%       Pixelsize in the y-direction. Unit mm
%   exp_conf.det_resx
%       Spatial resolution of the detector in the X-direction  Unit mm     
%   exp_conf.det_resy'}
%       Spatial resolution of the detector in the Y-direction Unit mm
%   added_constant
%       constant value that is to be added to the data in the 1D-average
%       plots
%   multiplication_fact
%       constant factor to be multiplied to the data in the 1D-average
%       average plots
%   userdata_cell
%       This is a cell_matrix that the user can fill with whatever is
%       desired. For example by defining it in the readfiles.
%   metadata
%       This is a field that can be filled with metadata from experiment
%       There is no set format except that it needs to be a structure.
%   history
%       A character array, each row of which is an history entry.  Most
%       entries are created automatically by the various SAXS methods.  Use
%       the history method to add your own comments.  The history method
%       only appends to the history.  It will not replace the old history.
