function snew=clear_reduction_parameters(snew)
% SAXS/CLEAR_REDUCTION_PARAMETERS clear reduction parameters from the saxsobject
%    to the present one...

snew.transfact=             [];
snew.sample_thickness=      [];
snew.reduction_state=       [];
snew.mask_filename=         [];
snew.mask_pixels=           [];
snew.flatfield_filename=    [];
snew.flatfield_pixels=      [];                                                      
snew.flatfieldtype=         [];                                                                               
snew.empty_filename=        [];                             
snew.empty_pixels=          [];                                                                   
snew.empty_transfact=       [];                                                                              
snew.empty_livetime=        [];   
snew.solvent_filename=      [];                             
snew.solvent_pixels=        [];                                                                   
snew.solvent_transfact=     [];                                                                              
snew.solvent_livetime=      []; 
snew.solvent_thickness=     []; 
snew.darkcurrent_filename=  [];                                                                              
snew.darkcurrent_pixels=    [];                                                                      
snew.darkcurrent_livetime=  [];
snew.readoutnoise_filename= [];                                                                              
snew.readoutnoise=          [];                                                                      
snew.zinger_removal=        0;
snew.abs_int_fact=          [];

