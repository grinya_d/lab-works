function argout = raw_center(s, raw_center)
%SAXS/RAW_CENTER Set or return ra_center paramter of the saxs-object
%   raw_center = raw_center(S) returns the center
%
% this function is called from saxsgui.m to update the
% original gui.img.xy saxs object

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.raw_center;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.raw_center = raw_center;
otherwise
    error('I need one or two arguments.')
end
