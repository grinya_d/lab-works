function a = subsref(s, subs)
%SAXS/SUBSREF Handle indexed references to SAXS variables.

a = s;
for sub = subs  % might be multiple levels of indexing
    switch sub.type
        case '()'  % retrieve indexed pixels
            if isa(a, 'saxs')
                a = s.pixels(sub.subs{:});  % index the pixels
            else
                a = a(sub.subs{:});
            end
        case '{}'  % retrieve indexed cells in cell array of SAXS objects?
            a = a{sub.subs{:}};
        case '.'  % retrieve a field
            if isfield(a, sub.subs)
                a = a.(sub.subs);
            else
                error([sub.subs, ' is not a field here.'])
            end
    end
end
