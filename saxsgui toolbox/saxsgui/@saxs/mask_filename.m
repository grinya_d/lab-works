function argout = mask_filename(s, name)
%SAXS/MASK_FILENAME Set or retrieve mask_filename for SAXS image.
%   MASK_FILENAME(S) returns the mask_filename for SAXS image S.
%
%   MASK_FILENAME(S, NAME) returns a SAXS image object copied from S with
%   mask_filename name.

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.mask_filename;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if ischar(name) 
        argout.mask_filename = name;
        if ~isempty(name)
            argout.history = strvcat(argout.history, ['mask_filename ', name]);
        end
    else
        error('The mask_filename is not a string.')
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
