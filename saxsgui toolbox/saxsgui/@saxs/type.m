function argout = type(s, saxstype)
%SAXS/TYPE Set or return the image type of a SAXS image object.
%   IMAGETYPE = TYPE(S) returns the image type, 'xy' or 'rth', of
%   SAXS image S.
%
%   S2 = TYPE(S1, IMAGETYPE) sets the image type, returning the
%   SAXS image object.

switch nargin
case 1
    % It must be a saxs object or we wouldn't be called.
    argout = s.type;
case 2
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    % Just believe whatever image type they tell us.
    argout = s;
    argout.type = saxstype;
    argout.history = strvcat(argout.history, ['type ', saxstype]);
otherwise
    error('I need one or two arguments.')
end
