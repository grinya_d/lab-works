function symval = ellipsymr(obj)
%SAXS/ELLIPSYMR Compute elliptical symmetry of rotated polar SAXS object.
pixels = obj.pixels;
pixelcenter = pixelfrac(obj, [0 0]);

pixels(isnan(pixels)) = 0;

maxbelow = fix(pixelcenter(2));
minabove = maxbelow + 1;
ymax = size(pixels, 1);

maxleft = fix(pixelcenter(1));
minright = maxleft + 1;
xmax = size(pixels, 2);

ynum = min(maxbelow, ymax - minabove + 1);
xnum = min(maxleft, xmax - minright + 1);

% Um, these have to be the same for all values in a run.
% Hm.  Plus what about those nasty NaNs left after rotation?

% Oh and don't forget to mask the inner region.
% Maybe go to polar first, mask inner and outer boundaries...

symy = (pixels(minabove : minabove + ynum - 1, :) - ...
    flipud(pixels(maxbelow - ynum + 1 : maxbelow, :))) .^ 2;

symx = (pixels(:, minright : minright + xnum - 1) - ...
    flipud(pixels(:, maxleft - xnum + 1 : maxleft))) .^ 2;

symval = sum(symy(:)) + sum(symx(:));
