function symvals_out = syms(S, angles)
%SAXS/SYMS Plot symmetry objective values for rotated angles.
%   SYMS(S, ANGLES) plots symmetry objective values for the SAXS image S
%   after rotation by the angles in the vector ANGLES.  Symmetry objective
%   values are computed by ELLIPSYMR.  Angles are in degrees.  Each point
%   is plotted as it is computed.
%
%   SYMVALS = SYMS(...) returns a vector of symmetry objective values.

axe = gca;

symvals(1 : length(angles)) = nan;  % vector of objective values
for iangle = 1 : length(angles)
    symvals(iangle) = ellipsymr(rotatexy(S, angles(iangle)));
    plot(angles, symvals, 'o:', 'Parent', axe)
    drawnow
end

if nargout
    symvals_out = symvals;
end
