function h = errorplot(s, varargin)
%SAXS/PLOT Plot a collapsed (summed or averaged) SAXS image with errorbars

% Generate potential x and y axis data.  We'll only use one.
x = s.xaxisfull;
 
% calibrated
if s.kcal
    distance_units = 'q (A^-^1)';   %was originally K, changed by KJ April 12, 2005
else
    distance_units = 'pixels';
end
%do we know the time, if not assume 1 second
if s.livetime
    if strcmp(s.reduction_state(1:2),'11')
        ylabeltext='Absolute intensity (cm^-1)';
    else
        ylabeltext='average intensity (counts per mr^2 per second)';
    end
    livetime=s.livetime;
else
    ylabeltext='average intensity (counts per mr^2)';
    livetime=1;
end
 
type = s.type;
switch type
    case 'r-ave'
        
        hplot = errorbar(x, s.pixels/livetime, s.pixels/livetime.*s.relerr);% added by KJ April 12, 2005 normalizes;
        % shows aveage number of counts per mr^2 (or pixels) per second
        % originally: hplot = plot(x, s.pixels, varargin{:});
        lastline=findobj(gcf,'Type','line');  %This returns all the line in the plot but last is always element number 1
        set(lastline(1),'UserData',s); %saving saxsobj in data in the UserData variable
        set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
% this seems to work for both new and added lines.
 
        xlabel(['momentum transfer, ', distance_units])
        ylabel(ylabeltext)
    case 'th-ave'
        hplot = errorbar(x, s.pixels/livetime, s.pixels/livetime.*s.relerr);
                                                    % originally: so.pixels
                                                    % = meannum(so.pixels), varargin{:});
        lastline=findobj(gcf,'Type','line');  %This returns all the line in the plot but last is always element number 1
        set(lastline(1),'UserData',s); %saving saxsobj in data in the UserData variable
        set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
        
        xlabel(['azimuth, degrees'])
        ylabel(ylabeltext)
    case 'x-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['x, ', distance_units])
        ylabel(ylabeltext)
    case 'y-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['y, ', distance_units])
        ylabel(ylabeltext)
    otherwise
        error('This is the wrong type of SAXS image for PLOT.  Try IMAGE.')
end


if nargout
    h = hplot;
end



