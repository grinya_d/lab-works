function s = rdivide(arg1, arg2)
%SAXS/RDIVIDE Divide saxs image objects.
%   One or both of the arguments is a saxs image object.
%   The result is a saxs image object after element-by-element
%   division of the pixels.  One argument may be a scalar value.
%   Otherwise the dimensions of the (pixel or other) arrays
%   must match.

quotient = double(arg1) ./ double(arg2);

if isa(arg1, 'saxs')
    s = arg1;
    s.history = strvcat(s.history, logarg('divided by', arg2, inputname(2)));
else  % arg2 must be a saxs object
    s = arg2;
    s.history = strvcat(s.history, logarg('reciprocal under', arg1, inputname(1)));
end
s.pixels = quotient;
