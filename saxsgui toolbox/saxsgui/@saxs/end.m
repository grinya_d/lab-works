function last = end(s, k, n)
%SAXS/END Return last index value of SAXS object
%   END(S, K, N) returns last index value for K-th index out of N indexes
%   into SAXS object S.

switch n
    case 1  % pixels as one vector
        % Assume k is one, too.
        last = length(s.pixels(:));
    case 2  % pixels in two dimensions
        % k is either 1 or 2.
        last = size(s.pixels, k);
    otherwise
        error('SAXS objects have only two dimensions.')
end