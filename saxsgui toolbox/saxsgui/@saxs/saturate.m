function sout = saturate(sin, saturationvalue)
%SAXS/SATURATE Returns SAXS object with indicated saturation value 
%   S_OUT = SATURATE(S_IN,SAT_VAL) returns a SAXS object S_out
%   equivalent to the S_IN, except where pixel values exceed the 
%   saturation value, in which case that pixel has been given the
%   saturation value


sout=sin;
if max(max(sin.pixels))>saturationvalue
    sout.pixels(sin.pixels>saturationvalue)=saturationvalue;
end
