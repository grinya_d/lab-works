function X = xdata(s)
%SAXS/XDATA Return vector of pixel x-coordinates.
%   XDATA(S) returns a vector of x coordinates for pixel columns in
%   SAXS object S.
ncols = size(s.pixels, 2);
xlimits = s.xaxis;
if isempty(xlimits)
    xlimits = [1 ncols];
end
X = [xlimits(1) : (xlimits(2) - xlimits(1)) / (ncols - 1) : xlimits(2)];
