function s = mtimes(arg1, arg2)
%SAXS/MTIMES Multiply a SAXS image by a scalar value.
%   This implements the  *  operator to multiply SAXS
%   images by a scalar value.  It is a synomym for
%   the element-by-element multiply operator  .* .
%   True matrix multiplication is undefined for SAXS
%   images.

s = times(arg1, arg2);
