function so=insert_reduction_state(si,rtype,rvalue)
%SAXS/Insert_reduction_state store reduction states in SAXS object
%this function sets the reduction_type
% so=reduction_type(si,rtype,rvale)
% with rtype=1 being the bit location
% and  rvalue=2 being the bit value
%
so=si;
if rvalue==1
    so.reduction_state(rtype)=char(49);
else
    so.reduction_state(rtype)=char(48);
end


