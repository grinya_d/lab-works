function argout = empty_transfact(s, t)
%SAXS/ENMPTY_TRANSFACT Set or retrieve transmission factor for SAXS image.
%   EMPTY_TRANSFACT(S) returns the transmission factor recorded for SAXS image S.
%
%   EMPTY_TRANSFACT(S, T) returns a SAXS image object copied from S with
%   transmission factor T for the empty holder

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.empty_transfact;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if ~isempty(t)
        if isnumeric(t) && length(t) == 1
            argout.empty_transfact = t;
            argout.history = strvcat(argout.history, ['transfact ', num2str(t)]);
        else
            error('I do not understand this EMPTY_TRANSFACT argument.')
        end
    else
        argout.empty_transfact = [];
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
