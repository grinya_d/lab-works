function Sred=reduction_state_from_files(S)
%SAXS/REDUCTION_STATE Calculate reduction state based on saxs_object values
Sred=S;
if nargin==0
    reduction_state=0;
else
    reduction_state=0;
    if ~isempty(S.mask_filename), reduction_state=reduction_state+8; end
    if ~isempty(S.flatfield_filename), reduction_state=reduction_state+32+16; end
    if ~isempty(S.transfact), reduction_state=reduction_state+64; end
    if ~isempty(S.empty_filename), reduction_state=reduction_state+128; end
    if ~isempty(S.darkcurrent_filename), reduction_state=reduction_state+256; end
    if ~isempty(S.abs_int_fact) && S.abs_int_fact~=1, reduction_state=reduction_state+512; end
    if ~isempty(S.sample_thickness), reduction_state=reduction_state+1024; end
    if ~isempty(S.readoutnoise_filename), reduction_state=reduction_state+3; end
    if (~isempty(S.zinger_removal) && S.zinger_removal==1), reduction_state=reduction_state+2; end
    if ~isempty(S.solvent_filename), reduction_state=reduction_state+1; end
end

if reduction_state==0;
    Sred.reduction_state=[];
else
    Sred.reduction_state=num2str(dec2bin(reduction_state,11)); %reduction_state is a string
end
%Sred.type='rth';
