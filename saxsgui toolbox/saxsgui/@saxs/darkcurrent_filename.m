function argout = darkcurrent_filename(s, name)
%SAXS/darkcurrent_FILENAME Set or retrieve darkcurrent_filename for SAXS image.
%   darkcurrent_FILENAME(S) returns the darkcurrent_filename for SAXS image S.
%
%   darkcurrent_FILENAME(S, NAME) returns a SAXS image object copied from S with
%   darkcurrent_filename name.

switch nargin
case 1  % return transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.darkcurrent_filename;
case 2  % set transfact
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isstr(name)  == 1
        argout.darkcurrent_filename = name;
        if ~isempty(name)
            argout.history = strvcat(argout.history, ['darkcurrent_filename ', name]);
        end
    else
        error('The darkcurrent_filename is not a string.')
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
