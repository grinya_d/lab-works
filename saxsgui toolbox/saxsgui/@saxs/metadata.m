function argout = metadata(s, metadatastructure)
%SAXS/METADATA Set or retrieve metadatastructure for SAXS image.
%   METADATA(S) returns the metadata structure for SAXS image S.
%
%   METADATA(S, METATDATA_STRUCTURE) returns a SAXS image object copied from S with
%   the metadatastructure name.

switch nargin
case 1  % return metadatastructure
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.metadata;
case 2  % set metadatastructure
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isstruct(metadatastructure) 
        argout.metadata = metadatastructure;
    else
        error('The metadata structure is not a structure.')
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
