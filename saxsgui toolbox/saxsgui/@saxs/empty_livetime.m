function argout = empty_livetime(s, t)
%SAXS/ENMPTY_livetime Set or retrieve transmission factor for SAXS image.
%   EMPTY_LIVETIME(S) returns the livetime recorded for SAXS image S.
%
%   EMPTY_LIVETIME(S, T) returns a SAXS image object copied from S with
%   livetime T for the empty holder

switch nargin
case 1  % return livetime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.empty_livetime;
case 2  % set livetime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if ~isempty(t)
        if isnumeric(t) && length(t) == 1
            argout.empty_livetime = t;
            argout.history = strvcat(argout.history, ['livetime ', num2str(t)]);
        else
            error('I do not understand this EMPTY_LIVETIME argument.')
        end
    else
        argout.empty_livetime=[];
    end
otherwise  % we don't understand
    error('I need one or two arguments.')
end
