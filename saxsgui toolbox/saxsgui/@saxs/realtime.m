function argout = realtime(s, real)
%SAXS/REALTIME Retrieve real time for SAXS image.
%   REALTIME(S) returns the real time recorded for SAXS image S.

switch nargin
case 1  % return realtime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s.realtime;
case 2  % set realtime
    if ~ isa(s, 'saxs')
        error('The first argument must be a SAXS image object.')
    end
    argout = s;
    if isnumeric(real) & length(real) == 1
        argout.realtime = real;
    else
        error('I do not understand this REALTIME argument.')
    end
    argout.history = strvcat(argout.history, ['realtime ', num2str(real)]);
otherwise  % we don't understand
    error('I need one or two arguments.')
end
