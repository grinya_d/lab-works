function a = double(s)
%SAXS/DOUBLE Convert a SAXS image to its pixel array.

a = s.pixels;
