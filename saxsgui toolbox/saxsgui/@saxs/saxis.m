function argout = axis(which, s, v)
%SAXS/SAXIS Set or return x or y axis scale for a SAXS image object.
%   SAXIS(N, S) returns the x or y axis scale for SAXS image S.
%   N specifies x or y axis with values 1 or 2, respectively.
%   The return value is a vector of two axis values corresponding
%   to the first and last column or row of the image.
%
%   SAXIS(N, S, V) returns a SAXS image copied from SAXS image S
%   but with a new x or y axis scale.  N selects x or y as above.
%   V is the scale vector of two ascending numbers.

if ~ isa(s, 'saxs')
    error('The second argument must be a SAXS image object.')
end

switch which
    case 1  % x axis
        if nargin == 2  % return axis vector
            argout = s.xaxis;
        else  % set axis vector
            checkvector(v)
            argout = s;
            argout.xaxis = v;
            argout.history = addhist(argout, 'xaxis', v);
        end
    case 2  % y axis
        if nargin == 2  % return axis vector
            argout = s.yaxis;
        else  % set axis vector
            checkvector(v)
            argout = s;
            argout.yaxis = v;
            argout.history = addhist(argout, 'yaxis', v);
        end
    otherwise
        error('First argument must be 1 or 2.')
end


function checkvector(v)
%CHECKVECTOR Subfunction to check axis vectors.
%   CHECKVECTOR(V) returns if V is a numeric vector
%   of two elements in ascending order.  Otherwise
%   CHECKVECTOR calls an error.

if ~ (isnumeric(v) & length(v) == 2 & v(1) < v(2))
    error('Invalid axis vector.')
end


function history = addhist(s, axis, v)
history = [axis, ' ', num2str(v(1)), ' ', num2str(v(2))];
history = strvcat(s.history, history);
