function str = char(s)
%SAXS/CHAR Convert a saxs image object to a displayable character string.

if isempty(s)
    str = 'empty saxs image';
else
    [m n] = size(s.pixels);
    str = sprintf('saxs image:  %d x %d', m, n);
end
