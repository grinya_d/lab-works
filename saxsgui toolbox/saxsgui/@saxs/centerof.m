function c = centerof(s)
%SAXS/CENTEROF Return center pixel coordinates of SAXS object.
%   CENTEROF(S) returns the raw pixel coordinates of the current center for
%   the SAXS object S.  Coordinates are in terms of the current pixel
%   array: [x, y] or equivalently [column, row] where row 1 is at the
%   bottom of the image. If no center has been set or if the center lies
%   outside the current (cropped?) pixel array, CENTEROF returns
%   [NaN,NaN].

c = pixelfrac(s, [0 0]);
