function S=apinputfile_merge(S,if_struct)
% SAXS/APINPUTFILE_MERGE merges data from the input-file into the SAXS
% object created by merging the ap_structure (from the AP-file) and the
% info_structure (from the info-file).

persistent Sold info_old ap_old

%now we step through each parameter in the input-file struct and put it
%into the cor'responding S structure
if_struct_fields=fields(if_struct);
for ii=1:length(length(if_struct_fields))
    if isfield(S,if_struct_fields(ii))
        S.(if_struct_fields(ii))=if_struct.(if_struct_fields(ii));
    end
end

% % Setting the error_norm_fact
% % The error norm factor is a function of the detector and does not change
% % on individual runs therefore the aa1 file is likely to have the most
% % correct value. Only if the image object has a value different from one we
% % will overide the auto-process value
% if (S.error_norm_fact==1)
%     if isfield(ap_struct,'error_calib')
%         if ~isempty(ap_struct.error_calib) &&  ap_struct.error_calib~=0
%             S.error_norm_fact=ap_struct.error_calib;
%         end
%     end
% end
% 
% % now establish the calibration.
% % if the calibration file is the same then simply use the old values.
% % Otherwise if the calib_file_name exist in the ap structure use it
% % if it is not there then the calibration will be in pixels
% 
% if isfield(ap_old,'matlab_calib_file') && isfield(ap_struct,'matlab_calib_file')
%     if strcmp(ap_old.matlab_calib_file,ap_struct.matlab_calib_file)
%         %if the new file is the same as the old file use old values
%         S.calibrationtype=Sold.calibrationtype;
%         S.kcal=Sold.kcal;
%         S.pixelcal=Sold.pixelcal;
%         S.koffset=Sold.koffset;
%         S.wavelength=Sold.wavelength;
%         S.pixelsize=Sold.pixelsize;
%         S.detector_dist=Sold.detector_dist;
%     else
%         if ~isempty(ap_struct.matlab_calib_file)
%             try
%                 load(ap_struct.matlab_calib_file)
%                 if ~ (isempty(k) | isempty(pixelcal) | isempty(koffset))
%                     S = calibrate(S, k, pixelcal, koffset,wavelength,pixelsize,detector_dist,calib_type);
%                 end
%             catch
%             end
%         end
%     end
% end
% % if none of this has yielded anything
% if isempty(S.koffset) && isempty(S.detector_dist)
%     S.calibrationtype=[];
%     S.kcal=[];
%     S.pixelcal=[];
%     S.koffset=[];
%     S.wavelength=[];
%     S.pixelsize=[];
%     S.detector_dist=[];
% end
% 
% % now establish the center
% % if the matlab_center_file file is the same as the old one then simply use the old values.
% % Otherwise if the matlab_center_file exists in the ap structure use it.
% if isfield(ap_old,'matlab_center_file') && isfield(ap_struct,'matlab_center_file')
%     if strcmp(ap_old.matlab_center_file,ap_struct.matlab_center_file)
%         %if the new file is the same as the old file use old values
%         S.raw_center=Sold.raw_center;
%     else
%         if ~isempty(ap_struct.matlab_center_file)
%             try
%                 load(ap_struct.matlab_center_file)
%                 if exist('c')
%                     if ~isempty(c)
%                         S.raw_center = c;
%                     end
%                 end
%             catch
%             end
%         end
%     end
% end
% % if none of this has yielded anything then set center to actual center of image
% if isempty(S.raw_center)
%     S.raw_center=floor(0.5*raw_pixels);
% end
% 
% % now establish the reduction
% % if the matlab_reduction_file file is the same as the old one then simply use the old values.
% % Otherwise if the matlab_reduction_file exists in the ap structure use it.
% if isfield(ap_old,'matlab_reduction_file') && isfield(ap_struct,'matlab_reduction_file')
%     if strcmp(ap_old.matlab_reduction_file,ap_struct.matlab_reduction_file)
%         S.mask_filename_=Sold.mask_filename;
%         S.mask_pixels=Sold.mask_pixels;
%         S.flatfield_filename=Sold.flatfield_filename;
%         S.flatfield_pixels=Sold.flatfield_pixels;
%         S.empty_filename=Sold.empty_filename;
%         S.empty_pixels=Sold.empty_pixels;
%         S.empty_transfact=Sold.empty_transfact;
%         S.empty_livetime=Sold.empty_livetime;
%         S.solvent_filename=Sold.solvent_filename;
%         S.solvent_pixels=Sold.solvent_pixels;
%         S.solvent_transfact=Sold.solvent_transfact;
%         S.solvent_livetime=Sold.solvent_livetime;
%         S.solvent_thickness=Sold.solvent_thickness;
%         S.darkcurrent_filename=Sold.darkcurrent_filename;
%         S.darkcurrent_pixels=Sold.darkcurrent_pixels;
%         S.darkcurrent_livetime=Sold.empty_livetime;
%         S.readoutnoise_filename=Sold.readoutnoise_filename;
%         S.readoutnoise=Sold.readoutnoise;
%         S.zinger_removal=Sold.zinger_removal;
%         S.abs_int_fact=Sold.abs_int_fact;
%         %S.transfact=Sold.transfact;
%         S.sample_thickness=Sold.sample_thickness;
%     else
%         try
%             load(ap_struct.matlab_reduction_file)
%         catch
%             errordlg('reduction configuration file not found')
%             return
%         end
%         if isstruct(reduc_par_temp)
%             reduc_par_temp=clean_reduc_par_temp(reduc_par_temp);
%             S.mask_filename=reduc_par_temp.maskname;
%             S.flatfield_filename=reduc_par_temp.flatfieldname;
%             S.empty_filename=reduc_par_temp.emptyname;
%             S.empty_transfact=reduc_par_temp.empty_trans;
%             %S.solvent_filename=reduc_par_temp.solventname;
%             %S.solvent_transfact=reduc_par_temp.solvent_trans;
%             %S.solvent_thickness=reduc_par_temp.solvent_thickness;
%             S.darkcurrent_filename=reduc_par_temp.darkcurrentname;
%             S.readoutnoise_filename=reduc_par_temp.readoutnoisename;
%             S.abs_int_fact=reduc_par_temp.abs_int_factor;
%             S.transfact=reduc_par_temp.sample_trans;
%             S.sample_thickness=reduc_par_temp.sample_thickness;
%             %now load the image files in
%             S=get_reduction_file(S,'mask');
%             S=get_reduction_file(S,'readoutnoise');
%         else
%             errordlg('reduction configuration file does not have the right format. Save it again')
%             return
%         end
%     end
% end
% 
% %So now we've read in a lot of it..but we still have to resolve potential
% %conflicts betweent the info-file, the ap-file and the possibly listed
% %reduction file
% 
% %1) Which sample transmission factor
% %2) Which background/empty holder
% %3) What should be the background file transmission
% %4) What if info file has a flatfield file
% %5) What if info file has a Dark current file listed
% %6) What if info file has a mask file
% %7) What if info file has read_out noise file
% %8) What if the info file indicates a zinger_removal
% %9) What if the info file has an absolute intensity factor
% %10) What if the info file has a sample thickness indicated
% %11) What if the info file has a solvent file mentioned
% 
% % So problem number 1:
% % If there is a transmission factor in image field use it
% % otherwise use the one in the ap_file
% %
% 
% if isfield(info_struct,'Trans_Factor')
%     S.transfact=info_struct.Trans_Factor;
% else
%     if isfield(ap_struct,'transmission')
%         S.transfact=ap_struct.transmission;
%     else
%         S.transfact=1;
%     end
% end
% if S.transfact>=1 || S.transfact<=0
%     S.transfact=1;
% end
% % So problem number 2: What if  the info file has a background file
% % i.e. an empty file 3: How do we find the transmission
% % We take the value in the info file
% %    if it has an info file with a transfact different from one
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version
% background_transmission=[];
% if isfield(info_struct,'Background_file')
%     if ~isempty(info_struct.Background_file) && exist(info_struct.Background_file)
%         if isfield(info_struct,'background_transmission')
%             background_file=info_struct.Background_file;
%             background_transmission=info_struct.background_transmission;
%         else
%             % look for transmission now does the info file for the background/empty file exist?
%             [pathstr,name,ext] = fileparts(info_struct.Background_file);
%             info_file=[fullfile(pathstr,name),'.info'];
%             file = fopen(info_file, 'rt');
%             if file == -1 % there was no info file and so we cannot use it
%                 warndlg('The background/empty holder file listed in the info file does not have an info file with transmission information. Will ignore this listing')
%             else % proceed to read the info file and use the parameters
%                 empty_info_struct=mpainforead(info_struct.Background_file);
%                 if isempty(empty_info_struct.Trans_Factor)
%                     warndlg('Warning:The transmission of the empty_holder is not listed. Will ignore this background file')
%                 else
%                     if empty_info_struct.Trans_Factor==1
%                         warndlg('Warning:The transmission of the empty_holder is 1. It may be an error!')
%                     end
%                     background_file=info_struct.Background_file;
%                     background_transmission=empty_info_struct.Trans_Factor;
%                 end
%             end
%             fclose(file);
%         end
%     end
% end
% if isempty(background_transmission)  % the background file from the info-file didn't work out.
%     if isfield(ap_struct,'empty_filename')
%         if ~isempty(ap_struct.empty_filename) && exist(ap_struct.empty_filename)
%             if isfield(ap_struct,'empty_transfact')
%                 background_file=ap_struct.empty_filename;
%                 background_transmission=ap_struct.empty_transfact;
%             else
%                 % now does the info file for the background/empty file exist?
%                 [pathstr,name,ext] = fileparts(ap_struct.empty_filename);
%                 info_file=[fullfile(pathstr,name),'.info'];
%                 file = fopen(info_file, 'rt');
%                 if file ~= -1 % there an info file and so we try use it
%                     % proceed to read the info file and use the parameters
%                     empty_info_struct=mpainforead(ap_struct.empty_filename);
%                     if isempty(empty_info_struct.Trans_Factor)
%                         warndlg('Warning:The transmission of the empty_holder is not listed. Will ignore this background file')
%                         ignore_ap=1;
%                     end
%                     if empty_info_struct.Trans_Factor==1
%                         warndlg('Warning:The transmission of the empty_holder is 1. It may be an error!')
%                     end
%                     background_file=ap_struct.empty_filename;
%                     background_transmission=empty_info_struct.Trans_Factor;
%                 end
%                 fclose(file);
%             end
%         end
%     end
% end
% if isempty(background_transmission)  % the background file from the info-file and the ap-file didn't work out.
%     % now there is only infor from the reduction file left to check
%     if exist('reduc_par_temp')
%         if isstruct(reduc_par_temp)
%             if ~strcmp(reduc_par_temp.emptyname,'filename')
%                 background_file=reduc_par_temp.emptyname;
%                 background_transmission=reduc_par_temp.empty_trans;
%             end
%         end
%     end
% end
% if isempty(background_transmission)
%     S.empty_filename=[];
%     S.empty_transfact=[];
% else
%     S.empty_filename=background_file;
%     S.empty_transfact=background_transmission;
% end
% S=get_reduction_file(S,'empty');
% 
% % So problem number 4: What if  the file has a flatfield file
% 
% % We take the value in the info file
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version ig at all
% 
% S.flatfield_filename=[];
% if isfield(info_struct,'Flatfield_file')
%     if ~isempty(info_struct.Flatfield_file) && exist(info_struct.Flatfield_file)
%         S.flatfield_filename=info_struct.Flatfield_file;
%     end
% end
% if isempty(S.flatfield_filename)
%     if isfield(ap_struct,'flatfield_filename')
%         if ~isempty(ap_struct.flatfield_filename) && exist(ap_struct.flatfield_filename)
%             S.flatfield_filename=ap_struct.flatfield_filename;
%         end
%     end
% end
% if isempty(S.flatfield_filename) % now there is only info from the reduction file left to check
%     if exist('reduc_par_temp')
%         if isstruct(reduc_par_temp)
%             if ~strcmp(reduc_par_temp.flatfieldname,'filename') && exist(reduc_par_temp.flatfieldname)
%                 S.flatfield_filename=reduc_par_temp.flatfieldname;
%             end
%         end
%     end
% end
% 
% S=get_reduction_file(S,'flatfield');
% 
% % So problem number 5: What if  the file has a darkcurrent file
% 
% % We take the value in the info file
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version ig at all
% S.darkcurrent_filename=[];
% if isfield(info_struct,'Dark_count_file')
%     if ~isempty(info_struct.Dark_count_file) && exist(info_struct.Dark_count_file)
%         S.darkcurrent_filename=info_struct.Dark_count_file;
%     end
% end
% if isempty(S.darkcurrent_filename)
%     if isfield(ap_struct,'darkcurrent_filename')
%         if ~isempty(ap_struct.darkcurrent_filename) && exist(ap_struct.darkcurrent_filename)
%             S.darkcurrent_filename=ap_struct.darkcurrent_filename;
%         end
%     end
% end
% if isempty(S.darkcurrent_filename)
%     if exist('reduc_par_temp')
%         if isstruct(reduc_par_temp)
%             if ~strcmp(reduc_par_temp.darkcurrentname,'filename') && exist(reduc_par_temp.darkcurrentname)
%                 S.darkcurrent_filename=reduc_par_temp.darkcurrentname;
%             end
%         end
%     end
% end
% 
% S=get_reduction_file(S,'darkcurrent');
% 
% % So problem number %6) What if info file has a mask file
% 
% % We take the value in the info file
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version if at all
% S.mask_filename=[];
% if isfield(info_struct,'mask_filename')
%     if ~isempty(info_struct.mask_filename) && exist(info_struct.mask_filename)
%         S.mask_filename=info_struct.mask_filename;
%     end
% end
% if isempty(S.mask_filename)
%     if isfield(ap_struct,'mask_filename')
%         if ~isempty(ap_struct.mask_filename) && exist(ap_struct.mask_filename)
%             S.mask_filename=ap_struct.mask_filename;
%         end
%     end
% end
% if isempty(S.mask_filename) % now there is only infor from the reduction file left to check
%     if exist('reduc_par_temp')
%         if isstruct(reduc_par_temp)
%             if ~strcmp(reduc_par_temp.maskname,'filename') && exist(reduc_par_temp.maskname)
%                 S.mask_filename=reduc_par_temp.maskname;
%             end
%         end
%     end
% end
% 
% S=get_reduction_file(S,'mask');
% 
% 
% % So problem number %7) What if info file has read_out noise file
% 
% % We take the value in the info file
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version if at all
% S.readoutnoise_filename=[];
% if isfield(info_struct,'readoutnoise_filename')
%     if ~isempty(info_struct.readoutnoise_filename) && exist(info_struct.readoutnoise_filename)
%         S.readoutnoise_filename=info_struct.readoutnoise_filename;
%     end
% end
% if isempty(S.readoutnoise_filename)
%     if isfield(ap_struct,'readoutnoise_filename')
%         if ~isempty(ap_struct.readoutnoise_filename) && exist(ap_struct.readoutnoise_filename)
%             S.readoutnoise_filename=ap_struct.readoutnoise_filename;
%         end
%     end
% end% now there is only infor from the reduction file left to check
% if isempty(S.readoutnoise_filename)
%     if exist('reduc_par_temp')
%         if isstruct(reduc_par_temp)
%             if ~strcmp(reduc_par_temp.readoutnoisename,'filename') && exist(reduc_par_temp.readoutnoisename)
%                 S.readoutnoise_filename=reduc_par_temp.readoutnoisename;
%             end
%         end
%     end
% end
% S=get_reduction_file(S,'readoutnoise');
% 
% %8) What if the info file indicates a zinger_removal
% % In this version the mask file of the info file will be preferred
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version if at all
% S.zinger_removal=[];
% if isfield(info_struct,'zinger_removal')
%     if ~isempty(info_struct.zinger_removal)
%         S.zinger_removal=info_struct.zinger_removal;
%     end
% end
% if isempty(S.zinger_removal)
%     if isfield(ap_struct,'zinger_removal')
%         S.zinger_removal=ap_struct.zinger_removal;
%     end
% end
% if isempty(S.zinger_removal)
%     % now there is only infor from the reduction file left to check
%     if isstruct(reduc_par_temp)
%         S.zinger_removal=reduc_par_temp.zinger_removal;
%     end
% end
% 
% %9) What if the info file indicates an absolute intensity
% % We take the value in the info file
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version if at all
% S.abs_int_fact=[];
% if isfield(info_struct,'abs_int_fact')
%     if ~isempty(info_struct.abs_int_fact)
%         S.abs_int_fact=info_struct.abs_int_fact;
%     end
% end
% if isempty(S.abs_int_fact)
%     if isfield(ap_struct,'abs_int_fact')
%         S.abs_int_fact=ap_struct.abs_int_fact;
%     end
% end
% if isempty(S.abs_int_fact)
%     % now there is only infor from the reduction file left to check
%     if isstruct(reduc_par_temp)
%         S.abs_int_fact=reduc_par_temp.abs_int_fact;
%     end
% end
% 
% 
% 
% %10) What if the info file has a sample thickness indicated
% % We take the value in the info file
% % Otherwise the AP file will be preferred
% % Finally the reduction file_version if at all
% 
% S.sample_thickness=[];
% if isfield(info_struct,'sample_thickness')
%     if ~isempty(info_struct.sample_thickness)
%         S.sample_thickness=info_struct.sample_thickness;
%     end
% end
% if isempty(S.sample_thickness)
%     if isfield(ap_struct,'sample_thickness')
%         S.sample_thickness=ap_struct.sample_thickness;
%     end
% end
% if isempty(S.sample_thickness)
%     % now there is only infor from the reduction file left to check
%     if exist('reduc_par_temp')
%         if isstruct(reduc_par_temp)
%             S.sample_thickness=reduc_par_temp.sample_thickness;
%         end
%     end
% end
% if isempty(S.sample_thickness), S.sample_thickness=1; end
% 
% % So problem number 11: What if  the info file has a solvent file
% % 3: How do we find the transmission,
% % We take the value in the info file 
% %    if it has an info file with a solvent_thickness different from one
% % Otherwise the AP file will be preferred
% 
% solvent_transmission=[];
% if isfield(info_struct,'solvent_filename')
%     if ~isempty(info_struct.solvent_filename) && exist(info_struct.solvent_filename)
%         if isfield(info_struct,'solvent_transmission') & isfield(info_struct,'solvent_thickness')
%             solvent_filename=info_struct.solvent_filename;
%             solvent_transmission=info_struct.solvent_transmission;
%             solvent_thickness=info_struct.solvent_thickness;
%         else
%             % look for transmission now, does the info file for the solvent exist?
%             [pathstr,name,ext] = fileparts(info_struct.solvent_filename);
%             info_file=[fullfile(pathstr,name),'.info'];
%             file = fopen(info_file, 'rt');
%             if file == -1 % there was no info file and so we cannot use it
%                 warndlg('The solvent file listed in the info file does not have an info file with transmission information. Will ignore this listing')
%             else % proceed to read the info file and use the parameters
%                 solvent_info_struct=mpainforead(info_struct.solvent_filename);
%                 if isempty(solvent_info_struct.Trans_Factor)
%                     warndlg('Warning:The transmission of the solvent is not listed. Will ignore this solvent file')
%                 else
%                     if solvent_info_struct.Trans_Factor==1
%                         warndlg('Warning:The transmission of the solvent is 1. It may be an error!')
%                     end
%                     solvent_filename=info_struct.solvent_filename;
%                     solvent_transmission=solvent_info_struct.Trans_Factor;
%                     solvent_thickness=solvent_info_struct.solvent_thickness;
%                 end
%             end
%             fclose(file);
%         end
%     end
% end
% if isempty(solvent_transmission)  % the solvent file from the info-file didn't work out.
%     if isfield(ap_struct,'solvent_filename')
%         if ~isempty(ap_struct.solvent_filename) && exist(ap_struct.solvent_filename) 
%             if isfield(ap_struct,'solvent_transfact') && isfield(ap_struct,'solvent_thickness')
%                 solvent_filename=ap_struct.solvent_filename;
%                 solvent_transmission=ap_struct.solvent_transfact;
%                 solvent_thickness=ap_struct.solvent_thickness;
%             else
%                 % now does the info file for the solvent file exist?
%                 [pathstr,name,ext] = fileparts(ap_struct.solvent_filename);
%                 info_file=[fullfile(pathstr,name),'.info'];
%                 file = fopen(info_file, 'rt');
%                 if file ~= -1 % there an info file and so we try use it
%                     % proceed to read the info file and use the parameters
%                     solvent_info_struct=mpainforead(ap_struct.solvent_filename);
%                     if isempty(solvent_info_struct.Trans_Factor)
%                         warndlg('Warning:The transmission of the solvent file is not listed. Will ignore this file')
%                         ignore_ap=1;
%                     end
%                     if solvent_info_struct.Trans_Factor==1
%                         warndlg('Warning:The transmission of the solventer is 1. It may be an error!')
%                     end
%                     solvent_filename=ap_struct.solvent_filename;
%                     solvent_transmission=solvent_info_struct.Trans_Factor;
%                     solvent_thickness=ap_struct.solvent_thickness;
%                 end
%                 fclose(file);
%             end
%         end
%     end
% end
% 
% if isempty(solvent_transmission)
%     S.solvent_filename=[];
%     S.solvent_transfact=[];
%     S.solvent_thickness=[];
% else
%     S.solvent_filename=solvent_filename;
%     S.solvent_transfact=solvent_transmission;
%     S.solvent_thickness=solvent_thickness;
% end
% S=get_reduction_file(S,'solvent');
% 
% 
% %the feature with the old is not yet used since it may lead to bad effect
% %if people use the same file names.
% 
% ap_old=[];
% info_old=[];
% Sold=[];

