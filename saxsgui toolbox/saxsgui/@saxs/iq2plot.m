function h = iq2plot(s, varargin)
%%SAXS/IQ2PLOT Plot a I versus q^2 plot of 1D SAXS object

% Generate potential x and y axis data.  We'll only use one.
x = s.xaxisfull;

%Karsten Joensen April 26, 2004
%parameters required for calculating errorbars
% x is actually r
% y is thetat

 
% calibrated
if s.kcal
    distance_units = 'q^2 (A^-^2)';   %was originally K, changed by KJ April 12, 2005
else
    distance_units = 'pixels^2';
end
%do we know the time? if not assume 1 second
if s.livetime
    if strcmp(s.reduction_state(1:2),'11')
        ylabeltext='Absolute intensity (cm^-1)';
    else
        ylabeltext='average intensity (counts per mr^2 per second)';
    end
    livetime=s.livetime;
else
    ylabeltext='average intensity (counts per mr^2)';
    livetime=1;
end
 
type = s.type;
switch type
    case 'r-ave'
        hplot = semilogy(x, s.pixels/livetime, varargin{:});% added by KJ April 12, 2005 normalizes;
        % shows aveage number of counts per mr^2 per second
        lastline=findobj(gcf,'Type','line');  %This returns all the line in the plot but last is always element number 1
        set(lastline(1),'UserData',s); %saving saxsobj in data in the UserData variable
        set(lastline(1),'Tag','Data'); %so we can find it later when we want to change display
% this seems to work for both new and added lines.
        axes(get(hplot, 'Parent'));
        xlabel(['(momentum transfer)^2, ', distance_units])
        ylabel(ylabeltext)
        xdata=get(hplot,'Xdata');
        xdatanew=xdata.*xdata;
        set(hplot,'Xdata',xdatanew)
        
    case 'th-ave' % should not happen
        errordlg('This plottype does not apply to Azimuthal averages')
        
% this seems to work for both new and added lines.
        axes(get(hplot, 'Parent'));
        xlabel(['azimuth, degrees'])
        ylabel(ylabeltext)
    case 'x-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['x, ', distance_units])
        ylabel(ylabeltext)
    case 'y-ave'
        hplot = plot(x, s.pixels/livetime, varargin{:});
        axes(get(hplot, 'Parent'));
        xlabel(['y, ', distance_units])
        ylabel(ylabeltext)
    otherwise
        error('This is the wrong type of SAXS image for PLOT.  Try IMAGE.')
end


if nargout
    h = hplot;
end

