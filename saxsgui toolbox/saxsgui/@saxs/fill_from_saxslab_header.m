function so=fill_from_saxslab_header(si)
%SAXS/FILL_FROM_SAXSLAB_HEADER this function inserts the values found in the
%saxlab header into the SAXS structure.
% so=fill_from_saxslab_header(si,pixels);
%global reduc_par_temp
so=si;
if isempty(si.reduction_state)
    so.reduction_state='00000000000';
end
if isfield(si.metadata,'sample_transfact')
    if ~isempty(si.metadata.sample_transfact)
        so.transfact=si.metadata.sample_transfact;
    end
end
if isfield(si.metadata,'beamcenter_nominal')
    if ~isempty(si.metadata.beamcenter_nominal)
        so=setcenter(so,[si.metadata.beamcenter_nominal(1)+1,si.metadata.beamcenter_nominal(2)+1]);
    end
end
if isfield(si.metadata,'beamcenter_nominal')
    if ~isempty(si.metadata.beamcenter_nominal)
        so.raw_center=[si.metadata.beamcenter_nominal(1)+1,si.metadata.beamcenter_nominal(2)+1];
    end
end
if isfield(si.metadata,'calibrationtype')
    if ~isempty(si.metadata.calibrationtype)
        so.calibrationtype=si.metadata.calibrationtype;
    end
end
if isfield(si.metadata,'sample_transfact')
    
    if ~isempty(si.metadata.sample_transfact)
        if (si.metadata.sample_transfact~=0) && isnumeric(si.metadata.sample_transfact)
            so.transfact=si.metadata.sample_transfact;
            so.reduction_state(5)=char(49); % that a 1
            reduc_par_temp.sample_trans=si.metadata.sample_transfact;
        else
            so.transfact=[];
            so.reduction_state(5)=char(48); % that a 1
            reduc_par_temp.sample_trans=[];
        end
    end
end
if isfield(si.metadata,'wavelength')
    if ~isempty(si.metadata.wavelength)
        so.wavelength=si.metadata.wavelength;
    end
end
if isfield(si.metadata,'detector_dist')
    if ~isempty(si.metadata.detector_dist)
        so.detector_dist=si.metadata.detector_dist+35;
    end
end
if isfield(si.metadata,'pixelsize')
    if ~isempty(si.metadata.pixelsize)
        so.pixelsize=si.metadata.pixelsize;
    end
end
try
    so = calibrate(so, so.kcal, so.pixelcal, so.koffset, so.wavelength, so.pixelsize, so.detector_dist, so.calibrationtype);
catch
    1;
end

