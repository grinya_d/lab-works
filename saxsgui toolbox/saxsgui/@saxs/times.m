function s = times(arg1, arg2)
%SAXS/TIMES Array multiply for SAXS image objects.
%   One or both of the arguments is a saxs image object.
%   The result is a saxs image object after array (element-by-element)
%   multiplication of the pixels.  One argument may be a scalar value.
%   Otherwise the dimensions of the (pixel or other) arrays
%   must match.

product = double(arg1) .* double(arg2);

if isa(arg1, 'saxs')
    s = arg1;
    s.history = strvcat(s.history, logarg('times', arg2, inputname(2)));
else  % arg2 must be a saxs object
    s = arg2;
    s.history = strvcat(s.history, logarg('times', arg1, inputname(1)));
end
s.pixels = product;
