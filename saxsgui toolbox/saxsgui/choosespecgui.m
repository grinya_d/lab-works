function varargout = choosespecgui(varargin)
% CHOOSESPECGUI M-file for choosespecgui.fig
%      CHOOSESPECGUI, by itself, creates a new CHOOSESPECGUI or raises the existing
%      singleton*.
%
%      H = CHOOSESPECGUI returns the handle to a new CHOOSESPECGUI or the handle to
%      the existing singleton*.
%
%      CHOOSESPECGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHOOSESPECGUI.M with the given input arguments.
%
%      CHOOSESPECGUI('Property','Value',...) creates a new CHOOSESPECGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before choosespecgui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to choosespecgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help choosespecgui

% Last Modified by GUIDE v2.5 28-Oct-2006 01:04:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @choosespecgui_OpeningFcn, ...
                   'gui_OutputFcn',  @choosespecgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before choosespecgui is made visible.
function choosespecgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to choosespecgui (see VARARGIN)


% put the spec-log file array (scanarray) in a variable we can recognize
handles.scanarray=varargin{1};
% Choose default command line output for choosespecgui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%inserting the available scan-numbers
set(handles.ChooseScanNumber,'String',handles.scanarray.scannum)
%update the whole gui with values from Scan=1 and point=1
update_gui(handles,1);
% UIWAIT makes choosespecgui wait for user response (see UIRESUME)
uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = choosespecgui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
uiresume(handles.figure1)
varargout{1}=handles.output;
close(handles.figure1)




% --- Executes during object creation, after setting all properties.
function ChooseScanNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChooseScanNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function ChooseScanPoint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChooseScanPoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ScanDetails_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ScanDetails (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AcceptChoice.
function AcceptChoice_Callback(hObject, eventdata, handles)
% hObject    handle to AcceptChoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)

% --- Executes on button press in Cancel.
function Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to Cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)
handles.output=[];
guidata(handles.figure1,handles)


% --- Executes on selection change in ChooseScanNumber.
function ChooseScanNumber_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseScanNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns ChooseScanNumber contents as cell array
%        contents{get(hObject,'Value')} returns selected item from

scannum=get(handles.ChooseScanNumber,'Value');
update_gui(handles,scannum);

% --- Executes on selection change in ChooseScanPoint.
function ChooseScanPoint_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseScanPoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
scan_index=get(handles.ChooseScanPoint,'Value');
scannum=get(handles.ChooseScanNumber,'Value');
update_image(handles,scannum,scan_index);

% Hints: contents = get(hObject,'String') returns ChooseScanPoint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChooseScanPoint

%************************************************
function strcell=numvec2strcell(numvector)
strcell=cell(1,length(numvector));
for ii=1:length(numvector)
    strcell{1,ii}=num2str(numvector(ii));
end
%************************************************
function outcell=ConstructScanDetails(scanarray,scanpoint)

outcell=cell(1,1+1+length(scanarray.data{scanpoint}));
outcell{1,1}=scanarray.scanline{scanpoint};
outcell{1,2}=scanarray.nameline{scanpoint};
outcell(1,3:end)=scanarray.data{scanpoint};

%************************************************
function saxs_image_pathname=get_saxs_image_name_by_fileroot(handles,scannum,scanpoint)

global spec_saxsimage_dir
scanfilename=[handles.scanarray.logfilename{1},'_',num2str(handles.scanarray.scannum{scannum}),'_',num2str(scanpoint),'.*'];

scanfile=dir([spec_saxsimage_dir,scanfilename]);

if isempty(scanfile)
    errordlg('No image files found for this scanpoint','modal')
else
    saxs_image_pathname=[spec_saxsimage_dir,scanfile(1).name];
end

%************************************************
function update_gui(handles,scannum)

%making a string cell of the available scan-points for the 1st scan-number
scanpoints=numvec2strcell([0:length(handles.scanarray.data{scannum})-1]);
%inserting the available scan-points for the scan-number
set(handles.ChooseScanPoint,'String',scanpoints)

%making a string cell the available info on the scan-number
scandetails=ConstructScanDetails(handles.scanarray,scannum);
set(handles.ScanDetails,'String',scandetails)

set(handles.ChooseScanPoint,'Value',1)
update_image(handles,scannum,1)


%************************************************
function update_image(handles,scannum,scan_index)
%finding the imagefile on the 1st first point in the first scan-number
saxsimage_pathname=get_saxs_image_name_by_fileroot(handles,scannum,scan_index);
%loading the data
try
    s=getsaxs(saxsimage_pathname);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading image file', 'modal')
    s = [];
end
warning off all
imagesc(log(s.pixels),'Parent',handles.ImagePreview)
set(handles.ImagePreview,'XTicklabel','')
set(handles.ImagePreview,'YTicklabel','')
set(handles.ImagePreview,'UserData',s)
handles.output=s;
guidata(handles.figure1,handles)



