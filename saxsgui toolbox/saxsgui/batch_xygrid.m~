function varargout = batch_xygrid(varargin)
% BATCH_XYGRID M-file for batch_xygrid.fig
%      BATCH_XYGRID, by itself, creates a new BATCH_XYGRID panel or raises the existing
%      singleton*.
%
%      H = BATCH_xygrid returns the handle to a new BATCH_xygrid or the handle to
%      the existing singleton*.
%
%      BATCH_xygrid('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BATCH_xygrid.M with the given input arguments.
%
%      BATCH_xygrid('Property','Value',...) creates a new BATCH_xygrid or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before batch_xygrid_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to batch_xygrid_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help batch_xygrid

% Last Modified by GUIDE v2.5 16-Jun-2010 10:38:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @batch_xygrid_OpeningFcn, ...
    'gui_OutputFcn',  @batch_xygrid_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before batch_xygrid is made visible.
function batch_xygrid_OpeningFcn(hObject, eventdata, handles, varargin)
global batch_xygrid_temp
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to batch_xygrid (see VARARGIN)

set(hObject,'Name','Batch Panel: XY Grid' )
batch_xygrid_temp=batch_xygrid_init; % gets initial values for GUI
batch_xygrid_GUIupdate(handles); %updates GUI

% we'll store the template SAXS structure used as a template in the title  "UserData"

set(handles.text22,'UserData',varargin{1})


% Choose default command line output for batch_xygrid
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes batch_xygrid wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = batch_xygrid_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout = {get(handles.StartBatch,'UserData')};



% --- Executes during object creation, after setting all properties.
function Xnum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Xnum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function Xnum_Callback(hObject, eventdata, handles)
% hObject    handle to Xnum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Xnum as text
%        str2double(get(hObject,'String')) returns contents of Xnum as a double
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes during object creation, after setting all properties.
function Ynum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ynum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function Ynum_Callback(hObject, eventdata, handles)
% hObject    handle to Ynum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Ynum as text
%        str2double(get(hObject,'String')) returns contents of Ynum as a double
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in ChooseFiles.
function ChooseFiles_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% here we choose the files to reduce
[path,names]=getfiles('Choose the files you want to reduce');
files.path=path;
files.names=names;
%save files-structure to the buttons 'UserData' parameter
set(hObject,'UserData',files);
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in cancel.
function Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(gcf)


% --- Executes on button press in StartBatch.
function StartBatch_Callback(hObject, eventdata, handles)
% hObject    handle to StartBatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global multiple_file_run

multiple_file_run=1;
files=get(handles.ChooseFiles,'UserData'); % this loads the names of the files to process
if ~exist(files.path)
    warndlg('No input files chosen')
    return
end
if isempty(files.path)
    warndlg('Input files are not valid')
    return
end

%Getting template data
S_template=get(handles.text22,'UserData'); 

% now we need to allow the user to put them in an appropriate directory
%One could argue this should go in the panel but we leave it out for now
outputpath=uigetdir(pwd,'Pick a directory for the output files');
if outputpath==0, outputpath=pwd; end
outputpath=[outputpath,filesep];

if str2num(S_template.reduction_state(5))==0 % if sample transmission is not to be used
    transmission=1;
else
    transmission=str2num(get(handles.Xnum,'String'));
end

if str2num(S_template.reduction_state(1))==0 % if sample thickness is to not to be used
    thickness=1;
else
    thickness=str2num(get(handles.Ynum,'String'));
end

%now to begin looping 
for ii=1:length(files.names)
    filename=[files.path files.names{ii}];    
    
    hstatus = figstatus(['Reducing: ',files.names{ii}],get(hObject,'Parent'));  % tell user what to do in a status bar
    [S,Sred]=reduce_templatewise(filename,S_template,transmission,thickness);
    %this is to ensure that xaxis and yaxis are also calibrated in 2D data
    %however there is a bug in the program since it does not show the
    %reduced image (pixel-by-pixel)
    S=calibrate(S,S.kcal,S.pixelcal,S.koffset,[],[],[],'std'); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
    S=center(S,S.raw_center); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
    delete(hstatus)
    
    X = Sred.xaxisfull;
    Y = Sred.pixels/Sred.livetime; 
    E=Sred.relerr.*Y;
    
    %now for various per-file-output options
    % saving 2D images
    if get(handles.Save2DImages,'Value')==1
        tag='2D';
        tag2=get(handles.DescriptiveTagString,'String');
        [pathstr,name,ext,versn] = fileparts(S.raw_filename);
        file=[outputpath,name,'_',tag,'_',tag2,'.jpg'];
        figure2D=findobj(get(0,'Children'),'Tag','2D SAXS image');
        if isempty(figure2D)
            figure2D=figure;
            set(figure2D,'Tag','2D SAXS image')
        end
        figure(figure2D)
        image(log(S))
        hax=gca;
        title([file,'(log intensity scale)'])

        set(hax,'XGrid','on')
        set(hax,'YGrid','on')
        colorbar
        warning off, drawnow, warning on
        print(figure2D,'-djpeg',file)
    end
    
    % saving 1D spectra
    if get(handles.Save1DFigures,'Value')==1
        tag='1D';
        tag2=get(handles.DescriptiveTagString,'String');
        [pathstr,name,ext,versn] = fileparts(S.raw_filename);
        file=[outputpath,name,'_',tag,'_',tag2,'.jpg'];
        figure1D=findobj(get(0,'Children'),'Tag','1D SAXS spectrum');
        if isempty(figure1D)
            figure1D=figure;
            set(figure1D,'Tag','1D SAXS spectrum')
        end
        figure(figure1D)
        image(Sred)
        title([file,''])
        hplot=get(figure1D,'Children');
        set(hplot,'Xscale','lin')
        set(hplot,'Yscale','log')
        warning off, drawnow, warning on
        print(figure1D,'-djpeg',file)
    end
    
    %saving to separate files
    if get(handles.ReducedDataToSeparateFiles,'Value')==1
        tag=get(handles.DescriptiveTagString,'String');
        [pathstr,name,ext,versn] = fileparts(S.raw_filename);
        file=[outputpath,name,'_',tag,'.csv'];
        if get(handles.xyformat,'Value')==1
            data = [X(:), Y(:)];
        else
            if length(Y)==length(E)
                data = [X(:), Y(:), E(:)];
            else
                data = [X(:), Y(:)];
            end
        end
        dlmwrite(file, data, ',')
    end
    
    %preparing data to save to same file (without errorbars)
    if get(handles.ReducedDataToSameFile,'Value')==1
        if get(handles.xyformat,'Value')==1
            if ii==1
                datagrand=zeros(length(files.names)+1,length(X));
                tag=[get(handles.DescriptiveTagString,'String'),'_combined_'];
                [pathstr,name,ext,versn] = fileparts(S.raw_filename);
                file=[outputpath,name,'_',tag,'.csv'];
                datagrand(ii,:)=[X];
            end
            datagrand(ii+1,:)=[Y];
        else
            if ii==1
                datagrand=zeros(length(files.names)+1,2*length(X));
                tag=[get(handles.DescriptiveTagString,'String'),'_combined_'];
                [pathstr,name,ext,versn] = fileparts(S.raw_filename);
                file=[outputpath,name,'_',tag,'.csv'];
                datagrand(ii,:)=[X X];
            end
            datagrand(ii+1,:)=[Y E];
        end
    end
    batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure    
end

% now to do the things that are batch-wide outputs

% first saving the file with everything
if get(handles.ReducedDataToSameFile,'Value')==1
    tag=[get(handles.DescriptiveTagString,'String'),'_combined_'];
    [pathstr,name,ext,versn] = fileparts(S.raw_filename); % filename will be associated with the last file in the list
    file=[outputpath,name,'_',tag,'.csv'];
    dlmwrite(file, datagrand', ',') 
end


multiple_file_run=0;





% --- Executes on button press in ReducedDataToSeparateFiles.
function ReducedDataToSeparateFiles_Callback(hObject, eventdata, handles)
% hObject    handle to ReducedDataToSeparateFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ReducedDataToSeparateFiles
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in ReducedDataToSameFile.
function ReducedDataToSameFile_Callback(hObject, eventdata, handles)
% hObject    handle to ReducedDataToSameFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ReducedDataToSameFile
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in xydeltayformat.
function xydeltayformat_Callback(hObject, eventdata, handles)
% hObject    handle to xydeltayformat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of xydeltayformat
if get(hObject,'Value')==1
    set(handles.xyformat,'Value',0)
else
    set(handles.xyformat,'Value',1)
end
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in xyformat.
function xyformat_Callback(hObject, eventdata, handles)
% hObject    handle to xyformat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of xyformat
if get(hObject,'Value')==1
    set(handles.xydeltayformat,'Value',0)
else
    set(handles.xydeltayformat,'Value',1)
end
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in Save1DFigures.
function Save1DFigures_Callback(hObject, eventdata, handles)
% hObject    handle to Save1DFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save1DFigures
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in Save2DImages.
function Save2DImages_Callback(hObject, eventdata, handles)
% hObject    handle to Save2DImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Save2DImages
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes during object creation, after setting all properties.
function DescriptiveTagString_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DescriptiveTagString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function DescriptiveTagString_Callback(hObject, eventdata, handles)
% hObject    handle to DescriptiveTagString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DescriptiveTagString as text
%        str2double(get(hObject,'String')) returns contents of DescriptiveTagString as a double
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure


% --------------------------------------------------------------------
function Save_Callback(hObject, eventdata, handles)
global batch_xygrid_temp
% hObject    handle to Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uiputfile('*.mat', 'Pick a file to save the parameters to');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    save(fullfilename,'batch_xygrid_temp')
end

% --------------------------------------------------------------------
function Load_Callback(hObject, eventdata, handles)
global batch_xygrid_temp
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat','Load xygrid parameter file ');
if isstr(filename)
    fullfilename=fullfile(pathname,filename);
    load(fullfilename)
    batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure
end

% --- Executes on button press in DefaultValues.
function DefaultValues_Callback(hObject, eventdata, handles)
global batch_xygrid_temp

% hObject    handle to DefaultValues (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

batch_xygrid_temp=[];
batch_xygrid_temp=batch_xygrid_init; % gets initial values for GUI
batch_xygrid_GUIupdate(handles); %updates GUI

% --- Executes on button press in Save_Special.
function Test_Files_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Special (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global multiple_file_run
global batch_xygrid_temp
global xygrid
rdt=batch_xygrid_temp;

multiple_file_run=1;
files=get(handles.ChooseFiles,'UserData'); % this loads the names of the files to process
if ~exist(files.path)
    warndlg('No input files chosen')
    return
end
if isempty(files.path)
    warndlg('Path to files are not valid')
    return
end
files=get(handles.ChooseFiles,'UserData');
if (length(files.names)~=rdt.Xnum*rdt.Ynum)
    uiwait(warndlg([{'Number of Chosen files and Grid-Dimensions don"t macht'},...
        {'That" OK...will just omit extra files or leave extra pixels empty'}],...
        'Filenumber Warning','modal'))
end
for ii=1:length(files.names)
    filename=[files.path files.names{ii}];    
    
    hstatus = figstatus(['Checking files for position and transmission data: ',...
        num2str(ii),': ',files.names{ii}],get(hObject,'Parent'));  % tell user what to do in a status bar
    result=check_files_for_PT(filename);
    if result==0
       warndlg('file.names{ii} does not have position or transmission information',...
           'File-meta content warning','modal')
       break
    end
    delete(hstatus)
end
Read_Meta_Data(handles,hObject)
% show Xpositions
h=figure;
set(h,'Name','XY Grid Test')
subplot(1,3,1)
imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.xposmm)
colorbar('EastOutside')
xlabel('Position(mm)')
ylabel('Position(mm)')
title('Recorded X-pos vs Pixel Position')
colormap('hot')
subplot(1,3,2)
imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.yposmm)
colorbar('EastOutside')
xlabel('Position(mm)')
ylabel('Position(mm)')
title('Recorded X-pos vs Pixel Position')
colormap('hot')
subplot(1,3,3)
imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.trans)
colorbar('EastOutside')
xlabel('Position(mm)')
ylabel('Position(mm)')
title('Recorded Transmission vs Pixel Position')
colormap('hot')

uiwait(msgbox([{'Files look OK. But please inspect the images'},...
    {'to ensure X, Y and transmissions are correct.'}]))

function result=check_files_for_PT(filename)
% this function checks for position information and transmission
% information
result=0;
[pathstr,name,ext] = fileparts(filename);
if (strcmp(ext,'.mpa')) % is mpa file look for .info file in same dir
    infofilename=fullfile(pathstr,[name,'.info']);
    if exist(infofilename,'file')
        result=1;
    end
elseif (ext=='tiff') % is possibly Pilatus file and then look for information 
        %inside the tiff file
        %not implemented
end
    
    

% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





% --- Executes on button press in show_orientation.
function Show_Transm_Callback(hObject, eventdata, handles)
% hObject    handle to show_orientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of show_orientation
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in Show_SAXS_Int.
function Show_SAXS_Int_Callback(hObject, eventdata, handles)
% hObject    handle to Show_SAXS_Int (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Show_SAXS_Int
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in ShowQuiver.
function show_orientation_Callback(hObject, eventdata, handles)
% hObject    handle to ShowQuiver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ShowQuiver
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in ShowQuiver.
function ShowQuiver_Callback(hObject, eventdata, handles)
% hObject    handle to ShowQuiver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ShowQuiver
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure



function qmin_Callback(hObject, eventdata, handles)
% hObject    handle to qmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of qmin as text
%        str2double(get(hObject,'String')) returns contents of qmin as a double
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function qmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qmax_Callback(hObject, eventdata, handles)
% hObject    handle to qmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of qmax as text
%        str2double(get(hObject,'String')) returns contents of qmax as a double
batch_xygrid_struct_update(handles) %updates the structure based on the input in GUI
batch_xygrid_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function qmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in Show_Special.
function Read_Meta_Data(handles,hObject)
% hObject    handle to Show_Special (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global batch_xygrid_temp
global multiple_file_run
global xygrid
multiple_file_run=1;


rdt=batch_xygrid_temp;
%First make room for grid
xygrid.xposint=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.yposint=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.xposmm=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.yposmm=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.trans=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.orient=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.direction=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.sumint=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.var1=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
xygrid.var2=zeros(floor(rdt.Ynum),floor(rdt.Xnum));
% now begin to populate this:
for ii=1:floor(rdt.Xnum)
    xygrid.xposint(:,ii)=ii;
end
for ii=1:floor(rdt.Ynum)
    xygrid.yposint(ii,:)=ii;
end
% the rest are populated from the files
files=get(handles.ChooseFiles,'UserData'); % this loads the names of the files to process
if isempty(files.path)
    warndlg('Path to files are not valid')
    return
end
%first lets get positions and transmissions
ix=1;
iy=1;
for ii=1:length(files.names)
    hstatus = figstatus(['Reading position and transmission data: ',...
        num2str(ii),': ',files.names{ii}],get(hObject,'Parent'));  % tell user what to do in a status bar
    filename=[files.path files.names{ii}];    
    info_struct=mpainforead(filename);
    xygrid.xposmm(iy,ix)=info_struct.SamplePositionMM(1);
    xygrid.yposmm(iy,ix)=info_struct.SamplePositionMM(2);
    xygrid.trans(iy,ix)=info_struct.PhotoDiode(1);
    delete(hstatus)
    ix=ix+1;
    if ix>rdt.Xnum
       ix=1;
       iy=iy+1;
    end
    if (iy>rdt.Ynum) 
        break
    end 
end

function Show_Special_Callback(hObject, eventdata, handles)
% hObject    handle to Show_Special (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global batch_xygrid_temp
global multiple_file_run
global xygrid
multiple_file_run=1;

rdt=batch_xygrid_temp;
Read_Meta_Data(handles,hObject)
files=get(handles.ChooseFiles,'UserData'); % this loads the names of the files to process

if rdt.Show_Transm==1
    h=figure;
    set(h,'Name','XY Grid: PhotoDiode (~transmission)')
    imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.trans)
    colorbar('EastOutside')
    colormap('hot')
    xlabel('Position(mm)')
    ylabel('Position(mm)')
    title('Avg. Photodiode Reading vs Position')
end

%Getting template data
S_template=get(handles.text22,'UserData');
% then let's get integrated intensities and orientations
% templatewise...
if (rdt.Show_SAXS_Int==1 || rdt.ShowQuiver==1)

    %now to begin looping
    ix=1;
    iy=1;
    transmission=1;
    thickness=1;
    for ii=1:length(files.names)
        filename=[files.path files.names{ii}];
        
        hstatus = figstatus(['Reducing (I vs. q): ',files.names{ii}],get(hObject,'Parent'));  % tell user what to do in a status bar
        [S,Sred]=reduce_templatewise(filename,S_template,transmission,thickness);
        %this is to ensure that xaxis and yaxis are also calibrated in 2D data
        %however there is a bug in the program since it does not show the
        %reduced image (pixel-by-pixel)
        S=calibrate(S,S.kcal,S.pixelcal,S.koffset,[],[],[],'std'); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
        S=center(S,S.raw_center); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
        delete(hstatus)
        
        X = Sred.xaxisfull;
        Y = Sred.pixels/Sred.livetime;
        E=Sred.relerr.*Y;
        
        %now calculating the integrated intensity
        jj=1;
        sumint=0;
        for jj=1:length(X-1)
            if (X(jj)<rdt.qmax && X(jj)>rdt.qmin)
                sumint=Y(jj)*(X(jj+1)-X(jj));
            end
        end
        xygrid.sumint(iy,ix)=sumint;
        
        ix=ix+1;
        if ix>rdt.Xnum
            ix=1;
            iy=iy+1;
        end
        if (iy>rdt.Ynum)
            break
        end
    end
end
%if rdt.Show_SAXS_int==1 || rdt.show_orientation==0 || rdt.ShowQuiver)
if (rdt.Show_SAXS_Int==1)
    hint=figure;
    set(hint,'Name','XY Grid: Integrated Intensity')
    imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.sumint)
    colorbar('EastOutside')
    colormap('hot')
    xlabel('Position(mm)')
    ylabel('Position(mm)')
    title([{'Integrated Intensity vs Position'}, {['qmin=',num2str(rdt.qmin),...
        ' qmax=',num2str(rdt.qmax)]}])
end

if (rdt.show_orientation==1 || rdt.ShowQuiver==1)
    % now we have to reduce the data for I vs phi
    
    %begin looping over the files
    ix=1;
    iy=1;
    transmission=1;
    thickness=1;
    for ii=1:length(files.names)
        filename=[files.path files.names{ii}];
        %chaning to phi average, over qmin,qmax,0,360
        S_templatephi=type(S_template,'th-ave');
        S_templatephi=xaxis(S_templatephi,[rdt.qmin,rdt.qmax]);
        S_templatephi=yaxis(S_templatephi,[0,360]);
        S_templatephi=xaxisfull(S_templatephi,[1:40]);
        
        
        hstatus = figstatus(['Reducing(I vs. phi): ',files.names{ii}],get(hObject,'Parent'));  % tell user what to do in a status bar
        [S,Sred]=reduce_templatewise(filename,S_templatephi,transmission,thickness);
        %this is to ensure that xaxis and yaxis are also calibrated in 2D data
        %however there is a bug in the program since it does not show the
        %reduced image (pixel-by-pixel)
        S=calibrate(S,S.kcal,S.pixelcal,S.koffset,[],[],[],'std'); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
        S=center(S,S.raw_center); %this is to ensure that xaxis and yaxis are also calibrated in 2D data
        delete(hstatus)
        
        PHI = Sred.xaxisfull;
        Y = Sred.pixels/Sred.livetime;
        E=Sred.relerr.*Y;
        % now to find orientation
        % first add the 2 halves together and get rid of the beamstop arm
        Iphi(1:floor(length(PHI)/2))=0;
        for jj=1:floor(length(PHI)/2)
            Iphi(jj)=Y(jj);
            if PHI(jj+floor(length(PHI)/2))<300; % without beasmtop arm
                Iphi(jj)=(Iphi(jj)+Y(jj+floor(length(PHI)/2)))/2;
            end
        end
        %maximum value and position
        Imax=max(Iphi);
        jjmax=find(Iphi==Imax,1);
        phimax=PHI(jjmax);
        if phimax>=90
            jjmin=jjmax-floor(length(Iphi)/2);
        else
            jjmin=jjmax+floor(length(Iphi)/2);
        end
        %phimin=PHI(jjmin);
        Imin=Iphi(jjmin);
        
        %now save the result
        xygrid.orient(iy,ix)=(Imax-Imin)/(Imin+Imax);
        xygrid.direction(iy,ix)=phimax;
        if ((xygrid.trans(iy,ix)<0.2*max(max(xygrid.trans))) && (xygrid.sumint(iy,ix)<0.2*max(max(xygrid.sumint))))
            xygrid.orient(iy,ix)=0;
            xygrid.direction(iy,ix)=0;
        end

        %now on to next file
        ix=ix+1;
        if ix>rdt.Xnum
            ix=1;
            iy=iy+1;
        end
        if (iy>rdt.Ynum)
            break
        end
    end
end
if (rdt.show_orientation==1)
    hshor=figure;
    set(hshor,'Name','XY Grid: Degree of Orientation (max-min)/(max+min)')
    imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.orient)
    colorbar('EastOutside')
    colormap('hot')
    xlabel('Position(mm)')
    ylabel('Position(mm)')
    title([{'Degree of Orientation vs Position'}, {['qmin=',num2str(rdt.qmin),...
        ' qmax=',num2str(rdt.qmax)]}])
end
if (rdt.ShowQuiver==1)
    %first plot against transmission
    h=figure;
    set(h,'Name','XY Grid Scan')
    imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.trans)
    colorbar('EastOutside')
    colormap('hot')
    xlabel('Position(mm)')
    ylabel('Position(mm)')
    hold on
    [X,Y]=meshgrid(xygrid.xposmm(1,:),xygrid.yposmm(:,1));
    U=xygrid.orient.*cos(xygrid.direction*pi()/180);
    V=xygrid.orient.*sin(xygrid.direction*pi()/180);
    quiver(X,Y,U,V,max(max(xygrid.orient)),'b','LineWidth',2)
    hold off
    title([{'Orientation vs Transmission and Position'},{[' qmin=',num2str(rdt.qmin),...
        ' qmax=',num2str(rdt.qmax),'  Max Deg of Orientation= ',num2str(max(max(xygrid.orient)))]}])
    %then plot against SAXS intensity
    h=figure;
    set(h,'Name','XY Grid Scan')
    imagesc(xygrid.xposmm(1,:),xygrid.yposmm(:,1),xygrid.sumint)
    colorbar('EastOutside')
    colormap('hot')
    xlabel('Position(mm)')
    ylabel('Position(mm)')
    hold on
    [X,Y]=meshgrid(xygrid.xposmm(1,:),xygrid.yposmm(:,1));
    U=xygrid.orient.*cos(xygrid.direction*pi()/180);
    V=xygrid.orient.*sin(xygrid.direction*pi()/180);
    quiver(X,Y,U,V,max(max(xygrid.orient)),'b','LineWidth',2)
    hold off
    title([{'Orientation vs Integrated Int. and Position'},{[' qmin=',num2str(rdt.qmin),...
        ' qmax=',num2str(rdt.qmax),'  Max Deg of Orientation= ',num2str(max(max(xygrid.orient)))]}])
end


% --- Executes on button press in Save_Special.
function Save_Special_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Special (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xygrid

[filename, pathname] = uiputfile('*.txt', 'Save XY scan speciel data as..');
fid=fopen(fullfile(pathname,filename),'w');
if (fid<1)
    return
end
    
tmpstr=['xpos,ypos,xpos in mm,ypos in smm,transmission,integrated intensity, orientation,degree of orientation'];
fprintf(fid,'%s \n',tmpstr);
for ii=1:length(xygrid.xposint(:,1))
    for jj=1:length(xygrid.xposint(1,:))
        tmpstr=sprintf('%i,%i,%g,%g,%g,%g,%g,%g',...
            xygrid.xposint(ii,jj),...
            xygrid.yposint(ii,jj),...
            xygrid.xposmm(ii,jj),...
            xygrid.yposmm(ii,jj),...
            xygrid.trans(ii,jj),...
            xygrid.sumint(ii,jj),...
            xygrid.orient(ii,jj),...
            xygrid.direction(ii,jj));
        fprintf(fid,'%s \n',tmpstr);
    end
end
fclose(fid);
                     
        






