function varargout = ffm_parameters(varargin)
% FFM_PARAMETERS M-file for ffm_parameters.fig
%      FFM_PARAMETERS, by itself, creates a new FFM_PARAMETERS or raises the existing
%      singleton*.
%
%      H = FFM_PARAMETERS returns the handle to a new FFM_PARAMETERS or the handle to
%      the existing singleton*.
%
%      FFM_PARAMETERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FFM_PARAMETERS.M with the given input arguments.
%
%      FFM_PARAMETERS('Property','Value',...) creates a new FFM_PARAMETERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ffm_parameters_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ffm_parameters_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ffm_parameters

% Last Modified by GUIDE v2.5 26-Jan-2009 04:16:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ffm_parameters_OpeningFcn, ...
                   'gui_OutputFcn',  @ffm_parameters_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ffmmaker_parameters is made visible.
function ffm_parameters_OpeningFcn(hObject, eventdata, handles, varargin)
global ffm_par_temp
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ffmmaker_parameters (see VARARGIN)

set(hObject,'Name','Flatfieldmaker Parameter Panel')
ffm_par_temp=ffm_par_init; % gets initial values for GUI
ffm_par_GUIupdate(handles); %updates GUI

% we'll store the old saxs structure on the cancel button "UserData"
% and the revised saxs structure on the apply button 
%%%% This was done for reduction parameters but for now not here
%set(handles.cancel,'UserData',varargin{1})
%set(handles.MakeFlatField,'UserData',varargin{1})


% Choose default command line output for ffm_parameters
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes ffm_parameters wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ffm_parameters_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout = {get(handles.MakeFlatField,'UserData')};

% --- Executes on button press in lowint_check.
function lowint_check_Callback(hObject, eventdata, handles)
% hObject    handle to lowint_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of lowint_check
%ignore a click
%ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in mask_check.
function mask_check_Callback(hObject, eventdata, handles)
% hObject    handle to mask_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mask_check
ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in highint_check.
function highint_check_Callback(hObject, eventdata, handles)
% hObject    handle to highint_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of highint_check
ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes on button press in empty_check.
function empty_check_Callback(hObject, eventdata, handles)
% hObject    handle to empty_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of empty_check
ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure

% --- Executes on button press in dark_check.

function dark_check_Callback(hObject, eventdata, handles)
% hObject    handle to dark_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dark_check

ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure



% --- Executes during object creation, after setting all properties.
function lowint_trans_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowint_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function lowint_trans_Callback(hObject, eventdata, handles)
% hObject    handle to lowint_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowint_trans as text
%        str2double(get(hObject,'String')) returns contents of lowint_trans as a double
ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure


% --- Executes during object creation, after setting all properties.
function empty_trans_CreateFcn(hObject, eventdata, handles)
% hObject    handle to empty_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function empty_trans_Callback(hObject, eventdata, handles)
% hObject    handle to empty_trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of empty_trans as text
%        str2double(get(hObject,'String')) returns contents of empty_trans as a double
ffm_par_struct_update(handles) %updates the structure based on the input in GUI
ffm_par_GUIupdate(handles) %updates the GUI based on the structure




% --- Executes on button press in maskfile_get.
function maskfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to maskfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.maskname,'UserData');
if exist(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mat', 'Pick a mask-file');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.maskname,'UserData',fullfilename);
    set(handles.maskname,'String',filename);
    ffm_par_struct_update(handles) %updates the structure based on the input in GUI
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end
% --- Executes on button press in highintfile_get.
function highintfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to highintfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.highintname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm;', 'Pick the high intensity-file');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.highintname,'UserData',fullfilename);
    set(handles.highintname,'String',filename);
    ffm_par_struct_update(handles) %updates the structure based on the input in GUI
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end
% --- Executes on button press in highintfile_get.
function highintcentfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to highintfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.highintcenname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mat', 'Pick the previously saved center file for the high intensity image');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.highintcenname,'UserData',fullfilename);
    set(handles.highintcenname,'String',filename);
    ffm_par_struct_update(handles) %updates the structure based on the input in GUI
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end

% --- Executes on button press in lowintfffile_get.
function lowintfffile_get_Callback(hObject, eventdata, handles)
% hObject    handle to lowintfffile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.lowintname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm', 'Pick the "low Intensity Flatfield"-file');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.lowintname,'UserData',fullfilename);
    set(handles.lowintname,'String',filename);
    ffm_par_struct_update(handles) %updates the structure based on the input in GUI
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end

% --- Executes on button press in emptyfile_get.
function emptyfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to emptyfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.emptyname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm', 'Pick the "empty"-file');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.emptyname,'UserData',fullfilename);
    set(handles.emptyname,'String',filename);
    ffm_par_struct_update(handles) %updates the structure based on the input in GUI
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end
% --- Executes on button press in darkcurrentfile_get.
function darkcurrentfile_get_Callback(hObject, eventdata, handles)
% hObject    handle to darkcurrentfile_get (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullfilename=get(handles.darkcurrentname,'UserData');
if ~isempty(fullfilename)
    [PATHSTR,NAME,EXT] = fileparts(fullfilename);
    if exist(PATHSTR) 
        try cd(PATHSTR), end
    end
end
[filename, pathname] = uigetfile('*.mpa;*.mat;*.tif;*.tiff;*.img;*.inf;*.unw;*.gfrm', 'Pick the "dark current"-file');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    set(handles.darkcurrentname,'UserData',fullfilename);
    set(handles.darkcurrentname,'String',filename);
    ffm_par_struct_update(handles) %updates the structure based on the input in GUI
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end


% --- Executes on button press in DoneFlatField.
function DoneFlatField_Callback(hObject, eventdata, handles)
% hObject    handle to DoneFlatField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


close(gcf)

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(gcf)



% --- Executes on button press in MakeFlatField.
function MakeFlatField_Callback(hObject, eventdata, handles)
% hObject    handle to MakeFlatField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

statusline = status('Commencing flatfile construction...', handles);
li=getsaxs(get(handles.lowintname,'UserData'));
li=apply_lowint_parameters(li,handles);
li=reduction_state_from_files(li);
profile=average_raw(li,'x',[0 0 sqrt(2)*li.xaxis(2)/2 360],200);
% find flatfield function function
p=find_flatfield_function_params(profile,'cosine');
delete(findobj(handles.figure1,'Tag','statusline'))
if ~isempty(p) %the fit was accepted and we proceed to correct the flatfield
    %first we need to load in the high intensity file
    hi=getsaxs(get(handles.highintname,'UserData'));
    %then we apply the center (only)
    hi=apply_highint_parameters(hi,handles);
    %then the mask
    hi=load_mask(hi,handles);
    statusline = status('Filling in flatfile image... will take a couple of minutes', handles);
    % Then we fill out the high intensity file in the masked area
    [f_hi,nearest_neighbors]=flatfieldfill(hi.pixels,hi.mask_pixels,5);
    delete(findobj(handles.figure1,'Tag','statusline'))
    % the result is a matrix which we make into a saxs object 
    hinew=saxs(f_hi);
    % Add the information on center
    hinew=loadcenter(hinew,get(handles.highintcenname,'UserData'));
    % Now we find the average 
    hi_profile=average_raw(hinew,'x',[0 0 floor(sqrt(2)*(hinew.xaxis(2)-hinew.xaxis(1))/2+50) 360],...
        floor(sqrt(2)*(hinew.xaxis(2)-hinew.xaxis(1))/2+50));
    [R,phi]=create_rphi(hinew.pixels,hinew.raw_center(2),hinew.raw_center(1),1,1);
    %himax=mean(mean(hinew.pixels());
    warning off MATLAB:divideByZero
    hinew2=hinew.pixels./hi_profile.pixels(max(0*R+1,floor(R-1))).*cos(R/p(2));
    warning on MATLAB:divideByZero
    %transform NaNs to 0
    findnans=logical(isnan(hinew2));
    hinew2(findnans)=0;
    hinew2=saxs(hinew2);
    % save new flatfield if OK.
    figure
    imagesc(hinew2)
    title('Constructed flatfield image')
    answ=questdlg('Is this acceptable','Constructed Flatfield','Yes','No','Yes');

    if strcmp(answ,'No')
        msgbox({'That is too bad..not much to do about it...except maybe...','1) make a longer measurement', '2) redefine the mask'});
    else
        [filename, pathname] = uiputfile('*.mat', 'Save constructed flatfile as');
        save([pathname,filename],'hinew2')
    end

end




delete(findobj(handles.figure1,'Tag','statusline'))









%s=make_flatfield(li,handles);




% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)

% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
global ffm_par_temp
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uiputfile('*.mat','Save flatfieldmaker paramater file as','ffm_.mat');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    save(fullfilename,'ffm_par_temp')
end

% --------------------------------------------------------------------
function LoadMenu_Callback(hObject, eventdata, handles)

% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat','Load flatfieldmaker parameter file ');
if ~isempty(filename)
    fullfilename=fullfile(pathname,filename);
    eval(['load ''',fullfilename,''' ffm_par_temp'])
    ffm_par_GUIupdate(handles) %updates the GUI based on the structure
end


% --------------------------------------------------------------------
function CreateMaskMenu_Callback(hObject, eventdata, handles)
% hObject    handle to CreatMaskMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
maskimage=getsaxs;
maskmakergui(maskimage.pixels)
%msgbox('If you created and saved a mask, you can enter now enter the filename')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATUS BAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h_out = status(message, gui, seconds)
% Display a status line message, returning its handle.
% If you the caller don't specify SECONDS, then you must delete the status
% text control via its handle.
if nargin == 2
    h = figstatus(message, gui.figure1);
elseif nargin == 3
    h = figstatusbrief(message, seconds, gui.figure);
end
if nargout
    h_out = h;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCTIONS FOR CREATING 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function saxsnew=apply_lowint_parameters(s,handles)
%SAXS/APPLY_LOWINT_PARAMETERS to SAXS Object
%   This routine puts the parameters, and the corresponding file content...
%   that have been added in in the FFM
%   parameters panel into the SAXS object.
%   As it proceeds it checks for reasonable consistency within the data-files
%   that are loaded.
% 
saxsnew=s;
if get(handles.lowint_check,'Value')
    saxsnew= transfact(saxsnew,str2num(get(handles.lowint_trans,'String')));
else
    saxsnew= transfact(saxsnew,[]);
end

saxsnew=load_mask(saxsnew,handles);

if get(handles.empty_check,'Value')
    saxsnew=empty_filename(saxsnew,get(handles.emptyname,'UserData'));
    if exist(saxsnew.empty_filename)
        statusline = status('Loading "empty holder" file...', handles);
        a=getsaxs(saxsnew.empty_filename);
        if ~(size(a.pixels)==size(saxsnew.raw_pixels)  )
            errordlg(['empty holder file is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        saxsnew=empty_pixels(saxsnew,a.pixels);
        saxsnew=empty_livetime(saxsnew,a.livetime);
        saxsnew=empty_transfact(saxsnew,str2num(get(handles.empty_trans,'String')));
        delete(statusline)
    else
        errordlg(['empty file:', saxsnew.empty_filename,' does not exist. Please correct'])
        return
    end
else
    saxsnew=empty_filename(saxsnew,'');
    saxsnew=empty_pixels(saxsnew,[]);
    saxsnew=empty_livetime(saxsnew,[]);
    saxsnew=empty_transfact(saxsnew,[]);
end

if get(handles.dark_check,'Value')
    saxsnew=darkcurrent_filename(saxsnew,get(handles.darkcurrentname,'UserData'));
    if exist(saxsnew.darkcurrent_filename)
        statusline = status('Loading darkcurrent file...', handles);
        a=getsaxs(saxsnew.darkcurrent_filename);
        if ~(size(a.pixels)==size(saxsnew.raw_pixels)  )
            errordlg(['darkcurrent image is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        saxsnew=darkcurrent_pixels(saxsnew,a.pixels);
        saxsnew=darkcurrent_livetime(saxsnew,a.livetime);
        delete(statusline)
    else
        errordlg(['dark current file:', saxsnew.darkcurrent_filename,' does not exist. Please correct'])
        return
    end
else
    saxsnew=darkcurrent_filename(saxsnew,'');
    saxsnew=darkcurrent_pixels(saxsnew,[]);
    saxsnew=darkcurrent_livetime(saxsnew,[]);
end

function saxsnew=apply_highint_parameters(s,handles)
%SAXS/APPLY_HIGHINT_PARAMETERS to SAXS Object
%   This routine puts the parameters, and the corresponding file content...
%   that have been added in in the FFM
%   parameters panel into the SAXS object.
%   As it proceeds it checks for reasonable consistency within the data-files
%   that are loaded.
% 
saxsnew=s;
if exist(get(handles.highintcenname,'UserData'))
    statusline = status('Loading center file...', handles);
    saxsnew=loadcenter(saxsnew,get(handles.highintcenname,'UserData'));
    delete(statusline)
else
    errordlg(['center file for high intensity image:',get(handles.highintcenname,'UserData'),' does not exist. Please correct'])
    return
end


function so=load_mask(s,handles)
    so=mask_filename(s,get(handles.maskname,'UserData'));
    if exist(so.mask_filename)
        statusline= status('Loading mask file...', handles);
        a=load(so.mask_filename); %mask file contains 1's and 0's
        if ~isfield(a,'mask') 
            errordlg(['mask file:', so.mask_filename,' does not contain variable "mask". Please make a new mask file'])
            delete(statusline)
            return
        end
        if ~(size(a.mask)==size(so.raw_pixels)  )
            errordlg(['mask is the wrong size. Please choose a different file'])
            delete(statusline)
            return
        end
        if ~islogical(a.mask)
            errordlg(['mask is not a logical file. Make new mask file '])
            delete(statusline)
            return
        end
        warning off MATLAB:divideByZero
        so=mask_pixels(so,double(a.mask)./double(a.mask)); % this transforms it into 1's and NaN's
        warning on MATLAB:divideByZero
        delete(statusline)
    else
        errordlg(['mask file:', so.mask_filename,' does not exist. Please correct'])
        return
    end









