function b=find_fitfunctions(imagedimension)
global fitfunction_dir_2D fitfunction_dir_1D

if imagedimension==1
    fitfunction_dir=fitfunction_dir_1D;
elseif imagedimension==2
    fitfunction_dir=fitfunction_dir_2D;
else
    error('fitfunction error- the image must be 1 or 2 dimensions')
end
if ~exist(fitfunction_dir,'dir')
    error('fitfunction directory not found. Check the SAXSGUI_setting.m file !')
end
if ~strcmp(fitfunction_dir(end),filesep)
    fitfunction_dir=strcat(fitfunction_dir,filesep);
end
files=dir([fitfunction_dir,'*.m']);
numfiles=length(files);
for ii=1:numfiles
    fitfile=fopen([fitfunction_dir,files(ii).name]);
    % find out whether it is a proper function file
    % first word in file must be "function"
    line = fgetl(fitfile);
    if ~ (strncmp(line,'function',8))
        fclose(fitfile);
        error(['fitfunction error - fitfunction',files(ii).name,...
            'cannot be recognized as a function'])
    end
    %find descriptor in funciton file thathas been prepared for reading by saxsgui
    line=fgetl(fitfile);
    while ~(strncmp(line,'% Description',12)) && ~feof(fitfile)
        line=fgetl(fitfile);
    end
    if feof(fitfile)
        fclose(fitfile);
        error({['The fitfunction',files(ii).name],'does not have the right descriptor',...
            'so please insert this this description into the file,',...
            'or remove the file from the fitfunction directory.'})
        return
    end
    %OK % Descriptor found now look for description end
    name_help=[];
    jj=0;
    while ~feof(fitfile)
        line=fgetl(fitfile);
        if ~(strncmp(line,'% Description end',12))
            jj=jj+1;
            name_help{jj}=line;            
        else
            break
        end
    end
    line=fgetl(fitfile);
    % Find number of parameters
    if ~ (strncmp(line,'% Number of Parameters:',22)) % did not find explicit number of parameters
       % will instead search the file...This section was contributed by Brian
        % Pauw
        numparams=0;
        while line~=-1
            if isempty(strfind(tline,'% parameter '))~=1 %we have reached a line with a parameter in it
                numparams=numparams+1;
            end
            line = fgetl(fid);
        end
        close(fitfile)
        % now I've used up file so I must reopen the file
        fitfile=fopen([fitfunction_dir,files(ii).name]);
    else
        numparams=sscanf(line,'%% Number of Parameters: %f');
    end
    if numparams==0 
        error({['The fit-function',files.name],'does not list the number of parameters',...
                     'nor the parameters in a recognizable format,',...
                     'so please check the format,',...
                     'or remove the file from the fitfunction directory.'})
    end

    clear param_name
    clear param_help
    clear param_bool
    for jj=1:numparams
        while(isempty(strfind(line,'% parameter ')))
            line=fgetl(fitfile);
        end
        linediv=strfind(deblank(line),':');
        if length(linediv)<2  || length(linediv)>3
            error({['The fit-function',files.name],'does not list the number of parameters',...
            'so please insert this this into the file,',...
            'or remove the file from the fitfunction directory.'})
        else
            param_name{jj}=line(linediv(1)+1:linediv(2)-1);
            param_help{jj}=line(linediv(length(linediv))+1:end);
            if length(linediv)==3 
                bool_txt=line(linediv(2)+1:linediv(3)-1);
            else
                bool_txt=' ';
            end
            if ~isempty(strfind(bool_txt,'bool'))
                param_bool{jj}=1;
            else
                param_bool{jj}=0;
            end
        end
        line=fgetl(fitfile);
    end
    a(ii).names=files(ii).name;
    a(ii).name_help=name_help;
    a(ii).numparams=numparams;
    a(ii).param_name=param_name;
    a(ii).param_bool=param_bool;
    a(ii).param_help=param_help;
end
%converting to cells
if imagedimension==1
    b.names={a.names};
    b.name_help={a.name_help};
    b.numparams={a.numparams};
    b.param_name={a.param_name};
    b.param_help={a.param_help};
    b.param_bool={a.param_bool};
elseif imagedimension==2
    b.names={a.names};
    b.name_help={a.name_help};
    b.numparams={a.numparams};
    b.param_name={a.param_name};
    b.param_help={a.param_help};
    b.param_bool={a.param_bool};
end

