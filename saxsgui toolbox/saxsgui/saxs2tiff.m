function  saxs2tiff(S,tiff_pathname,bitsize)
%SAXS2TIFF Save a SAXS object to a tiff-file.
%       saxs2tiff(S,'tiff_pathname'); save the saxsobject S to a file
%   called tiff_pathname, returning a status flag as to whether it worked
%   or not
%
% June 5, 2006, compression changed to none to allow FIT2D to read the
% files
% May, 2008, included option to allow different bitsize to be given

[pathstr,name,ext] = fileparts(tiff_pathname);
tiffname=fullfile(pathstr,[name,'.tif']);
switch bitsize
    case '8'
        S=saturate(S,2^8-1);
        imwrite(uint8(S.pixels),tiffname,'tiff','Compression','none')
    case '16'
        S=saturate(S,2^16-1);
        imwrite(uint16(S.pixels),tiffname,'tiff','Compression','none')
    case '24'
        S=saturate(S,2^24-1);
        %imwrite(uint32(S.pixels),tiffname,'tiff','Compression','none')
    case '32'
        S=saturate(S,2^32-1);
        imwrite(uint32(S.pixels),tiffname,'tiff','Compression','none')
    otherwise
        if max(max(S.pixels))> (2^8-1)
            if max(max(S.pixels))> (2^16-1)
                errordlg({'Counts per pixel greater','than 16bit (65535) is presently', 'not supported','No tif-file saved'})
                return
                %imwrite(uint32(S.pixels),tiffname,'tiff')
            else
                imwrite(uint16(S.pixels),tiffname,'tiff','Compression','none')
            end
        else
            imwrite(uint8(S.pixels),tiffname,'tiff','Compression','none')
        end
end
