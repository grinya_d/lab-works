February 2007
A bug has sneaked in that only allow a certain number of files to open.
Readout noise reduction doesn't seem to work correctly - fixed 
Darkcurrent reduction doesn't seem to work correctly - fixed
Smoothing is forgotten when averaging with reductions
Hangs when flatfield is not very strong
BIFT had trouble for Hannover data on nanoparticles
Trouble when saving in reduction panel


May 30, 2006
-Bug in printout of azimuthally averaged data reported. ---fixed
-Windows look very different on different computers - fixed


Mar 11, 2006
-After smoothing Calibration-same-as-last does not work...works after center same as last.
-1D flatfield routines have problems with flatfields with beamstops. - Corrected with ffvalue function in 1.4.03
-Corrections on smoothed data does not smoothe the correction data.- fixed
-Sometimes the coloring of consequitive plots does not work.- not yet fixed
-2D flatfield normalization is not really done very well. - Corrected in Version 1.4.03


Mar 8, 2006
Bug found: impossible to calibrate smoothed data....however one can smoothe calibrated data! - fixed

Feb 20, 2006
Bug reported in reading fuji img plate files.- fixed
Bug was caused by erroneous parsing of fuji inf file 'with date information'
Fixed Feb 21,2006 version 1.4.02

Report May 19, 2005
Matlab version 7 does not do a good job of rescaling with errorbars. 
Seems to be fixed in Service Pack 3