function Icalc=GaussPeakSpline(q,I,varargin)
% Description
% This function calculates 1D data for a gaussian peak with a sloped
% background (Exponential of q^2)
% Description end
% Number of Parameters: 5
% parameter 1: Centre : Centre of Gaussian 
% parameter 2: Amp : Amplitude of Gaussian
% parameter 3: Width : Width (std. dev) of Gaussian (FWHM/2.35)
% parameter 4: Backg : Background of Gaussian
% parameter 5: Slope : Slope of the background
%%%%%% This marks the end of the header required by SAXSGUI.
% Please keep the format since it is used by saxsgui to create interactive
% windows.
%% Here you can be put additional info
%
% GaussPeakSpline
% This is a simple gaussian function with a sloped background
% The Bacgkround is exponential function of q^2

global fitpar
values=varargin{1};
Centre=values(1);
Amp=values(2);
Width=values(3);
Inten=values(4);
B_exp=values(5);



Icalc=Amp.*exp(-0.5*((q-Centre)/Width).^2)+Inten.*exp(B_exp.*q.^2);
%--------------------------------------------------------------------------

