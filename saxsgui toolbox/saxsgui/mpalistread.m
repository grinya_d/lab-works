function liststruct=mpalistread(pathname)


% Try to open the list file for reading.
file = fopen(pathname, 'rt');
if file == -1
    error('Could not open MPA list file.')
end

% Extract array x dimension.

line = findline(file, '[ADC1]');
if isempty(line)
    fclose(file);
    error('MPA list file error - no [ADC1] tag.')
end

line = findline(file, 'range=', '[');
if isempty(line)
    fclose(file);
    error('MPA list file error - no range= after [ADC1]')
end

xdim = str2double(line(length('range=')+1:end));

% Extract starting date from header

line = findline(file, 'cmline0=', '[');
if isempty(line)
    fclose(file);
    error('MPA list file error - no date= after [ADC1]')
end

filedate = (line(length('cmline0=')+1:end));

% Extract array y dimension.

line = findline(file, '[ADC2]');
if isempty(line)
    fclose(file);
    error('MPA list file error - no [ADC2] tag.')
end

line = findline(file, 'range=', '[');
if isempty(line)
    fclose(file);
    error('MPA list file error - no range= after [ADC2]')
end

ydim = str2double(line(length('range=')+1:end));

line = findline(file, '[MAP0]');
if isempty(line)
    fclose(file);
    error('MPA list file error - no [MAP0] tag.')
end

line = findline(file, 'timerreduce=', '[');
if isempty(line)
    fclose(file);
    error('MPA list file error - no timerreduce= before  [LISTDATA]')
end

timerreduce = str2double(line(length('timerreduce=')+1:end));

line = findline(file, '[LISTDATA]');
if isempty(line)
    fclose(file);
    error('MPA file error - no [LISTDATA, tag.')
end

fclose(file);
databin=freadslice(pathname);
% file=fopen(pathname,'r');
% databin=fread(file);
% fclose('all');
% ii=strfind(char(databin)','[LISTDATA');
% iii=strfind(char(databin(ii:end)'),']');
% databin=databin(ii+iii(1)+2:end);
% b=reshape(databin,4,length(databin)/4);
% databin=b(4,:)'*256^3+b(3,:)'*256^2+b(2,:)'*256+b(1,:)';

%
liststruct.rate=zeros(fix(length(databin)/2),1);
liststruct.list=zeros(3,fix(length(databin)/2));
timebin=0;
jj=0;
newevent=0;
newtime=0;

for ii=1:length(databin)
    if newevent==1
        liststruct.list(1,jj)=timebin;
        liststruct.list(2,jj)=min(xdim,max(1,fix(databin(ii)/(256*256))));
        liststruct.list(3,jj)=min(ydim,max(1,mod(databin(ii),256*256)));
        newevent=0;
    end
    if databin(ii)==4294967295
        timebin=timebin+1;
    end
    if databin(ii)==3
        jj=jj+1;
        liststruct.rate(timebin)=liststruct.rate(timebin)+1;
        if rem(jj,10000)==0 
            if exist('hstatus')
                delete(hstatus)
            end
            disp([num2str(jj),' photons'])
            hstatus=figstatus(['converted ',num2str(jj),' photons of ',num2str(length(databin)/2),' expected']);

        end
        newevent=1;
    end
end
if exist('hstatus')
    delete(hstatus)
end
liststruct.pathname=pathname;
liststruct.list=liststruct.list(:,1:jj);
liststruct.rate=liststruct.rate(1:timebin);
liststruct.timerreduce=timerreduce;
liststruct.xdim=xdim;
liststruct.ydim=ydim;
liststruct.timestart=(liststruct.list(1,1)-1)*timerreduce/1000;
liststruct.timeend=(liststruct.list(1,end)-1)*timerreduce/1000;


%***********************************************
function databin = freadslice(pathname)

readsize=2000000;
D=dir(pathname);
databin=[1:floor(D.bytes/4)];
file=fopen(pathname,'r');
tempbin=fread(file,min(readsize,D.bytes),'uint8');
cnt=length(tempbin);
ii=strfind(char(tempbin)','[LISTDATA');
iii=strfind(char(tempbin(ii:end)'),']');
headerlength=ii+iii(1)+1;
fclose(file);
file=fopen(pathname,'r');
header=fread(file,headerlength,'uint8');
cnt=length(header);
%tempbinlength=cnt-headerlength;
%extralength=tempbinlength-4*floor(tempbinlength/4);
%tempbin=tempbin(ii+iii(1)+2:end-extralength);
%b=reshape(tempbin,4,floor(length(tempbin)/4));
%addlength=length(b);
%databin(1:addlength)=double(b(4,:))'*256^3+double(b(3,:))'*256^2+double(b(2,:))'*256+double(b(1,:))';
%templength=0+addlength;
ii=0;
templength=0
while cnt<D.bytes
    cnt
    hstatus=figstatus(['read ',num2str((cnt-headerlength)/4),' photons of ',num2str((D.bytes-headerlength)/4),' expected']);
    tempbin=fread(file,min(readsize,D.bytes-headerlength-ii*readsize),'uint8');
    cnt=cnt+length(tempbin);
    b=reshape(tempbin,4,floor(length(tempbin)/4));
    addlength=length(b);
    databin(templength+1:templength+addlength)=double(b(4,:))'*256^3+double(b(3,:))'*256^2+double(b(2,:))'*256+double(b(1,:))';
    templength=templength+addlength;
    delete(hstatus)
end
fclose(file);





%***********************************************

% ---------------------------------------------------------
function line = findline(file, findstr, varargin)
%FINDLINE Look for line in file starting with string.
%  line = FINDLINE(file, findstr)
%    Look forward in file (handle) for a line starting with 
%    the characters in findstr.
%    If not found before end of file, return an empty array.
%    Otherwise return the line found as a character array.
%
%  line = FINDLINE(file, findstr, stopstr)
%    Same but stop if we find a line starting with stopstr,
%    returning an empty array.  An enhancement
%    would be to begin the next search at that line.  But
%    for now a subsequent search would start at the line after
%    the stop line.  So far, we're going to bail out if one
%    of our FINDLINE's fails, so we don't need subsequent 
%    FINDLINE's.

lenfind = length(findstr);
stopstr = [];
lenstop = 0;
if nargin == 3
    stopstr = varargin{1};
    lenstop = length(stopstr);
end

while ~ feof(file)
    line = fgetl(file);
    if strncmp(line, findstr, lenfind)
        break
    end
    if strncmp(line, stopstr, lenstop)  % returns false if lenstop is 0
        break
    end
end

if ~ strncmp(line, findstr, lenfind)
    line = [];  % string not found
end