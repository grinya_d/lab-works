function mult = getbkgmult(im, bkg)
%GETBKGMULT Get multiplier for background subtraction of two SAXS images.
%   MULT = GETBKGMULT(IM, BKG) returns a multiplier by which to multiply
%   SAXS object IM before subtracting SAXS object BKG.  GETBKGMULT does not
%   perform the subtraction.  It interacts with the user through a dialog
%   window, displaying the data collection times for both SAXS images.
prompt = strvcat(['Image real, live times:  ', num2str(im.realtime), '; ', num2str(im.livetime)], ...
    ['Background real, live times:  ', num2str(bkg.realtime), '; ', num2str(bkg.livetime)], ...
    ' ', ...
    ['Image multiplier before background subtraction:']);
multstr = inputdlg(prompt, 'Multiplier');
if isempty(multstr)
    multstr = '0';
end
mult = str2double(multstr);
if isnan(mult)
    mult = 0;
end
