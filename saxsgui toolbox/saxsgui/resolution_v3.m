function [smear_shape,q_use,A,R]=resolution_v2(varargin)

%This function will compute the smearing matrix R using system parameters
%obtained from the resolution_input() function.
%
%The function is defined as follows:
%[smear_shape,q_use,R,A]=resolution_v2(varargin)
%with inputs:
% q: the matrix with the q for each pixel
% phi: the matrix with the angle phi for each pixel
% sigma_d1: the FWHM of the detector smearing in direction 1
% sigma_d2: the FWHM of the detector smearing in direction 2
% sigma_w: the FWHM of the wavelength distribution
% sigma_c1: the collimation smearing in direction 1
% sigma_c2: the collimation smearing in direction 2
% q_limit: the limit in q, below which the data is incomparable due to
%           beamstop shadowing or parasitic scattering
% cutoff: the number below which contributions to the smearing are
%           considered negligible.
%This function will calculate the radius of the area on the detector that
%is unuseable due to parasitic scattering from the second pinhole in a
%three-pinhole collimation section.
%written by Brian R. Pauw on 19-12-2006
%this second version uses the gaussian system as described by J. S.
%Pedersen et al. j. app. cryst. 23 (1990) 321-333

%the function is to be called like the normal function, but with the
%smearing parameters as first set of variables, then the function name of
%the function to smear and its variables. If no smearing function is given,
%the q limit will be returned, as well as the extra q-radius and phi values
%needed for completely smearing the requested function. the Fminsearchbnd
%call should then expand its bounds to also have the function calculate
%these additional values.

%for the first implementation this weird callback of this function is
%neglected, and the data is smeared within the calculated set. This
%implicates that the fitted data is actually smaller than that defined by
%its bounds. Such an implementation is easier to implement than the
%"nicer" method described above.

% if nargin==0
%     [sigma_d1,sigma_d2,sigma_w,sigma_c1,sigma_c2,q_limit]=resolution_input()
% end
i=1; %read the variables..
q=varargin{i};i=i+1;
phi=varargin{i};i=i+1;
sigma_d1=varargin{i};i=i+1;
sigma_d2=varargin{i};i=i+1;
sigma_w=varargin{i};i=i+1;
sigma_c1=varargin{i};i=i+1;
sigma_c2=varargin{i};i=i+1;
q_limit=varargin{i};i=i+1;
npixels=varargin{i};i=i+1; %4,8,12,20 or 24.
q_use=varargin{i};i=i+1;

clear i;

switch npixels
    case 4
        extra_boundary=1; % 1 pixel more needed surrounding the <q>
        smear_shape=[0 1 0;1 0 1;0 1 0]; %the pixels used in smearing are 1 around the central point (0)
    case 8
        extra_boundary=1; % 1 pixel more needed surrounding the <q>
        smear_shape=[1 1 1;1 0 1;1 1 1]; %the pixels used in smearing are 1 around the central point (0)
    case 12
        extra_boundary=2; % 2 pixel more needed surrounding the <q>
        smear_shape=[0 0 1 0 0;0 1 1 1 0;1 1 0 1 1;0 1 1 1 0;0 0 1 0 0]; %the pixels used in smearing are 1 around the central point (0)
    case 20
        extra_boundary=2; % 2 pixel more needed surrounding the <q>
        smear_shape=[0 1 1 1 0;1 1 1 1 1;1 1 0 1 1;1 1 1 1 1;0 1 1 1 0]; %the pixels used in smearing are 1 around the central point (0)
    case 24
        extra_boundary=2; % 2 pixel more needed surrounding the <q>
        smear_shape=[1 1 1 1 1;1 1 1 1 1;1 1 0 1 1;1 1 1 1 1;1 1 1 1 1]; %the pixels used in smearing are 1 around the central point (0)
    otherwise
        error('number of smearing pixels not 4,8,12,20 or 24 pixels!')
end
%set q_use to zero for those pixels in the 1 or tw

a=2*pi*sqrt(sigma_d1^2+sigma_w^2+sigma_c1^2+sigma_c2^2+sigma_d2^2);%these should not give imaginary values.
%crop the data to exclude shadowed points. not the most expedient
%route, but it is robust enough.
% this is not necessary, only needed for desmearing purposes. see appendix of implementation file.
%   [q_indices1,q_indices2]=find(q>q_limit);
%   for j=1:size(q_indices1)
%       for k=1:size(q_indices2)
%           q_temp(j,k)=q(q_indices1(j),q_indices2(k));
%           phi_temp(j,k)=phi(q_indices1(j),q_indices2(k));
%       end
%   end
%   q=q_temp;
%   phi=phi_temp;
%   clear q_temp phi_temp;
%determine q limits
%q_cutoff_1=sqrt(-2*log(a*cutoff)*(sigma_w^2+sigma_c1^2+sigma_d1^2))+q; %with q2=0 --> q1 limit %these should not give imaginary values.
%q_cutoff_2=sqrt((-2*log(a*cutoff)+(q).^2./((sigma_w^2+sigma_c1^2+sigma_d1)^2)).*(sigma_c2^2+sigma_d2^2)); %with q1=0 --> q2 limit %these should not give imaginary values: sigma_d1 or sigma_w should be not equal to zero.

%check for bullshit input:
% if a*cutoff>=1
%     error('Smearing widths out of bounds, check the system parameters'); %especially check the sigma values that are larger than 1.
% end

%     if nargin>i  %No longer handled here.
%         function_name=varargin{i};i=i+1;
%         isnow=i;
%         for i=i:nargin;
%             function_variables{i-isnow+1}=varargin{i};i=i+1;
%         end
%     else
%         return %return to the calling function, no use to linger around here any longer... q_cutoff has been determined.
%     end


%Determine the valus for q_{1} and q_{2} around each <q> % alas, this eats
%too much memory. If common computers have 8 TB of memory, implement this..

% for i=1:size(q,1)
%     for j=1:size(q,2)
%         if isnan(q_use(i,j))~=1 % no need making it bigger than it already is.
%             q2(i,j,1:(size(q,1)),1:(size(q,2)))=tan(phi(i,j)-phi).*q; %please give a warm welcome to the first appearance of a four-dimensional matrix!!! q2 is calculated here
%             q1(i,j,1:(size(q,1)),1:(size(q,2)))=tan(phi(i,j)-phi).*q; %please give a warm welcome to the second appearance of a four-dimensional matrix!!! q1 is calculated here
%         end
%     end
% end

%Determine which q-values lie within the boundaries as set by the q_cutoff
%values this thus determines the limit of the width of which data is used for smearing. This is a different q_cutoff than the one in resolution_input.m.
%q_use=ones(size(q,1),size(q,2));

%an attempt at speeding up the process:
[usex,usey]=find(q_use==1);
%q_speed=q.*q_use; %does not make any difference.
[qsize1,qsize2]=size(q);

%test if substituting values for NaN speeds up a calculation... no
%  t=cputime;
%      q2(1:qsize1,1:qsize2)=tan(phi(50,50)-phi).*q_speed;
%      cputime-t
%      t=cputime;
%          q2(1:qsize1,1:qsize2)=tan(phi(50,50)-phi).*q;
%         cputime-t

% waitbarhandle=waitbar(0,'Preprocessing datapoints for exclusion');
% waitbarmax=max(size(usex));
 %tic;
for i=1:size(usex)
    q_use_small=q_use(usex(i)-extra_boundary:usex(i)+extra_boundary,usey(i)-extra_boundary:usey(i)+extra_boundary);
%    q2=tan(phi(usex(i),usey(i))-phi).*q; 
%    q1=tan(phi(usex(i),usey(i))-phi).*q;
 %   [indicesx,indicesy,vecs]=find(q2<abs(q(usex(i),usey(i))-q_cutoff_2(usex(i),usey(i))) && q1<abs(q(usex(i),usey(i))-q_cutoff_1(usex(i),usey(i))) ); % find all the qs that lie within the cutoff bounds
    if isnan(sum(sum(q_use_small))) || usex(i)-extra_boundary<1 || usex(i)+extra_boundary>qsize1 || usey(i)-extra_boundary<1 || usey(i)+extra_boundary>qsize2;
        q_use(usex(i),usey(i))=0; % if one of the smearing datapoints is an unuseable point
    end
 %    if round(i/30)==i/30
%         waitbar(i/waitbarmax,waitbarhandle);
%         elaps=toc;
%         est=elaps*waitbarmax/i
         %display('estimated time %d minutes',est)
 %    end
end

%Determine the smearing matrix at points q(i,j)
%--> this will be done by smear_v2, since the smearing matrix R is too large to
%be put into memory.

%reimplemented for the smaller matrices
if strcmp(loadsofmemory,'Yes')==1;
    R=zeros((extra_boundary*2+1),(extra_boundary*2+1),size(q_use,1),size(q_use,2));
    [all_i,all_j]=find(q_use==1);
    for i=1:size(all_i)
        q2=abs(tan(phi(all_i(i),all_j(i))-phi(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary)).*q(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary));
        q1=abs(q(all_i(i),all_j(i))-sqrt(q(all_i(i)-extra_boundary:all_i(i)+extra_boundary,all_j(i)-extra_boundary:all_j(i)+extra_boundary).^2+q2.^2));
        R(:,:,all_i(i),all_j(i))=smear_shape.*(a^(-1).*exp(-1/2.*((q1-q(all_i(i),all_j(i)))).^2./b+q2.^2./c));
    end
else
    R=0; %would've been neater with varargout
end

%Calculate the area of each pixel, a little different around the edges and
%in the center around the beam
%determine the size of the pixels in q and extrapolate for the q at
%position 1
%first find the lowest value for q in the matrix
%note: around the beam center this implementation is likely to cause errors. a better method is to compute the distance between two vectors.
% [x,Ix]=min(q,[],1);[y,iy]=min(x,[],2);ix=Ix(iy); %ix,iy give the rowcol values/indices for the minimum
% clear(x,y);
Dx=zeros(size(q,1));
Dx(1)=sqrt(q(2,1)^2+q(1,1)^2-2*q(2,1)*q(1,1)*cos(abs(phi(2,1)-phi(1,1))));
for i=2:(size(q,1)-1)
    Dx=(sqrt(q(i-1,1)^2+q(i,1)^2-2*q(i-1,1)*q(i,1)*cos(abs(phi(i-1,1)-phi(i,1))))+sqrt(q(i+1,1)^2+q(i,1)^2-2*q(i+1,1)*q(i,1)*cos(abs(phi(i+1,1)-phi(i,1)))))/2;
end
Dx(size(q,1))=sqrt(q(size(q,1)-1,1)^2+q(size(q,1),1)^2-2*q(size(q,1)-1,1)*q(size(q,1),1)*cos(abs(phi(size(q,1)-1,1)-phi(size(q,1),1))));

Dy=zeros(size(q,2));
Dy(1)=sqrt(q(1,2)^2+q(1,1)^2-2*q(1,2)*q(1,1)*cos(abs(phi(1,2)-phi(1,1))));
for i=2:(size(q,2)-1)
    Dy=(sqrt(q(1,i-1)^2+q(1,i)^2-2*q(1,i-1)*q(1,i)*cos(abs(phi(1,i-1)-phi(1,i))))+sqrt(q(1,i+1)^2+q(1,i)^2-2*q(1,i+1)*q(1,i)*cos(abs(phi(1,i+1)-phi(1,i)))))/2;
end
Dy(size(q,2))=sqrt(q(1,size(q,2)-1)^2+q(1,size(q,2))^2-2*q(1,size(q,1)-1)*q(1,size(q,1))*cos(abs(phi(1,size(q,1)-1)-phi(1,size(q,1)))));
%
% Dy(1)=abs(q(ix,iy+1)-q(ix,iy));
% for i=2:(size(q,2)-1) %calculate all distances in direction 2 normally
%     Dy(i)=(abs(q(ix,iy-1)-q(ix,iy))+abs(q(ix,iy+1)-q(ix,iy)))/2;
% end
% Dy(size(q,2))=abs(q(ix,iy-1)-q(ix,iy));
%and calculate the final pixel at the end

%if it is in the center
%    A(ix,iy)=(abs(q(ix-1,iy)-q(ix,iy))+abs(q(ix+1,iy)-q(ix,iy)))/2*(abs(q(ix-1,iy)-q(ix,iy))+abs(q(ix+1,iy)-q(ix,iy)))/2; %distance to the next pixels divided by two.

A=Dx'*Dy;

% % %calculate the data from the supplied function NO. THIS WILL BE DONE IN
% SEPARATE FUNCTIONS!
% f_handle=str2func(function_name);
% I=f_handle(q_orig,phi_orig,function_variables);

