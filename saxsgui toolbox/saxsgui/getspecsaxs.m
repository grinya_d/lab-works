function [got, gotname, fromdir] = getspecsaxs(specifiedpath, title)
%GETSPECSAXS Get SAXS image from file that has been taken in a SPEC taken.
%   GETSPECSAXS displays a dialog window to allow the user to choose
%   a scan point SPEC scan resident in a SPEC log-file.
%   This scan point should have a SAXS image associated with it.
%
%   GETSPECSAXS then reads the SAXS image from the image file, and it
%   returns the SAXS image object.  Otherwise it returns an empty array. 
%
%   GETSPECSAXS can read SAXS images from any of the files types supported
%   by GETSAXS (see GETSAXS). 
%
%   GETSPECSAXS('directory') starts in the specified directory.
%
%   GETSPECSAXS('directory', 'title') uses the specified title for the dialog
%   window.  Default is 'Open SAXS image file'.  To specify a title but let
%   GETSPECSAXS use the last directory it worked in, specify an empty string
%   for 'directory'.
%
%   GETSPECSAXS('file') where 'file' is not a directory, attempts to read a
%   SPEC log-file, without displaying the usual dialog window.
%
%   [S NAME] = GETSPECSAXS(...) returns saxs object S and the file name NAME,
%   sans directory path.
%
%   [S NAME DIR] = GETSPECSAXS(...) returns saxs object S, file name NAME, and
%   directory path DIR.
%
%   See also GETSAXS, MPAREAD, SAXS, SAXS/IMAGE, LOAD, SAVE.

persistent rememberdir  % remember where we've been
file = [];  % no file specified yet

if nargin > 0 && ~ isempty(specifiedpath)
    % We have either a directory to start in or a file to open.
    if isdir(specifiedpath)
        rememberdir = fullpath(specifiedpath);
    else
        [dir file ext] = fileparts(specifiedpath);
        file = [file, ext];
        if isempty(dir)
            dir = '.';
        end
        dir = fullpath(dir);
        clear ext 
    end
end
if nargin > 1 && ischar(title)
    dlgtitle = title;
else
    dlgtitle = 'Find SPEC log-file';
end
if isempty(rememberdir)
    rememberdir = '';  % avoid warning from FULLFILE
end
got = [];  % we haven't read a SAXS object yet

if isempty(file)
    [file dir] = uigetfile(fullfile(rememberdir, '*'), dlgtitle);
end


if file  % we have a file path
    pathname = fullfile(dir, file);
    rememberdir = dir;  % look in this directory first next time
    got = tryspec(pathname);
end

% Take care of optional output arguments.
if isempty(got)  % we didn't read a SAXS object
    if nargout > 1
        gotname = [];
        if nargout > 2
            fromdir = [];
        end
    end
else  % we did read a SAXS object
    if nargout > 1
        gotname = got.raw_filename;
        if nargout > 2
            fromdir = dir;
        end
    end
    % let's just put a comment in the history about how many counts
    totalint=num2str(sum(sum(got.raw_pixels)));
    got=history(got,['Total Intensity ',totalint]);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = tryspec(pathname)
% Try reading a spec file.
% Return a SAXS object or report an error and return empty.
try
    S = specread(pathname);
catch
    caught = lasterr;
    errordlg(caught, 'Error reading SPEC file', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function s = specread(pathname)
%SPECREAD Read a specfile to allow the user to select a scan_number and a
%scan_point from a 

s=[];
% Try to open the file for reading.
file = fopen(pathname, 'rt');
if file == -1
    error('Could not open spec file.')
end

% find out whether it is a proper spec file
line = fgetl(file);
if ~ (strncmp(line,'#F ',3))
    fclose(file);
    error('SPEC file error - no #F in first line - perhaps corrupt specfile')
end
ii=1;
while ~ feof(file)
    line = fgetl(file);
    if strncmp(line,'#S ',3) && ~ feof(file)  %find the beginning of a scan
        scanline=line;
        scannum=sscanf(scanline,'#S %f');
        line = fgetl(file);
        %We'search until we have a scan with images or next scan
        while ~ strncmp(line,'#X SAXS',7) && ~ strncmp(line,'#S ',3) && ~ feof(file)
            line=fgetl(file);
        end
        while  ~ strncmp(line,'#N ',3) && ~strncmp(line,'#S ',3) && ~ feof(file)
            %looking for #N Lines
            line=fgetl(file);
        end
        if strncmp(line,'#N ',3) % we now believe we have it
            numvalueline=line;
            numvalue=sscanf(numvalueline,'#N %f');
            nameline=fgetl(file);
            line=fgetl(file);
            jj=0;
            while ~ strncmp(line,'#',1) && ~ feof(file)
                jj=jj+1;
                dataline{jj}=line;
                line=fgetl(file);
            end
            % now put the scan data in the appropriate array
            if jj>0
                scanarray.scanline{ii}=scanline;
                scanarray.scannum{ii}=scannum;
                scanarray.numvalue{ii}=numvalue;
                scanarray.nameline{ii}=nameline;
                scanarray.data{ii}=dataline;
                ii=ii+1;
            end
        end
    end
end
fclose(file);
if isempty(scanarray)
    errordlg('No Image Scans in this Spec File','modal')
    return
else
    [filepath,filename]=fileparts(pathname);
    scanarray.logfilepath={strcat(filepath,filesep)};
    scanarray.logfilename={filename};
    
    try
        s=choosespecgui(scanarray);
    catch
        s=[]; %if the user deletes the window
    end
end








