function varargout = scale(varargin)
%SCALE Interactively adjust the color scale for an image.
%   SCALE, by itself, opens a scale control window for the current figure.
%
%   The scale controls include low and high limit sliders,
%   and low and high limit text boxes.  If you enter a value
%   into one of the text boxes outside the slider range,
%   the slider range expands.
%
%   Check or uncheck the INTEGERS check-box to restrict slider
%   values to integers or not.
%
%   H = SCALE returns the handle of the scale control window.
%
%   SCALE('callback_name', ...) invokes a callback.  For internal
%   use.

% Note to self:  when we are called back, check to see if our image (handle)
% is still there.  If not, load a new instance of ourself and close this one.

if nargin == 0  % LAUNCH GUI
    
    % Capture the current figure before we open ours.
    
    % Is there a current figure?
    target_figure = get(0, 'CurrentFigure');
    if isempty(target_figure)
        error('No current figure!')
    end
    
    % What about an axes?
    our_axes = get(target_figure, 'CurrentAxes');
    if isempty(our_axes)
        error('Current figure has no axes!')
    end
    
    % And is there a scaled color image or surface or patch here?
    scale_obj = findobj(our_axes, 'CDataMapping', 'scaled');
    if isempty(scale_obj)
        error('There are no scaled color objects here!')
    end
    % So get the minimum and maximum data values.
    datamin = min(min(get(scale_obj(1), 'CData')));
    datamax = max(max(get(scale_obj(1), 'CData')));
    
    % And just in case there are multiple scaled objects...
    for obj = 2:length(scale_obj)
        datamin = min(datamin, min(min(get(obj, 'CData'))));
        datamax = max(datamax, max(max(get(obj, 'CData'))));
    end
    
    % Raise the image figure.
    figure(target_figure)

	% Open our figure window.
    fig = openfig(mfilename,'new');
    
	% Use system color scheme.
	set(fig,'Color',get(0,'defaultUicontrolBackgroundColor'));

    % Position our window to the right of and alligned with
    % the bottom of the target image window.
    
    % We'll work in pixels.
    set([0, target_figure, fig], 'Units', 'pixels')
    % Where is the target window?
    target_position = get(target_figure, 'Position');
    % Where are we?
    our_position = get(fig, 'Position');
    % How big is the screen?
    screen_size = get(0, 'ScreenSize');
    % Our left edge at their right edge:
    our_position(1) = target_position(1) + target_position(3);
    % But stay on screen.
    if our_position(1) + our_position(3) > screen_size(3)
        our_position(1) = screen_size(3) - our_position(3);
    end
    % Our bottom edge at their bottom edge:
    our_position(2) = target_position(2);
    % But stay on screen.
    if our_position(2) < 1
        our_position(2) = 1;
    end
    % Put ourselves in place.
    set(fig, 'Position', our_position)
    % Hack!
    %   The window isn't moving.  The position property gets
    %   set but the window doesn't move.  Sometimes I can
    %   get the window to move from the keyboard by changing
    %   the height as well as the left and bottom, but that
    %   isn't working in here.  Phhht.
    % 
    %   Okay, it works if I issue a *second* set call with
    %   a modified height element.  Yikes.  I wonder what
    %   happens on Windows?
    our_position(4) = our_position(4) + 1;
    set(fig, 'Position', our_position)
    % This may be gratuitous.  It doesn't work well enough
    % by itself, which is why we had to check the edges of
    % the screen above.
    movegui(fig, 'onscreen')

	% Generate a structure of handles to pass to callbacks, and store it. 
	handles = guihandles(fig);
	guidata(fig, handles);
    
    % Set up our controls according to the axes CLim property and
    % the min and max data values we captured above.
    handles.axes = our_axes;
    handles.datamin = datamin;
    handles.datamax = datamax;
    guidata(fig, handles) 
    
    % If datamin and datamax are integers, set
    % handles.integers checked, otherwise set it unchecked.
    if datamin == fix(datamin) && datamax == fix(datamax)
        set(handles.integers, 'Value', 1)
    else
        set(handles.integers, 'Value', 0)
    end
        
    clim = get(our_axes, 'CLim');
    set(handles.low_text, 'String', num2str(clim(1)))
    set(handles.high_text, 'String', num2str(clim(2)))
    set([handles.low_slider, handles.high_slider], ...
        'Min', datamin, ...
        'Max', datamax)
    adjust_step_size(handles)
    set(handles.low_slider, 'Value', constrain(clim(1), datamin, datamax))
    set(handles.high_slider, 'Value', constrain(clim(2), datamin, datamax))

    % Note to self:  We should set a figure callback routine
    % so if the figure is closed, we close.  If we close first
    % we must remove the callback from the figure as we close.
    
	% Return our handle if requested.
    if nargout > 0
		varargout{1} = fig;
	end

elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK

	try
		[varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
	catch
		disp(lasterr);
	end

end


%| ABOUT CALLBACKS:
%| GUIDE automatically appends subfunction prototypes to this file, and 
%| sets objects' callback properties to call them through the FEVAL 
%| switchyard above. This comment describes that mechanism.
%|
%| Each callback subfunction declaration has the following form:
%| <SUBFUNCTION_NAME>(H, EVENTDATA, HANDLES, VARARGIN)
%|
%| The subfunction name is composed using the object's Tag and the 
%| callback type separated by '_', e.g. 'slider2_Callback',
%| 'figure1_CloseRequestFcn', 'axis1_ButtondownFcn'.
%|
%| H is the callback object's handle (obtained using GCBO).
%|
%| EVENTDATA is empty, but reserved for future use.
%|
%| HANDLES is a structure containing handles of components in GUI using
%| tags as fieldnames, e.g. handles.figure1, handles.slider2. This
%| structure is created at GUI startup using GUIHANDLES and stored in
%| the figure's application data using GUIDATA. A copy of the structure
%| is passed to each callback.  You can store additional information in
%| this structure at GUI startup, and you can change the structure
%| during callbacks.  Call guidata(h, handles) after changing your
%| copy to replace the stored original so that subsequent callbacks see
%| the updates. Type "help guihandles" and "help guidata" for more
%| information.
%|
%| VARARGIN contains any extra arguments you have passed to the
%| callback. Specify the extra arguments by editing the callback
%| property in the inspector. By default, GUIDE sets the property to:
%| <MFILENAME>('<SUBFUNCTION_NAME>', gcbo, [], guidata(gcbo))
%| Add any extra arguments after the last argument, before the final
%| closing parenthesis.



% --------------------------------------------------------------------
function varargout = low_slider_Callback(h, eventdata, handles, varargin)
% Adjust low color limit.  Set the text string.
num = get(h, 'Value');
high_limit = get(handles.high_slider, 'Value');
if num >= high_limit
    stepsize = get(h, 'SliderStep') * (get(h, 'Max') - get(h, 'Min'));
    num = high_limit - stepsize(1);
end
if get(handles.integers, 'Value')
    num = round(num);
end
num = constrain(num, get(h, 'Min'), get(h, 'Max'));
set(h, 'Value', num)
update_image(1, num, handles)
set(handles.low_text, 'String', num2str(num))


% --------------------------------------------------------------------
function varargout = high_slider_Callback(h, eventdata, handles, varargin)
% Adjust high color limit.  Set the text string.
num = get(h, 'Value');
low_limit = get(handles.low_slider, 'Value');
if num <= low_limit
    stepsize = get(h, 'SliderStep') * (get(h, 'Max') - get(h, 'Min'));
    num = low_limit + stepsize(1);
end
if get(handles.integers, 'Value')
    num = round(num);
end
num = constrain(num, get(h, 'Min'), get(h, 'Max'));
set(h, 'Value', num)
update_image(2, num, handles)
set(handles.high_text, 'String', num2str(num))


% --------------------------------------------------------------------
function varargout = low_text_Callback(h, eventdata, handles, varargin)
% Set the low scale limit.  If necessary, expand the slider range.
try
    num = str2double(get(h, 'String'));
catch
    errordlg('That''s not a number.', 'Error', 'modal')
    return
end
if num >= get(handles.high_slider, 'Value')
    errordlg('The low limit must be less than the high limit.', ...
        'Error', 'modal')
    return
end
update_image(1, num, handles)
if num < get(handles.low_slider, 'Min')
    set([handles.low_slider, handles.high_slider], ...
        'Min', num)
end
set(handles.low_slider, 'Value', num)
adjust_step_size(handles)


% --------------------------------------------------------------------
function varargout = high_text_Callback(h, eventdata, handles, varargin)
% Set the high scale limit.  If necessary, expand the slider range.
try
    num = str2double(get(h, 'String'));
catch
    errordlg('That''s not a number.', 'Error', 'modal')
    return
end
if num <= get(handles.low_slider, 'Value')
    errordlg('The high limit must be greater than the low limit.', ...
        'Error', 'modal')
    return
end
update_image(2, num, handles)
if num > get(handles.high_slider, 'Max')
    set([handles.low_slider, handles.high_slider], ...
        'Max', num)
end
set(handles.high_slider, 'Value', num)
adjust_step_size(handles)


% --------------------------------------------------------------------
function varargout = integers_Callback(h, eventdata, handles, varargin)
% Toggle integer step sizes for the sliders.
adjust_step_size(handles)


% --------------------------------------------------------------------
function update_image(which_limit, num, handles)
% Update the axes CLim property.

% First the image.
clim = get(handles.axes, 'CLim');
clim(which_limit) = num;
set(handles.axes, 'CLim', clim)

% If there is a color bar, it needs a colorbar command to update it.
fig = get(handles.axes, 'Parent');
hbar = findobj(fig, 'Tag', 'TMW_COLORBAR');
if ~ isempty(hbar)
    colorbar
end


% --------------------------------------------------------------------
function adjust_step_size(handles)
% Set step sizes for sliders.
min = get(handles.low_slider, 'Min');
max = get(handles.low_slider, 'Max');
if get(handles.integers, 'Value')
    smallstep = 1 / (max - min);  % one integer at a time
    bigstep = 0.1;  % 1/10 range at a time
    % Increase bigstep to nearest integer chunk.
    next_integer_up = ceil(bigstep * (max - min));
    bigstep = next_integer_up / (max - min);
else  % noninteger steps
    smallstep = 0.01;
    bigstep = 0.1;
end
set([handles.low_slider, handles.high_slider], ...
    'SliderStep', [smallstep bigstep]) 


% --------------------------------------------------------------------
function value = constrain(num, minval, maxval)
value = min(max(num, minval), maxval);