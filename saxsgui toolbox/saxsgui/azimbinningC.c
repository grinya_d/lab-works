/*
* =============================================================
* azimbinningC.c
* This is a function wirten in C aimed at speeding up the MatLab
* equivalent (azimbinning.m)
* Function called with:
* 
*[q1,azimave1,azimaver1,azimaverr1]=azimbinningC(DATA,qmin,qmax,qpoint,qtype,phistart,phiend,ycen,xcen,ycalib,xcalib,error_norm_fact)
*
* this routine calculates the azimuthally averaged data  (I vs. phi) given a 2D image and
* some boundary values.
*  it returns
*  q             the x-array into which the data has been binned
*  azimave       the averaged data
*  azimaveerr    the relative error on the average (assuming counting
*  statistics    of gas-detectors)
*  azimaverr2    the relative erron on the average where each pixel value is
*  used as measurement and the error is thus taking on these values...this
*  is usefull for CCD's, IP's. This second approach breaks down for images with lot's
*  of 0, 1, and 2 ..in short for Poisson statistics
* no need to recreate rphi if it's already been created for the same
* parameters
* P.M. Denby 2007
* ============================================================
*/

#include "mex.h"
#include "math.h"

void azimbinningC(double *DATA, int mrows, int ncols, double qmin, double qmax, 
double qpoint, char *qtype, double phimin, double phimax, 
double ycen, double xcen, double ycalib, double xcalib, double ErrorNormFactor,
double *QData, double *AzimAv, double *AzimAvErr, double *AzimAvErr2) // The return of th funcion is void as matlab recieves pointers to the function output
	{
		int   i, j,Bin;
		double qstep, phi, Qmin, q; 
        double Q[2048][2048];
		double Databin[5000][3] ; //define the array which will be outputed (phi, "sumcount", "sumnumpixels", "sumsqcount" (maxiumum size of the array must be set at this point. 5000 rows by 2 columns (may need enlarging)
		const double Pi =3.141593; 
		if (*qtype=='l' && *(qtype+1)=='o' && *(qtype+2)=='g' ) {//(*qtype is simply the pointer to the first charater of "qtype")
			if(qmin<=0) {
				qmin=0.0001; //protects from qmin=0 and log of zero
			}
			qstep=(log(qmax)-log(qmin))/(qpoint-1);
		}
		else
			qstep=(qmax-qmin)/(qpoint-1); //calculate the step value in q  (if log or linear selected)
		Qmin=qmin;
		i=0;
		do
		{
						Databin[i][0]=0;
						Databin[i][1]=0;
						Databin[i][2]=0;
						i=i+1;
		} while (i<=qpoint); //initialise the arrays for the data


		i=0;
		do
		{
			j=0;
			do
			{
				Q[i][j]=sqrt( (((i+1)-xcen)*xcalib)*(((i+1)-xcen)*xcalib) + (((j+1)-ycen)*ycalib)*(((j+1)-ycen)*ycalib)); // calculate R for each point in the inputed Data includeing the calibration array row / col cal.
				phi=-atan2(((j+1)-ycen),-((i+1)-xcen))/(2*Pi)+0.5;  //Note matrix element 1,1 = array element 0,0 hence the i+1
				phi=phi*360; // calculate the value of Phi (taken from creat_rphi)

			if ((phi>=phimin) && (phi<=phimax) ){//	if ((Q>=qmin) && (Q<=qmax) && (phi>=phimin) && (phi<=phimax)) { // if the values of R and phi are within the selected range could install Q limits here to speed up
					if (*qtype=='l' && *(qtype+1)=='o' && *(qtype+2)=='g' )
							Bin=floor((log(Q[i][j])-log(qmin))/qstep); //determin which bin the R goes into (R/phistep) rounded down to nearest integer
						else
							Bin=floor((Q[i][j]-qmin)/qstep);//+1;// removed as Bin=0 exists;

					Databin[Bin][0]=Databin[Bin][0]+*(DATA+i*mrows+j); // (sumcount) sum the number of counts in this bin (the address of DATA[i][j] is i*number of columns+j from the start!
					Databin[Bin][1]=Databin[Bin][1]+1; //(sumpixels) increment the number of pixels contributing to this data bin
					Databin[Bin][2]=Databin[Bin][2]+*(DATA+i*mrows+j)**(DATA+i*mrows+j); //(sumsqcount)determin the relative error
					}
				j=j+1;
			}
			while (j<mrows) ; //a 10x10 matrix has row numbers from 0-9
			i=i+1; //increment i to get the next data point
		}
		while (i<ncols) ;
		// do this loop until the "end of file" is reached

		if (*qtype=='l' && *(qtype+1)=='o' && *(qtype+2)=='g' ) {
			qmin=Qmin; //for some unexplicable reason qmin gets lost without this!!
			q=exp(log(Qmin)+0.5*qstep);
			i=0;
			do
			{
			 // Do a for loop to set the phi value of the data points to be calculated from...to... increment  Do the same with the counter i
			//And while your at it determin the radialaverage, ridialaverage error too!

				*(QData+i)=q; //set column 0 row i to the value of Q
								//** So this means take the address of the first entry of QData and move i entries from it
				*(AzimAv+i)=0;
				if (Databin[i][1] >0) {  //avoid dividing by zero and work out the average count per mr^2
					*(AzimAv+i)=Databin[i][0]/Databin[i][1];// (sumcount/sumpixels)
				}
				*(AzimAvErr+i)=0;
				if (Databin[i][0] >0) {  //avoid dividing by zero and work out the error
					*(AzimAvErr+i)=sqrt(Databin[i][0])/Databin[i][0]*ErrorNormFactor; //(sqrt(sumcount)/sumcount*error_norm_factor)
				}
				*(AzimAvErr2+i)=0;
				if (Databin[i][1]-1 >0 && *(AzimAv+i)*ErrorNormFactor>0) {  //avoid dividing by zero and work out the error
					*(AzimAvErr2+i)=sqrt( (Databin[i][2]-Databin[i][1]*(*(AzimAv+i)* *(AzimAv+i)) ) /(Databin[i][1]-1)) / *(AzimAv+i)*ErrorNormFactor; //sqrt((sumsqcount-sumnumpixels.*azimave.^2)./(sumnumpixels-1))./azimave*error_norm_fact;
				}
				i=i+1;
				q=exp(log(q)+(qstep));
			} while (q<=exp(log(qmax)+(qstep)));
		}

		else {

			for (q=qmin+0.5*qstep, i=0; q<=qmax+0.5*qstep; q=q+qstep, i=i+1) // Do a for loop to set the phi value of the data points to be calculated from...to... increment  Do the same with the counter i
			{																					//And while your at it determin the radialaverage, ridialaverage error too!
				*(QData+i)=q; //set column 0 row i to the value of phi
				*(AzimAv+i)=0;
				if (Databin[i][2] >0) {  //avoid dividing by zero and work out the average count per mr^2
					*(AzimAv+i)=Databin[i][0]/Databin[i][1];
				}
				*(AzimAvErr+i)=0;
				if (Databin[i][0] >0) {  //avoid dividing by zero and work out the error
					*(AzimAvErr+i)=sqrt(Databin[i][0])/Databin[i][0]*ErrorNormFactor;
				}
				*(AzimAvErr2+i)=0;
				if (Databin[i][1]-1 >0 && *(AzimAv+i)*ErrorNormFactor>0) {  //avoid dividing by zero and work out the error
					*(AzimAvErr2+i)=sqrt( (Databin[i][2]-Databin[i][1]*(*(AzimAv+i)* *(AzimAv+i)) ) /(Databin[i][1]-1)) / *(AzimAv+i)*ErrorNormFactor;
				}
			}
		}
	}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] ) //This part takes care of passing the matlab information to and from the function using pointers
{
	int  qtypeLength,mrows, ncols, status;
	char *qtype;
	double qmin, *DATA, qmax, qpoint, phimin, phimax, ycen, xcen, ycalib, xcalib, ErrorNormFactor, *QData, *AzimAv, *AzimAvErr, *AzimAvErr2;
	int npoints;

	npoints=mxGetScalar(prhs[3]); // get the number of data plot points to allow the length of the output array to be set

	plhs[0] = mxCreateDoubleMatrix(1, npoints, mxREAL);
	plhs[1] = mxCreateDoubleMatrix(npoints, 1, mxREAL);
	plhs[2] = mxCreateDoubleMatrix(npoints, 1, mxREAL);
	plhs[3] = mxCreateDoubleMatrix(npoints, 1, mxREAL); // Create blank output matrises

	DATA=mxGetPr(prhs[0]);
	mrows = mxGetM(prhs[0]);
	ncols = mxGetN(prhs[0]); //Need to know the number of rows and columns for the function

	qmin=mxGetScalar(prhs[1]);
	qmax=mxGetScalar(prhs[2]);
	qpoint=mxGetScalar(prhs[3]);

	qtypeLength = (mxGetM(prhs[4]) * mxGetN(prhs[4])) + 1;
	qtype=mxCalloc(qtypeLength, sizeof(char)); //allocate memory for qtype
	status = mxGetString(prhs[4], qtype, qtypeLength); //copy qtype over to c string

	phimin=mxGetScalar(prhs[5]);
	phimax=mxGetScalar(prhs[6]);
	ycen=mxGetScalar(prhs[7]);
	xcen=mxGetScalar(prhs[8]);
	ycalib=mxGetScalar(prhs[9]);
	xcalib=mxGetScalar(prhs[10]);
	ErrorNormFactor=mxGetScalar(prhs[11]);

	QData=mxGetPr(plhs[0]);
	AzimAv=mxGetPr(plhs[1]);
	AzimAvErr=mxGetPr(plhs[2]); // Assign pointers to inputs and outputs
	AzimAvErr2=mxGetPr(plhs[3]);

	azimbinningC(DATA, mrows, ncols, qmin, qmax, qpoint, qtype, phimin, phimax, ycen, xcen, ycalib, xcalib, ErrorNormFactor,
				QData,AzimAv,AzimAvErr,AzimAvErr2); // inputs and outputs to function
	return;
}

