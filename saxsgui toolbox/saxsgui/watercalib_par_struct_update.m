function    watercalib_par_struct_update(handles)
global watercalib_par_temp abs_int_fact_last
% this routine updates the structure based on the changes that have gone on in 
% in the GUI. 
%
% hObject is the handle for where the changes are taking place
%
% this routine only changes the structure  it does not change GUI 

rdt=watercalib_par_temp; %watercalib_par_temp was too long to write

rdt.sample_check=get(handles.sample_check,'Value');
rdt.sample_trans=str2num(get(handles.sample_trans,'String'));
if isempty(rdt.sample_trans), rdt.sample_trans=1; end
if rdt.sample_trans<=0, warndlg('Negative Transmission is garbage', 'Reduction Parameter Warning'); end
if rdt.sample_trans>1, warndlg('Transmission > 1 is non-physical', 'Reduction Parameter Warning'); end

rdt.mask_check=get(handles.mask_check,'Value');
rdt.maskname=get(handles.maskname,'UserData');

rdt.flatfield_check=get(handles.flatfield_check,'Value');
rdt.flatfieldname=get(handles.flatfieldname,'UserData');

rdt.empty_check=get(handles.empty_check,'Value');
rdt.empty_trans=str2num(get(handles.empty_trans,'String'));
if isempty(rdt.empty_trans), rdt.empty_trans=1; end
if rdt.empty_trans>1, warndlg('Transmission > 1 is non-physical', 'Reduction Parameter Warning'); end
if rdt.empty_trans<=0, warndlg('Negative Transmission is garbage', 'Reduction Parameter Warning'); end
rdt.emptyname=get(handles.emptyname,'UserData');

rdt.dark_check=get(handles.dark_check,'Value');
rdt.darkcurrentname=get(handles.darkcurrentname,'UserData');

rdt.readout_check=get(handles.readout_check,'Value');
rdt.readoutnoisename=get(handles.readoutnoisename,'UserData');

rdt.zinger_check=get(handles.zinger_check,'Value');

watercalib_par_temp=rdt;