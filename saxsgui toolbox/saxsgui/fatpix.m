function hfig = fatpix(varargin)
%FATPIX Magnify image pixels under the pointer.
%   FATPIX opens a window showing magnified pixels near the pointer in an
%   image.  FATPIX shows magnified pixels when you press a mouse button in
%   the image and continues updating its view as you move the mouse until
%   you release the mouse button.
%
%   FATPIX(IM) follows the image whose handle is IM.  By default FATPIX
%   tries to find an image in the current axes.
%
%   H = FATPIX returns the handle of the FATPIX window.
%
%   You may combine any of the FATPIX command forms.
%
%   You may change the font size of FATPIX intensity labels by editing the
%   file saxs_preferences.m.

% Default parameters.
saxs_preferences_read  % get fatpix_fontsize
my.targim = [];
my.fontsize = fatpix_fontsize;

% Parse input arguments.
if nargin
    for arg = varargin{:}
        if ishandle(arg) && isequal(get(arg, 'Type'), 'image')
            my.targim = arg;
        else
            error('Bad argument.')
        end
    end
end

% If no image specified, look for one in the current axes.
if isempty(my.targim)
    my.targim = findobj(gca, 'Type', 'image');
    if length(my.targim) > 1
        my.targim = my.targim(1);  % just take first image
    end
end
if isempty(my.targim)
    error('No image in the current axes.')
end

my = createfatpix(my);
figure(my.targfig)  % raise target figure

guidata(my.fig, my)  % store data with our figure

if nargout  % return handle to fatpix window
    hfig = my.fig;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function my = createfatpix(myin)
% Create a fatpix window following my.targim.
my = myin; 
my.fig = figure('Tag', 'fatpix figure', 'Menubar', 'none', ...
    'NumberTitle', 'off', 'Name', 'fatpix', ...
    'Units', 'pixels', 'Position', [1 1 halfscreen halfscreen]);
%try, pack, catch, end  % try packing memory
set(my.fig, 'Colormap', get(get(get(my.targim, 'Parent'), 'Parent'), 'Colormap'))
my.ax = copyobj(get(my.targim, 'Parent'), my.fig);
set(my.ax, 'Units', 'normalized', 'Position', [0.1 0.1 0.8 0.8])
delete(cell2mat(get(my.ax, {'XLabel', 'YLabel', 'Title'})))
grid(my.ax, 'off')
my.targax = get(my.targim, 'Parent');
my.targfig = get(my.targax, 'Parent');
my = setpix(my);
fatnow(my, centralpoint(my.targax));
set(my.targim, 'ButtonDownFcn', {@ButtonDown, my.fig})
guidata(my.fig, my) % throws my-structure into figure-data
drawnow
set(my.fig,'ResizeFcn', @Resize);
set(my.fig,'DeleteFcn', @goodbye);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function my = setpix(my0)
my = my0;
my.charwidth = charwidth(my);
figposn = get(my.fig, 'Position');
my.pixacross = fix(figposn(3) / (4 * my.charwidth));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function width = charwidth(my)
testlabel = text(0, 0, '1234567890', 'Parent', my.ax, 'Units', 'pixels');
extent = get(testlabel, 'Extent');
width = extent(3) / 10;
delete(testlabel)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Resize(fig, event)
my = guidata(fig);
my = setpix(my);
fatnow(my, currentpoint(my));
guidata(fig, my)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ButtonDown(im, event, fatfig)
my = guidata(fatfig);
set(my.targfig, 'WindowButtonUpFcn', @ButtonUp)
set(my.targfig, 'WindowButtonMotionFcn', {@ButtonMove, fatfig})
fatnow(my, currentpoint(my));
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ButtonUp(fig, event)
set(fig, 'WindowButtonMotionFcn', '')
set(fig, 'WindowButtonUpFcn', '')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ButtonMove(fig, event, fatfig)
my = guidata(fatfig);
fatnow(my, currentpoint(my));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fatnow(my, point)
delete(findobj(my.ax, 'Tag', 'F'))
% Get the pixel coordinates of the nearest image pixel to POINT.
x = ax2imx(my.targim, point(1));
y = ax2imy(my.targim, point(2));
% Compute how many pixels away from the point we'll display.
pixdist = fix(my.pixacross / 2);
% Compute x and y ranges in pixel coordinates.
xrange_pix = [x - pixdist, x + pixdist];
yrange_pix = [y - pixdist, y + pixdist];
% Extend endpoints half a pixel for axis limits.
xlim_pix = xrange_pix + [-0.5, 0.5];
ylim_pix = yrange_pix + [-0.5, 0.5];
% Convert to axis coordinates.
xlim_ax = im2axx(my.targim, xlim_pix);
ylim_ax = im2axy(my.targim, ylim_pix);
% Set axis limits to zoom in to the fat pixels.
set(my.ax, 'xlim', xlim_ax, 'ylim', ylim_ax)
% Get pixel coordinates for for all fat pixels.
X_pix = repmat((xrange_pix(1) : xrange_pix(2))', 2 * pixdist + 1, 1);
Y_pix = repmat(yrange_pix(1) : yrange_pix(2), 2 * pixdist + 1, 1);
Y_pix = Y_pix(:);
% Figure axis coordinates to locate labels.
X_label = im2axx(my.targim, X_pix + 0.5);
Y_label = im2axy(my.targim, Y_pix);
% Pull pixel values out of the image.
T = get(my.targim, 'CData');
Theight = size(T, 1);
% Restrict values to those displayed.
T = T(Y_pix + Theight .* (X_pix - 1));
% Create labels.
text(X_label, Y_label, num2str(T, '%.0f'), 'Parent', my.ax, 'Tag', 'F',...
    'FontSize', my.fontsize, 'HorizontalAlignment', 'right', 'EraseMode', 'xor');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function goodbye(obj, event)
my = guidata(gcbo);
try
    set(my.targim, 'ButtonDownFcn', '')  % clear our hook in the image
catch
end
figure(my.fig)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function npixels = halfscreen
oldunits = get(0, 'Units');
set(0, 'Units', 'pixels')
size = get(0, 'ScreenSize');
npixels = 0.5 * min(size(3:4));
set(0, 'Units', oldunits)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function point = currentpoint(my)
point = get(my.targax, 'CurrentPoint');
point = [point(1), point(3)];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function point = centralpoint(ax)
point = [mean(get(ax, 'XLim')), mean(get(ax, 'YLim'))];
