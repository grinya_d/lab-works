function    batch_timeseries_GUIupdate(handles)
global batch_timeseries_temp
% this routine updates the window where Batch parameters can be entered
% to reflect the changes that are made interactively

% hObject is the handle for the figure window
% handles are all the handles referred to by name f.eks. handle.sample
% reduc_par_temp is a structure that holds all the relevant values
% 
% this routine only changes the GUI it does not change any underlying
% parameter values in the structure
rdt=batch_timeseries_temp; %

set(handles.UseIncrementalTime,'Value',rdt.UseIncrementalTime)
set(handles.UseFiledSavedTime,'Value',rdt.UseFiledSavedTime)
set(handles.UseTimeFromHeader,'Value',rdt.UseTimeFromHeader)

s=get(handles.StartBatch,'UserData');
if isempty(strfind(s.detectortype,'PILATUS'))
    set(handles.UseMetaData,'Value',0)
    set(handles.UseMetaData,'Visible','off')
else
    set(handles.UseMetaData,'Visible','on')
    set(handles.UseMetaData,'Value',rdt.UseMetaData)
end
    
set(handles.sample_trans,'String',num2str(rdt.sample_trans))
set(handles.Sample_thickness,'String',num2str(rdt.Sample_thickness))

if get(handles.UseMetaData,'Value')==1
    set(handles.sample_trans,'Visible','Off')
    set(handles.Sample_thickness,'Visible','Off')
    set(handles.text9,'Visible','Off')
    set(handles.text48,'Visible','Off')
else
    set(handles.sample_trans,'Visible','On')
    set(handles.Sample_thickness,'Visible','On')
    set(handles.text9,'Visible','On')
    set(handles.text48,'Visible','On')

end

% set(handles.EvalExpr1,'Enable','Off')
% set(handles.EvalExpr2,'Enable','Off')
% set(handles.EvalExpr3,'Enable','Off')
% set(handles.EvalExpr4,'Enable','Off')
% set(handles.EvalExpr5,'Enable','Off')

set(handles.ReducedDataToSameFile,'Enable','On')

set(handles.EvalExpr1String,'String',num2str(rdt.EvalExpr1String))
set(handles.EvalExpr2String,'String',num2str(rdt.EvalExpr2String))
set(handles.EvalExpr3String,'String',num2str(rdt.EvalExpr3String))
set(handles.EvalExpr4String,'String',num2str(rdt.EvalExpr4String))
set(handles.EvalExpr5String,'String',num2str(rdt.EvalExpr5String))

if rdt.EvalExpr1==1
    set(handles.EvalExpr1,'Value',1)
    set(handles.EvalExpr1String,'Enable','On') 
else
    set(handles.EvalExpr1,'Value',0)
    set(handles.EvalExpr1String,'Enable','Off')
end

if rdt.EvalExpr2==1
    set(handles.EvalExpr2,'Value',1)
    set(handles.EvalExpr2String,'Enable','On')
else
    set(handles.EvalExpr2,'Value',0)
    set(handles.EvalExpr2String,'Enable','Off')
end
if rdt.EvalExpr3==1
    set(handles.EvalExpr3,'Value',1)
    set(handles.EvalExpr3String,'Enable','On')
else
    set(handles.EvalExpr3,'Value',0)
    set(handles.EvalExpr3String,'Enable','Off')
end
if rdt.EvalExpr4==1
    set(handles.EvalExpr4,'Value',1)
    set(handles.EvalExpr4String,'Enable','On')
else
    set(handles.EvalExpr4,'Value',0)
    set(handles.EvalExpr4String,'Enable','Off')
end
if rdt.EvalExpr5==1
    set(handles.EvalExpr5,'Value',1)
    set(handles.EvalExpr5String,'Enable','On')
else
    set(handles.EvalExpr5,'Value',0)
    set(handles.EvalExpr5String,'Enable','Off')
end

set(handles.Save2DImages,'Value',rdt.Save2DImages)
set(handles.Save1DFigures,'Value',rdt.Save1DFigures)

set(handles.ReducedDataToSeparateFiles,'Value',rdt.ReducedDataToSeparateFiles)
set(handles.ReducedDataToSameFile,'Value',rdt.ReducedDataToSameFile)
set(handles.xyformat,'Value',rdt.xyformat)
set(handles.xydeltayformat,'Value',rdt.xydeltayformat)

if (rdt.ReducedDataToSeparateFiles==0 && rdt.ReducedDataToSameFile==0)
    set(handles.xyformat,'Enable','Off')
    set(handles.xydeltayformat,'Enable','Off')
    set(handles.DescriptiveTagString,'Enable','Off')
    set(handles.text46,'Enable','Off')
else
    set(handles.xyformat,'Enable','On')
    set(handles.xydeltayformat,'Enable','On')
    set(handles.DescriptiveTagString,'Enable','On')
    set(handles.text46,'Enable','On')
end

if (rdt.ReducedDataToSeparateFiles==0)
    set(handles.FileFormatText,'Enable','Off')
    set(handles.FileFormat,'Enable','Off')
else
    set(handles.FileFormatText,'Enable','On')
    set(handles.FileFormat,'Enable','On')
end 

files=get(handles.ChooseFiles,'UserData');
if isempty(files)
    set(handles.StartBatch,'Enable','Off')
elseif ~exist(files.path)
    set(handles.StartBatch,'Enable','Off')
else
    set(handles.StartBatch,'Enable','On')
end

if (strcmp(rdt.FileFormat,'PDH')||strcmp(rdt.FileFormat,'RAD') )
    set(handles.xyformat,'Enable','Off')
else
    set(handles.xyformat,'Enable','On')
end 


