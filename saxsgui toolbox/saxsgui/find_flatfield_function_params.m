function p=find_flatfield_function_params(profile,function_type)
global fitpar

pp=profile.pixels(~isnan(profile.pixels));
dx=profile.xaxisfull(2)-profile.xaxisfull(1);
switch function_type
    case 'cosine'
        %try to find first estimates of values
        ybeg=mean(pp([floor(length(pp)/5):floor(length(pp)/4)]));
        yend=mean(pp([floor(length(pp)*18/20):end]));
        xbeg=floor(length(pp)*1/5);
        xend=floor(mean([floor(length(pp)*18/20):length(pp)]));
        if yend<ybeg
            cos_period=xend/(acos(yend/ybeg))*dx;
        else
            cos_period=1000;
        end
        %plot(ybeg*cos((1:length(pp))/cos_period))
        %now try to find better parameters
end
p=[ybeg;cos_period];
%now perform optimization
fitpar.name='ff_cosine.m';
fitpar.binning=1;
fitpar.fit=[1 1];
fitpar.start=[p(1) p(2)];
fitpar.ul= [Inf Inf];
fitpar.ll= [0 0];
fitpar.numiter= 1000;
fitpar.tol=1.0000e-003;
fitpar.graphics= 0;

Fitparam=fitsaxs1D(profile,...
    [profile.xaxisfull(xbeg) profile.xaxisfull(xend)]);
p=Fitparam;
hfig=figure;
plot(profile.xaxisfull,profile.pixels,'ro');
hold on;
plot((1:length(profile.xaxisfull))*dx,Fitparam(1)*cos(profile.xaxisfull/Fitparam(2)),'b')
title('fit to low intensity flatfield data')
xlabel('pixels from image center')
ylabel('intensity (a.u.)')
hold off

answ=questdlg('Is the fit acceptable','Fitting of Low Intensity Image finished','Yes','No','Yes');

if strcmp(answ,'No')
    msgbox({'That is too bad..not much to do about it...except maybe...','1) make a longer measurement', '2) change the used function in matlab (see the manual)'});
    p=[]; 
end
    

