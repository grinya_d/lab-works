function ctop(top)
%CTOP Set top limit of color scaling for scaled image.
%   CTOP(TOP) sets the color scaling for the current image
%   to cover the range 0 to TOP by issuing a CAXIS([0 TOP])
%   command.
%
%   CTOP TOP works too.
%
%   CTOP without an argument sets the top of the mapped
%   intensity range to 10.
if nargin == 0
    top = 10;
elseif ischar(top)
    top = str2num(top);
end
caxis([0 top])

% Note to self:  Check if a color bar is present and if so,
% update it.
