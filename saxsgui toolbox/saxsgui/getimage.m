function [s_out, name_out, dir_out] = getimage(start_in, title)
%GETIMAGE Get SAXS image from file.
%   GETIMAGE displays a dialog window to open a SAXS image file.  If
%   GETIMAGE reads a SAXS image from the file, it returns the SAXS image
%   object.  Otherwise it returns an empty array.
%
%   GETIMAGE can read SAXS images from MPA detector files (see MPAREAD)
%   and MAT files saved from MATLAB (see SAVE or SAXSGUI).  If a MAT file
%   contains more than one SAXS object, GETIMAGE displays a second dialog
%   window to select an image.  [Or it might someday.  Right now it just
%   takes the first SAXS image it finds.]
%
%   GETIMAGE('directory') starts in the specified directory.
%
%   GETIMAGE('directory', 'title') uses the specified title for the dialog
%   window.  Default is 'Open SAXS image file'.  To specify a title but let
%   GETIMAGE use the last directory it worked in, specify an empty string
%   for 'directory'.
%
%   GETIMAGE('file') where 'file' is not a directory, attempts to read a
%   SAXS image from the file without displaying the usual dialog window.
%
%   [S NAME] = GETIMAGE(...) returns saxs object S and the file name NAME,
%   sans directory path.
%
%   [S NAME DIR] = GETIMAGE(...) returns saxs object S, file name NAME, and
%   directory path DIR.
%
%   See also MPAREAD, SAXS, SAXS/IMAGE, LOAD, SAVE.

persistent startdirectory  % remember where we've been
file = [];  % no file specified yet

if nargin > 0 && ~ isempty(start_in)
    % We have either a directory to start in or a file to open.
    if isdir(start_in)
        startdirectory = fullpath(start_in);
    else
        [dir file ext] = fileparts(start_in);
        file = [file, ext];
        if isempty(dir)
            dir = '.';
        end
        dir = fullpath(dir);
        clear ext 
    end
end
if nargin > 1 && ischar(title)
    open_title = title;
else
    open_title = 'Open SAX image file';
end
if isempty(startdirectory)
    startdirectory = '';  % avoid warning from FULLFILE
end
s_out = [];  % we haven't read a SAXS object yet

if isempty(file)
    [file dir] = uigetfile([fullfile(startdirectory, '*.mpa'), ';*.mat'], open_title);
end

if file  % we have a file path
    path = fullfile(dir, file);
    startdirectory = dir;  % look in this directory first next time
    switch filetype(file)
        case 'mpa'
            s_out = trympa(path);
        case 'mat'
            s_out = trymat(path);
        otherwise
            errordlg('I can only read MPA or MAT files.', ...
                'Wrong file type', 'modal')
            s_out = [];
    end
end

% Take care of optional output arguments.
if isempty(s_out)  % we didn't read a SAXS object
    if nargout > 1
        name_out = [];
        if nargout > 2
            dir_out = [];
        end
    end
else  % we did read a SAXS object
    if nargout > 1
        name_out = file;
        if nargout > 2
            dir_out = dir;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trympa(path)
% Try reading an MPA file.
% Return a SAXS object or report an error and return empty.
try
    S = mparead(path);
catch
    problem = lasterr;
    errordlg(problem, 'Error reading MPA file', 'modal')
    S = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = trymat(path)
% Try reading a MAT file.
% Return a SAXS object or report an error and return empty.
% A MAT file may contain 0, 1, or more SAXS objects.
S = [];
% We shall be brave and load the entire MAT file.  This could take up an
% arbitrary amount of space and time.
try
    loaded = load('-mat', path);
catch
    errordlg(lasterr, 'Error reading MAT file', 'modal')
    return
end
% loaded is a structure containing variables read from the file.
% Take the first SAXS variable we find.  Someday, select one from a list.
list = {};
vars = struct2cell(loaded);
for ivar = 1 : length(vars)
    var = vars{ivar};
    if isa(var, 'saxs')
        list = horzcat(list, {var});
    end
end
switch length(list)
    case 0
        errordlg('There are no SAXS variables in that file.', ...
            'No SAXS variables', 'modal')
        return
    otherwise
        S = list{1};
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function type = filetype(path)
% Return file type string, or 0 if we don't recognize the file.
if regexpi(path, '\.mpa$')
    type = 'mpa';
elseif regexpi(path, '\.mat$')
    type = 'mat';
elseif regexpi(path, '\.tiff$') || regexpi(path, '\.tif$')
    type = 'tiff';
else
    type = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
