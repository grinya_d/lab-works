function ii=existstruct(param_name)
ii=0
param_name
if exist(param_name)
    ii=eval(['isstruct(',param_name,')']);
end
