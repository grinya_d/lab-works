function [q,azimave,azimaveerr,azimaveerr2]=azimbinning(DATA,qmin,qmax,qpoint,qtype,phistart,phiend,ycen,xcen,ycalib,xcalib,wavelength,error_norm_fact)
persistent ycenold xcenold ycalibold xcalibold wavelengthold sizedataold Q PHI
global use_saxsgui_mex
%this routine calculates the azimuthally averaged data  (I vs. phi) given a 2D image and
%some boundary values.
% it returns
% q             the x-array into which the data has been binned
% azimave       the averaged data
% azimaveerr    the relative error on the average (assuming counting statistics of gas-detectors)
% azimaverr2    the relative erron on the average where each pixel value is 
%               used as measurement and the error is thus taking on these values...this
%               is usefull for CCD's, IP's. This second approach breaks down for images with lot's
%               of 0, 1, and 2 ..in short for Poisson statistics
%
% March 2007 the option to calcualte the azimuthal binning by C-mex
% routines has been included.
if isempty(wavelengthold), wavelengthold=-1; end
wavelengthtemp=wavelength;
% if c-routines are desired
if use_saxsgui_mex
    disp('c')
    [q,azimave,azimaveerr,azimaveerr2]=azimbinningC(DATA,qmin,qmax,qpoint,qtype,phistart,phiend,ycen,xcen,ycalib,xcalib,error_norm_fact);
else
    %no need to recreate rphi if it's already been created for the same
    %parameters

    if  isempty(ycenold)
        [Q,PHI]=create_qphi_gen(DATA,ycen,xcen,ycalib,xcalib,wavelength) ; %creates 2 matrices having the q-value and phi-values for each pixel
    else
        if isempty(wavelengthtemp), wavelengthtemp=0; end
        if ~(ycenold==ycen && xcenold==xcen && ycalibold==ycalib && xcalibold==xcalib && wavelengthold==wavelengthtemp && isequal(sizedataold,size(DATA),1))
            [Q,PHI]=create_qphi_gen(DATA,ycen,xcen,ycalib,xcalib,wavelength) ; %creates 2 matrices having the q-value and phi-values for each pixel
        end
    end

    if (strcmp(qtype,'log'))
        qsteplog=(log(qmax)-log(qmin))/(qpoint-1);
        q=exp(log(qmin)+0.5*qsteplog:qsteplog:log(qmax)+0.5*qsteplog);
        DATAbin=(floor((log(Q)-log(qmin))/qsteplog)+1).*(PHI>=phistart&PHI<=phiend);
    else  %default is linear binning
        qstep=(qmax-qmin)/(qpoint-1);
        q=qmin+0.5*qstep:qstep:qmax+0.5*qstep;
        DATAbin=floor(((Q-qmin)/qstep)+1).*(PHI>=phistart&PHI<=phiend);
    end

    sumcount=zeros(qpoint,1);
    sumnumpixels=zeros(qpoint,1);
    sumsqcount=zeros(qpoint,1);
    datasize=size(DATA);
    for jj=1:datasize(2)
        for ii=1:datasize(1)
            if ~isnan(DATA(ii,jj)) && ~isinf(DATA(ii,jj)) && DATAbin(ii,jj)>0 && DATAbin(ii,jj)<=qpoint
                sumcount(DATAbin(ii,jj))=sumcount(DATAbin(ii,jj))+DATA(ii,jj);
                sumnumpixels(DATAbin(ii,jj))=sumnumpixels(DATAbin(ii,jj))+1;
                sumsqcount(DATAbin(ii,jj))=sumsqcount(DATAbin(ii,jj))+DATA(ii,jj).^2;
            end
        end
    end
    warning('off','MATLAB:divideByZero')
    azimave=sumcount./sumnumpixels;
    azimaveerr=sqrt(sumcount)./sumcount*error_norm_fact;
    azimaveerr2=sqrt((sumsqcount-sumnumpixels.*azimave.^2)./(sumnumpixels-1))./azimave*error_norm_fact;
    warning('on','MATLAB:divideByZero')

    %updating old for next time the function is called
    ycenold=ycen;
    xcenold=xcen;
    ycalibold=ycalib;
    xcalibold=xcalib;
    wavelengthold=wavelengthtemp;
    sizedataold=size(DATA);
end
