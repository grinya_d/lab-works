function    batch_xygrid_struct_update(handles)
global batch_xygrid_temp
% this routine updates the structure based on the changes that have gone on in 
% in the GUI. 
%
% hObject is the handle for where the changes are taking place
%
% this routine only changes the structure  it does not change GUI 

rdt=batch_xygrid_temp; %reduc_par_temp was too long to write

rdt.Show_Transm=get(handles.Show_Transm,'Value');
rdt.Show_SAXS_Int=get(handles.Show_SAXS_Int,'Value');
rdt.show_orientation=get(handles.show_orientation,'Value');
rdt.ShowQuiver=get(handles.ShowQuiver,'Value');

rdt.Xnum=floor(str2num(get(handles.Xnum,'String')));
if isempty(rdt.Xnum), rdt.Xnum=5; end
if rdt.Xnum<1, warndlg('That"s a strange grid"', 'Grid Warning'); end

rdt.Ynum=floor(str2num(get(handles.Ynum,'String')));
if isempty(rdt.Ynum), rdt.Xnum=5; end
if rdt.Xnum<1, warndlg('That"s a strange grid"', 'Grid Warning'); end

rdt.qmin=str2num(get(handles.qmin,'String'));
if isempty(rdt.qmin), rdt.qmin=0.01; end
if rdt.qmin<0, warndlg('That"s a strange q-min"', 'Qmin Warning'); end

rdt.qmax=str2num(get(handles.qmax,'String'));
if isempty(rdt.qmax), rdt.qmax=0.3; end
if rdt.qmax>2, warndlg('That"s a very large q-max"', 'Qmax Warning'); end

rdt.Save2DImages=get(handles.Save2DImages,'Value');
rdt.Save1DFigures=get(handles.Save1DFigures,'Value');

rdt.ReducedDataToSeparateFiles=get(handles.ReducedDataToSeparateFiles,'Value');
rdt.ReducedDataToSameFile=get(handles.ReducedDataToSameFile,'Value');
rdt.xyformat=get(handles.xyformat,'Value');
rdt.xydeltayformat=get(handles.xydeltayformat,'Value');

rdt.DescriptiveTagString=get(handles.DescriptiveTagString,'String');

batch_xygrid_temp=rdt;