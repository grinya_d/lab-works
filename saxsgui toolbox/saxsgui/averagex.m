function varargout = averagex(varargin) 
%AVERAGEX  Explore intensity averaging in a SAXS image.
%   AVERAGEX(SAXSIM) starts the average explorer window with SAXS image
%   SAXSIM.  
%
%   With the average explorer you select regions of the SAXS image and
%   generate plots of average intensity versus either the x or y
%   coordinate.  Usually SAXSIM will be a SAXS image in polar coordinates
%   and the plots will show average intensity versus momentum transfer
%   (azimuthal averaging).  You can generate plots in different figure
%   windows or overlay plots in one figure.
%
%   AVERAGEX(SAXSIM, CTOP, CMAP) displays SAXSIM using colormap CMAP (an
%   array of RGB values) and upper color scaling limit CTOP.  Leave off
%   CMAP to use the default colormap.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   AVERAGEX, by itself, creates a new AVERAGEX or raises the existing
%   singleton*.
%
%   H = AVERAGEX returns the handle to a new AVERAGEX or the handle to
%   the existing singleton*.
%
%   AVERAGEX('CALLBACK',hObject,eventData,handles,...) calls the local
%   function named CALLBACK in AVERAGEX.M with the given input arguments.
%
%   AVERAGEX('Property','Value',...) creates a new AVERAGEX or raises the
%   existing singleton*.  Starting from the left, property value pairs are
%   applied to the GUI before averagex_OpeningFunction gets called.  An
%   unrecognized property name or invalid value makes property application
%   stop.  All inputs are passed to averagex_OpeningFcn via varargin.
%
%   *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%   instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help averagex

% Last Modified by GUIDE v2.5 24-Mar-2009 13:03:12
%Modified March 24, 2009, by KDJ to allow for "same as last" behavior

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @averagex_OpeningFcn, ...
                   'gui_OutputFcn',  @averagex_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes just before averagex is made visible.
function averagex_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to averagex (see VARARGIN)

global averagex_prefs

% Choose default command line output for averagex
handles.output = hObject;

% Get the input SAXS image.
if length(varargin) > 0 && isa(varargin{1}, 'saxs')
    handles.saxsin = varargin{1};
else
    handles.saxsin = saxs(ones(1024));  % temporary cheat to display empty figure
%     error('I need a SAXS argument.');
end

% Get optional CTOP, CMAP and DISPLOG parameters.  
% No validation, because good programmers are lazy.
if length(varargin) > 1
    handles.ctop = varargin{2};
end
if length(varargin) > 2
    handles.cmap = varargin{3};
end
if length(varargin) > 3
    handles.displog = varargin{4};
end

% saxs_preferences lists a number of averagex x preferences.
% First we should try to set those handles.

if strcmp(averagex_prefs.azbintype,'lin') 
    set(handles.plotazavlin,'Value',1)
    set(handles.plotazavlog,'Value',0) 
end
if strcmp(averagex_prefs.azbintype,'log') 
    set(handles.plotazavlin,'Value',0)
    set(handles.plotazavlog,'Value',1)
end
if strcmp(averagex_prefs.avtype,'az') 
    set(handles.plotazav,'Value',1);
    set(handles.plotmoav,'Value',0);
    plotazav_Callback(handles.plotazav, eventdata, handles)
else
    set(handles.plotazav,'Value',0);
    set(handles.plotmoav,'Value',1);
    plotmoav_Callback(handles.plotmoav, eventdata, handles)
end

% The radiobuttons in the panel have to show only those reductions which
% are available, i.e. those that have been edited in the reduction
% parameter window. This will be done in a separate subroutine.

set_reduction_radiobuttons(handles)

% Record pixel spacing in the SAXS image.
handles.xspacing = (handles.saxsin.xaxis(2) - handles.saxsin.xaxis(1)) ...
    / (size(handles.saxsin.pixels, 2) - 1);
handles.yspacing = (handles.saxsin.yaxis(2) - handles.saxsin.yaxis(1)) ...
    / (size(handles.saxsin.pixels, 1) - 1);
    
% Display the SAXS image (in log if log in SAXSGUI window)
if handles.displog==1
    saxsin=log10(handles.saxsin);
else
    saxsin=handles.saxsin;
end
handles.image1 = image(saxsin, 'Parent', handles.axes1);

if isfield(handles, 'ctop')
    ctop_tmp=handles.ctop;
    ctop=max(max(handles.saxsin.pixels));
else
    ctop=max(max(handles.saxsin.pixels));
end
set(handles.axes1, 'CLim', [0 ctop_tmp])
if isfield(handles, 'cmap')
    set(handles.figure1, 'Colormap', handles.cmap)
end

%Geet Slider Top Value
set(handles.top_text, 'String', num2str(ctop_tmp))
%Set Slider min max Value
set(handles.top_slider, 'Max', max(ctop,ctop_tmp))

%Set Slider Edit Value
set(handles.top_slider, 'Value', ctop_tmp)

% Adjust the title
tit = get(handles.axes1, 'Title');


if handles.displog==1
    set(handles.top_text,'Visible','off')
    set(handles.top_slider,'Visible','off')
    set(handles.text15,'Visible','off')
    set(tit, 'String', 'Logarithmic color scale')
else
    set(handles.top_text,'Visible','on')
    set(handles.top_slider,'Visible','on')
    set(handles.text15,'Visible','on')
    set(tit, 'String', 'Linear Color Scale')
end


% Insert the filename
filename=find_filename(handles.saxsin);
set(handles.Filename, 'String', filename);

% Set the bounds.
set(handles.ytop, 'String', num2str(handles.saxsin.yaxis(2)))
set(handles.ybottom, 'String', num2str(handles.saxsin.yaxis(1)))
set(handles.xleft, 'String', num2str(handles.saxsin.xaxis(1)))
set(handles.xright, 'String', num2str(handles.saxsin.xaxis(2)))

% Create the bounding rectangle.
handles.rect = rectangle('EraseMode', 'xor');
position_rectangle(handles)

% % Save the color order for plotting multiple lines.
% handles.colororder = get(handles.axes2, 'ColorOrder');

% Pick up mouse button down to drag out a rectangle in the left image.
set([handles.image1, handles.rect, handles.axes1, handles.figure1], ...
    'ButtonDownFcn', {@drag, handles})

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes averagex wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Outputs from this function are returned to the command line.
function varargout = averagex_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function position_rectangle(handles)
% Set the bounding rectangle position.
xl = str2double(get(handles.xleft, 'String'));
xr = str2double(get(handles.xright, 'String'));
yb = str2double(get(handles.ybottom, 'String'));
yt = str2double(get(handles.ytop, 'String'));
set(handles.rect, 'Position', [xl, yb, xr - xl, yt - yb])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function argout = bounded(handles)
% Return the left image cropped by the bounding lines.
xl = str2double(get(handles.xleft, 'String'));
xr = str2double(get(handles.xright, 'String'));
yb = str2double(get(handles.ybottom, 'String'));
yt = str2double(get(handles.ytop, 'String'));
argout = crop(handles.saxsin, [xl, yb; xr, yt]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function drag(hobject, eventdata, handles)
% Called on mouse button down over the left image, bounding rectangle, axes
% object, or unoccupied figure space, we drag out a new area for the
% bounding rectangle.  Clip the rectangle to the axes.
point1 = get(handles.axes1, 'CurrentPoint');  % initial mouse position in axes
rbbox;
point2 = get(handles.axes1, 'CurrentPoint');  % final mouse position in axes
point1 = point1(1,1:2);  % extract just x and y
point2 = point2(1,1:2);
p1 = min(point1,point2);  % lower left corner
offset = abs(point1-point2);  % dimensions

if all(offset)  % rectangle has area (no dimension = 0)
    % Select entire image first so we don't run into old boundaries.
    selectall(handles)
    % Update text boxes.
    set(handles.xleft, 'String', num2str(p1(1)))
    xleft_validate(handles)
    set(handles.xright, 'String', num2str(p1(1) + offset(1)))
    xright_validate(handles)
    set(handles.ybottom, 'String', num2str(p1(2)))
    ybottom_validate(handles)
    set(handles.ytop, 'String', num2str(p1(2) + offset(2)))
    ytop_validate(handles)
    
    % Reposition bounding rectangle.
    position_rectangle(handles)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function ytop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ytop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ytop_Callback(hObject, eventdata, handles)
% Interpret new ytop string as a number.
% Keep the number within the limits of the SAXS image.
% Put the interpreted number back in ytop as a string, in case
% it differs from what the user intended.
% Redraw the corresponding bounding line in axes1.
% Replot the right-hand curve.
%
% hObject    handle to ytop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ytop_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ytop_validate(handles)
val = str2double(get(handles.ytop,'String'));  % interpret
val = min([val, handles.saxsin.yaxis(2)]);  % limit high value
val = max([val, str2double(get(handles.ybottom, 'String')) + handles.yspacing]);
    % at least one pixel above bottom bounding line
set(handles.ytop, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function ybottom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ybottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ybottom_Callback(hObject, eventdata, handles)
% hObject    handle to ybottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ybottom_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ybottom_validate(handles)
val = str2double(get(handles.ybottom,'String'));  % interpret
val = max([val, handles.saxsin.yaxis(1)]);  % limit low value
val = min([val, str2double(get(handles.ytop, 'String')) - handles.yspacing]);
    % at least one pixel below top bounding line
set(handles.ybottom, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function xleft_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xleft_Callback(hObject, eventdata, handles)
% hObject    handle to xleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xleft_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xleft_validate(handles)
val = str2double(get(handles.xleft,'String'));  % interpret
val = max([val, handles.saxsin.xaxis(1)]);  % limit low value
val = min([val, str2double(get(handles.xright, 'String')) - handles.xspacing]);
    % at least one pixel short of right bounding line
set(handles.xleft, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes during object creation, after setting all properties.
function xright_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xright_Callback(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xright_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xright_validate(handles)
val = str2double(get(handles.xright,'String'));  % interpret
val = min([val, handles.saxsin.xaxis(2)]);  % limit high value
val = max([val, str2double(get(handles.xleft, 'String')) + handles.xspacing]);
    % at least one pixel right of left bounding line
set(handles.xright, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in plotazav.
function plotazav_Callback(hObject, eventdata, handles)
% hObject    handle to plotazav (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotazav
set(handles.plotazav, 'Value', 1)
set(handles.text9,'Visible','on')
set(handles.plotazavlin,'Visible','on')
set(handles.plotazavlog,'Visible','on')
set(handles.text10,'Visible','On')
set(handles.plotazav_npts,'Visible','on')
set(handles.plotmoav, 'Value', 0)
set(handles.plotmoav_npts,'Visible','off')
set(handles.text11,'Visible','off')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in plotazavlin.
function plotazavlin_Callback(hObject, eventdata, handles)
% hObject    handle to plotazav (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotazav
set(handles.plotazavlin, 'Value', 1)
set(handles.plotazavlog, 'Value', 0)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in plotazav.
function plotazavlog_Callback(hObject, eventdata, handles)
% hObject    handle to plotazav (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotazav
set(handles.plotazavlin, 'Value', 0)
set(handles.plotazavlog, 'Value', 1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotazav_npts_Callback(hObject, eventdata, handles)
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
plotazav_npts_validate(handles)
position_rectangle(handles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotazav_npts_validate(handles)
val = str2double(get(handles.plotazav_npts,'String'));  % interpret
val = max(2,min(10000,val));  % limit between 2 and 10000
if val~=str2double(get(handles.plotazav_npts,'String'))
    msgbox('Value must be between 2 and 10000')
end
set(handles.plotazav_npts, 'String', num2str(val))  % display what we interpreted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in plotmoav.
function plotmoav_Callback(hObject, eventdata, handles)
% hObject    handle to plotmoav (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotmoav
set(handles.plotazav, 'Value', 0)
set(handles.text9,'Visible','off')
set(handles.plotazavlin,'Visible','off')
set(handles.plotazavlog,'Visible','off')
set(handles.text10,'Visible','off')
set(handles.plotazav_npts,'Visible','off')
set(handles.plotmoav, 'Value', 1)
set(handles.plotmoav_npts,'Visible','on')
set(handles.text11,'Visible','on')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotmoav_npts_Callback(hObject, eventdata, handles) 
% hObject    handle to xright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
plotmoav_npts_validate(handles)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotmoav_npts_validate(handles)
val = str2double(get(handles.plotmoav_npts,'String'));  % interpret
val = max(2,min(3600,val));  % limit between 2 and 3600
if val~=str2double(get(handles.plotmoav_npts,'String'))
    msgbox('Value must be beween 2 and 3600')
end
set(handles.plotmoav_npts, 'String', num2str(val))  % display what we interpreted

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in newplot.
function newplot_Callback(hObject, eventdata, handles)
% hObject    handle to newplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% First saving some of the parameters to be used later in the SameAsLast 
% buttons
SameAsLast_Callback(hObject, eventdata, handles)
SameAsLastAveParams_Callback(hObject, eventdata, handles)
% now to continue

remove_figstatus % this line deletes all the yellow "statuslines"
az = get(handles.plotazav, 'Value');  % true for azimuthal average
bounds=get(handles.rect, 'Position');
if get(handles.pixel_by_pixel_reduction,'Value')==1
    handles.saxsin=reduction_type(handles.saxsin,1);
else
    handles.saxsin=reduction_type(handles.saxsin,0);
end
hstatus=figstatus('Reducing Data',get(hObject,'Parent'));
drawnow

if az
    npts=str2num(get(handles.plotazav_npts,'String'));
    handles.saxsin=xaxisbintype(handles.saxsin,get(handles.plotazavlog,'Value'));
    handles.saxsin=reduction_state(handles.saxsin,handles);
    if strcmp(handles.saxsin.reduction_type,'s')
        %perform spectra-by-spectra averaging
        profile = average_raw(handles.saxsin, 'x',bounds, npts); 
    else
        %perform pixel-by-pixel averaging
        profile =average_p(handles.saxsin,'x',bounds,npts);
    end
    txt='averagex_plot';
else
    npts=str2num(get(handles.plotmoav_npts,'String'));
    handles.saxsin=xaxisbintype(handles.saxsin,0); %0 means it is linear
    handles.saxsin=reduction_state(handles.saxsin,handles);
    if strcmp(handles.saxsin.reduction_type,'s')
        %perform spectra-by-spectra averaging
        profile = average_raw(handles.saxsin, 'y',bounds, npts); 
    else
        %perform pixel-by-pixel averaging
        profile =average_p(handles.saxsin,'y',bounds,npts);
    end
    txt='averagey_plot';
end

profile=strip_reduction_pixels(profile);
fig = plotx(profile);
set(fig, 'Tag', txt)  % mark it so we can find it later
set(findobj(get(fig,'Children'),'Type','axes'),'Tag','plot');
h=reduction_text(findobj(get(fig,'Children'),'Tag','plot'));
delete(hstatus)
% leg = regionstr(handles);
% legend(leg)
% set(gca, 'UserData', {leg})
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in addplot.
function addplot_Callback(hObject, eventdata, handles)
% Add an average intensity plot to the topmost spawned window.
% hObject    handle to addplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% First saving some of the parameters to be used later in the SameAsLast 
% buttons
SameAsLast_Callback(hObject, eventdata, handles)
SameAsLastAveParams_Callback(hObject, eventdata, handles)
% now to continue
remove_figstatus % this line deletes all the yellow "statuslines"
az = get(handles.plotazav, 'Value');  % true for azimuthal average
bounds=get(handles.rect, 'Position');
if get(handles.pixel_by_pixel_reduction,'Value')==1
    handles.saxsin=reduction_type(handles.saxsin,1);
else
    handles.saxsin=reduction_type(handles.saxsin,0);
end
hstatus=figstatus('Reducing Data',get(hObject,'Parent'));
drawnow
if az
    npts=str2num(get(handles.plotazav_npts,'String'));
    handles.saxsin=xaxisbintype(handles.saxsin,get(handles.plotazavlog,'Value')) ;
    handles.saxsin=reduction_state(handles.saxsin,handles);
    if strcmp(handles.saxsin.reduction_type,'s')
        %perform spectra-by-spectra averaging
        profile = average_raw(handles.saxsin, 'x',bounds, npts); 
    else
        %perform pixel-by-pixel averaging
        profile =average_p(handles.saxsin,'x',bounds,npts);
    end
    txt='averagex_plot';
else
    npts=str2num(get(handles.plotmoav_npts,'String'));
    handles.saxsin=xaxisbintype(handles.saxsin,0); %0 means it is linear
    handles.saxsin=reduction_state(handles.saxsin,handles);
    if strcmp(handles.saxsin.reduction_type,'s')
        %perform spectra-by-spectra averaging
        profile = average_raw(handles.saxsin, 'y',bounds, npts); 
    else
        %perform pixel-by-pixel averaging
        profile =average_p(handles.saxsin,'y',bounds,npts);
    end
    txt='averagey_plot';
end

figs = get(0, 'Children');  % handles to all figure windows
thefig = 0;  % figure to add to if nonzero
if az
    for fig = figs'
        if isequal(get(fig, 'Tag'), 'averagex_plot')  % one of ours
            thefig = fig;
            break
        end
    end
else
    for fig = figs'
        if isequal(get(fig, 'Tag'), 'averagey_plot')  % one of ours
            thefig = fig;
            break
        end
    end
end
if thefig  % if we found one
    figure(thefig)
    ax = findobj(get(thefig,'Children'),'Type','axes');
    if strcmp(get(ax(2),'Tag'),'legend')
        ax=ax(1);
    else
        ax = ax(2);  % first one found
    end
    set(ax, 'NextPlot', 'add')
    %     leg = get(ax, 'UserData');
    %     leg = [leg, {regionstr(handles)}];  % extend cell array
    %     set(ax, 'UserData', leg)
    % Cycle through color order.
    colororder = get(ax, 'ColorOrder');
    color = colororder(mod(nlines(ax)-1,7)+1, :);
    profile=strip_reduction_pixels(profile);
    plot(profile, 'Color', color, 'Parent', ax)
    %     legend(leg{:})
    h=reduction_text(ax);
    
    % now removing fitting capability
    %analysismenu=findobj(get(get(ax,'Parent'),'Children'),'Label','Analysis');
    %quikfitmenu=findobj(get(analysismenu,'Children'),'Label','QuikFit');
    %set(quikfitmenu,'Visible','Off')


else
    newplot_Callback(hObject, eventdata, handles)
end

delete(hstatus)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function n = nlines(hax)
% Return number of average intensity lines in current plot.
n = 0;
childtypes = get(get(hax, 'Children'), 'Type');
% May have a one-line character array or a cell array.
if ischar(childtypes)
    childtypes = {childtypes};  % make it a cell just for uniformity
end
for itype = 1 : length(childtypes)
    if isequal(childtypes{itype}, 'line')
        n = n + 1;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function argout = regionstr(handles)
% Return a string '(x1,y1) to (x2,y2)' for the bounded region.
argout = ['(', get(handles.xleft, 'String'), ...
        ',', get(handles.ybottom, 'String'), ...
        ') to (', get(handles.xright, 'String'), ...
        ',', get(handles.ytop, 'String'), ')'];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function selectall(handles)
% Set bounds to select all of image.  Note we don't position the bounding
% rectangle so if you want to, call position_rectangle after us.
set(handles.xleft, 'String', num2str(handles.saxsin.xaxis(1)));
set(handles.xright, 'String', num2str(handles.saxsin.xaxis(2)));
set(handles.ybottom, 'String', num2str(handles.saxsin.yaxis(1)));
set(handles.ytop, 'String', num2str(handles.saxsin.yaxis(2)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes during object creation, after setting all properties.
function plotazav_npts_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotazav_npts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global averagex_prefs

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
set(hObject,'String',num2str(averagex_prefs.aznumpoints));

% --- Executes during object creation, after setting all properties.
function plotmoav_npts_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotmoav_npts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global averagex_prefs
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
set(hObject,'String',num2str(averagex_prefs.monumpoints));

% --- Executes during object creation, after setting all properties.
function set_reduction_radiobuttons(handles)

global averagex_prefs

if isempty(handles.saxsin.reduction_state) || strcmp(averagex_prefs.reducttype,'s')
    set(handles.spectra_by_spectra_reduction,'Value',1)
    set(handles.pixel_by_pixel_reduction,'Value',0)
else
    set(handles.spectra_by_spectra_reduction,'Value',0)
    set(handles.pixel_by_pixel_reduction,'Value',1)
end

if isempty(handles.saxsin.mask_filename)
    set(handles.mask_cor,'Enable','off')
else 
    set(handles.mask_cor,'Enable','on')
    set(handles.mask_cor,'Value',1)
end
if isempty(handles.saxsin.flatfield_filename)
    set(handles.flatfield_cor_ave,'Enable','off')
    set(handles.flatfield_cor_raw,'Enable','off')
else 
    set(handles.flatfield_cor_ave,'Enable','on')
    set(handles.flatfield_cor_raw,'Enable','on')
    set(handles.flatfield_cor_raw,'Value',1)
end
if isempty(handles.saxsin.transfact)
    set(handles.sampletrans_cor,'Enable','off')
else 
    set(handles.sampletrans_cor,'Enable','on')
    set(handles.sampletrans_cor,'Value',1)
end
if isempty(handles.saxsin.empty_filename)
    set(handles.empty_sub,'Enable','off')
else 
    set(handles.empty_sub,'Enable','on')
    set(handles.empty_sub,'Value',1)
end
if isempty(handles.saxsin.darkcurrent_filename)
    set(handles.darkcurrent_sub,'Enable','off')
else 
    set(handles.darkcurrent_sub,'Enable','on')
    set(handles.darkcurrent_sub,'Value',1)
end
if isempty(handles.saxsin.readoutnoise_filename)
    set(handles.readout_sub,'Enable','off')
else 
    set(handles.readout_sub,'Enable','on')
    set(handles.readout_sub,'Value',1)
end
if (handles.saxsin.zinger_removal == 1)
    set(handles.zinger_cor,'Enable','on')
    set(handles.zinger_cor,'Value',1)
else 
    set(handles.zinger_cor,'Enable','off')
end
if isempty(handles.saxsin.abs_int_fact)
    set(handles.absolute_cor,'Enable','off')
else 
    set(handles.absolute_cor,'Enable','on')
    set(handles.absolute_cor,'Value',1)
end

if isempty(handles.saxsin.sample_thickness) 
    set(handles.samplethick_cor,'Enable','off')
else 
    set(handles.samplethick_cor,'Enable','on')
    set(handles.samplethick_cor,'Value',1)
end

% --- Executes on button press in mask_cor.
function mask_cor_Callback(hObject, eventdata, handles)
% hObject    handle to mask_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mask_cor


% --- Executes on button press in flatfield_cor_ave.
function flatfield_cor_ave_Callback(hObject, eventdata, handles)
% hObject    handle to flatfield_cor_ave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flatfield_cor_ave
if get(hObject,'Value')==1 
    set(handles.flatfield_cor_raw,'Value',0)
end

% --- Executes on button press in flatfield_cor_raw.
function flatfield_cor_raw_Callback(hObject, eventdata, handles)
% hObject    handle to flatfield_cor_raw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flatfield_cor_raw
if get(hObject,'Value')==1 
    set(handles.flatfield_cor_ave,'Value',0)
end

% --- Executes on button press in sampletrans_cor.
function sampletrans_cor_Callback(hObject, eventdata, handles)
% hObject    handle to sampletrans_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of sampletrans_cor
% you can only turn sample transmission off if you do have have empty
% holder subtraction or darkcurrent subtraction
if get(hObject,'Value')==0 && (get(handles.empty_sub,'Value')==1 || get(handles.darkcurrent_sub,'Value')==1)
    set(hObject,'Value',1)
end

% --- Executes on button press in empty_sub.
function empty_sub_Callback(hObject, eventdata, handles)
% hObject    handle to empty_sub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of empty_sub
if get(hObject,'Value')==1 
    set(handles.sampletrans_cor,'Value',1)
end 

% --- Executes on button press in darkcurrent_sub.
function darkcurrent_sub_Callback(hObject, eventdata, handles)
% hObject    handle to darkcurrent_sub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of darkcurrent_sub
if get(hObject,'Value')==1 
      set(handles.sampletrans_cor,'Value',1)
end

% --- Executes on button press in absolute_cor.
function absolute_cor_Callback(hObject, eventdata, handles)
% hObject    handle to absolute_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of absolute_cor
if get(hObject,'Value')==1 
    set(handles.sampletrans_cor,'Value',1)
end

% --- Executes on button press in samplethick_cor.
function samplethick_cor_Callback(hObject, eventdata, handles)
% hObject    handle to samplethick_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of samplethick_cor




% --- Executes on button press in readout_sub.
function readout_sub_Callback(hObject, eventdata, handles)
% hObject    handle to readout_sub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of readout_sub


% --- Executes on button press in zinger_cor.
function zinger_cor_Callback(hObject, eventdata, handles)
% hObject    handle to zinger_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of zinger_cor


% --- Executes on button press in pixel_by_pixel_reduction.
function pixel_by_pixel_reduction_Callback(hObject, eventdata, handles)
% hObject    handle to pixel_by_pixel_reduction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pixel_by_pixel_reduction
set(handles.pixel_by_pixel_reduction, 'Value', 1)
set(handles.spectra_by_spectra_reduction, 'Value', 0)


% --- Executes on button press in spectra_by_spectra_reduction.
function spectra_by_spectra_reduction_Callback(hObject, eventdata, handles)
% hObject    handle to spectra_by_spectra_reduction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of spectra_by_spectra_reduction
set(handles.pixel_by_pixel_reduction, 'Value', 0)
set(handles.spectra_by_spectra_reduction, 'Value', 1)


% --- Executes during object creation, after setting all properties.
function top_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to top_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function top_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to top_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLOR SCALE CONTROL CALLBACKS AND UTILITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_slider(slider, gui)
% Callback for slider controlling top of color scale.
% top_text calls this too.
top = max(1,fix(get(slider, 'Value')));
set(slider, 'Value', top)
set(gui.top_text, 'String', num2str(top))
set([gui.axes1], 'CLim', [0 top])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function top_text(edit, gui)
% Callback for text edit box controlling top of color scale.
num = str2double(get(edit, 'String'));
if ~ isnan(num)  % we have a number
    slider = gui.top_slider;
    if num >= get(slider, 'Min') && num <= get(slider, 'Max')
        set(slider, 'Value', num)
    end
    top_slider(slider, gui)  % update everything
end


% --- Executes on button press in SameAsLast.
function SameAsLast_Callback(hObject, eventdata, handles)
% hObject    handle to SameAsLast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 
% We allow this to be called from both the "same as" buttons in the gui 
%(where it updates the gui using the persistent parameters) 
% or from the newplot and addplot routines where
% the persistent parameters are updated
persistent averagex_bounds_old
try
    if hObject==(handles.SameAsLast)
        set(handles.xleft,'String',num2str(averagex_bounds_old.xleft))
        xleft_Callback(handles.xleft,eventdata,handles)
        set(handles.xright,'String',num2str(averagex_bounds_old.xright))
        xright_Callback(handles.xright,eventdata,handles)
        set(handles.ybottom,'String',num2str(averagex_bounds_old.ybottom))
        ybottom_Callback(handles.ybottom,eventdata,handles)
        set(handles.ytop,'String',num2str(averagex_bounds_old.ytop))
        ytop_Callback(handles.ytop,eventdata,handles)
    else
        averagex_bounds_old.xleft=get(handles.xleft,'String');
        averagex_bounds_old.xright=get(handles.xright,'String');
        averagex_bounds_old.ybottom=get(handles.ybottom,'String');
        averagex_bounds_old.ytop=get(handles.ytop,'String');
    end
catch
    averagex_bounds_old.xleft=get(handles.xleft,'String');
    averagex_bounds_old.xright=get(handles.xright,'String');
    averagex_bounds_old.ybottom=get(handles.ybottom,'String');
    averagex_bounds_old.ytop=get(handles.ytop,'String');
end

% --- Executes on button press in SameAsLastAveParams.
function SameAsLastAveParams_Callback(hObject, eventdata, handles)
% hObject    handle to SameAsLastAveParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Executes on button press in SameAsLast.

% We allow this to be called from both the "same as" buttons in the gui
%(where it updates the gui using the persistent parameters)
% or from the newplot and addplot routines where
% the persistent parameters are updated
persistent averagex_params_old
try
    if hObject==(handles.SameAsLastAveParams)
        set(handles.plotazavlin,'Value',averagex_params_old.plotazavlin)
        set(handles.plotazavlog,'Value',averagex_params_old.plotazavlog)
        set(handles.plotazav,'Value',averagex_params_old.plotazav)
        set(handles.plotmoav,'Value',averagex_params_old.plotmoav)
        set(handles.spectra_by_spectra_reduction,'Value',averagex_params_old.spectra_by_spectra_reduction)
        set(handles.pixel_by_pixel_reduction,'Value',averagex_params_old.pixel_by_pixel_reduction)
        set(handles.plotazav_npts,'String',num2str(averagex_params_old.plotazav_npts));
        set(handles.plotmoav_npts,'String',num2str(averagex_params_old.plotmoav_npts));
        % hide the parameters that are not relevant
        if averagex_params_old.plotazav==1
            plotazav_Callback(handles.plotazav, eventdata, handles)
        else
            plotmoav_Callback(handles.plotmoav, eventdata, handles)
        end
    else
        averagex_params_old.plotazavlin=set(handles.plotazavlin,'Value');
        averagex_params_old.plotazavlog=set(handles.plotazavlog,'Value');
        averagex_params_old.plotazav=set(handles.plotazav,'Value');
        averagex_params_old.plotmoav=set(handles.plotmoav,'Value');
        averagex_params_old.spectra_by_spectra_reduction=set(handles.spectra_by_spectra_reduction,'Value');
        averagex_params_old.pixel_by_pixel_reduction=set(handles.pixel_by_pixel_reduction,'Value');
        averagex_params_old.plotazav_npts=str2num(set(handles.plotazav_npts,'String'));
        averagex_params_old.plotmoav_npts=str2num(set(handles.plotmoav_npts,'String'));
    end
catch
    averagex_params_old.plotazavlin=get(handles.plotazavlin,'Value');
    averagex_params_old.plotazavlog=get(handles.plotazavlog,'Value');
    averagex_params_old.plotazav=get(handles.plotazav,'Value');
    averagex_params_old.plotmoav=get(handles.plotmoav,'Value');
    averagex_params_old.spectra_by_spectra_reduction=get(handles.spectra_by_spectra_reduction,'Value');
    averagex_params_old.pixel_by_pixel_reduction=get(handles.pixel_by_pixel_reduction,'Value');
    averagex_params_old.plotazav_npts=str2num(get(handles.plotazav_npts,'String'));
    averagex_params_old.plotmoav_npts=str2num(get(handles.plotmoav_npts,'String'));

end


