function coeff = transcoeff(pd_sample, pd_empty, pd_dark)
%TRANSCOEFF Transmission coefficient from photodiode intensities.
%   TRANSCOEFF(PD_SAMPLE, PD_EMPTY, PD_DARK) returns the transmission
%   coefficient of a sample given the photodiode intensities recorded with
%   sample, without sample (empty), and without the x-ray beam (dark).

coeff = (pd_sample - pd_dark) / (pd_empty - pd_dark);
