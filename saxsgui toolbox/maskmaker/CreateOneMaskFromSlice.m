function binimgout=CreateOneMaskFromSlice(imgin,linepoints)
% this routine creates an 0/1-image the size of imgin with pointvalues
% equal to 1 for pixels that have a position on the inside of the
% circleslice given by the 3 vertices in linepoints (center of slice, first
% edge of slice, last edge of slice).
%
% By convention the second edge of slice is always counter clockwise
%
% The technique is very basic and essentially consist of
% 1) locating the center of the figure 
% 2) locating the angle (with respect to the center) for the two corners of
% the slice
% 2) creating an x y image using meshgrid
% 4) then calculating a phi image
% 5) creating an image in which phi-values between the two corners give an image value of 1
%
% so here goes

%First we want to make sure that linepoints has the right format (since it
%is a 2-dimensional matrix (x- and y-positions) and that is does not have
%more points than needed.
cen=linepoints(:,1);
firstp=linepoints(:,2);
lastp=linepoints(:,3);
radius=sqrt(sum((firstp(1:2)-cen(1:2)).^2));


a1=atan2(firstp(2)-cen(2),firstp(1)-cen(1)); % angle of 1st edge of slice
a2=atan2(lastp(2)-cen(2),lastp(1)-cen(1)); % angle of last (2nd) edge of slice
% atan2 generates results from -pi to +pi with 0 being horizontal, to the
% right...if one wants to find the angle between the two one has to jump
% through some hoops

if (a1<=a2)
    a=a2-a1;
else
    a=2*pi+(a2-a1);
end

%at this we generate some necessary imagedata

[ymax,xmax]=size(imgin);
[x,y]=meshgrid(1:xmax,1:ymax);


%now we associate with each pixel a distance to the center
R=sqrt((x-cen(1)).^2+(y-cen(2)).^2);
% and an angle
phi=atan2(y-cen(2),x-cen(1));
phia1=phi*0+a1; %this is used to allow the logical comparison of phi to a1.


phirel=mod(2*pi+(phi-phia1),2*pi);
   

binimgout=(R<=radius(1)).*(phirel<a);
