function edit_rect_wbdf(nodummy)
% --- Executes on button click in editing mode. Register mouse position and
% updates position of rectangle vertexes
global x y jj kk xtemp ytemp
if nargin==1 % this is just for touching the file (for executable)
    return
end
kk=1; %this serves as a flag for the updating to stop
point1 = get(gca,'CurrentPoint');
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
xtemp=x;
ytemp=y;
if jj(1)==1 || jj(1)==5  %first point is being edited
    xtemp(1)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(1)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(2)=xtemp(3);
    ytemp(2)=ytemp(1);
    xtemp(3)=xtemp(3);
    ytemp(3)=ytemp(3);
    xtemp(4)=xtemp(1);
    ytemp(4)=ytemp(3);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
elseif jj(1)==2   %second point is being edited
    xtemp(2)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(2)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(1)=xtemp(1);
    ytemp(1)=ytemp(2);
    xtemp(3)=xtemp(2);
    ytemp(3)=ytemp(3);
    xtemp(4)=xtemp(4);
    ytemp(4)=ytemp(4);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
elseif jj(1)==3 %third point is being edited
    xtemp(3)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(3)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(1)=xtemp(1);
    ytemp(1)=ytemp(1);
    xtemp(2)=xtemp(3);
    ytemp(2)=ytemp(2);
    xtemp(4)=xtemp(4);
    ytemp(4)=ytemp(3);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
elseif jj(1)==4 %third point is being edited
    xtemp(4)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(4)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(1)=xtemp(4);
    ytemp(1)=ytemp(1);
    xtemp(2)=xtemp(2);
    ytemp(2)=ytemp(2);
    xtemp(3)=xtemp(3);
    ytemp(3)=ytemp(4);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
end
x=xtemp;
y=ytemp;
