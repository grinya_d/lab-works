function edit_circle_wbmf(nodummy)
% --- Executes on button mousemove in editing mode. Updating as you go
global xtemp ytemp jj
if nargin==1 % this is just for touching the file (for executable)
    return
end
point1 = get(gca,'CurrentPoint');
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
if jj(1)==1 %center is active
    xtemp1=min(xrange(2),max(xrange(1),point1(1,1))); %make sure new point is withing window boundaries
    ytemp1=min(yrange(2),max(yrange(1),point1(1,2))); %make sure new point is withing window boundaries
    center = [xtemp1,ytemp1];           % use new center
    radius = sqrt((xtemp(2)-xtemp(1)).^2+(ytemp(2)-ytemp(1)).^2);
end
if jj(1)>1 %perimeter is active
    xtemp(2)=min(xrange(2),max(xrange(1),point1(1,1))); %make sure new point is withing window boundaries
    ytemp(2)=min(yrange(2),max(yrange(1),point1(1,2))); %make sure new point is withing window boundaries
    center = [xtemp(1),ytemp(1)]    ;       
    radius = sqrt((xtemp(2)-xtemp(1)).^2+(ytemp(2)-ytemp(1)).^2); %use new radius
end
ang=[0:1:360]*2*pi/360; %use 360 points
xtemp=[center(1),center(1)+radius*cos(ang)];
ytemp=[center(2),center(2)+radius*sin(ang)];
