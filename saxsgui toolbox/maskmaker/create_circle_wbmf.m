function create_circle_wbmf(nodummy)
global point
if nargin==1 % this is just for touching the file (for executable)
    return
end
% this routine builds the circle during creation
c=get(gca,'Children');
delete(c(1)); % deletes old figure.
temppoint=[point;get(gca,'CurrentPoint')]; %this add the clicked point to the description of the circle

center = temppoint(1,:);              % calculations
radius = sqrt(sum((temppoint(2,1:2)-temppoint(1,1:2)).^2));
ang=[0:1:360]*2*pi/360;
xvalues=[center(1),center(1)+radius*cos(ang)];
yvalues=[center(2),center(2)+radius*sin(ang)];
h=plot(xvalues,yvalues,'r-','linewidth',2); %this creates the new plot

