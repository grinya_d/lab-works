function edit_rect_wbmf(nodummy)
% --- Executes on button mousemove in editing mode. continuously updating
global xtemp ytemp jj
if nargin==1 % this is just for touching the file (for executable)
    return
end
point1 = get(gca,'CurrentPoint'); %picks up postion of cursor
xrange=get(gca,'XLim');
yrange=get(gca,'YLim');
if jj(1)==1 || jj(1)==5  %first point is being edited
    xtemp(1)=min(xrange(2),max(xrange(1),point1(1,1))); 
    ytemp(1)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(2)=xtemp(3);
    ytemp(2)=ytemp(1);
    xtemp(3)=xtemp(3);
    ytemp(3)=ytemp(3);
    xtemp(4)=xtemp(1);
    ytemp(4)=ytemp(3);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
elseif jj(1)==2   %second point is being edited
    xtemp(2)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(2)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(1)=xtemp(1);
    ytemp(1)=ytemp(2);
    xtemp(3)=xtemp(2);
    ytemp(3)=ytemp(3);
    xtemp(4)=xtemp(4);
    ytemp(4)=ytemp(4);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
elseif jj(1)==3 %third point is being edited
    xtemp(3)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(3)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(1)=xtemp(1);
    ytemp(1)=ytemp(1);
    xtemp(2)=xtemp(3);
    ytemp(2)=ytemp(2);
    xtemp(4)=xtemp(4);
    ytemp(4)=ytemp(3);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
elseif jj(1)==4 %third point is being edited
    xtemp(4)=min(xrange(2),max(xrange(1),point1(1,1)));
    ytemp(4)=min(yrange(2),max(yrange(1),point1(1,2)));
    xtemp(1)=xtemp(4);
    ytemp(1)=ytemp(1);
    xtemp(2)=xtemp(2);
    ytemp(2)=ytemp(2);
    xtemp(3)=xtemp(3);
    ytemp(3)=ytemp(4);
    xtemp(5)=xtemp(1);
    ytemp(5)=ytemp(1);
end