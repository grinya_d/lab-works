function binimgout=CreateOneMaskFromLine(imgin,linepoints)
% this routine creates an 0/1-image the size of imgin with pointvalues
% equal to 1 for pixels that have a position on the inside of the
% convex (very important!) polygon given by vertices in linepoints
%
% The technique is very basic and essentially consist of
% 1) locating the center of the figure 
% 2) for each line dividing the image up in 2 sections...one pointing
% towards the center (positive) and one pointing away from the center negative (with
% respect to the line.
% 3) if we let the positive sides be represented by 1 and the negativesize
% by 0...then multiplying the results for each line in the figure will
% yield 1's on the inside of the figure and 0's outside....which is what we
% wanted
%
% so here goes


%First we want to make sure that linepoints has the right format (since it
%is a 2-dimensional matrix (x- and y-positions) and that is does not have
%more points than needed.
dimens=size(linepoints);

if max(linepoints(1,:))-0.5>length(imgin(1,:)) || max(linepoints(2,:))-0.5>length(imgin(2,:))
    warndlg('The mask exceeds the boundary of the image. No mask created')
    binimgout= [];
    return
end

if dimens(1)>dimens(2)
    linepoints=linepoints';
end

%First we want to make sure that it is a closed polygon...i.e. the first point
%is equal to the last
npoints=length(linepoints);
if linepoints(1)~=linepoints(npoints)
    linepoints(:,npoints+1)=linepoints(:,1);
end
npoints=length(linepoints);

%Now we find the center
cen(1)=sum(linepoints(1,1:end-1))/(npoints-1); % here we don't want to count the first/last one twice
cen(2)=sum(linepoints(2,1:end-1))/(npoints-1);% here we don't want to count the first/last one twice


% at this we generate some necessary imagefiles
binimgtemp=imgin*0+1;
[ymax,xmax]=size(imgin);
[x,y]=meshgrid(1:xmax,1:ymax);


%Now we step through each of the lines
for ii=1:npoints-1
    linetemp1=linepoints(:,ii+1)-linepoints(:,ii);
    % to facilitate calculations we use dot and crossproducts and therefore
    % need a 3-dimensional line
    linetemp2=linetemp1;
    linetemp1(3,:)=0*linetemp1(1,:)+1;
    linetemp2(3,:)=0*linetemp1(1,:)+0;
    linenorm=cross(linetemp1,linetemp2); % this is the normal to the line pointing towards the center
    % now to find out which direction the center is in
    signcen=(cen(1)-linepoints(1,ii)).*linenorm(1,:)+(cen(2)-linepoints(2,ii)).*linenorm(2,:); 
    % now splitting the image up in positive and negatives...the signcen
    % ensure positive is to the center
    imgsplit=((x-linepoints(1,ii)).*linenorm(1,:)+(y-linepoints(2,ii)).*linenorm(2,:))*signcen;
    %finally multiplying it to the image
    binimgtemp=binimgtemp.*(imgsplit>=0);
end

binimgout=binimgtemp;
    



    