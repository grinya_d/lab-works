# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'lcpwriterUI.ui'
#
# Created: Fri Nov 28 21:20:04 2014
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(944, 925)
        self.coordinatesText = QtGui.QTextBrowser(Dialog)
        self.coordinatesText.setGeometry(QtCore.QRect(30, 90, 261, 621))
        self.coordinatesText.setMidLineWidth(0)
        self.coordinatesText.setReadOnly(False)
        self.coordinatesText.setObjectName(_fromUtf8("coordinatesText"))
        self.plateIDLine = QtGui.QLineEdit(Dialog)
        self.plateIDLine.setGeometry(QtCore.QRect(30, 30, 171, 31))
        self.plateIDLine.setObjectName(_fromUtf8("plateIDLine"))
        self.scriptText = QtGui.QPlainTextEdit(Dialog)
        self.scriptText.setGeometry(QtCore.QRect(300, 90, 441, 621))
        self.scriptText.setObjectName(_fromUtf8("scriptText"))
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(30, 10, 161, 20))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(750, 80, 191, 20))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.coordinatesScaleLine = QtGui.QLineEdit(Dialog)
        self.coordinatesScaleLine.setGeometry(QtCore.QRect(750, 100, 171, 21))
        self.coordinatesScaleLine.setObjectName(_fromUtf8("coordinatesScaleLine"))
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(300, 30, 171, 31))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.importFRAPFromFileButton = QtGui.QPushButton(Dialog)
        self.importFRAPFromFileButton.setGeometry(QtCore.QRect(30, 720, 261, 31))
        self.importFRAPFromFileButton.setObjectName(_fromUtf8("importFRAPFromFileButton"))
        self.importFromDBButton = QtGui.QPushButton(Dialog)
        self.importFromDBButton.setGeometry(QtCore.QRect(210, 30, 81, 31))
        self.importFromDBButton.setObjectName(_fromUtf8("importFromDBButton"))
        self.exposureLine = QtGui.QLineEdit(Dialog)
        self.exposureLine.setGeometry(QtCore.QRect(750, 160, 171, 21))
        self.exposureLine.setObjectName(_fromUtf8("exposureLine"))
        self.label_4 = QtGui.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(750, 140, 181, 20))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.authorLine = QtGui.QLineEdit(Dialog)
        self.authorLine.setGeometry(QtCore.QRect(750, 220, 171, 21))
        self.authorLine.setObjectName(_fromUtf8("authorLine"))
        self.label_5 = QtGui.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(750, 200, 181, 20))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.generateScriptButton = QtGui.QPushButton(Dialog)
        self.generateScriptButton.setGeometry(QtCore.QRect(300, 720, 441, 31))
        self.generateScriptButton.setObjectName(_fromUtf8("generateScriptButton"))
        self.saveScriptButton = QtGui.QPushButton(Dialog)
        self.saveScriptButton.setGeometry(QtCore.QRect(750, 720, 171, 31))
        self.saveScriptButton.setObjectName(_fromUtf8("saveScriptButton"))
        self.debugText = QtGui.QTextBrowser(Dialog)
        self.debugText.setGeometry(QtCore.QRect(30, 760, 891, 151))
        self.debugText.setObjectName(_fromUtf8("debugText"))
        self.coordinateSourceComboBox = QtGui.QComboBox(Dialog)
        self.coordinateSourceComboBox.setGeometry(QtCore.QRect(750, 680, 171, 27))
        self.coordinateSourceComboBox.setObjectName(_fromUtf8("coordinateSourceComboBox"))
        self.coordinateSourceComboBox.addItem(_fromUtf8(""))
        self.label_6 = QtGui.QLabel(Dialog)
        self.label_6.setGeometry(QtCore.QRect(750, 660, 181, 20))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.ertLabel = QtGui.QLabel(Dialog)
        self.ertLabel.setGeometry(QtCore.QRect(490, 30, 251, 31))
        self.ertLabel.setObjectName(_fromUtf8("ertLabel"))
        self.label_7 = QtGui.QLabel(Dialog)
        self.label_7.setGeometry(QtCore.QRect(750, 610, 181, 20))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.scriptIDLine = QtGui.QLineEdit(Dialog)
        self.scriptIDLine.setGeometry(QtCore.QRect(750, 630, 171, 21))
        self.scriptIDLine.setObjectName(_fromUtf8("scriptIDLine"))
        self.label_8 = QtGui.QLabel(Dialog)
        self.label_8.setGeometry(QtCore.QRect(30, 60, 171, 31))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.label_9 = QtGui.QLabel(Dialog)
        self.label_9.setGeometry(QtCore.QRect(300, 60, 171, 31))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.label_10 = QtGui.QLabel(Dialog)
        self.label_10.setGeometry(QtCore.QRect(750, 260, 181, 20))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.repeatLine = QtGui.QLineEdit(Dialog)
        self.repeatLine.setGeometry(QtCore.QRect(750, 280, 171, 21))
        self.repeatLine.setObjectName(_fromUtf8("repeatLine"))
        self.sortCheckBox = QtGui.QCheckBox(Dialog)
        self.sortCheckBox.setGeometry(QtCore.QRect(750, 320, 171, 22))
        self.sortCheckBox.setObjectName(_fromUtf8("sortCheckBox"))
        self.sortOptionComboBox = QtGui.QComboBox(Dialog)
        self.sortOptionComboBox.setGeometry(QtCore.QRect(750, 350, 171, 27))
        self.sortOptionComboBox.setObjectName(_fromUtf8("sortOptionComboBox"))
        self.sortOptionComboBox.addItem(_fromUtf8(""))
        self.sortOptionComboBox.addItem(_fromUtf8(""))
        self.sortOptionComboBox.addItem(_fromUtf8(""))
        self.savePic = QtGui.QCheckBox(Dialog)
        self.savePic.setGeometry(QtCore.QRect(750, 580, 171, 22))
        self.savePic.setObjectName(_fromUtf8("savePic"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Rigaku LCP SAXS script generator", None))
        self.label.setText(_translate("Dialog", "Plate ID", None))
        self.label_2.setWhatsThis(_translate("Dialog", "<html><head/><body><p>Parameter which is used to recalculate FRAP coordinates from original units (usually um) to mm</p></body></html>", None))
        self.label_2.setText(_translate("Dialog", "*Coordinates scale (to mm)", None))
        self.label_3.setText(_translate("Dialog", "Estimated script runtime:", None))
        self.importFRAPFromFileButton.setWhatsThis(_translate("Dialog", "<html><head/><body><p>Import FRAP coordinates from local CSV file</p></body></html>", None))
        self.importFRAPFromFileButton.setText(_translate("Dialog", "Import FRAP coordinates from file", None))
        self.importFromDBButton.setWhatsThis(_translate("Dialog", "<html><head/><body><p>Import FRAP coordinates from RockMaker SQL database</p></body></html>", None))
        self.importFromDBButton.setText(_translate("Dialog", "DB import", None))
        self.label_4.setWhatsThis(_translate("Dialog", "<html><head/><body><p>General exposure of the sample to X-ray beam</p></body></html>", None))
        self.label_4.setText(_translate("Dialog", "*Exposure, sec", None))
        self.label_5.setWhatsThis(_translate("Dialog", "<html><head/><body><p>Author name mentioned in script .info</p></body></html>", None))
        self.label_5.setText(_translate("Dialog", "*Author", None))
        self.generateScriptButton.setText(_translate("Dialog", "Generate script", None))
        self.saveScriptButton.setText(_translate("Dialog", "Save script", None))
        self.coordinateSourceComboBox.setItemText(0, _translate("Dialog", "FRAP", None))
        self.label_6.setText(_translate("Dialog", "Coordinate source", None))
        self.ertLabel.setWhatsThis(_translate("Dialog", "<html><head/><body><p>Time interval the script will run</p></body></html>", None))
        self.ertLabel.setText(_translate("Dialog", "ERT", None))
        self.label_7.setWhatsThis(_translate("Dialog", "<html><head/><body><p>String which is used to identify script in database, must be unique</p></body></html>", None))
        self.label_7.setText(_translate("Dialog", "Script ID (optional)", None))
        self.label_8.setText(_translate("Dialog", "SAXS Coordinates", None))
        self.label_9.setText(_translate("Dialog", "Script text", None))
        self.label_10.setWhatsThis(_translate("Dialog", "<html><head/><body><p>How many times to repeat the script</p></body></html>", None))
        self.label_10.setText(_translate("Dialog", "*Repeat script", None))
        self.sortCheckBox.setText(_translate("Dialog", "Sort microplate for:", None))
        self.sortOptionComboBox.setItemText(0, _translate("Dialog", "SHORT TRAVEL", "SHORT_TRAVEL"))
        self.sortOptionComboBox.setItemText(1, _translate("Dialog", "ROWS FIRST", "ROW_FIRST"))
        self.sortOptionComboBox.setItemText(2, _translate("Dialog", "COLUMNS FIRST", "COLUMN_FIRST"))
        self.savePic.setText(_translate("Dialog", "Save microplate pic", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

