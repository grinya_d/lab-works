from uuid import uuid1
from jinja2 import Template
import datetime


def main():
    username = "KUKLIN"
    experiment_name = "VZR_Kuklin_Poly"
    samples = range(1, 13)
    xtable = [1.70,
              4.15,
              14.18,
              25.20,
              35.978,
              47.130,
              58.060,
              68.514,
              80.130,
              91.092,
              102.273,
              112.933]
    ytable = [0.136 for i in samples]
    temperatures = [22, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70]
    # waiting between temperatures in seconds
    wait = 20 * 60
    REALTIME = 150

    head = """;; SCRIPT UUID = {{UUID}}
;; ESTIMATED RUNTIME = {{RUNTIME}}
SCRIPT START
REALTIME({{REALTIME}})
LIVETIME({{LIVETIME}})

"""
    head = Template(head)

    section = """
TEMP({{TEMPERATURE}})
WAIT({{WAIT}})

"""
    section = Template(section)

#     body = """
# ;; {{PREFIX}}
# MPA3 CLEAR
# DATA_INFO({{DATANAME}},{{USERNAME}},,,,,,,,,)
# MM({{POSX}},{{POSY}})
# MPA3 START
# WAIT FOR MPA3
# MPA3 STORE()

# """

    body = """
;; {{PREFIX}}
MPA3 CLEAR
DATA_INFO({{DATANAME}},{{USERNAME}},,,,,,,,,)
MPA3 START
WAIT FOR MPA3
MPA3 STORE()

"""
    body = Template(body)

    tail = """
TEMP(22)
SCRIPT END

"""
    tail = Template(tail)

    uuid = str(uuid1())
    LIVETIME = REALTIME + 2
    BIAS = 13.2

    script = ""
    ert = 0

    for i, t in enumerate(temperatures):
        script += section.render({
            "TEMPERATURE": t,
            "WAIT": wait
        })
        ert += wait
        for j in range(len(samples)):
            # moving relative across the sample in Y axis
            script += "\nMM(%f,%f)\n" % (xtable[j], ytable[j])
            script += "MR(0.0,-1.0)\n"
            script += body.render({
                "PREFIX": "%d of %d" % (
                    i * len(xtable) + j + 1,
                    len(temperatures) * len(xtable) * 5),
                "DATANAME": experiment_name +
                " Sample %d Temperature %d SC1 SOURCE %s RPOS 0.0 -1.0" % (
                    samples[j], t, uuid),
                "USERNAME": username
            })
            ert += LIVETIME + BIAS

            script += "\nMR(0.0,0.5)\n"
            script += body.render({
                "PREFIX": "%d of %d" % (
                    i * len(xtable) + j + 1,
                    len(temperatures) * len(xtable) * 5),
                "DATANAME": experiment_name +
                " Sample %d Temperature %d SC1 SOURCE %s RPOS 0.0 -0.5" % (
                    samples[j], t, uuid),
                "USERNAME": username
            })
            ert += LIVETIME + BIAS

            script += "\nMR(0.0,0.5)\n"
            script += body.render({
                "PREFIX": "%d of %d" % (
                    i * len(xtable) + j + 1,
                    len(temperatures) * len(xtable) * 5),
                "DATANAME": experiment_name +
                " Sample %d Temperature %d SC1 SOURCE %s RPOS 0.0 0.0" % (
                    samples[j], t, uuid),
                "USERNAME": username
            })
            ert += LIVETIME + BIAS

            script += "\nMR(0.0,0.5)\n"
            script += body.render({
                "PREFIX": "%d of %d" % (
                    i * len(xtable) + j + 1,
                    len(temperatures) * len(xtable) * 5),
                "DATANAME": experiment_name +
                " Sample %d Temperature %d SC1 SOURCE %s RPOS 0.0 +0.5" % (
                    samples[j], t, uuid),
                "USERNAME": username
            })
            ert += LIVETIME + BIAS

            script += "\nMR(0.0,0.5)\n"
            script += body.render({
                "PREFIX": "%d of %d" % (
                    i * len(xtable) + j + 1,
                    len(temperatures) * len(xtable) * 5),
                "DATANAME": experiment_name +
                " Sample %d Temperature %d SC1 SOURCE %s RPOS 0.0 +1.0" % (
                    samples[j], t, uuid),
                "USERNAME": username
            })
            ert += LIVETIME + BIAS

    script = head.render({
        "UUID": uuid,
        "RUNTIME": datetime.timedelta(seconds=ert),
        "REALTIME": REALTIME,
        "LIVETIME": LIVETIME
    }) + script

    script += tail.render()
    with open("./scripts/" +
              str(datetime.datetime.now().strftime('%Y%m%d')) +
              "_" +
              experiment_name +
              ".dat", "w") as f:
        f.write(script)


    # with open("chupin12.dat","w") as f:
if __name__ == '__main__':
    main()
