import zipfile
import sys
import os


def getMetadata(path):
	archive = zipfile.ZipFile(path, 'r')
	metadata = archive.read('Metadata.xml')
	return metadata


if __name__ == '__main__':
	zipfilepath = sys.argv[1]
	print zipfilepath
	metadata = getMetadata(zipfilepath)
	print metadata