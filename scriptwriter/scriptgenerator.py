from uuid import uuid1
from jinja2 import Template
import datetime
import sys
import csv
import os


class ScriptGenerator(object):

    """docstring for ScriptGenerator"""

    user_name = None
    script_name = None
    experiment_name = None
    livetime = None
    realtime = None
    servicetime = None
    repeat = None
    uuid = None
    section_wait = None

    script = None

    head = None
    body = None
    section = None
    tail = None

    estimatedRunTime = None

    def __init__(self, settings=None):
        super(ScriptGenerator, self).__init__()
        if settings is not None:
            self.updateSettings(settings)

        if self.user_name is None:
            self.user_name = "PYWRITER"
        if self.experiment_name is None:
            self.experiment_name = "NONAME"
        if self.script_name is None:
            self.script_name = self.experiment_name
        if self.realtime is None:
            self.realtime = 0
            self.livetime = self.realtime + 2
        if self.servicetime is None:
            self.servicetime = 13.2
        if self.repeat is None:
            self.repeat = 1
        if self.section_wait is None:
            self.section_wait = 600

        self.uuid = str(uuid1())

        self.updateTemplate(
            os.path.join(os.path.dirname(__file__),
                         'templates')
        )

    def updateTemplate(self, rootdir=None):
        if rootdir is None:
            rootdir = './'
        try:
            fname = os.path.join(rootdir, 'head.txt')
            with open(fname, 'r') as f:
                self.head = Template(f.read())
        except Exception, e:
            print "failed to load head template %s" % fname, e

        try:
            fname = os.path.join(rootdir, 'section.txt')
            with open(fname, 'r') as f:
                self.section = Template(f.read())
        except Exception, e:
            print "failed to load section template %s" % fname, e

        try:
            fname = os.path.join(rootdir, 'body.txt')
            with open(fname, 'r') as f:
                self.body = Template(f.read())
        except Exception, e:
            print "failed to load body template %s" % fname, e

        try:
            fname = os.path.join(rootdir, 'tail.txt')
            with open(fname, 'r') as f:
                self.tail = Template(f.read())
        except Exception, e:
            print "failed to load tail template %s" % fname, e

    def updateSettings(self, settings=None):
        if settings is not None:
            if "user_name" in settings:
                self.user_name = settings["user_name"]
            if "script_name" in settings:
                self.script_name = settings["script_name"]
            if "experiment_name" in settings:
                self.experiment_name = settings["experiment_name"]
            if "exposure" in settings:
                self.realtime = int(settings["exposure"])
                self.livetime = self.realtime + 2
            if "realtime" in settings:
                self.realtime = int(settings["realtime"])
                self.livetime = self.realtime + 2
            if "livetime" in settings:
                self.livetime = int(settings["livetime"])
            if "servicetime" in settings:
                self.servicetime = settings["servicetime"]
            if "repeat" in settings:
                self.repeat = int(settings['repeat'])
            if "section_wait" in settings:
                self.section_wait = int(settings['section_wait'])

    def generateScript(self, data=None, tempStatus=None):
        if tempStatus is None:
            return self.generateSimpleScript(data)
        elif tempStatus == "OFF":
            return self.generateSimpleScript(data)
        elif tempStatus is "ON":
            return self.generateTemperatureScript(data)
        else:
            return None

    def generateSimpleScript(self, data=None):
        if data is None:
            print "None to write"
            return None

        sampleNumber = len(data)
        if sampleNumber < 1:
            print "0 samples to write"
            return None

        print "%d samples to write" % sampleNumber

        repeat = self.repeat
        username = self.user_name
        experiment_name = self.experiment_name
        REALTIME = self.realtime
        uuid = self.uuid
        LIVETIME = self.livetime
        BIAS = self.servicetime

        head, body, tail = self.head, self.body, self.tail
        script = ""
        ert = 0

        for r in range(repeat):
            for j, d in enumerate(data):
                sampleName, sampleX, sampleY = d
                try:
                    script += body.render({
                        "PREFIX": "%d of %d" % (
                            r * sampleNumber + j + 1,
                            repeat * sampleNumber),
                        "DATANAME": experiment_name +
                        " %s SC1 CYCLE %d SOURCE %s" % (
                            sampleName, r+1, uuid),
                        "USERNAME": username,
                        "POSX": sampleX,
                        "POSY": sampleY
                    })
                    ert += LIVETIME + BIAS
                    print r * sampleNumber + j + 1, d
                except Exception, e:
                    print "failed to write sample", j, d
                    print e

        script = head.render({
            "UUID": uuid,
            "RUNTIME": datetime.timedelta(seconds=ert),
            "REALTIME": REALTIME,
            "LIVETIME": LIVETIME
        }) + script

        script += tail.render()

        self.script = script
        self.estimatedRunTime = ert

        return script

    def generateTemperatureScript(self, data=None):
        if data is None:
            print "None to write"
            return None

        sampleNumber = len(data)
        if sampleNumber < 1:
            print "0 samples to write"
            return None

        print "%d samples to write" % sampleNumber

        repeat = self.repeat
        username = self.user_name
        experiment_name = self.experiment_name
        REALTIME = self.realtime
        uuid = self.uuid
        LIVETIME = self.livetime
        BIAS = self.servicetime
        WAIT = self.section_wait

        head, body, tail, section = self.head, self.body, self.tail, self.section
        currentTemperature = 666
        script = ""
        ert = 0

        for r in range(repeat):
            for j, d in enumerate(data):
                sampleName, sampleX, sampleY, sampleT = d
                if sampleT != currentTemperature:
                    try:
                        script += section.render({
                            "WAIT": WAIT,
                            "TEMP": "%.2f" % sampleT
                            })
                        currentTemperature = sampleT
                        ert += WAIT
                    except Exception, e:
                        print "failed to write section", j, d
                        print e
                try:
                    script += body.render({
                        "PREFIX": "%d of %d" % (
                            r * sampleNumber + j + 1,
                            repeat * sampleNumber),
                        "DATANAME": experiment_name +
                        " %s CYCLE %d TEMP %.2f SC1 SOURCE %s" % (
                            sampleName, r+1, sampleT, uuid),
                        "USERNAME": username,
                        "POSX": sampleX,
                        "POSY": sampleY
                    })
                    ert += LIVETIME + BIAS
                    print r * sampleNumber + j + 1, d
                except Exception, e:
                    print "failed to write sample", j, d
                    print e

        script = head.render({
            "UUID": uuid,
            "RUNTIME": datetime.timedelta(seconds=ert),
            "REALTIME": REALTIME,
            "LIVETIME": LIVETIME
        }) + script

        script += tail.render()

        self.script = script
        self.estimatedRunTime = ert

        return script


def main():

    """
    This program reads names and coordinates of samples from csv file and
    generates script for it.

    UPD: added temperature information as 4th column in csv file
    """

    coordinatesPath = sys.argv[1]
    with open(coordinatesPath, 'r') as coordinatesFile:
        coordinates = csv.reader(coordinatesFile, delimiter=',')
        coordinates = list(coordinates)
        data = []
        tempStatus = "OFF"
        if len(coordinates[0]) == 4:
            tempStatus = "ON"
        for c in coordinates:
            try:
                sampleName = c[0]
                sampleX = float(c[1])
                sampleY = float(c[2])
                if tempStatus == "OFF":
                    data.append((sampleName, sampleX, sampleY))
                elif tempStatus == "ON":
                    sampleTemp = float(c[3])
                    data.append((sampleName, sampleX, sampleY, sampleTemp))
            except Exception, e:
                print "unable to append row", c, e

    coordName = os.path.split(coordinatesPath)[-1]
    coordName = os.path.splitext(coordName)[0]
    print coordName

    scriptSettings = {
        "user_name": "SINTSOV",
        "experiment_name": "TEMPERATURE MOSAICITY",
        "exposure": 60,
        "repeat": 10,
        "section_wait": 600
    }

    if scriptSettings.get("experiment_name", None) is None:
        scriptSettings['experiment_name'] = coordName

    scrGen = ScriptGenerator(scriptSettings)

    script = scrGen.generateScript(data, tempStatus)

    scriptPath = ("./scripts/" +
                  str(datetime.datetime.now().strftime('%Y%m%d')) +
                  "_" +
                  scrGen.script_name +
                  ".dat")
    print 'writing script to %s' % scriptPath
    with open(scriptPath, "w") as f:
        f.write(script)

if __name__ == '__main__':
    main()
