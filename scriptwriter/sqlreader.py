import pyodbc
from datetime import datetime as dt
import pprint
import os
import jinja2

###############################################################
# WARNING!!!
# LINUX and WINDOWS connection settings differ
# in WINDOWS it works right from the box,

# but in LINUX one has to set it up, please refer to
# http://www.tryolabs.com/Blog/2012/06/25/connecting-sql-server-database-python-under-ubuntu/

# 1. sudo apt-get install unixodbc unixodbc-dev freetds-dev tdsodbc

# 2. /etc/freetds/freetds.conf
# Formulatrix MS-SQL server
# [formulatrixServer]
#     host = 93.175.16.6
#     port = 1433
#     tds version = 7.0

# 3. /etc/odbc.ini
# [formulatrixServerDataSource]
# Driver = FreeTDS
# Description = ODBC connection via FreeTDS
# Trace = No
# Servername = formulatrixServer
# Database = RockMaker

# 4. /etc/odbcinst.ini
# [FreeTDS]
# Description = TDS driver (Sybase/MS SQL)
# Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
# Setup = /usr/lib/x86_64-linux-gnu/odbc/libtdsS.so
# CPTimeout =
# CPReuse =
# FileUsage = 1
###############################################################


class SQLReader(object):
    ip = None
    sqlUsername = None

    # RockMaker database cursor
    cursorRM = None
    cnxnRM = None

    # RockImager database cursor
    cursorRI = None
    cnxnRI = None

    # Templates
    get_frap_saxs_coordinates = None

    def __init__(self, ip=None, login=None, password=None):
        self.ip = ip

        # IN WINDOWS
        # cnxnRM = pyodbc.connect(
        #     'DRIVER={SQL Server};' +
        #     'SERVER=%s;' % ip +
        #     'DATABASE=RockMaker;' +
        #     'UID=%s;PWD=%s' % (login, password))
        # cnxnRI = pyodbc.connect(
        #     'DRIVER={SQL Server};' +
        #     'SERVER=%s;' % ip +
        #     'DATABASE=RockImager;' +
        #     'UID=%s;PWD=%s' % (login, password))

        # In Linux

        try:
            cnxnRM = pyodbc.connect(
                'dsn=formulatrixServerDataSource;' +
                'DATABASE=RockMaker;' +
                'UID=%s;PWD=%s' % (login, password))
            if not cnxnRM.cursor():
                raise Exception("failed to connect to RockMaker db")
            self.cursorRM = cnxnRM.cursor()
            self.cnxnRM = cnxnRM
            print 'connected to RockMaker db successfully'
        except Exception, e:
            print 'failed to connect to RockMaker database',
            print e

        try:
            cnxnRI = pyodbc.connect(
                'dsn=formulatrixServerDataSource;' +
                'DATABASE=RockImager;' +
                'UID=%s;PWD=%s' % (login, password))
            if not cnxnRI.cursor():
                raise Exception("failed to connect to RockImager db")
            self.cursorRI = cnxnRI.cursor()
            self.cnxnRI = cnxnRI
            print 'connected to RockImager db successfully'
        except Exception, e:
            print 'failed to connect to RockImager database',
            print e

        # SQL template reading
        path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "templates", "get_frap_saxs_coordinates.sql"
        )
        with open(path, "r") as f:
            self.get_frap_saxs_coordinates = jinja2.Template(f.read())

        # Capture profile ID corresponding to SAXS coordinates = 19
        self.CaptureProfileID = 19

    def getPlateCoordinates(self,
                            plateID=None,
                            CaptureProfileID=None
                            ):
        """
        The function gets plateID and CaptureProfileID and requests
        drop coordinates from RockMaker SQL database, constructed from
        Jinja2 template located in self.get_frap_saxs_coordinates:
        ||XCenter|YCenter|RowLetter ASC2|ColumnNumber ASC3|DateImaged ASC1||

        Then it constructs the most actual information about drop location
        in scaled coordinates:
        ||RowColumn|XCenter|YCenter||
        """

        if plateID is None:
            return None

        if CaptureProfileID is None:
            CaptureProfileID = self.CaptureProfileID

        print ('attempting to load plate coordinates for ' +
               'plateID %s for CaptureProfileID %s') % (
            str(plateID), str(CaptureProfileID))

        try:
            request = self.get_frap_saxs_coordinates.render({
                "PlateID": plateID,
                "CaptureProfileID": CaptureProfileID
            })
        except Exception, e:
            print "failed to construct request for get_frap_saxs_coordinates",
            print e
            raise e

        try:
            self.cursorRM.execute(request)
            res = self.cursorRM.fetchall()
        except Exception, e:
            print "failed to select from db for get_frap_saxs_coordinates"
            print e
            raise e

        coordinatesDict = {}
        for r in res:
            rowcolumn = str(r[2]) + str(r[3])
            coordinatesDict[rowcolumn] = (r[0], r[1])

        print 'loaded %d drops' % len(coordinatesDict)

        return [(rc, xy[0], xy[1]) for rc, xy in coordinatesDict.items()]


if __name__ == '__main__':
    plateID = 1012
    sql = SQLReader("93.175.16.6", "guest", "Lurige17")
    pprint.pprint(sql.getPlateCoordinates(plateID))
