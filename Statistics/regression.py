'''
Author: Leonid Vlasenkov
Mail:   leo.vlasenkov@gmail.com
Date:   Sep 9 2014

The script gets a CSV file and makes regression on its columns using Ordinary 
Least Squares method. You can just declare names of needed columns and it will
collect and handle the data. Also there is a loop which you can use to change 
the y variable automatically.
'''

import pandas as pd
import numpy as np
import statsmodels.api as sm

df_adv = pd.read_csv('../data/og_sets1234.csv')
regressors = ['Fraction', 'OG', 'salt']
y_values = ['68d-d', '1/d68', '1/(d68)2', 'test']

for p in y_values:
	
	X = df_adv[regressors] 
	y = df_adv[p]

	est = sm.OLS(y, X).fit()
	print est.summary()
	print '\n\n'
